#!/usr/bin/env python
####################################################
#
#  Run: plot_energy_trace.py and plot_spectral_trace.py
#
####################################################

#import plot_energy_trace.py
#import plot_spectral_trace.py
import sys
import os

#rayleighscripts=os.getenv("rayleighscripts")
rayleighscripts='/Users/pdriscoll/memex/rayleigh/rayleigh_diagnostics/rayleigh_diagnostics'
print(rayleighscripts)

#execfile('/Users/pdriscoll/memex/rayleigh/rayleigh_git/rayleigh/analysis_routines/python/plot_energy_trace.py')
#sys.argv = ['-d']
#execfile(rayleighscripts+'/plot_global_avgs.py')
#sys.argv = ['']
#execfile(rayleighscripts+'/plot_spectral_trace.py')

os.system(rayleighscripts+"/plot_global_avgs.py -d &")
os.system(rayleighscripts+"/plot_spectral_trace.py &")
