#!/usr/bin/env python

### boundary_layer_analysis.py (1/22/21) ###
# This script should be run in a dir that contains
# subdir of Rayleigh runs at different Ra numbers.
# For example, run this in memex_kluo/.../kappa-1/.

import sys
import matplotlib as mpl
mpl.use('tkagg')
#mpl.use('MacOSX')
import matplotlib.pyplot as plt
import numpy as np
import os
import glob
from scipy.interpolate import UnivariateSpline,InterpolatedUnivariateSpline
from scipy import interpolate
from scipy.signal import find_peaks
from scipy.integrate import odeint,simps
pi=np.pi
plt.ion()
#sys.path.insert(0, "/home/kluo/pkg/Rayleigh.20190926.master")

from rayleigh_diagnostics import G_Avgs, Shell_Avgs, build_file_list
from rayleigh_diagnostics import get_lut
import rayleigh_codes

######################
### Begin FUNCTIONS ###
### Function: plot_moment_r
def plot_moment_r(irun,moment,iq,labels=False,legend=False,option='Value'):
    f = interpolate.interp1d(radius, savg_all[irun,:nr,moment,lut[iq]])
    y = f(x)
    spl = UnivariateSpline(x,y, k=4, s=3)
    if option=='Value':
        y_spl = spl.__call__(x)
        option_pref=''
    if option=='Derivative':
        y_spl = spl.derivative().__call__(x)
        option_pref=option+' of '
    if option=='rfactor':
        y_spl = x*spl.__call__(x)
        option_pref='r* '
    plt.plot(x,y_spl,label=str(Ralisting[irun]),color=ra_color[irun],linestyle=ra_linestyle[irun%n_ls])
    ylabel=option_pref+moment_pref[moment]+' '+rayleigh_codes.index_to_string[iq]
    if labels:
        plt.ylabel(ylabel); plt.xlabel(xlabel); plt.grid(True)
    ### oplot TBL as 'x'
    if rayleigh_codes.index_to_string[iq]=='kinetic_energy':
        if option=='Value':
            y_spl_delta_i=spl.__call__(R_i+delta_i_ekman[irun])
            y_spl_delta_c=spl.__call__(R_c-delta_c_ekman[irun])
        if option=='Derivative':
            y_spl_delta_i=spl.derivative().__call__(R_i+delta_i_ekman[irun])
            y_spl_delta_c=spl.derivative().__call__(R_c-delta_c_ekman[irun])
        plt.plot(R_i+delta_i_ekman[irun],y_spl_delta_i,'x',color=ra_color[irun])
        plt.plot(R_c-delta_c_ekman[irun],y_spl_delta_c,'x',color=ra_color[irun])        
    if legend: plt.legend(fancybox=True, framealpha=0.5,fontsize=legend_font_size)

### Conductive temperature integrand.
def integrand(r,k):
    return(1./(r**2*k))
### Function to compute T_k(r)
def get_T_k(T_i=10.,dT_dr_c=-1.,k_r=np.ones(96),r=np.ones(96)):
    Q_c=-4*pi*R_c**2*k_r[0]*dT_dr_c
    T_k=np.zeros(len(r))
    ### numerical integral of int(dr/(r^2*k))
    integral=0.; integral_dr=0.
    integral_simps=np.zeros(nr)
    for ir in np.arange(nr-1,0,-1):  #integrate from R_i to R_c
        integral_dr = integral_dr + (r[ir-1]-r[ir])
        integral = integral + (r[ir-1]-r[ir])/(r[ir-1]**2*k_r[ir-1])
        T_k[ir-1] = T_i - Q_c/(4*pi)*integral
        #print(ir-1,r[ir-1],T_k[ir-1],k_r[ir-1])

        integral_simps[ir-1] = simps(integrand(r[ir-1:],k_r[ir-1:]),r[ir-1:])*(-1)
    T_k_simps = T_i-Q_c/(4*pi)*integral_simps
    T_k = T_k_simps
    #for ir in np.arange(nr-2,0,-1):  #integrate from R_i to R_c        
    #    integral_dr = integral_dr + (r[ir]-r[ir+1])
    #    integral = integral + (r[ir]-r[ir+1])/(r[ir]**2*k_r[ir])
    #    T_k[ir] = T_i - Q_c/(4*pi)*integral
    #    print(ir,T_k[ir])
    #integral_dr = integral_dr + (r[0]-r[1])
    #integral = integral + (r[0]-r[1])/(r[0]**2*k_r[0])
    #T_k[0] = T_i - Q_c/(4*pi)*integral
    #print(0,T_k[0])
    
    T_k[-1] = T_i
    #integral=R_c**2*k_r[0]   #approximation, for constant k.
    #import ipdb; ipdb.set_trace()
    print('integral=%f integral_dr=%f integral_simps=%f'%(integral,integral_dr,integral_simps[0]))
    return(T_k)

### Function to compute delta_thermal
def get_delta_thermal(irun,T_norm_crit=1e-3,Q_norm_crit=-1e-2,k_r=np.ones(96),
                          plot_on=False,interpolate_on=False,nx=500,T_k=np.ones(96)):
    k_c=k_r[0]
    k_i=k_r[-1]
    T_entropy=savg_all[irun,:nr,0,lut[501]]
    dT_entropy_dr=savg_all[irun,:nr,0,lut[507]]
    T_i=T_entropy[-1]
    q_entropy=-k_r*dT_entropy_dr #=dT/dr*k_c, conducted heat flux
    r=radius  #why do I need this here??
    Q_entropy=4*pi*r**2*q_entropy   #conducted heat flow
    q_c=q_entropy[0]
    Q_c=Q_entropy[0]
    #T_k=T_i-q_c*R_c**2/k_r*(1./R_i-1./r)  #conductive profile
    dT_k_dr=-Q_c/(4*pi*r**2*k_r)  #fully conductive dT/dr
    #T_k=T_i+Q_c/(4*pi*k_r)*(1./r-1./R_i)  #conductive profile
    #T_k=T_i-r**2*dT_k_dr*(1./r-1./R_i)  #conductive profile
    q_k=-k_r*dT_k_dr  #fully conductive soln hflux
    Q_k=4*pi*r**2*q_k
    Q_c=Q_entropy[0]
    #T_norm=(T_entropy-T_k)/np.mean(T_entropy)
    T_norm=(T_entropy-T_k)/T_k
    #T_norm=(T_entropy-T_k)/T_i
    T_norm_crit_r=T_norm_crit+np.zeros(len(r))
    #Q_norm=(Q_entropy-Q_k)/np.mean(Q_entropy) #similar to Nu
    Q_norm=(Q_entropy-Q_k)/Q_k
    #Q_norm=(Q_entropy-Q_k)/Q_c
    ### IC TBL: BC=isothermal, so use T
    if interpolate_on:
        T_diff_spl = UnivariateSpline(radius[::-1],T_norm[::-1]-T_norm_crit, k=1, s=0) #piece-wise linear
        Q_diff_spl = UnivariateSpline(radius[::-1],Q_norm[::-1]-Q_norm_crit, k=1, s=0)
        r = np.linspace(R_i,R_c,nx) #interpolation of r in Rayleigh r-space        
        #index_delta_i_thermal=np.abs(T_diff_spl(r)).argmin()
        #index_delta_c_thermal=np.abs(Q_diff_spl(r)).argmin()
        index_delta_i_thermal=np.argwhere(np.diff(np.sign(T_diff_spl(r)))).flatten()
        index_delta_c_thermal=np.argwhere(np.diff(np.sign(Q_diff_spl(r)))).flatten()
        if len(index_delta_i_thermal)==0: index_delta_i_thermal=[-1]
        if len(index_delta_c_thermal)==0: index_delta_c_thermal=[0]
        delta_i_thermal=r[index_delta_i_thermal[0]]-R_i    
        delta_c_thermal=R_c-r[index_delta_c_thermal[-1]]
    else:
        index_delta_i_thermal=np.argwhere(np.diff(np.sign(T_norm-T_norm_crit_r))).flatten()
        index_delta_c_thermal=np.argwhere(np.diff(np.sign(Q_norm-Q_norm_crit))).flatten()
        if not any(index_delta_i_thermal): index_delta_i_thermal=[0]
        if not any(index_delta_c_thermal): index_delta_c_thermal=[0]        
        index_delta_i_thermal=index_delta_i_thermal[0]
        index_delta_c_thermal=index_delta_c_thermal[-1]
        delta_i_thermal=r[index_delta_i_thermal]-R_i    
        delta_c_thermal=R_c-r[index_delta_c_thermal]
    ### Compute T_1
    R_L=R_i+delta_i_thermal; R_U=R_c-delta_c_thermal
    index_L=np.argwhere(radius>=R_L).max()
    index_U=np.argwhere(radius<=R_U).min()
    dTdr_i=(R_c/R_i)**2*dT_entropy_dr[0]
    if index_U>=index_L:
        T_1_r=np.zeros(nr)
        radius_conv=radius
    else:
        index_conv=np.arange(index_U,index_L)
        radius_conv=radius[index_conv]
        T_1_r=(T_entropy[index_conv]-T_i-delta_i_thermal*dTdr_i)/(1./radius_conv-1./R_L)
    ### Debugging
    #import ipdb; ipdb.set_trace()    #Debugger
    T_1=T_1_r[0]  #np.mean(T_1_r)
    #print(Ras[irun],R_L,R_U,index_L,index_U)
    #print(radius_conv)
    #print(Ras[irun],delta_i_thermal,delta_c_thermal,R_i+delta_i_thermal,R_c-delta_c_thermal)
    #plt.figure(plt.gcf().number,figsize=figsize)
    #plt.subplot(2,1,1)
    #plt.plot(radius,Q_norm,'o-',ms=2,label='norm')
    #plt.xlabel('r'); plt.ylabel(r'$Q^*$')
    #plt.subplot(2,1,2)
    #plt.plot(radius[::-1],Q_norm[::-1]-Q_norm_crit,'o-',ms=2,label='norm diff')
    #plt.plot(r,Q_diff_spl(r),'o-',ms=2,label='spl diff')
    #plt.legend()
    #exit(0)
    ### Plotting
    if plot_on:
        ### Plot T
        plt.figure(plt.gcf().number,figsize=figsize)
        plt.subplot(2,1,1)
        plt.plot(radius,T_entropy,label='full,'+Ralisting[irun],color=ra_color[irun],linestyle=ra_linestyle[irun%n_ls])
        if irun==0:
            plt.plot(radius,T_k,'-',label='conductive,'+Ralisting[irun],color='k')
            plt.ylabel('T'); plt.xlabel('r')
        if n>15: ncol=int(np.rint(n/15))
        else: ncol=1
        plt.legend(fontsize=8,loc='best',ncol=ncol)
        plt.subplot(2,2,3)
        plt.plot(radius,T_norm,color=ra_color[irun],linestyle=ra_linestyle[irun%n_ls])
        plt.plot(R_i+delta_i_thermal,T_norm_crit,'x',label='Normalized',
                     color=ra_color[irun])
        if irun==0:
            plt.hlines(T_norm_crit,R_i,R_c,linestyle='-',label='Critical',color='k')
            plt.xlabel('r');plt.ylabel(r'$T^*=(T-T_k)/T_i$')
            plt.xlim(R_i,R_i+.5)
            plt.legend()
        plt.subplot(2,2,4)
        plt.plot(radius,T_norm,marker='o',ms=1,color=ra_color[irun],linestyle=ra_linestyle[irun%n_ls])
        plt.plot(R_i+delta_i_thermal,T_norm_crit,'x',color=ra_color[irun])
        if irun==0:
            plt.hlines(T_norm_crit,R_i,R_c,linestyle='-',color='k')
            plt.xlabel('r') #;plt.ylabel('(T-T_k)/mean(T)')
            plt.xlim(R_i,R_i+.20); plt.ylim(0.5*T_norm_crit,1.5*T_norm_crit)
        ### Debugging: Plot T_diff_spl
        if interpolate_on:
            plt.plot(r,T_diff_spl(r)+T_norm_crit,linestyle='dashdot',color=ra_color[irun])
        plt.tight_layout()
        ### Plot Q
        plt.figure(plt.gcf().number+1,figsize=figsize)
        plt.subplot(2,1,1)
        plt.plot(radius,Q_entropy,label='full, '+Ralisting[irun],color=ra_color[irun],linestyle=ra_linestyle[irun%n_ls])
        if irun==0:
            plt.plot(radius,Q_k,'-',label='conductive',color='k')
            plt.xlabel('r'); plt.ylabel('Q')
            plt.legend()
            plt.xlim(R_i,R_c)
        plt.subplot(2,2,3)
        plt.plot(radius,Q_norm,label='full',color=ra_color[irun],linestyle=ra_linestyle[irun%n_ls])
        plt.plot(R_c-delta_c_thermal,Q_norm_crit,'x',label='Normalized',
                     color=ra_color[irun])
        if irun==0:
            plt.hlines(Q_norm_crit,R_i,R_c,linestyle='-',label='Critical',color='k')
            plt.xlabel('r'); plt.ylabel(r'$Q^*=(Q-Q_k)/Q_c$')
            plt.xlim(R_i+.5,R_c)
            plt.legend()
        plt.subplot(2,2,4)
        plt.plot(radius,Q_norm,label='full',color=ra_color[irun],linestyle=ra_linestyle[irun%n_ls])
        plt.plot(R_c-delta_c_thermal,Q_norm_crit,'x',color=ra_color[irun])
        if irun==0:
            plt.hlines(Q_norm_crit,R_i,R_c,linestyle='-',label='Critical',color='k')
            plt.xlabel('r') #; plt.ylabel('Q')
            plt.xlim(R_i+.85,R_c); plt.ylim(1.5*Q_norm_crit,0.5*Q_norm_crit)
        plt.tight_layout()
        ### Plot T_1(r)
        plt.figure(plt.gcf().number+1,figsize=figsize)
        plt.plot(radius_conv,T_1_r,marker='o',ms=2,color=ra_color[irun],label=Ras[irun],linestyle=ra_linestyle[irun%n_ls])
        plt.xlabel('r'); plt.ylabel('T_1')
        plt.legend(fontsize=8)
        ### Rewind nfig back to T(r).
        plt.figure(plt.gcf().number-2,figsize=figsize) 
    ### Return    
    return(delta_i_thermal,delta_c_thermal,T_1)
### End FUNCTIONS ###
######################







######################
### Begin ANALYSIS ###
init=0
fnal=1000000
n_files_append=10  #number of files to append for each Ra.
#listings=np.sort(glob.glob('Ra*'))
dirlist=os.listdir('.')
listings=[y for y in [x for x in dirlist if x[:2]=='Ra'] if y[2:3].isnumeric()] #find dir that start with 'Ra' and then have a number
listings[0:]#, constant_listings
n=len(listings)
### Find Ra's to keep.
for i in range(n):
    if not 'Shell_Avgs' in os.listdir(listings[i]): print('Warning: No Shell_Avgs in '+listings[i])
#listings=listings
ind_keep=[i for i in range(n) if 'Shell_Avgs' in os.listdir(listings[i])] # check if Shell_Avgs dir exists
paths=[os.path.join(l,'Shell_Avgs') for l in np.array(listings)[ind_keep]]    #list of paths
ind_keep2=[i for i in range(len(ind_keep)) if len(build_file_list(init,fnal,path=paths[i]))>n_files_append]   #make sure there are enough files to read.
listings=np.array(listings)[np.array(ind_keep)[ind_keep2]]
#Ras = np.array([int(item.strip('Ra')) for item in listings])
Ras = np.array([])
Ralisting = []
for item in listings:
    if item.strip('Ra')[-1].isnumeric():   #make sure last char is a number
        Ras = np.append(Ras,int(item.strip('Ra')))
        Ralisting.append(item)

#Ralisting=np.array(listings)[np.argsort(Ras)]
#Ralisting = ['Ra'+x for x in Ras.astype('str')]
#Ralisting = Ralisting[np.argsort(Ras)]
Ralisting = [x for _,x in sorted(zip(Ras,Ralisting))]
Ras = np.sort(Ras)

n=len(Ralisting)
derivmins=np.zeros((n,2))
delta_i_min=0.01
delta_c_min=0.01
R_i=0.5384615384615383; R_c=1.5384615384615383
nx=500  #num interpolated scaled radius
x=np.linspace(R_i,R_c,nx)
rootmin=R_i+delta_i_min; rootmax=R_c-delta_c_min
delta_i_ekman=np.zeros(n)
delta_c_ekman=np.zeros(n)
dr=(1.5384-0.5384)/nx
nq=8 #default number, will be increased after reading in first shell_avg.
savg_all=np.zeros((n,96,4,nq))  #(n_runs,nr,nmom,nq)
time_last=np.zeros(n)  #last time steps.

### Save Path
if '-s' in sys.argv:
    #PDF_PATH='PDF_nx'+str(nx)+'/'
    PDF_PATH='PDF_nf'+str(n_files_append)+'/'
    if not os.path.exists(PDF_PATH):
        os.makedirs(PDF_PATH)

### Colors
#cmap=plt.cm.get_cmap('Paired')
#ra_color=np.zeros((n,4))
ra_color=['purple','blue','orange','green','red','brown','pink','navy','palegreen',
              'plum','skyblue','sienna','teal','hotpink','dimgray','gold','deepskyblue',
              'seagreen','deeppink','lightgray','firebrick1']
ra_linestyle=['solid','dashed','dashdot','dotted']
n_ls=len(ra_linestyle)  #n linestyles
ra_color2=['']*len(ra_color)*n_ls  #ra_color2=np.empty(len(ra_color)*2,dtype='str')
for i_ls in range(n_ls):   #repeat colors n_ls times
    ra_color2[i_ls::n_ls]=ra_color  # colors to use with alternating linestyles
ra_color=ra_color2
nmom = 4 
moment = 1  #from Shell_Avgs. 0=mean, 1=variance
k_spl=4
s_spl=3

### Read in Shell_Avgs data ###
for irun in range(n):
    l=Ralisting[irun]
    path=os.path.join(l,'Shell_Avgs')
    files=build_file_list(init,fnal, path=path)
    a = Shell_Avgs(filename=files[-1], path='') #load last Shell_Avgs file.
    a_vals_all=a.vals
    for ifile in range(1,n_files_append):
        a_new = Shell_Avgs(filename=files[-ifile-1],path='')
        a_vals_all=np.append(a_vals_all,a_new.vals,axis=3)
    nr = a.nr
    nq = a.nq
    niter = a.niter
    niter_all=niter*n_files_append
    radius = a.radius
    R_i=radius[-1]; R_c=radius[0]
    savg=np.zeros((nr,nmom,nq),dtype='float64')
    if nq>savg_all.shape[3]:
        n_append=nq-savg_all.shape[3]
        savg_all=np.append(savg_all,np.zeros((n,nr,4,n_append)),axis=3)
    #for i in range(niter): savg[:,:,:] += a.vals[:,:,:,i]
    for i in range(niter_all): savg[:,:,:] += a_vals_all[:,:,:,i]
    savg = savg*(1.0/niter_all)
    savg_all[irun,:nr,:,:nq] = savg
    time_last[irun]=a.time[-1]
    lut = a.lut
    #vr = lut[1]        # Radial Velocity
    #vtheta = lut[2]    # Theta Velocity
    #vphi = lut[3]      # Phi Velocity
    #thermal = lut[501] # Temperature
    #ke=lut[401] # kinetic energy
    #eflux = lut[1438]  # Convective Heat Flux (radial)
    #cflux = lut[1468]  # Conductive Heat Flux (radial)
    
    ### Interpolate KE to get delta_ekman
    #f = interpolate.interp1d(radius, savg[:,moment,ke])
    ke_var=savg[:,1,lut[401]]
    #x=np.arange(min(radius),max(radius)-dr,dr) #interpolation of r
    x=np.linspace(R_i,R_c,nx)
    #y=f(x)
    #spl = UnivariateSpline(x,y, k=k_spl, s=s_spl)
    spl = UnivariateSpline(radius[::-1],ke_var[::-1], k=k_spl, s=s_spl)
    ### Find peaks
    peaks,_=find_peaks(spl.__call__(x),height=0)
    #x=np.array(x)
    peak_i=peaks[0]
    peak_c=peaks[-1]
    #print(l,len(peaks),x[peak_i],x[peak_c])
    if x[peak_i] < rootmin:
        print('peak_i < rootmin: '+str(x[peak_i])+' < '+str(rootmin))
        print(peaks,np.array(x)[peaks])
        peak_i=peaks[1]  #if first peak too close to Ri use next peak.
        print(l,len(peaks),x[peak_i],x[peak_c])
    if x[peak_c] > rootmax:
        print('peak_c > rootmax: '+str(x[peak_c])+' > '+str(rootmax))
        print(peaks,np.array(x)[peaks])
        peak_c=peaks[-2] #if last peak too close to Rc use prev peak.
        print(l,len(peaks),x[peak_i],x[peak_c])
    delta_i_ekman[irun] = np.array(x)[peak_i]-radius[-1] #set IBL to x of first peak
    delta_c_ekman[irun] = radius[0]-np.array(x)[peak_c] #set OBL to x of last peak
    ### Debugging
    #if irun==0: plt.figure()
    #plt.plot(x,spl.__call__(x),'o-',ms=2,label='spl',color=ra_color[irun])
    #plt.plot(R_i+delta_i_ekman[irun],spl.__call__(R_i+delta_i_ekman[irun]),'x',color=ra_color[irun])
    #plt.xlabel('r'); plt.ylabel('var KE')

subRas = np.array([x for i,x in enumerate(Ras)] )
subRas
moment_pref={0: 'Mean', 1: 'Var'}
#R_i=radius[-1]; R_c=radius[0]

### Read in k(r) data ###
from read_f import get_k_r,read_f
r_f,k_r = get_k_r(Ralisting[0]+'/eqcoeff_kluo')
plt.plot(r_f,k_r,'-o')
plt.xlabel('r'); plt.ylabel('k')
if '-s' in sys.argv:
    plt.savefig(PDF_PATH+'k.pdf', quality=1, dpi=600)
### Compute conductive T_k(r) ###
T_k=get_T_k(T_i=10.,dT_dr_c=-1.,k_r=k_r,r=r_f)

### Plot all quantities vs r.
### Note: this assumes all files have the same qv's.
qv=a.qv
xlabel=r'$r/r_c$'
legend_font_size=5
figsize=(8,10)
for iq in qv:
    plt.figure(plt.gcf().number+1,figsize=figsize)
    for irun in range(n):
        ### Mean of quantities
        plt.subplot(311)
        labels=False; legend=True
        if irun==0:
            labels=True; legend=True
        plot_moment_r(irun,0,iq,labels,legend)
        ### Radial derivative of mean
        plt.subplot(312)
        plot_moment_r(irun,0,iq,labels,option='Derivative')
        ###Variance of quantities
        plt.subplot(313)
        plot_moment_r(irun,1,iq,labels)
    if '-s' in sys.argv:
        plt.savefig(PDF_PATH+rayleigh_codes.index_to_string[iq]+'.pdf', quality=1, dpi=600)

r=radius
k_spl=3 #4   #default is 3.
s_spl=2

### Plot T(r)
#k_c=1.0
k_c=k_r[0]
delta_i_thermal=np.zeros(n)
delta_c_thermal=np.zeros(n)
T_1_Ra = np.zeros(n)
T_norm_crit=1e-3 #BL crit norm T
Q_norm_crit=-T_norm_crit/0.1
plt.figure(plt.gcf().number+1,figsize=figsize)
for irun in range(n):
    delta_thermal=get_delta_thermal(irun,T_norm_crit,Q_norm_crit,k_r=k_r,T_k=T_k,
                                        plot_on=True,interpolate_on=True,nx=500)
    delta_i_thermal[irun]=delta_thermal[0]
    delta_c_thermal[irun]=delta_thermal[1]
    T_1_Ra[irun]=delta_thermal[2]
if '-s' in sys.argv:
    plt.figure(plt.gcf().number,figsize=figsize)
    print('saving figure to: temperature.pdf')
    plt.savefig(PDF_PATH+'temperature.pdf')
    plt.figure(plt.gcf().number+1,figsize=figsize)
    print('saving figure to: heatflow.pdf')
    plt.savefig(PDF_PATH+'heatflow.pdf')
    plt.figure(plt.gcf().number+1,figsize=figsize)
    print('saving figure to: T1.pdf')
    plt.savefig(PDF_PATH+'T1.pdf')

    
### Find Ra_crit
Ek=2e-3
Ra_crit=7500.  #Kai in slack on 6/17/20
ke=savg_all[:,:nr,0,lut[401]]
iRa_crit = np.argwhere(np.amax(ke,axis=1)>1.)[0]
Ra_crit=Ras[iRa_crit]-1. #-1 to reduce Ra_c slightly so alpha does not have a zero in denom.
print('Ra_crit=%f'%Ra_crit)
### Write stats to file.
f=open('stats.txt','w')
f.write('Ra Ra/Ra_crit k_mean,max KE_mean\n')
for ir in range(n):
    f.write('%e %e %e %e %e \n'%(Ras[ir],Ras[ir]/Ra_crit,k_r.mean(),k_r.max(),ke[ir].mean()))
f.close()

### Calculate theoretical BL ### Boundary Layers    
i_A_nonrot=-1
#i_A_rot=0   #use hi Ra for nonrot, low Ra for rot.
i_A_rot=np.max([np.argmax(delta_i_thermal<1.),np.argmax(delta_c_thermal<1.)]) #max i where both BLs found
D=1.
#A = Ra_crit/Ek**(-4./3)  #(13)
#A_nonrot = (delta_c_thermal[i_A_nonrot]*Ek/D)**(+3.)*Ras[i_A_nonrot]
#delta_thermal_nonrot = D*A_nonrot**(+1./3)*Ek**(-1.)*Ras**(-1/3.)
#A_rot = (delta_c_thermal[i_A_rot]/D)**(+1./3)*Ras[i_A_rot]*Ek**(-5./9)
#delta_thermal_rot = D*Ras**(-3.)*A_rot**(+3.)*Ek**(+5./3)
A_c_rot = (delta_c_thermal[i_A_rot]/D)**(1./3)*Ras[i_A_rot]*Ek**(4./3)
delta_c_thermal_rot = D*A_c_rot**(3)*Ras**(-3.)*Ek**(-4.)
A_c_nonrot = (delta_c_thermal[i_A_nonrot]/D)**(3.)*Ras[i_A_nonrot]*Ek**(4./3)
delta_c_thermal_nonrot = D*A_c_nonrot**(1/3.)*Ras**(-1/3.)*Ek**(-4./9)
print('A_c_nonrot=%.4e, A_c_rot=%.4e'%(A_c_nonrot,A_c_rot))
dTdr_c=savg_all[0,0,0,lut[507]]
T_1_crit=-dTdr_c*R_c**2 #(8)
alpha=np.zeros(n)
#for i in np.argwhere(T_1_Ra): alpha[i]=np.log(T_1_Ra[i]/T_1_crit)/np.log(Ra_crit/Ras[i])
#T_1_Ra=np.clip(T_1_Ra,0,1e3)  # clip T_1_Ra<0
#alpha=np.log(T_1_Ra/T_1_crit)/np.log(Ra_crit/Ras)
#alpha[alpha==-np.inf]=0. #replace infs with 0
#alpha=np.nan_to_num(alpha,0)  #replace nans with 0
for i in np.argwhere(T_1_Ra>0.): alpha[i]=np.log(T_1_Ra[i]/T_1_crit)/np.log(Ra_crit/Ras[i])

### Plot delta_thermal vs Ra
if '-s' not in sys.argv:
    nfig=plt.gcf().number+3  #+3 bc loop in get_delta_thermal does -2 at end and need new fig.
else:
    nfig=plt.gcf().number+1
plt.figure(nfig,figsize=figsize)
plt.subplot(311)
plt.plot(Ras, delta_i_thermal, 'o-',  label='inner boundary layer thickness')
plt.plot(Ras, delta_c_thermal, 'o-',  label='outer boundary layer thickness')
plt.plot(Ras, delta_c_thermal_nonrot, 'x-', label='theory, non-rotating',color='grey')
plt.plot(Ras, delta_c_thermal_rot, '+-', label='theory, rotating',color='grey')
plt.xlabel('Ra');plt.ylabel(r'$\delta_T$');plt.grid()
plt.legend()
### Plot Ekman BL delta vs Ra
plt.subplot(312)
plt.plot(Ras, delta_i_ekman, 'o-',  label='inner boundary layer thickness')
plt.plot(Ras, delta_c_ekman, 'o-', label='outer boundary layer thickness')
plt.xlabel('Ra'); plt.ylabel(r'$\delta_E$'); plt.grid()
#plt.ylim(delta_i_min,0.1)
### Plot ratio of thermal/Ekman BL
plt.subplot(313)
plt.plot(subRas*Ek**(3./2),delta_i_thermal/delta_i_ekman,'o-',label='inner boundary')
plt.plot(subRas*Ek**(3./2),delta_c_thermal/delta_c_ekman,'o-',label='outer boundary')
plt.yscale('log');plt.xscale('log')
#plt.xlabel('Ra');
plt.xlabel(r'RaEk$^{3/2}$');
plt.ylabel(r'$\delta_T/\delta_E$')
plt.grid()
### Add second xaxis below, for Ra*Ek**(3/2).
#ax2=plt.twiny()
#ax2.xaxis.set_ticks_position("bottom")
#ax2.xaxis.set_label_position("bottom")
#ax2.spines["bottom"].set_position(("axes",-0.1))
#ax2.set_xticks(subRas)
#ax2.set_xticklabels(subRas*Ek**(3./2))
#ax2.set_xlabel(r"RaEk$^{3/2}$")
### Save
if '-s' in sys.argv:
    print('saving figure to: delta.pdf')
    plt.savefig(PDF_PATH+'delta.pdf')


### Plot T_1 vs Ra and alpha vs Ra
plt.figure(plt.gcf().number+1,figsize=figsize)
plt.subplot(211)
plt.plot(Ras,T_1_Ra,'o-',ms=5)
plt.xlabel('Ra');plt.ylabel('T_1')
plt.subplot(212)
plt.plot(Ras,alpha,'o-',ms=5)
plt.xlabel('Ra');plt.ylabel(r'$\alpha$')
if '-s' in sys.argv:
    print('saving figure to: alpha.pdf')
    plt.savefig(PDF_PATH+'alpha.pdf')


mpl.rcParams.update({'font.size':18})
### Contour ke(Ra,r)
r_mesh,Ra_mesh=np.meshgrid(r,Ras)
levels=np.linspace(0,ke.max(),50)
### First, contour smoothly (second, contour in vertical bars)
### Ra/Ra_crit
plt.figure(plt.gcf().number+1,figsize=figsize)
plt.contourf(Ra_mesh/Ra_crit,r_mesh,ke,levels=levels,cmap='YlOrRd')
plt.xlim(0,Ra_mesh.max()/Ra_crit)
plt.colorbar(label='KE')
ke_crit=1.  # value of ke considered "convective"
plt.contourf(Ra_mesh/Ra_crit,r_mesh,ke,levels=[0,ke_crit],colors=['white','white'])
plt.xlabel(r'$Ra/Ra_{crit}$')
plt.ylabel('r')
if '-s' in sys.argv:
    print('saving figure to: ke_contour_Rac.pdf')
    plt.savefig(PDF_PATH+'ke_contour_Rac.pdf')
### Ra
plt.figure(plt.gcf().number+1,figsize=figsize)
plt.contourf(Ra_mesh,r_mesh,ke,levels=levels,cmap='YlOrRd')
plt.xlim(0,Ra_mesh.max())
plt.colorbar(label='KE')
ke_crit=1.  # value of ke considered "convective"
plt.contourf(Ra_mesh,r_mesh,ke,levels=[0,ke_crit],colors=['white','white'])
plt.xlabel(r'$Ra$')
plt.ylabel('r')
if '-s' in sys.argv:
    print('saving figure to: ke_contour_Ra.pdf')
    plt.savefig(PDF_PATH+'ke_contour_Ra.pdf')

### Second, contour in vertical bars
### Ra
#dRa = 5e3  #Ra bin width
fig = plt.figure(plt.gcf().number+1,figsize=figsize)
ax = fig.add_subplot(111)
dRa = np.diff(Ras); dRa = np.append(dRa,dRa[-1])
Ralo = Ra_crit; Rahi = Ras[iRa_crit]+dRa[iRa_crit]/2.
for iRa in range(int(iRa_crit),n):
    #print('Ra=%f Ra/crit=%f Ralo=%f Rahi=%f max(KE)=%f'%(Ras[iRa],Ras[iRa]/Ra_crit,Ralo,Rahi,ke[iRa,:].max()))
    r_grid,Ra_grid = np.meshgrid(r,np.array([Ralo,Rahi]))
    plt.contourf(Ra_grid,r_grid,[ke[iRa,:],ke[iRa,:]],levels=levels,cmap='YlOrRd')
    if iRa<n-1:
        Ralo = Rahi; Rahi = Ras[iRa+1]+dRa[iRa+1]/2.
        Ralo = max(Ra_crit,Ralo) #for Ras=Ra_crit set lo right at Ra_crit
plt.ylabel('r'); plt.xlabel('Ra'); plt.ylim(R_i,R_c); plt.xlim(0); plt.colorbar(label='KE')
ax2 = ax.twiny()
ax2.set_xlim(ax.get_xlim()[0]/Ra_crit,ax.get_xlim()[1]/Ra_crit)
ax2.set_xticks(np.arange(0,int(ax2.get_xlim()[1])+1))
plt.vlines(1,r[0],r[-1],color='k')
ax2.set_xlabel(r'$Ra/Ra_{crit}$')
if '-s' in sys.argv:
    print('saving figure to: ke_contour_Ra_bar.pdf')
    plt.savefig(PDF_PATH+'ke_contour_Ra_bar.pdf')
    
#plt.show()

### Plot 3-panel k(r), T(r), KE(r)
cmap_lines = mpl.cm.get_cmap('tab20')
plt.figure(plt.gcf().number+1,figsize=(8,10))
plt.subplot(311)
plt.plot(r_f,k_r,'-')
plt.xlabel('r'); plt.ylabel('k')
plt.subplot(312)
i_skip = max([int(n/10),1])  #only plot 10 curves, so skip i_skip indexes. max to avoid 0.
for irun in range(0,n,i_skip):
    T_entropy=savg_all[irun,:nr,0,lut[501]]
    plt.plot(radius,T_entropy,label=Ras[irun],color=cmap_lines(irun/n))
plt.xlabel('r'); plt.ylabel('T')
plt.legend(title='Ra',fontsize=8,ncol=2)
plt.subplot(313)
for irun in range(0,n,i_skip):
    KE=savg_all[irun,:nr,0,lut[401]]
    plt.plot(radius,KE,label=Ras[irun],color=cmap_lines(irun/n))
plt.xlabel('r'); plt.ylabel('KE')
if '-s' in sys.argv:
    savename = 'k_T_KE.pdf'
    print('saving figure to: '+savename)
    plt.savefig(PDF_PATH+savename)


### Plot KE vs v_r*T to compare KE to advected heat flux.
plt.figure(plt.gcf().number+1,figsize=(8,10))
irun=-1
# compute v_r*T
adv = abs(savg_all[irun,:nr,0,lut[1]])*savg_all[irun,:nr,0,lut[501]]
plt.plot(adv,savg_all[irun,:nr,0,lut[401]],'o-')
plt.ylabel(rayleigh_codes.index_to_string[401])
plt.xlabel(r'$v_r T$')
### Plot KE vs v*T to compare KE to advected heat flux.
plt.figure(plt.gcf().number+1,figsize=(8,10))
for irun in range(n):
    # compute v*T
    v = np.sqrt(savg_all[irun,:nr,0,lut[1]]**2+savg_all[irun,:nr,0,lut[2]]**2+savg_all[irun,:nr,0,lut[3]]**2)
    adv = v*savg_all[irun,:nr,0,lut[501]]
    plt.plot(adv,savg_all[irun,:nr,0,lut[401]],'o-',label=Ras[irun],color=cmap_lines(irun/n))
plt.ylabel(rayleigh_codes.index_to_string[401])
plt.xlabel(r'$v T$')
if '-s' in sys.argv:
    savename = 'KE_vT.pdf'
    print('saving figure to: '+savename)
    plt.savefig(PDF_PATH+savename)
