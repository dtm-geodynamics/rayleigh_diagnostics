#!/usr/bin/env python

#Equatorial Slice Plotting Example.
#Displays a single variable from the equatorial_slice data structure
#
# Equatorial Slice Data stucture format:
#    """Rayleigh Equatorial Slice Structure
#    ----------------------------------
#    self.niter                                    : number of time steps
#    self.nq                                       : number of diagnostic quantities output
#    self.nr                                       : number of radial points
#    self.nphi                                     : number of phi points
#    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
#    self.radius[0:nr-1]                           : radial grid
#    self.vals[0:phi-1,0:nr-1,0:nq-1,0:niter-1]    : The equatorial_slices
#    self.phi[0:nphi-1]                            : phi values (in radians)
#    self.iters[0:niter-1]                         : The time step numbers stored in this output file
#    self.time[0:niter-1]                          : The simulation time corresponding to each time step
#    self.version                                  : The version code for this particular output (internal use)
#    self.lut                                      : Lookup table for the different diagnostics output
#    """

#from diagnostic_reading import Equatorial_Slice, build_file_list
from rayleigh_diagnostics import Equatorial_Slices
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.pyplot import ion,plot
from diagnostic_plotting import lutlabels,getlabel
from diagnostic_tools import file_list
import argparse

### Check python version
import sys
if sys.version_info[0]==3:
    raw_input=input

parser = argparse.ArgumentParser(description="""Plot global averages (G_Avgs): KE vs time.""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False, default=None, 
                    help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False, default=None, 
                    help='final index to plot to')
parser.add_argument('-in', '--index', action='store', type=float, dest='index', required=False, default=-1, 
                    help='index to plot')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-q', '--quantity', action='store', dest='quantity', type=int, nargs='*', default=[501], 
                    required=False, help='quantity')
parser.add_argument('-d', '--default',action='store_true',dest='default',help='use default plotting options',
                    default=False)  #otherwise queue user.
args = parser.parse_args()

#Set savefig to True to save to savefile.  Interactive plot otherwise.
savefig = False
#savefig = True
savefig = args.save
savefile = 'eq_slice'
if savefig: print('savefig='+str(savefig))
    
path='Equatorial_Slices'
initial=args.initial
final=args.final
files = file_list(initial,final,path=path)
nfiles=len(files)

a = Equatorial_Slices(filename=files[0],path='')
if not args.default:
    print('Labels: '+str(lutlabels(a.qv).labels))
    print('Codes: '+str(a.qv))
    user_codes = raw_input('Enter quantity codes: ').split(" ")
    q_codes = [int(num) for num in user_codes]
else:
    ### Default q_codes
    #q_codes = [3,501,802]  #v_phi, entropy, b_theta
    q_codes = [1,2,3]  #v_phi, entropy, b_theta
nquants=len(q_codes)        
remove_means = np.zeros(nquants) #boolean
if 501 in q_codes: remove_means[np.where(np.asarray(q_codes)==501)] = 1.

#Set up the grid
nr = a.nr
nphi = a.nphi
r = a.radius/np.max(a.radius)
phi = np.zeros(nphi+1,dtype='float64')
phi[0:nphi] = a.phi
phi[nphi] = np.pi*2  # For display purposes, it is best to have a redunant data point at 0,2pi
radius_matrix, phi_matrix = np.meshgrid(r,phi)
X = radius_matrix * np.cos(phi_matrix)
Y = radius_matrix * np.sin(phi_matrix)
#Extract fields to plot
qindexes = a.lut[q_codes]
qtitles = lutlabels(q_codes)
fields = np.zeros((nphi+1,nr,nquants))
#Extract data and time
data={}
time={}
niter=np.zeros(nfiles)
for i,f in enumerate(files):
    #a = Equatorial_Slice(filename=f,path='')
    a = Equatorial_Slices(filename=f,path='')
    niter[i]=a.niter
niters=np.sum(niter)

def plot_quivers(itime):
    #### Add velocity quivers
    if 1 in a.qv and 3 in a.qv: #check if (vr,vt,vp) are in a.qv
        cosphi = np.cos(a.phi); sinphi = np.sin(a.phi)
        vscale=5e-2  #maximum v arrow length
        vmax = np.max([a.vals[:,:,a.lut[1],itime],a.vals[:,:,a.lut[3],itime]])
        nr_quiv = 12.; nphi_quiv = 26.  #number of quivers
        #rskip = 10; pskip = 4  #indexes to skip for quiver spacing
        rskip = int(nr/nr_quiv); pskip = int(nphi/nphi_quiv)
        for ir in range(1,a.nr-1,rskip):
            for iphi in range(0,a.nphi-1,pskip):
                ### for even ir move phi over half a spacing.
                if (ir%2)==0 and iphi+pskip/2<a.nphi: iphi=int(iphi+pskip/2) 
                x = X[iphi,ir]; y = Y[iphi,ir]
                vr = a.vals[iphi,ir,a.lut[1],itime]; vp = a.vals[iphi,ir,a.lut[3],itime]
                vx = vr*cosphi[iphi] - vp*sinphi[iphi]; vy = vr*sinphi[iphi] + vp*cosphi[iphi]
                #plt.quiver(x,y,vx/vmax*vscale,vy/vmax*vscale,width=2e-3)
                plt.quiver(x,y,vx,vy,scale=vmax/vscale,width=2e-3)

#Choose time to plot
if not args.default:  #if index not input by user then ask them.
    tindex = 0          # Display the first timestep from the file
    user_time = raw_input('Choose iteration to plot out of %i: '%(niters-1))
    if int(user_time)==-1: user_time=niters-1
    tindex = int(user_time)
else: tindex=args.index
if tindex==-1: tindex=int(niters-1)
# find ifile where tindex lives.
niter_cumsum=np.cumsum(niter).astype(int)-niter.astype(int)
ifile=np.max(np.where(niter_cumsum<=tindex))
itime=tindex-niter_cumsum[ifile]   #index within ifile of tindex

# Read in data only for the time of interest here.
a = Equatorial_Slices(filename=files[ifile],path='')
print('iteration %f is at time=%f'%(tindex,a.time[itime]))
plot_range_factor = 3.   #this factor determines plotting min,max in std from mean.
for j in range(nquants):
    fields[0:nphi,:,j] =a.vals[:,:,qindexes[j],itime]
    fields[nphi,:,j] = fields[0,:,j]  #replicate phi=0 values at phi=2pi
    if (remove_means[j]):
        for i in range(nr):
            fields[:,i,j] = fields[:,i,j]-np.mean(fields[:,i,j])
    field_meanminussigma = np.mean(fields[:,:,j]) - plot_range_factor*np.std(fields[:,:,j])
    field_meanplussigma = np.mean(fields[:,:,j]) + plot_range_factor*np.std(fields[:,:,j])
    # Plot Equatorial Slices
    plt.figure(j)
    img = plt.pcolormesh(X,Y,fields[:,:,j],cmap='jet',shading='gouraud',edgecolors=None,
                             vmin=field_meanminussigma,vmax=field_meanplussigma)
    cbar_min=field_meanminussigma; cbar_max=field_meanplussigma
    if q_codes[j]==401: #limit colorbar to >0
        cbar=plt.colorbar(label=qtitles.labels[j],boundaries=np.linspace(0,cbar_max,100),ticks=np.linspace(0,int(cbar_max),11,dtype=int))
    else:
        plt.colorbar(label=qtitles.labels[j])
    plt.title('t=%.2f'%(a.time[itime]))
    plt.axis('off')
    plt.plot([0.35,1.0],[0,0],'k') #horiz line at phi=0
    plot_quivers(itime)
    if (savefig):
        savename=str(savefile+'_t%.2f'%(a.time[itime]))+'_'+qtitles.labels[j]+'.pdf'
        plt.savefig(savename)
        print('Saved '+savename)




        
if not savefig:
    print('showing...')
    ion()
    plt.tight_layout()
    plt.show()
print('finished script.')
