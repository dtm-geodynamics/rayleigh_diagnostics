#!/usr/bin/env python
########################################################################3
#
#   plot_spectral_trace_gauss.py
#   same as plot_spectra_trace.py but using Gauss coef rather than Br(l,m)
#
#   Plotting Example:  Shell_Spectra
#
#   plot_spectral_trace.py
#   Plot time series (trace) of kinetic and
#   magnetic power spectral elements.
#
#   We plot the velocity power spectrum from one
#   shell spectrum output using the PowerSpectrum
#   class from diagnostic_reading.py.  
#   
#   Ideally, one should loop over several spectra
#   and take a time-average, similar to the approach
#   taken in plot_energy_flux.py.  This routine plots
#   a SINGLE SNAPSHOT of the power.
#
#   When a PowerSpectrum object is initialized, 
#   only power is computed from the Shell_Spectra
#   file and saved.
#  
#   If we want to look at individual m-values,
#   we can use the ShellSpectra class instead.
#   See diagnostic_reading.py for a brief 
#   description of that data structure.
#
#   The power spectrum structure is simpler to
#   work with.  It contains the following attributes:
#    ----------------------------------
#    self.niter                                    : number of time steps
#    self.nr                                       : number of radii at which power spectra are available
#    self.lmax                                     : maximum spherical harmonic degree l
#    self.radius[0:nr-1]                           : radii of the shell slices output
#    self.inds[0:nr-1]                             : radial indices of the shell slices output
#    self.power[0:lmax,0:nr-1,0:niter-1,0:2]       : the velocity power spectrum.  The third
#                                                  : index indicates (0:total,1:m=0, 2:total-m=0 power)
#    self.mpower[0:lmax,0:nr-1,0:niter-1,0:2]      : the magnetic power spectrum
#    self.iters[0:niter-1]                         : The time step numbers stored in this output file
#    self.time[0:niter-1]                          : The simulation time corresponding to each time step
#    self.magnetic                                 : True if mpower exists
#
#    self.vals[0:lmax,0:mmax,0:nr-1,0:nq-1,0:niter-1] 
#                                                  : The complex spectra of the shells output 
#    -------------------------------------
##################################

from rayleigh_diagnostics import Power_Spectrum, Shell_Spectra
from diagnostic_tools import file_list, calculate_Ylms,renormalize_shell_spectra,\
     gauss_coef,dipole_latitude,dipole_longitude,polarity_chrons,elike_stats,\
     rms_mean,rms_stdev,rms_sum,remove_longitude_jumps
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.pyplot import plot,ion,show
import numpy as np
from math import acos
import sys
import os 
cwd = os.getcwd()
r2d=180./np.pi  #radians/degree
ion()
pi=np.pi
fontsize=20
from matplotlib import rc, rcParams
rcParams['legend.fontsize'] = fontsize
rcParams['axes.labelsize'] = fontsize
rcParams['xtick.labelsize'] = fontsize
rcParams['ytick.labelsize'] = fontsize
rcParams['font.size'] = fontsize
#rc('text', usetex=True)


import argparse
parser = argparse.ArgumentParser( \
                       description="""Plot Gauss components.""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False, default=None,
                    help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False, default=None, 
                    help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-sn', '--savename', action='store', dest='savename', type=str, default='gauss', 
                    required=False, help='savename of the figure')
parser.add_argument('-n', '--noplot', action='store_const', dest='noplot', const=True, default=False, 
                    required=False, help='suppress plotting windows')
parser.add_argument('-t', '--text', action='store_const', dest='text', const=True, default=False, 
                    required=False, help='print stats to text file')
parser.add_argument('-mcl', '--min_chron_len', action='store', type=float, dest='min_chron_len',
                    required=False, default=0.4,help='initial index to plot from')
parser.add_argument('-v', '--version', action='store', type=float, dest='version', required=False, default=2,
                    help='Rayleigh version (1=bitbucket, 2=github (def))')
args = parser.parse_args()

print("initial="+str(args.initial)+" final="+str(args.final))

def set_plot_fontsize(plt,fontsize=None,xlabel=None,ylabel=None):
    plt.ylabel(ylabels[i],{'fontsize':fontsize})
    plt.xlabel(xlabel,{'fontsize':fontsize})
    plt.yticks(size=fontsize)
    plt.xticks(size=fontsize)

### Rayleigh version
br_lut = 801
if args.version==1: br_lut=401

### Input parameters
eps_r=0.35  #r_i/r_o
r_o=1+1/(1/eps_r-1.)  #dimless outer boundary radius.
Pm=10.  #need to read this in from main_input?
#Set savefig to True to save to savefile.  Interactive plot otherwise.
savefig = args.save
savefile = args.savename
if savefig: print('savefig='+str(savefig))
window_title = 'spectral_trace ('+cwd+')'
initial=args.initial
final=args.final
print("Reading files...")
#files = file_list(initial, final, 'Shell_Spectra')
### Shell_Spectra
files_ss = file_list(initial, final, path='Shell_Spectra')
#files_ss = file_list(initial, final, 'Shell_Spectra')
#spec0 = Power_Spectrum(files_ss[0],magnetic=True)

nfiles_ss=len(files_ss)
if nfiles_ss==0: print('No files found!')
lmax=8  #number of gauss coef to compute stats for.
gauss=np.zeros((lmax+1,lmax+1,1),dtype=complex)
powers=np.zeros((lmax+1,1,3))  #powers(nl,ntimes,3). (0:total,1:m=0, 2:total-m=0 power)
mpowers=np.zeros((lmax+1,1,3))  #powers(nl,ntimes,3).
time=np.zeros(1)
dip_lat=np.zeros(1)
for ifile,file in enumerate(files_ss):
    sys.stdout.write('file '+str(ifile)+'/'+str(nfiles_ss)+' \r')
    sys.stdout.flush()
    a=Shell_Spectra(file,path='')   #note path is already in filename 'file'.
    spec_i = Power_Spectrum(files_ss[ifile],magnetic=True,path='')

    br_index=a.lut[br_lut]
    # Convert Brlm to orthonormal
    brlm_on=renormalize_shell_spectra(a.vals[:lmax+1,:lmax+1,0,br_index,:])
    # Compute Gauss
    gauss_i=gauss_coef(brlm_on)
    i_start=0
    if ifile==0:
        gauss[:,:,0]=gauss_i[:,:,0]  #initial time.
        time[0]=a.time[0]
        powers[:lmax+1,0,:]=spec_i.power[:lmax+1,0,0,:]
        mpowers[:lmax+1,0,:]=spec_i.mpower[:lmax+1,0,0,:]
        i_start=1  #skip this index in next loop only for ifile=0
    gauss=np.append(gauss,gauss_i[:,:,i_start:],axis=2)    #append along time axis
    powers=np.append(powers,spec_i.power[:lmax+1,0,i_start:,:],axis=1)
    mpowers=np.append(mpowers,spec_i.mpower[:lmax+1,0,i_start:,:],axis=1)
    time=np.append(time,a.time[i_start:])
g10=gauss[1,0,:].real
dip_lat=dipole_latitude(gauss[1,0,:].real,gauss[1,1,:].real,gauss[1,1,:].imag)*180./pi  #[deg]
dip_lon = dipole_longitude(gauss[1,0,:].real,gauss[1,1,:].real,gauss[1,1,:].imag)*180./pi  #[deg]
times=time
#Compute mpowers_tot as fn of time.
ntimes=len(times)

mpowers_tot=np.zeros(ntimes)
gauss_tot=np.zeros(ntimes)
gauss_tot_vec=np.zeros(ntimes)
gauss_nad=np.zeros(ntimes)  #sum of all glm terms except g10.
gauss_zonal=np.zeros(ntimes)   #sum of zonal terms (m=0)
lowes=np.zeros((lmax+1,ntimes))  #sum of all m for each l.
for t in range(ntimes):
    mpowers_tot[t]=np.sum(mpowers[:,t,0])  #sum over all l at time t.
    gauss_tot[t]=np.sum([gauss[:,:,t].real,gauss[:,:,t].imag])  #sum real+imag?
    #gauss_tot_vec[t]=np.linalg.norm([gauss[:,:,t].real,gauss[:,:,t].imag])  #vector sum = sqrt of sum of squares.
    gauss_tot_vec[t]=rms_sum([gauss[:,:,t].real,gauss[:,:,t].imag])
    gauss_nad[t]=gauss_tot[t]-g10[t]     #sum all terms except g10.
    gauss_zonal[t]=np.sum([gauss[:,0,t].real,gauss[:,0,t].imag])  
    for l in range(lmax+1):
        lowes[l,t]=np.linalg.norm([gauss[l,:l,t].real,gauss[l,:l,t].imag]) #sqrt(sum(glm^2+hlm^2))
# Time ave of each glm.
gauss_ave=np.zeros((lmax+1,lmax+1),dtype=complex)   #time ave of each glm
for l in range(lmax+1):
    for m in range(l):
        gauss_ave[l,m]=complex(np.mean(gauss[l,m,:].real),np.mean(gauss[l,m,:].imag))
        
# Lat Shift.
lat_shift_yes=np.zeros(ntimes)
dip_lat_shifted=np.zeros(ntimes)
for k in range(ntimes):
    dip_lat_shifted[k]=dip_lat[k]
    if g10[k]<0:
        lat_shift_yes[k]=1.
        dip_lat_shifted[k]=2*np.pi-dip_lat[k]
# ME (vol,rms) Dipolarity
#dipolarity=np.sqrt(mpowers_dip)/np.sqrt(mpowers_tot)
me_dipolarity=np.sqrt(mpowers[1,:,0])/np.sqrt(mpowers_tot)
me_adipolarity=np.sqrt(mpowers[1,:,1])/np.sqrt(mpowers_tot)

# Gauss (surf) Dipolarity: ratio of power in Gauss coef of dipole (l=1) to sum of all.
gauss_dipolarity=(gauss[1,0,:].real**2+gauss[1,1,:].real**2+gauss[1,1,:].imag**2)/np.square(gauss_tot_vec[:])
gauss_adipolarity=(gauss[1,0,:].real**2)/np.square(gauss_tot_vec[:])

### PRINTING ###
print("ME mean: tot=%8.2e dip=%8.2e dip/tot=%8.2e from t=%8.2f-%8.2f" %(np.mean(mpowers_tot),np.mean(mpowers[1,:,0]),np.mean(me_dipolarity),times[0],times[-1]))
print("ME final: tot=%8.2e dip=%8.2e dip/tot=%8.2e" %(mpowers_tot[-1],mpowers[1,-1,0],me_dipolarity[-1]))

# Time Scaling #
time_index = 0  #plot the first record?
# Scale time from kappa units to kyr
Pm=10.
Pr=1.
kyr_dip_decay=50.
print('Assume for time scaling: Pm=%f, Pr=%f'%(Pm,Pr))
times_mag=times/Pm
times_dip=times_mag/(1.538461/pi)**2.
times_kyr=times_dip*kyr_dip_decay

### Elike stats
adnad,oddeven,zonality,elike_means=elike_stats(gauss)
# Times
#time_dip=time/(Pm*(r_o/pi)**2)  #[dipole decay times]
#time_dip_yr=50e3                #[yr/dip_decay]
#time_yr=time_dip*time_dip_yr       #[yr]
len_time=max(times)-min(times)    #[tau_kappa]
len_dip=max(times_dip)-min(times_dip)  #[tau_dip]
len_kyr=max(times_kyr)-min(times_kyr)     #[yr]
# Reversal stats
min_chron_len=args.min_chron_len
print('min_chron_len=%f [dipdecay] %f [kyr]'%(min_chron_len,min_chron_len*kyr_dip_decay))
nrev,nexcursions,n_eq_cross,pchrons=polarity_chrons(times_dip,dip_lat,min_chron_len=min_chron_len)
print('n_eq_cross=%i, %f/Myr. nrev=%i, %f/Myr'%(n_eq_cross,n_eq_cross/(len_kyr*1e-3),nrev,nrev/(len_kyr*1e-3)))

if not args.noplot:
    
    ### PLOTTING ###
    nfig=1
    plt.figure(nfig,figsize=(15,10))
    plt.figure(nfig).canvas.set_window_title(window_title)
    lw = 1.5
    label0='All m'
    label1='m=0'
    label2='Total - m=0'

    # Plot ME Tot, Dipole, Axi Dip
    plt.subplot(221)
    plt.plot(times,mpowers_tot,label='Total',linewidth=lw)
    plt.plot(times,mpowers[1,:,0],label='Dipole',linewidth=lw)
    plt.plot(times,mpowers[1,:,1],label='Axial Dipole',linewidth=lw)
    plt.xlabel('Time')
    plt.ylabel('Magnetic Power')
    legend = plt.legend(loc='lower left', shadow=True, ncol = 1)
    #plt.title(cwd,fontsize=10)

    # Plot Dipolarity: dipole/total
    plt.subplot(222)
    plt.plot(times,me_dipolarity,label='dip/tot',linewidth=lw)
    plt.plot(times,me_adipolarity,label='dipaxi/tot',linewidth=lw)
    plt.xlabel('Time')
    plt.ylabel('Dipole/Total (CMB)')
    legend = plt.legend(loc='lower left', shadow=True, ncol = 1)

    # Plot Dipole Tilt
    plt.subplot(223)
    plt.plot(times,dip_lat,linewidth=lw)
    plt.xlabel('Time')
    plt.ylabel('Dipole Co-Latitude [deg]')
    plt.ylim(180,0)

    # Plot Dipole longitude.
    plt.subplot(224)
    plt.plot(times,dip_lon,linewidth=lw)
    plt.xlabel('Time')
    plt.ylabel('Dipole Longitude [deg]')
    plt.ylim(-180,180)

    plt.tight_layout()
    if (savefig):
        plt.savefig(savefile+str(nfig)+'.pdf')
        print('Saved '+savefile)
    else:
        plt.show()

    ### Add second Figure for dipole lat-lon path.
    nfig+=1
    plt.figure(nfig,figsize=(10,10))
    plt.subplot(221)  #create 2x2 subplots, but use top 2 as single mollweide.
    plt.subplot2grid((2,2),(0,0),colspan=2,projection='mollweide')
    plt.grid(True)
    # wrap dip_lat to -180,+180.
    dip_lat_shift = np.zeros(ntimes)
    for k in range(ntimes):
        dip_lat_shift[k] = 90.-dip_lat[k]  #shift 0 to +90, 180 to -90.
    dip_lon_shift,dip_lat_shift = remove_longitude_jumps(dip_lon,dip_lat_shift)
    for i in range(len(dip_lon_shift)):
        plt.plot(dip_lon_shift[i]*pi/180,dip_lat_shift[i]*pi/180,color='blue')
    plt.title('Dipole Axis')
    
    ### Add 3rd plot for polar view.
    #Plot polar views.
    #Latitude labels
    rvals=np.arange(0,1.1,1/6.)      #use six latitude labels
    rlabels=90-np.arange(0,91,15)    #latitude labels, numbers.
    rlabels=rlabels.astype(np.str)   #change to str array.
    #North
    plt.subplot2grid((2,2),(1,0),colspan=1,projection='polar')
    plt.grid(True)
    plt.rgrids(rlabels[:-1].astype(np.float),rlabels[:-1])
    plt.title('North Pole')
    ind_n = np.where((90.-dip_lat)>=0.)[0]
    if len(ind_n)>1:
        rho=np.cos((90.-dip_lat[ind_n])*pi/180.)
        plt.plot(dip_lon[ind_n]*pi/180,rho)
    #South
    plt.subplot2grid((2,2),(1,1),colspan=1,projection='polar')
    plt.grid(True)
    plt.rgrids(rlabels[:-1].astype(np.float),rlabels[:-1])
    plt.title('South Pole')
    ind_s = np.where((90.-dip_lat)<0.)[0]
    if len(ind_s)>1:
        rho=np.cos((90.-dip_lat[ind_s])*pi/180.)
        plt.plot(dip_lon[ind_s]*pi/180,rho)

    plt.tight_layout()
    if (savefig):
        plt.savefig(savefile+str(nfig)+'.pdf')
        print('Saved '+savefile)
    else:
        plt.show()

    ################################################################################################

    # Davies running ave, D14 eq 8.
    g10_run=np.zeros(ntimes)
    glm_run=np.zeros(ntimes)
    gnad_run=np.zeros(ntimes)      #nad=nonaxdip
    gzonal_run=np.zeros(ntimes)
    for i in range(1,ntimes):
        g10_run[i]=g10_run[i-1]*(i-1)/i + (1./i)*g10[i]
        glm_run[i]=glm_run[i-1]*(i-1)/i + (1./i)*gauss_tot[i]
        gnad_run[i]=gnad_run[i-1]*(i-1)/i + (1./i)*gauss_nad[i]
        gzonal_run[i]=gzonal_run[i-1]*(i-1)/i + (1./i)*gauss_zonal[i]
    # Compute E-like stats
    adnad,oddeven,zonality,elike_means=elike_stats(gauss)
    # Print stats
    #print('ave dipolarity=%.2e, AD/NAD=%.2e, O/E=%.2e, Z/NZ=%.2e'%(np.mean(dipolarity),np.mean(adnad),np.mean(oddeven),np.mean(zonality)))
    print('ave Gauss dipolarity=%.2e, AD/NAD=%.2e, O/E=%.2e, Z/NZ=%.2e, chi2=%.2e'%(np.mean(gauss_dipolarity),elike_means[0],elike_means[1],elike_means[2],elike_means[3]))

    ### Plot time averages
    nfig+=1
    plt.figure(nfig,figsize=(15,10))
    # Plot g10(t), g10_run(t)
    plt.subplot(221)
    plot(times_kyr,g10)
    plot(times_kyr,g10_run)
    plt.xlabel('kyr')
    plt.ylabel('g10, g10_run')
    # Plot glm(t), glm_run(t)
    plt.subplot(222)
    plot(times_kyr,gauss_tot)
    plot(times_kyr,glm_run)
    plt.xlabel('kyr')
    plt.ylabel('g_tot, g_tot_run')
    # Plot nonaxdip=nad
    plt.subplot(223)
    #plot(times_kyr,gauss_nad)
    plot(times_kyr,gauss_nad)
    plot(times_kyr,gnad_run)
    plt.xlabel('kyr')
    plt.ylabel('(g_tot-g10), _run')
    # Plot zonal terms only
    plt.subplot(224)
    plot(times_kyr,gauss_zonal)
    plot(times_kyr,gzonal_run)
    plt.xlabel('kyr')
    plt.ylabel('g_zonal (m=0) , _run')

    plt.tight_layout()
    if (savefig):
        plt.savefig(savefile+str(nfig)+'.pdf')
        print('Saved '+savefile)
    else:
        plt.show()

    ### Plot time averages of the first few harmonics
    nfig+=1
    plt.figure(nfig,figsize=(15,10))
    plt.subplot(221)
    plot(times_kyr,gauss[1,0,:].real,label='(1,0)')
    plot(times_kyr,gauss[1,1,:].real,label='(1,+1)')
    plot(times_kyr,gauss[1,1,:].imag,label='(1,-1)')
    plot(times_kyr,gauss[2,0,:].real,label='(2,0)')
    plot(times_kyr,gauss[2,1,:].real,label='(2,+1)')
    plot(times_kyr,gauss[2,1,:].imag,label='(2,-1)')
    plt.ylabel('glm')
    plt.xlabel('kyr')
    plt.legend()
    plt.subplot(222)
    lm_vals=[[1,0],[1,1],[2,0],[2,1],[2,2],[3,0],[3,1],[3,2],[3,3]]
    nlm=len(lm_vals)
    barwidth=0.35
    xlabels=['']
    for i in range(nlm):
        lm=(lm_vals[i][0],lm_vals[i][1])
        plt.bar(i,gauss_ave[lm].real,barwidth,label=str(lm)+'.real')
        plt.bar(i+barwidth,gauss_ave[lm].imag,barwidth,label=str(lm)+'.imag')
        xlabels.append(str(lm))
    plt.xlim((0,nlm-barwidth))
    plt.xlabel('time ave glm')
    plt.xticks(np.arange(nlm)+barwidth*0.5,xlabels[1:])  #xtick at center of bar

    plt.tight_layout()
    if (savefig):
        plt.savefig(savefile+str(nfig)+'.pdf')
        print('Saved '+savefile)
    else:
        plt.show()


    ### Plot spectral contour trace of B^2_l  (mpower)
    # First plot dip_lat timeseries
    nfig+=1
    fig=plt.figure(nfig,figsize=(15,10))
    xlabel='kyr'
    ylabels=['Dipole Co-Latitude [deg]','Degree, l']
    axlt=fig.add_subplot(211)
    axlt.plot(times_kyr,dip_lat,color='black',linewidth=lw)
    axlt.set_xlim(min(times_kyr),max(times_kyr))
    axlt.set_ylabel(ylabels[0])
    axlt.set_xlabel(xlabel)
    # Add shaded area for reversed polarity.
    for i in range(nrev+1):
        #if beginning of this chron at time+1 has rev polarity:
        if dip_lat[np.where(times_dip==pchrons[i,0])[0]+1][0]>90.: 
            plt.axvspan(pchrons[i,0]*kyr_dip_decay,pchrons[i,1]*kyr_dip_decay,color='0.9')
    axrt = axlt.twinx()
    axrt.plot(times_kyr,lowes[1,:],color='green',linewidth=lw)
    axrt.set_xlim(min(times_kyr),max(times_kyr))
    axrt.set_ylabel('Dipole Energy')

    # Use contourf
    axl=fig.add_subplot(212)
    nl=8
    lmin=1
    larr=range(lmin,nl+lmin)
    y,x=np.meshgrid(larr,times_kyr)
    z=mpowers[lmin:nl+lmin,:,0]/mpowers[1,:,0]   #normalize to dipole
    im=axl.contourf(x,y,np.transpose(z),cmap='PuRd')
    axl.set_xlabel(xlabel)
    axl.set_ylabel(ylabels[1])
    axl.set_xlim(min(times_kyr),max(times_kyr))
    label_cb='ME_l/ME_dip'
    cb=fig.colorbar(im,label=label_cb,pad=0.12)
    axr = axl.twinx()
    axr.plot(times_kyr,dip_lat,color='black',linewidth=lw)
    axr.set_xlim(min(times_kyr),max(times_kyr))
    axr.set_ylabel(ylabels[0])


    #plt.tight_layout()
    if (savefig):
        plt.savefig(savefile+str(nfig)+'.pdf')
        print('Saved '+savefile)
    else:
        plt.show()

    ######  Plot elike stats  #######
    nfig+=1
    plt.figure(nfig,figsize=(15,10))
    plt.subplot(221,xlabel='kyr',ylabel='Gauss dipolarity')
    plot(times_kyr,gauss_dipolarity)
    plt.subplot(222,xlabel='kyr',ylabel='AD/NAD')
    plot(times_kyr,adnad)
    plt.subplot(223,xlabel='kyr',ylabel='O/E')
    plot(times_kyr,oddeven)
    plt.subplot(224,xlabel='kyr',ylabel='Z/NZ')
    plot(times_kyr,zonality)

    plt.tight_layout()
    if (savefig):
        plt.savefig(savefile+str(nfig)+'.pdf')
        print('Saved '+savefile)
    else:
        plt.show()

    ### Histogram of DM ###
    nfig+=1
    plt.figure(nfig,figsize=(15,10))
    bins=50.
    hist=plt.hist(lowes[1,:],bins=50,log=True)
    plt.ylabel('N')
    plt.xlabel('DM')
    if (args.save):
        plt.savefig('hist_DM'+'.eps',format='eps')

    ### Histogram of Gauss components ###
    nfig+=1
    plt.figure(nfig,figsize=(15,10))
    bins=50.
    alpha=0.5
    fsl=12.
    plt.subplot(331)
    plt.hist(gauss[1,0,:].real,bins=bins,label='g(1,0)',alpha=alpha)
    plt.legend(fontsize=fsl)
    plt.subplot(332)
    plt.hist(gauss[1,1,:].real,bins=bins,label='g(1,1)',alpha=alpha)
    plt.hist(gauss[1,1,:].imag,bins=bins,label='h(1,1)',alpha=alpha)
    plt.legend(fontsize=fsl)
    plt.subplot(333)
    plt.hist(gauss[2,0,:].real,bins=bins,label='g(2,0)',alpha=alpha)
    plt.legend(fontsize=fsl)
    plt.subplot(334)
    plt.hist(gauss[2,1,:].real,bins=bins,label='g(2,1)',alpha=alpha)
    plt.hist(gauss[2,1,:].imag,bins=bins,label='h(2,1)',alpha=alpha)
    plt.legend(fontsize=fsl)
    plt.subplot(335)
    plt.hist(gauss[2,2,:].real,bins=bins,label='g(2,2)',alpha=alpha)
    plt.hist(gauss[2,2,:].imag,bins=bins,label='h(2,2)',alpha=alpha)
    plt.legend(fontsize=fsl)
    plt.subplot(336)
    plt.hist(gauss[3,0,:].real,bins=bins,label='g(3,0)',alpha=alpha)
    plt.legend(fontsize=fsl)
    plt.subplot(337)
    plt.hist(gauss[3,1,:].real,bins=bins,label='g(3,1)',alpha=alpha)
    plt.hist(gauss[3,1,:].imag,bins=bins,label='h(3,1)',alpha=alpha)
    plt.legend(fontsize=fsl)
    plt.subplot(338)
    plt.hist(gauss[3,2,:].real,bins=bins,label='g(3,2)',alpha=alpha)
    plt.hist(gauss[3,2,:].imag,bins=bins,label='h(3,2)',alpha=alpha)
    plt.legend(fontsize=fsl)
    plt.subplot(339)
    plt.hist(gauss[3,3,:].real,bins=bins,label='g(3,3)',alpha=alpha)
    plt.hist(gauss[3,3,:].imag,bins=bins,label='h(3,3)',alpha=alpha)
    plt.legend(fontsize=fsl)
    

### Print to text file
if args.text:

    # Compute Gauss stats
    nstats=2  #number of statistics to compute.  eg 2 for mean & stdev.
    gauss_rms=np.zeros((lmax+1,lmax+1,nstats),dtype=complex)  #0=mean, 1=stdev
    power_stats=np.zeros((lmax+1,3,nstats))
    mpower_stats=np.zeros((lmax+1,3,nstats))
    imean=0
    istd=1
    brms_gauss=0.
    for l in range(1,lmax+1):
        for m in range(l+1):
            gauss_rms[l,m,imean]=complex(rms_mean(gauss[l,m,:].real),rms_mean(gauss[l,m,:].imag))  #rms mean
            gauss_rms[l,m,istd]=complex(rms_stdev(gauss[l,m,:].real),rms_stdev(gauss[l,m,:].imag))  #stdev
            brms_gauss+=np.sum(np.square([gauss[l,m,:].real,gauss[l,m,:].imag]))  #running sum of squares.
        for j in range(3):  #loop stat type, all m, m=0, m=!0.
            power_stats[l,j,imean]=np.mean(powers[l,:,j])
            power_stats[l,j,istd]=np.std(powers[l,:,j])
            mpower_stats[l,j,imean]=np.mean(mpowers[l,:,j])
            mpower_stats[l,j,istd]=np.std(mpowers[l,:,j])
    brms_gauss=np.sqrt(brms_gauss/ntimes)   #sqrt(sum of squares/N)
    diprms=np.mean(np.asarray([gauss_rms[1,0,imean].real,gauss_rms[1,1,imean].real,gauss_rms[1,1,imean].imag]))  #mean(g10,g11,h11)
    #dipolarity=diprms/brms_gauss
    # Note (1/6/18): should the dipolarity calc above be a func of time, and then take the ave of dipolarity(t)?  That's
    # what we do in plot_gauss_ave.py.


    textfile='gauss.txt'
    f=open('./'+textfile,'w')  #open in pwd
    # Print format:
    # pwd
    # glm,hlm
    f.write(cwd+'\n')
    #f.write('initial,final '+str(initial)+' '+str(final)+'\n')
    f.write('initial,final '+files_ss[0].split('/')[1]+' '+files_ss[-1].split('/')[1]+'\n')
    f.write(('length[kappa,dip,kyr] %f %f %f'%(len_time,len_dip,len_kyr))+'\n')
    f.write(('gauss[brms,diprms,dipolarity,adipolarity] %.2e %.2e %.2e %.2e'\
                 %(brms_gauss,diprms,np.mean(gauss_dipolarity),np.mean(gauss_adipolarity)))+'\n')
    f.write(('AD/NAD,O/E,Z/NZ,chi2 %.2e %.2e %.2e %.2e'\
                 %(elike_means[0],elike_means[1],elike_means[2],elike_means[3]))+'\n')
    f.write(('nrev,nexecursions,n_eq_cross,min_chron_len %i %i %i %.2f'\
                 %(nrev,nexcursions,n_eq_cross,min_chron_len))+'\n')
    for l in range(1,lmax+1):
        for m in range(l+1):
            if m==0: line='g,h(l=%i) %e %e'%(l,gauss_rms[l,m,0].real,gauss_rms[l,m,0].imag)
            else: line=line+' %e %e'%(gauss_rms[l,m,0].real,gauss_rms[l,m,0].imag)
        f.write(line+'\n')
    line='mpowers(l,allm) %e'%(mpower_stats[1,0,imean])
    for l in range(2,lmax+1): line=line+' %e'%(mpower_stats[l,0,imean])
    f.write(line+'\n')
    line='mpowers(l,m=0) %e'%(mpower_stats[1,1,imean])
    for l in range(2,lmax+1): line=line+' %e'%(mpower_stats[l,1,imean])
    f.write(line+'\n')
    line='mpowers(l,m!=0) %e'%(mpower_stats[1,2,imean])
    for l in range(2,lmax+1): line=line+' %e'%(mpower_stats[l,2,imean])
    f.write(line+'\n')
    f.close()    
    print('Closed '+textfile)
