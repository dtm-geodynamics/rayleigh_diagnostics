
import numpy as np
def read_f(path, nr=96):
	"""
	read in functions defined in Rayleigh, e.g. f1, f2.. f14
	the defaul size of radial grid is 96

    the fortran code that generates the eqcoeff_kluo file in Physics/PDE_Coefficients.F90 is

 765         !KL: print the constants and functions
 766 
 767         If (my_rank .eq. 0) Then
 768             Open(unit=119, file='eqcoeff_kluo', form='formatted')
 769 
 770             !do i=1, n_ra_functions
 771             do i=1, 14
 772             do k=1, n_r
 773                !write(119, *) radius(k), (ra_functions(k, i), i=1,14)
 774                write(119, *) radius(k), ra_functions(k, i)
 775             enddo
 776             write(119,*)
 777             write(119,*)
 778             enddo
 779 
 780             Close(119)
 781         Endif

    Note: the resultant array is from outter radius to inner radius
	"""
	#arr=np.zeros((14,)
	#with open(path) as f:
	arr = np.loadtxt(path)
	
	totrows, col = arr.shape

	# check if totrows is a multiple of nr
	if totrows % nr == 0:
		nfunc = totrows/nr
		if nfunc != 14:
			print("There should be 14 functions")
			exit(1)
	else:
		print("file %s is corrupted\n" %(path))
		exit(1)
	
	res = arr.reshape((14, nr, 2))

	#if reverse==True:
	#	res.sort(axis=1)
	#	#res=np.sort(res, axis=1)
	#	print(res)
	return res

### get_k_r()  -- return k(r) from path, where path is from pwd to eqcoeff file
def get_k_r(path='Ra1000/eqcoeff_kluo'):
    loadedarr = read_f(path)
    r = loadedarr[4][:,0]
    k = loadedarr[4][:,1]
    return(r,k)

if __name__ == '__main__':
#	loadedarr = read_f('/lustre/scratch/kluo/critical/convection/Ekman2d-3/Pr1.0_CONST/isotherm/kappa-qmax/Ra13000/eqcoeff_kluo')
    #loadedarr = read_f('Ra1000/eqcoeff_kluo')
    import matplotlib.pyplot as plt
    
    plt.ion()
    #r = loadedarr[4][:,0]
    #k = loadedarr[4][:,1]
    r,k=get_k_r()
    plt.plot(r, k)
    plt.xlabel('r')
    plt.ylabel('k(r)')
    plt.savefig("kappa.pdf",format='pdf')
	#plt.show()
	#print(loadedarr)


