#!/usr/bin/env python

###############################################
#
#  Global-Averages (G_Avgs) plotting
#  Plots average KE vs time
#
#  This example routine makes use of the GlobalAverage
#  data structure associated with the G_Avg output.
#  Upon initializing a GlobalAverage object, the 
#  object will contain the following attributes:
#
#    ----------------------------------
#    self.niter                  : number of time steps
#    self.nq                     : number of diagnostic quantities output
#    self.qv[0:nq-1]             : quantity codes for the diagnostics output
#    self.vals[0:niter-1,0:nq-1] : Globally averaged diagnostics as function of time and quantity index
#    self.iters[0:niter-1]       : The time step numbers stored in this output file
#    self.time[0:niter-1]        : The simulation time corresponding to each time step
#    self.lut                    : Lookup table for the different diagnostics output


#from diagnostic_reading import GlobalAverage
from rayleigh_diagnostics import G_Avgs
from diagnostic_tools import file_list
from diagnostic_plotting import getlabel
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import plot,ion,show
import numpy as np
import argparse
import os 
cwd = os.getcwd()
#ion()
pi=np.pi

q1_default=[401, 402, 403, 404]      #[125,126,127,128]
q2_default=[1101, 1102, 1103, 1104]  #[475,476,477,478]
parser = argparse.ArgumentParser(description="""Plot global averages (G_Avgs): KE vs time.""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False, default=None, 
                    help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False, default=None, 
                    help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-q1', '--quantities1', action='store', dest='quantities1', type=int, nargs='*', default=q1_default, 
                    required=False, help='quantities in panel 1')
parser.add_argument('-q2', '--quantities2', action='store', dest='quantities2', type=int, nargs='*', default=q2_default, 
                    required=False, help='quantities in panel 2')
parser.add_argument('-d', '--default',action='store_true',dest='default',help='use default plotting options',
                    default=False)  #otherwise queue user.
parser.add_argument('-n', '--noplot', action='store_const', dest='noplot', const=True, default=False, 
                    required=False, help='suppress plotting windows')
parser.add_argument('-t', '--text', action='store_const', dest='text', const=True, default=False, 
                    required=False, help='print stats to text file')
args = parser.parse_args()

# Set saveplot to True to save to a .png file. 
# Set to False to view plots interactively on-screen.
saveplot = args.save
savefile = 'global_avgs.pdf'  #Change .pdf to .png if pdf conversion gives issues
window_title = 'global_avgs ('+cwd+')'
files = file_list(args.initial, args.final, path='G_Avgs')
#The indices associated with our various outputs are stored in a lookup table
#as part of the GlobalAverage data structure.  We define several variables to
#hold those indices here:
#a = GlobalAverage(filename=files[0],path='')  # read a file to get the lookup table
a = G_Avgs(filename=files[0],path='')  # read a file to get the lookup table
# Note a only has file[0], should probably look at all file outputs here!

# Get quantities to plot
#if quants are default and -d not set, then ask user.
if args.quantities1==q1_default and args.default is False:
    print(' ')  # blank line
    print('Quantities:'+str(a.nq)+' Codes:'+str(a.qv))+' Labels:'+str(getlabel(a.qv))
    # Ask user to choose  #will take in a string of numbers separated by a space
    user_codes1_in = raw_input('Choose up to 4 codes for first panel (eg 1 2 3):').split(' ')   
    q1 = [int(num) for num in user_codes1_in]   #make int array.
else: q1=args.quantities1  
if args.quantities2==q2_default and args.default is False:
    print(' ')  # blank line
    print('Quantities:'+str(a.nq)+' Codes:'+str(a.qv))+' Labels:'+str(getlabel(a.qv))
    user_codes2_in = raw_input('Choose up to 4 codes for second panel (eg 1 2 3):').split(' ')  
    q2 = [int(num) for num in user_codes2_in]   #make int array.
else: q2=args.quantities2

print('Panel 1: labels quantities:'+str(getlabel(q1))+str(q1))
print('Panel 2: labels quantities:'+str(getlabel(q2))+str(q2))

# We define an empty list to hold data and time.  
data = {}  #dictionary
time = {}
# Next, we loop over all files and grab the desired data
for f in files:
    #a = GlobalAverage(filename=f,path='')
    a = G_Avgs(filename=f,path='')
    for iq in a.qv:
        if iq not in data:
            data[iq]=[]
            time[iq]=[]
    for i,d in enumerate(a.time):
        for iq in a.qv:
            data[iq].append(a.vals[i,a.lut[iq]])
            time[iq].append(d)

#Define times.
time1=time[q1[0]]  #array of times for q1[0] quantity.
time1=np.asarray(time1)  #make it an np array
ntimes=len(time1)
tra=np.max(time1)-np.min(time1)  #time range
i_trim=0.  #default, compute stats from t=0.
if tra>5. :
    i_trim=np.min(np.where(time1-time1[0] > 5.))  #cut out data before i_trim
    ntime_stat=ntimes-i_trim  #ntimes to use for statistics
ind_stat=np.arange(i_trim,ntimes,dtype=np.uint64)
print('Length of run: '+str(tra))

# Times
### Input parameters
eps_r=0.35  #r_i/r_o
r_o=1+1/(1/eps_r-1.)  #dimless outer boundary radius.
Pm=10.  #need to read this in from main_input?
time_dip=time1/(Pm*(r_o/pi)**2)  #[dipole decay times]
time_dip_yr=50e3                #[yr/dip_decay]
time_yr=time_dip*time_dip_yr       #[yr]
len_time=max(time1)-min(time1)    #[tau_kappa]
len_dip=max(time_dip)-min(time_dip)  #[tau_dip]
len_yr=max(time_yr)-min(time_yr)     #[yr]

# Labels
labels=getlabel(list(data.keys()))

#Print stats.
#print(" ")
if 125 in data:  # if KE available
    ke=data[125]
    print("KE=%8.2e +/- %8.2e  for t=[%4.2f,%4.2f]" %(np.mean(ke),np.std(ke),time1[ind_stat[0]],time1[max(ind_stat)]))
if 475 in data:  # if ME available
    me=data[475]
    print("ME=%8.2e +/- %8.2e  for t=[%4.2f,%4.2f]" %(np.mean(me),np.std(me),time1[ind_stat[0]],time1[max(ind_stat)]))

if not args.noplot:  #if not "-n" set.
    # Plot two panels
    # Figure 1
    if (saveplot):
        plt.figure(1,figsize=(7.5, 4.0), dpi=300)
        plt.rcParams.update({'font.size': 12})
    else:
        plt.figure(1,figsize=(15,5),dpi=100)
        plt.rcParams.update({'font.size': 14})
    plt.figure(1).canvas.set_window_title(window_title)
    # Plot Kinetic Energy.
    plt.subplot(121)
    #plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    q1_found=0
    ylog=1
    for iq in q1:
        if iq in data:
            plot(time[iq],data[iq],label=getlabel(iq))
            q1_found=1
            if min(data[iq])<0: ylog=0
    if q1_found:
        if ylog: plt.yscale('log')
        plt.xlabel('Time')  # (days)')
        plt.ylabel(getlabel(q1[0]))  #default ylabel as first element in q1
        #    plt.title(cwd,fontsize=10)
        if (saveplot):
            legend = plt.legend(shadow=True, ncol = 2, fontsize = 'x-small') 
        else:
            legend = plt.legend(shadow=True, ncol = 2)
    else:
        print('No '+str(getlabel(q1))+' found.')
    
    # Plot Magnetic Energy.    
    plt.subplot(122)
    t1='ME'
    q2_found=0
    ylog=1
    for iq in q2:
        if iq in data:
            plot(time[iq],data[iq],label=getlabel(iq))
            q2_found=1
            if min(data[iq])<0: ylog=0
    if q2_found:
        if ylog: plt.yscale('log')
        plt.xlabel('Time')
        plt.ylabel(getlabel(q2[0]))  #default ylabel as first element in q2.
        if (saveplot):
            legend = plt.legend(shadow=True, ncol = 2, fontsize = 'x-small') 
        else:
            legend = plt.legend(shadow=True, ncol = 2) 
        plt.tight_layout()
    else:
        print('No '+str(getlabel(q2))+' found.')
    
    # If any data found show it.
    if q1_found or q2_found:
        if (saveplot):
            plt.savefig(savefile)
        else:
            plt.show()
else:
    print("suppressed plots.")

### Print stats to text file
if args.text:
    textfile='global_avgs.txt'
    f=open('./'+textfile,'w')  #open in pwd
    f.write(cwd+'\n')
    f.write('initial,final '+str(args.initial)+' '+str(args.final)+'\n')
    f.write('length[kappa,dip,kyr] %f %f %f'%(len_time,len_dip,len_yr*1e-3)+'\n')
    if 125 in data:  # if KE available
        ke=data[125]
        f.write("KE %8.2e %8.2e" %(np.mean(ke),np.std(ke))+'\n')
    if 475 in data:  # if ME available
        me=data[475]
        f.write("ME %8.2e %8.2e" %(np.mean(me),np.std(me))+'\n')
    f.close()
    print('Closed '+textfile)
