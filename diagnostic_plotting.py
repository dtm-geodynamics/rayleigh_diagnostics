import numpy as np
import os
import sys
import rayleigh_codes

### merge 2 objects. ###
def merge(A,B):
    """
    an object's __dict__ contains all its 
    attributes, methods, docstrings, etc.
    """
    C=A
    C.__dict__.update(B.__dict__)
    return C

### Look up table labels ###
class lutlabels:

    def __init__(self,codes):
        ncodes=len(codes)
        self.codes = codes
        self.ncodes = ncodes
        self.labels=[' ']*ncodes  # create blank array of strings.
#        for code in codes:
        for i in range(ncodes):
            code=codes[i]
            #print(code)
            #print(getlabel(code))
            self.labels[i]=getlabel(code)
            
### Get Labels given lookup code.  ###
def getlabel(x):
    #labels=labellist()
    labels=rayleigh_codes.index_to_string
    if type(x) is list or type(x) is np.ndarray:
        return [labels[i] for i in x]
    else: return labels[x]


### Get Labels given lookup code.  ###
def momentlabel(x):
    labels=momentlabellist()
    if type(x) is list or type(x) is np.ndarray:
        return [labels[i] for i in x]
    else: return labels[x]

def momentlabellist():
    return {
        0: 'mean',
        1: 'rms^2',
        2: 'skewness',
        3: 'kurtosis'
        }
