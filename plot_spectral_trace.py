#!/usr/bin/env python
########################################################################3
#
#   Plotting Example:  Shell_Spectra
#
#   plot_spectral_trace.py
#   Plot time series (trace) of kinetic and
#   magnetic power spectral elements.
#
#   We plot the velocity power spectrum from one
#   shell spectrum output using the PowerSpectrum
#   class from diagnostic_reading.py.  
#   
#   Ideally, one should loop over several spectra
#   and take a time-average, similar to the approach
#   taken in plot_energy_flux.py.  This routine plots
#   a SINGLE SNAPSHOT of the power.
#
#   When a PowerSpectrum object is initialized, 
#   only power is computed from the Shell_Spectra
#   file and saved.
#  
#   If we want to look at individual m-values,
#   we can use the ShellSpectra class instead.
#   See diagnostic_reading.py for a brief 
#   description of that data structure.
#
#   The power spectrum structure is simpler to
#   work with.  It contains the following attributes:
#    ----------------------------------
#    self.niter                                    : number of time steps
#    self.nr                                       : number of radii at which power spectra are available
#    self.lmax                                     : maximum spherical harmonic degree l
#    self.radius[0:nr-1]                           : radii of the shell slices output
#    self.inds[0:nr-1]                             : radial indices of the shell slices output
#    self.power[0:lmax,0:nr-1,0:niter-1,0:2]       : the velocity power spectrum.  The third
#                                                  : index indicates (0:total,1:m=0, 2:total-m=0 power)
#    self.mpower[0:lmax,0:nr-1,0:niter-1,0:2]      : the magnetic power spectrum
#    self.iters[0:niter-1]                         : The time step numbers stored in this output file
#    self.time[0:niter-1]                          : The simulation time corresponding to each time step
#    self.magnetic                                 : True if mpower exists
#    -------------------------------------
##################################

from rayleigh_diagnostics import Power_Spectrum, Shell_Spectra
from diagnostic_tools import file_list,rms_mean
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.pyplot import plot,ion,show
import numpy as np
from math import acos,pi
import os
import rayleigh_codes
ion()
cwd = os.getcwd()
r2d=180./np.pi  #radians/degree
#ion()
### Check python version
import sys
if sys.version_info[0]==3:
    raw_input=input
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

import argparse
parser = argparse.ArgumentParser( \
                       description="""Plot spectral trace and dipole lat,lon.""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False,
                    default=None,help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False, default=None, 
                    help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-d', '--default',action='store_true',dest='default',
                    help='use default plotting options',default=False)  #otherwise queue user.
args = parser.parse_args()

print("initial="+str(args.initial)+" final="+str(args.final))

#Set savefig to True to save to savefile.  Interactive plot otherwise.
savefig = False
#savefig = True
savefig = args.save
savefile = 'spectral_trace'
if savefig: print('savefig='+str(savefig))
window_title = 'spectral_trace ('+cwd+')'
initial=args.initial
final=args.final
print("Reading files...")
files = file_list(initial, final, 'Shell_Spectra')

nfiles=len(files)
#print('NOTE: Only looking at file '+files[0])
sspec0 = Shell_Spectra(files[0],path=os.path.join(os.path.curdir,""))
dims=[sspec0.lmax,sspec0.nr,sspec0.niter]   #get dims.
sspec = [Power_Spectrum('blank',dims) for i in range(nfiles)]  #define spec as a blank list to get a template object.
sspec[0]=sspec0  #def 1st element.
lmax=sspec0.lmax
print('lmax=%d'%lmax)
nr=sspec0.nr
niter=sspec0.niter
times=sspec0.time

# We use the lookup table to find where br, vtheta, and bphi are stored
#br_index = sspec0.lut[801]
# Get quantities to plot
if not args.default:    #then ask user.
    print('Iterations:'+str(sspec0.niter)+' N Quantities:'+str(sspec0.nq)+' Codes:'+str(sspec0.qv))
    user_codes1_in = raw_input('Choose code for first panel (eg 401):').split(' ')  
    user_codes2_in = raw_input('Choose code for second panel (eg 501):').split(' ') 
    q1 = int(user_codes1_in[0]); q2 = int(user_codes2_in[0])
    print('Radius levels available: '+str(sspec0.radius))
    rad_index = int(input('Choose index of radius level (0 is top) :'))
    print('lmax=%i'%sspec0.lmax)
    lmax_use = int(input('Enter lmax to plot :'))
else:  #use defaults
    q1 = args.quantities1  
    q2 = args.quantities2
    rad_index = 0
    lmax_use = sspec0.lmax
### index of qv's in vals.
iq1 = sspec0.lut[q1]; iq2 = sspec0.lut[q2]
### Define q_vals[l,m,t] arrays
q1_vals = sspec0.vals[:lmax_use,:lmax_use,rad_index,iq1,:]
q2_vals = sspec0.vals[:lmax_use,:lmax_use,rad_index,iq2,:]

### Restore sspec from all files
for i in range(1,nfiles):
    #sspec[i] = Shell_Spectra(files[i],path=os.path.join(os.path.curdir,""))
    sspec_i = Shell_Spectra(files[i],path=os.path.join(os.path.curdir,""))
    times = np.append(times,sspec_i.time)
    q1_vals = np.append(q1_vals,sspec_i.vals[:lmax_use,:lmax_use,rad_index,iq1,:],axis=2)
    q2_vals = np.append(q2_vals,sspec_i.vals[:lmax_use,:lmax_use,rad_index,iq2,:],axis=2)

### PLOTTING ###
plt.figure(1,figsize=(10,10))
plt.figure(1).canvas.set_window_title(window_title)
lw = 1.5
#label0='All m'
#label1='m=0'
#label2='Total - m=0'
for il in range(lmax_use):
    for im in range(il+1):
        #plt.plot(times,sspec0.vals[il,im,rad_index,iq1,:].real,label='(%i,+%i)'%(il,im))
        #if im>0: plt.plot(times,sspec0.vals[il,im,rad_index,iq1,:].imag,label='(%i,-%i)'%(il,im))        
        plt.plot(times,q1_vals[il,im,:].real,label='(%i,+%i)'%(il,im))
        if im>0: plt.plot(times,q1_vals[il,im,:].imag,label='(%i,-%i)'%(il,im))        
plt.ylabel(rayleigh_codes.index_to_tex[q1]+'(l,m)')
plt.xlabel('Time')
legend = plt.legend(loc='lower left', shadow=True, ncol = 4)
if (savefig):
    plt.savefig(savefile+str(q1)+'_trace.pdf')
    print('Saved '+savefile+str(q1)+'_trace.pdf')
   
### RMS Histogram ###
plt.figure(plt.gcf().number+1,figsize=(15,10))
for il in range(1,lmax_use):
    for im in range(il+1):
        #plt.bar([str('%i,+%i'%(il,im))],[rms_mean(sspec0.vals[il,im,rad_index,iq1,:].real)])
        #if im>0: plt.bar([str('%i,-%i'%(il,im))],[rms_mean(sspec0.vals[il,im,rad_index,iq1,:].imag)])
        plt.bar([str('%i,+%i'%(il,im))],[rms_mean(q1_vals[il,im,:].real)])
        if im>0: plt.bar([str('%i,-%i'%(il,im))],[rms_mean(q1_vals[il,im,:].imag)])
plt.ylabel('rms '+rayleigh_codes.index_to_tex[q1]+'(l,m)')
plt.xlabel('(l,m)')
if (savefig):
    plt.savefig(savefile+str(q1)+'_hist.pdf')
    print('Saved '+savefile+str(q1)+'_hist.pdf')

### q2 Figure 
plt.figure(plt.gcf().number+1,figsize=(10,10))
for il in range(lmax_use):
    for im in range(il+1):
        #plt.plot(times,sspec0.vals[il,im,rad_index,iq2,:].real,label='(%i,+%i)'%(il,im))
        #if im>0: plt.plot(times,sspec0.vals[il,im,rad_index,iq2,:].imag,label='(%i,-%i)'%(il,im))        
        plt.plot(times,q2_vals[il,im,:].real,label='(%i,+%i)'%(il,im))
        if im>0: plt.plot(times,q2_vals[il,im,:].imag,label='(%i,-%i)'%(il,im))        
plt.ylabel(rayleigh_codes.index_to_tex[q2]+'(l,m)')
plt.xlabel('Time')
legend = plt.legend(loc='lower left', shadow=True, ncol = 4)
if (savefig):
    plt.savefig(savefile+str(q2)+'.pdf')
    print('Saved '+savefile+str(q2))
### RMS Histogram ###
plt.figure(plt.gcf().number+1,figsize=(15,10))
###NOte: plot (0,0) for everything except radial variables.
for il in range(1,lmax_use):
    for im in range(il+1):
        #plt.bar([str('%i,+%i'%(il,im))],[rms_mean(sspec0.vals[il,im,rad_index,iq2,:].real)])
        #if im>0: plt.bar([str('%i,-%i'%(il,im))],[rms_mean(sspec0.vals[il,im,rad_index,iq2,:].imag)])
        plt.bar([str('%i,+%i'%(il,im))],[rms_mean(q2_vals[il,im,:].real)])
        if im>0: plt.bar([str('%i,-%i'%(il,im))],[rms_mean(q2_vals[il,im,:].imag)])
plt.ylabel('rms '+rayleigh_codes.index_to_tex[q2]+'(l,m)')
plt.xlabel('(l,m)')
if (savefig):
    plt.savefig(savefile+str(q2)+'_hist.pdf')
    print('Saved '+savefile+str(q2)+'_hist.pdf')

