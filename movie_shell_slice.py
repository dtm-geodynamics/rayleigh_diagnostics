#!/usr/bin/env python

####################################################################################################
#
#  Movie Shell-Slice (Shell_Slices) plotting example
#  - Reads in a single Shell_Slice file.
#  - Plots vr, vphi, and entropy
#
#  This example routine makes use of the ShellSlice
#  data structure associated with the Shell_Slices output.
#  Upon initializing a ShellSlice object, the 
#  object will contain the following attributes:
#    ----------------------------------
#    self.niter                                    : number of time steps
#    self.nq                                       : number of diagnostic quantities output
#    self.nr                                       : number of shell slices output
#    self.ntheta                                   : number of theta points
#    self.nphi                                     : number of phi points
#    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
#    self.radius[0:nr-1]                           : radii of the shell slices output
#    self.inds[0:nr-1]                             : radial indices of the shell slices output
#    self.costheta[0:ntheta-1]                     : cos(theta grid)
#    self.sintheta[0:ntheta-1]                     : sin(theta grid)
#    self.vals[0:nphi-1,0:ntheta-1,0:nr-1,0:nq-1,0:niter-1] 
#                                                  : The shell slices 
#    self.iters[0:niter-1]                         : The time step numbers stored in this output file
#    self.time[0:niter-1]                          : The simulation time corresponding to each time step
#    self.version                                  : The version code for this particular output (internal use)
#    self.lut                                      : Lookup table for the different diagnostics output
#    -------------------------------------

#import matplotlib
#matplotlib.use("Agg")

import pylab as p 
import numpy as np
from rayleigh_diagnostics import Shell_Slices
from diagnostic_tools import file_list
import matplotlib.pyplot as plt
from matplotlib import ticker
from diagnostic_plotting import lutlabels
from diagnostic_plotting import getlabel
import argparse
import matplotlib.animation as animation

parser = argparse.ArgumentParser( \
                       description="""Plot Shell Slice (spherical surface).""")
parser.add_argument('-i', '--initial', action='store', type=str, dest='initial', required=False, default=None, 
                    help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=str, dest='final', required=False, default=None, 
                    help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-d', '--default',action='store_true',dest='default',help='use default plotting options',
                    default=False)
args = parser.parse_args()

if args.default: print("default set")

# Set saveplot to True to save to a .png file. 
# Set to False to view plots interactively on-screen.
saveplot = args.save
savefile = 'shell_slice.png'

rnorm = 1./(1-0.35)
path='Shell_Slices'
files = file_list(args.initial, args.final, path=path)
nfiles=len(files)
### Default files to plot
ifile_start=nfiles-2.   # index of file to read.
ifile_end=nfiles-1.   # index of file to read.
### Read in file indexes to plot.
if not args.default:  # ask user
    #print(' ')  # blank line
    #print('Files found:'+str(files))
    ifile_start,ifile_end=map(int,raw_input('Choose INDEX of files to start,stop out of %i:'%(len(files)-1)).split(','))
nfiles_read=ifile_end-ifile_start
sslices=[]
### Read in shell slice
for i in range(ifile_start,ifile_end+1):
    a = Shell_Slices(filename=files[i],path='')
    sslices.append(a)
#Default Plotting options.
user_codes=801    #Br
rad_inds=0
niter=np.sum([sslices[i].niter for i in range(len(sslices))])  # full niter, sum of all files.
### Read in quantities to plot.
if not args.default:    #ask user.
    #Print Info
    print('Iterations:'+str(niter)+'. Codes='+str(a.qv)+'. Labels='+str(getlabel(a.qv)))
    # Ask user to choose
    user_codes_in = raw_input('Choose 1 code to plot (eg 801):').split(' ')
    user_codes = int(user_codes_in[0])
### Print the label for each code.  for this we need to create a look up table for labels, like p.labels.
q=lutlabels([user_codes])
print('Variable:'+q.labels[0])
### Variables indices for quantity.
var_inds = [a.lut[user_codes]]
### User choose radial levels.
if not args.default:  #ask user.
    print('r/R='+str(sslices[0].radius/rnorm)+' are Levels:'+str(sslices[0].inds))
    user_rad_ind_in = raw_input('Choose INDEX of radial level to plot (eg 0 or 1):').split(' ')
    user_rad_ind = int(user_rad_ind_in[0])
    radius_user = sslices[0].radius[user_rad_ind]
    print('user_rad_ind:',user_rad_ind)
    rad_inds=[int(user_rad_ind)]
    ### Get index of this radius in each file.
    for i in range(1,nfiles_read+1):
        if radius_user in sslices[i].radius:
            rad_inds.append(np.where(sslices[i].radius==radius_user)[0][0])
        else:
            print('ERROR: radius %f not found in slice i=%i'%(radius_user,i))
            exit()
print('rad_inds='+str(rad_inds))
r_str='%.4f'%radius_user
### Tex can be enclosed in dollar signs within a string.  The r in front of the string is necessary for strings enclosing Tex
vnames=q.labels[0]

### Create the plots.  This first portion only handles the projection and colorbars.
f1 = plt.figure(figsize=(7.5*1, 7*1), dpi=900)   #single plot instead of 3x3.
pleft = 0.15
pright = 0.95
ptop = 0.95
cspace = (pright-pleft)#/ncol
ypos = ptop
xpos = 0.45 #pleft+cspace*0.47
fs=20

##########  BEGIN PLOTTING  #####################################
### New: single 1 plot.
sslice_t = sslices[0].vals[:,:,rad_inds[0],var_inds[0],:]  # initial file.
times = sslices[0].time  # initial file.
for i in range(1,nfiles_read+1):
    sslice_t = np.append(sslice_t,sslices[i].vals[:,:,rad_inds[i],var_inds[0],:],axis=2) # all remaining files
    times = np.append(times,sslices[i].time,axis=0) # all remaining files
### Set the projection
ax1 = f1.add_subplot(1,1,1, projection="mollweide")
twosigma = 2*np.std(sslice_t)  #limits are set to +/- twosigma
contour_levels = twosigma*np.linspace(-1,1,256)
if user_codes==501:  ### If entropy, remove mean.
    sslice_t = sslice_t-np.mean(sslice_t)
    print('Removed mean entropy.')

### Function passed to "animation" module.
def animate(i):
    f1.clear()  # clear figure space.
    time = times[i] 
    time_str = 't='+str(time)
    f1.text(pleft,ypos,vnames,fontsize=fs)   #fig title
    f1.text(pleft,ypos-0.05,r_str,fontsize=fs)  #rad level
    f1.text(pleft,ypos-0.10,time,fontsize=fs)   #time

    ax1 = f1.add_subplot(1,1,1, projection="mollweide")
    sslice = sslice_t[:,:,i].reshape(a.nphi,a.ntheta)       # snapshot
    sslice = np.transpose(sslice)
    ### Plot slice.
    image1 = ax1.imshow(sslice,vmin=-twosigma, vmax=twosigma,
                    extent=(-np.pi,np.pi,-np.pi/2,np.pi/2), clip_on=False,
                    aspect=0.5, interpolation='bicubic')
    image1.axes.get_xaxis().set_visible(False)  # Remove the longitude and latitude axis labels
    image1.axes.get_yaxis().set_visible(False)
    image1.set_cmap('RdYlBu_r')  # Red/Blue map
    cbar = f1.colorbar(image1,orientation='horizontal', shrink=0.5)
    tick_locator = ticker.MaxNLocator(nbins=5)
    cbar.locator = tick_locator
    cbar.update_ticks()
    return ax1  

# Make Movie.
print('Making movie...')
movie_name=vnames+'_'+str('r%.2f'%radius_user)+'_'+str('t%.3f'%max(times))+'_'+str('n%i'%niter)+'.mp4'
anim = animation.FuncAnimation(f1, animate, frames=niter, repeat=False,interval=100)
anim.save(movie_name, writer=animation.FFMpegWriter())
print('Saved '+movie_name)

