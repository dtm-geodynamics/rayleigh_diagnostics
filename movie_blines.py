#!/usr/bin/env python

### movie_blines.py 10/22/18 PED ###
### Make movie of plot_blines.py: magnetic field lines from shell_spectra at cmb ###
### Code developed from plot_shell_spec.py and contour_bfield.py

from rayleigh_diagnostics import Power_Spectrum, Shell_Spectra
from diagnostic_tools import file_list, calculate_Ylms,renormalize_shell_spectra,gauss_coef,\
     dipole_latitude,dipole_longitude,polarity_chrons,elike_stats,rms_mean,rms_stdev,rms_sum,\
     plm_schmidt
from plot_blines import *
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.pyplot import plot,ion,show
from matplotlib.patches import Circle
from matplotlib import cm, rc, rcParams
import numpy as np
from scipy.integrate import ode as ode
from math import acos
from itertools import product
from sys import stdout
import argparse
import os
import subprocess
pi=np.pi
cwd = os.getcwd()
r2d=180./np.pi  #radians/degree
#plt.ion()
parser = argparse.ArgumentParser(description="""Create magnetic field streamlines""")
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=True, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-mn', '--moviename', action='store', type=str, dest='moviename', required=False,
                    default=False, help='name of movie to save')
parser.add_argument('-dir', '--dirname', action='store', type=str, dest='dirname', required=False,
                    default='blines', help='name of directory to save images')
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False,
                    default=None,help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False,
                    default=None,help='final index to plot from')
parser.add_argument('-d', '--default', action='store_const', dest='default', const=True, default=False, 
                    required=False, help='default options')
parser.add_argument('-lmax', '--lmax', action='store', type=int, dest='lmax', required=False,
                    default=8,help='lmax to plot')
parser.add_argument('-clim0', '--clim0', action='store', type=float, dest='clim0', required=False,
                    default=-2,help='minimum contour limit')
parser.add_argument('-clim1', '--clim1', action='store', type=float, dest='clim1', required=False,
                    default=+2,help='maximum contour limit')
args = parser.parse_args()

### Rayleigh version
br_lut = 801
#if args.version==1: br_lut=401

### Input parameters
eps_r=0.35  #r_i/r_o
r_o=1+1/(1/eps_r-1.)  #dimless outer boundary radius.
Pm=10.  #need to read this in from main_input?

### Generate frame with plot_blines.py

#exit()

### Shell_Spectra
files_ss = file_list(args.initial, args.final, path='Shell_Spectra')
#print(files_ss)
nfiles = len(files_ss)
lmax = args.lmax
asg = 1.   #radius of Gauss sources (ie E surface or CMB)
#ar = 1.
ar = 3481./6371   #radius to render above
clim0 = args.clim0; clim1 = args.clim1
nlines = 40
num = 25  #potential grid number
dirname = args.dirname
if not os.path.isdir(dirname): os.mkdir(dirname) # Create dir to save figs if it doesn't exist.
iframe = 0

### Loop all files.
print('Calculating field lines...')
for ifile,file in enumerate(files_ss):
    time,gauss,iters = read_gauss(file,lmax)
    ntimes=len(time)
    ### Loop all iters, save png.
    for itime in range(ntimes):
        stdout.write('\r file %d/%d iter %d/%d' %(ifile,nfiles-1,itime,ntimes-1)) 
        stdout.flush()
        components = components_list(gauss[:,:,itime],asg)
        xs,ys,zs = blines_meridional(components,nlines=nlines,ar=ar)
        xxs,zzs,vvs = meridional_potential(components,num=num,ar=ar)
        title = '%.2f kyr'%(convert_to_kyr(time[itime])[0])
        #render_blines(components,xs,zs,xxs,zzs,vvs,ar=ar,asg=asg,clim0=clim0,clim1=clim1,title=title)
        render_blines(components,xs,zs,ar=ar,asg=asg,title=title)
        render_potential(xxs,zzs,vvs,clim0=clim0,clim1=clim1)
        #savename = str(iters[itime])+'.png'
        savename = '%02d.png'%iframe
        save_blines_fig(dirname+'/'+savename)
        plt.close()
        iframe += 1
### Make movie from pngs.
if args.moviename: moviename = dirname+'/'+args.moviename
if not args.moviename: moviename = dirname+'/lmax%d_n%d.mp4'%(lmax,iframe)
infiles = dirname+'/%02d.png'
fps = '10.0'
subprocess.call(['ffmpeg','-y','-framerate',fps,'-i',infiles,'-vcodec','mpeg4','-loglevel','panic',moviename])
# cmd line: ffmpeg -y -r 10.0 -i blines/%02d.png -vcodec mpeg4 -loglevel panic blines/lmax8_n.mp4
print('\n Saved '+moviename)
