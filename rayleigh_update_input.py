#!/usr/bin/python

import re
import rayleigh_codes as rc
import rayleigh_legacy_codes as rlc
import os

current_version = 1                   # should be incremented when this script is update
updated_comment = '! updated by rayleigh_update_input.py, version '

this_version = 0                      # assumed default if not found in file

class LegacyCodeUnknown(Exception):
  def __init__(self, code):
    self.code = code
    self.message = "Unknown legacy code %d"%(code,)

class LegacyStringUnknown(Exception):
  def __init__(self, string, code):
    self.string = string
    self.code = code
    self.message = "Unknown legacy code name %s (%d)"%(string, code,)

def scan_update_version(lines):
  """Scan (and update) any comments about update versions."""
  global this_version
  for line in lines:
    recomment = re.search('^'+updated_comment, line, re.IGNORECASE)
    if recomment:
      this_version = int(line[recomment.end():].split()[0].strip())
      line = updated_comment+`current_version`+os.linesep
  if this_version == 0: lines.insert(0, updated_comment+`current_version`+os.linesep)
  return lines

def update_legacy_codes(lines):
  """Given a line of an input file return that line with the legacy codes updated."""
  global this_version
  valid_version = 1
  if this_version >= valid_version: return lines
  for l, line in enumerate(lines):
    if re.search('^\w+_values', line, re.IGNORECASE):
      old_codes = line.split('!')[0].split('=')[1].rstrip(os.linesep)
      old_codes_l = old_codes.split(',')
      new_codes_l = []
      for oc in old_codes_l:
        try:
          new_code = old_to_new_code(int(oc.strip()))
          new_codes_l.append(oc.split(oc.strip())[0]+`new_code`+oc.split(oc.strip())[1])
        except LegacyStringUnknown as e:
          print "WARNING: dropping unknown legacy code name %s (%d)"%(e.string, e.code,)
        except LegacyCodeUnknown as e:
          print "WARNING: dropping unknown legacy code %d"%(e.code,)
      if len(new_codes_l) > 0:
        new_codes = ','.join(new_codes_l)
        lines[l] = line.replace(old_codes, new_codes)
      else:
        lines.pop(l)
  return lines

def old_to_new_code(old_code):
  """Given a legacy quantity code return the new version's equivalent quantity code or raise an exception if not available."""
  old_string = rlc.index_to_string.get(old_code, None)
  if old_string is not None:
    new_code = rc.string_to_index.get(old_string, None)
    if new_code is not None:
      return new_code
    else:
      raise LegacyStringUnknown(old_string, old_code)
  else:
    raise LegacyCodeUnknown(old_code)

if __name__ == "__main__":
  import glob
  import argparse
  import shutil

  # parse options:
  parser = argparse.ArgumentParser( \
                         description="""Update a Rayleigh input file.""")
  parser.add_argument(action='store', type=str, dest='filename', nargs='+',
                      help='name of file to update - normally main_input')
  args = parser.parse_args()


  filenames = []                     # parse input filenames into filenames
  for ifilename in args.filename: filenames += glob.glob(ifilename)

  for filename in filenames:
    f = open(filename, 'r')
    lines = f.readlines()
    lines = scan_update_version(lines)
    lines = update_legacy_codes(lines)
    f.close()
    if this_version < current_version:
      bakfilename = filename+".bak"
      while os.path.exists(bakfilename): bakfilename += ".bak"
      shutil.move(filename, bakfilename)
      of = open(filename, 'w')
      of.writelines(lines)
      of.close()

