#!/usr/bin/env python

###############################################
#
#  Shell-Averages (Shell_Avgs) plotting example
#  Reads in time steps 3 million through 3.3 million
#  Plots average KE and v' vs. radius
#
#  This example routine makes use of the ShellAverage
#  data structure associated with the Shell_Avg output.
#  Upon initializing a ShellAverage object, the 
#  object will contain the following attributes:
#
#    ----------------------------------
#    self.niter                         : number of time steps
#    self.nq                            : number of diagnostic quantities output
#    self.nr                            : number of radial points
#    self.qv[0:nq-1]                    : quantity codes for the diagnostics output
#    self.radius[0:nr-1]                : radial grid
#
#    self.vals[0:nr-1,0:3,0:nq-1,0:niter-1] : The spherically averaged diagnostics
#                                             0-3 refers to moments (index 0 is mean, index 3 is kurtosis)    
#    self.iters[0:niter-1]              : The time step numbers stored in this output file
#    self.time[0:niter-1]               : The simulation time corresponding to each time step
#    self.version                       : The version code for this particular output (internal use)
#    self.lut                           : Lookup table for the different diagnostics output
#   -------------------------------------

from diagnostic_reading import ShellAverage
from diagnostic_plotting import getlabel
from diagnostic_tools import file_list
import matplotlib.pyplot as plt
from matplotlib.pyplot import show,ion,plot
import numpy as np
import os 
cwd = os.getcwd()
#ion()

# Set saveplot to True to save to a file. 
# Set to False to view plots interactively on-screen.
saveplot = False 
savefile = 'energy_distro.pdf'  #If pdf gives issues, try .png instead.
window_title='energy_distro'

#Time-average the data
# Note that this averaging scheme assumes taht the radial grid didn't
# change during the course of the run. 
files = file_list(path='Shell_Avgs')
nfiles=len(files)
icount = 0
for i,f in enumerate(files):
    a = ShellAverage(f,path='')
    if (i == 0):
        data = np.zeros((a.nr,4,a.nq),dtype='float64')
        alldata = np.zeros((nfiles*a.niter,a.nr,4,a.nq),dtype='float64')  #[iteration,r,value,quantity]  initialize this.
        times = np.zeros(nfiles*a.niter)
    for j in range(a.niter):
        data[:,:,:] = data[:,:,:]+a.vals[:,:,:,j]
        alldata[icount,:,:,:] = a.vals[:,:,:,j]
        times[icount]=a.time[j]
        icount = icount+1
ntimes=icount  #number of time steps outputted.
data = data/icount  # The average is now stored in data
radius = a.radius
rnorm = radius/radius[0] # normalize radius so that upper boundary is r=1


#For plot 1, we plot the three components of the kinetic energy as a function of radius
#rke_index = a.lut[126]  # KE associated with radial motion
#tke_index = a.lut[127]  # KE associated with theta motion
#pke_index = a.lut[128]  # KE associated with azimuthal motion
#rke = data[:,0,rke_index] # Grab appropriate slices of the data array
#tke = data[:,0,tke_index]
#pke = data[:,0,pke_index]

#For plot 2, we plot the square root of the variance of each velocity component.
#In this case, the mean used in the variance is computed on spherical shells.
# Note that since we are grabbing the 2nd moment of the distribution, we
# grab index 1, not 0.  Skewness is stored in index 2, and Kurtosis in 3.

vr_index = a.lut[1]  # V_r
vt_index = a.lut[2]  # V_theta
vp_index = a.lut[3]  # V_phi
q_moment=1  #moment of quantity to plot.
user_moment_in = input('Choose moment to plot (0=mean,1=rms):')
q_moment=user_moment_in
vr = data[:,q_moment,vr_index]  #*0.01 # Grab appropriate slices of the data array
vt = data[:,q_moment,vt_index]  #*0.01 # convert to m/s
vp = data[:,q_moment,vp_index]  #*0.01  #PD: commented out *0.01 scaling.
vr_t = alldata[:,:,q_moment,vr_index]  #*0.01 # Grab appropriate slices of the data array
vt_t = alldata[:,:,q_moment,vt_index]  #*0.01 # convert to m/s
vp_t = alldata[:,:,q_moment,vp_index]  #*0.01  #PD: commented out *0.01 scaling.

#For Plot 3, get entropy.
ent_index=a.lut[64]
ent=data[:,0,ent_index]

# Linewidth
lw = 1.5

if (saveplot):
    plt.figure(1,figsize=(7.5, 4.0), dpi=300)
    plt.rcParams.update({'font.size': 12})
else:
    plt.figure(1,figsize=(15,5),dpi=100)
    plt.rcParams.update({'font.size': 14})

plt.figure(1).canvas.set_window_title(window_title)
xlabel='Radius '+r'(r/r$_{o}$)'

# Choose quantities to plot
qua1=vr
q1_t=vr_t
label1=getlabel(1)
qua2=vt
q2_t=vt_t
label2=getlabel(2)
qua3=vp
q3_t=vp_t
label3=getlabel(3)

#Plot 1: Energy density
plt.subplot(121)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.plot(rnorm,qua1,label = label1,linewidth=lw)
plt.plot(rnorm,qua2,label = label2,linewidth=lw)
plt.plot(rnorm,qua3,label = label3,linewidth=lw)
plt.plot(rnorm,q1_t[0,:],label = label1+'(t='+str(times[0])+')',linewidth=lw)  #initial profiles
plt.plot(rnorm,q2_t[0,:],label = label2+'(t='+str(times[0])+')',linewidth=lw)
plt.plot(rnorm,q3_t[0,:],label = label3+'(t='+str(times[0])+')',linewidth=lw)
plt.xlabel(xlabel)
legend = plt.legend(loc='upper left', shadow=True, ncol = 1) 
plt.title(cwd,fontsize=10)  #title=pwd

#Plot 2: velocity stdev.
#plt.subplot(222)
#plt.plot(rnorm,vr,label = "v'"+r'$_{r}$',linewidth=lw)
#plt.plot(rnorm,vt,label = "v'"+r'$_\theta$',linewidth=lw)
#plt.plot(rnorm,vp,label = "v'"+r'$_\phi$',linewidth=lw)
#plt.xlim([0.75,1])
#plt.ylabel('Speed '+r'( m s$^{-1}$)')
#plt.xlabel(xlabel)
#legend = plt.legend(loc='upper left', shadow=True, ncol = 1)

#Plot 3: Entropy
plt.subplot(122)
plt.plot(rnorm,ent,label='ave',linewidth=lw)
plt.ylabel('Entropy')
plt.xlabel(xlabel)
#ind=np.linspace(0,ntimes-1,num=5,dtype='int16')
#ind=np.linspace(0,ntimes-1,num=5)
ind=np.arange(0,ntimes-1,(ntimes-1)/5)
for i in ind: plot(rnorm,alldata[i,:,0,ent_index],label=str(times[i]))  #profile at i
plt.legend()

plt.tight_layout()

if (saveplot):
    plt.savefig(savefile)
else:
    plt.show()


