#Must have rayleigh_vapor class
#Useful to have gen_3d_filelist helper function
from rayleigh_diagnostics import rayleigh_vapor , gen_3d_filelist

#rundir="/home/feathern/vis/demo2"         # Not necessary, but useful
rundir="/Users/pdriscoll/RESEARCH/rayleigh/visualization/3D"
data_dir=rundir+'/Spherical_3D'           # Where the Rayleigh 3-D output is located
                                          # This is required -- default is './Spherical_3D'
gf = rundir+'/Spherical_3D/00278100_grid' # Grid file

vaporfile = rundir+'/ra_vis_demo'         # dataset name (omit the .vdc or .vdf) 
cube_size_N=256                           # interpolate on to NxNxN uniform Cartesian grid

#rroot='/home/feathern/devel/forks/Nick/Rayleigh/bin'  # Rayleigh repo bin location
rroot='/Users/pdriscoll/RESEARCH/rayleigh/github/Rayleigh/bin'

#tempdir='/home/feathern/y_r_u_deleting_me' # for temporary IO (default is '.')
#tempdir=rroot+'/y_r_u_deleting_me' # for temporary IO (default is '.')
tempdir='.'

# Next, we need to specify where Vapor is installed
# Specifically, we want the Vapor "bin" directory.
# Looking inside the bin directory, you will files like raw2vdf{vdc} and vdf{vdc}create
vapor_version=3
if (vapor_version == 3):
    #vbin='/custom/software/VAPOR3-3.2.0-Linux/bin'   # base directory for Vapor
    vbin='/Applications/vapor.app/Contents/MacOS/'
    # FOR MAC, it will be something like: vroot=`/Applications/vapor.app/Contents/MacOS`
else:
    vbin='/custom/software/vapor-2.6.0.RC0/bin'
    vaporfile=vaporfile+'_v2'   # not necessary, but if you're trying Vapor 2 and 3 with same data, it's useful.


diter = 20          # time-step difference between outputs
first_iter = 278020 # First time-step number to process
last_iter  = 278100 # Last time-step number to process

#First, generate a list of files to use for scalar variables
var_codes = [501, 3]          # quantity codes for temperature and vphi (from Rayleigh)
var_names = ['temp','vphi']    # associated names (user choice)
files=gen_3d_filelist(var_codes,diter,first_iter,last_iter,directory=data_dir)
print(files)


remove_spherical_means=[True , False]  # Adjust temperature, but leave vphi unchanged


rmaxes=[None, None]

rmins=[None, None]

#Same, but now a list of quantity codes for
# the r,theta,phi components of any vector we want to convert to a Cartesian vector
vqcodes = [1,2,3]        # velocity (r,theta,phi)
wqcodes = [301,302,303]  # vorticity (r,theta,phi)

vfiles=gen_3d_filelist(vqcodes,diter,first_iter,last_iter,directory=data_dir)
wfiles=gen_3d_filelist(wqcodes,diter,first_iter,last_iter,directory=data_dir)
vnames = ['vx', 'vy', 'vz']         # velocity
wnames = ['wx', 'wy', 'wz', 'wmag'] # vorticity, including magnitude 
                                    # the magnitude (optional) is calculated automatically
                                    # useful for coloring vortex lines

vector_names = [vnames, wnames]     # 2-D nested list
vector_files = [vfiles, wfiles]     # 3-D nested list (each 2-D sublist is structured as for scalars above)
print(vector_files)

#Same, but now a list of quantity codes for
# the r,theta,phi components of any vector we want to convert to a Cartesian vector
vqcodes = [1,2,3]        # velocity (r,theta,phi)
wqcodes = [301,302,303]  # vorticity (r,theta,phi)

vfiles=gen_3d_filelist(vqcodes,diter,first_iter,last_iter,directory=data_dir)
wfiles=gen_3d_filelist(wqcodes,diter,first_iter,last_iter,directory=data_dir)
vnames = ['vx', 'vy', 'vz']         # velocity
wnames = ['wx', 'wy', 'wz', 'wmag'] # vorticity, including magnitude 
                                    # the magnitude (optional) is calculated automatically
                                    # useful for coloring vortex lines

vector_names = [vnames, wnames]     # 2-D nested list
vector_files = [vfiles, wfiles]     # 3-D nested list (each 2-D sublist is structured as for scalars above)
print(vector_files)

test = rayleigh_vapor(name=vaporfile,varnames=var_names,varfiles=files,vapor_bin=vbin,
                       nxyz=cube_size_N, grid_file=gf, rayleigh_root=rroot, force=True,
                       remove_spherical_means=remove_spherical_means, rmaxes=rmaxes, rmins=rmins, 
                       vapor_version=vapor_version,
                       vector_names=vector_names, vector_files=vector_files, tempdir=tempdir)
test.create_dataset(force=True)

test.populate_dataset()
