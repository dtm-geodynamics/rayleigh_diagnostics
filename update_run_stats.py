#!/usr/bin/env python
#!/usr/bin/env python
### update_run_stats.py 11/29/17 PED ###
# Purpose: update (or create) a file   #
# 'run_stats.txt' in a rayleigh run    #
# dir that contains a summary table of #
# statistics.                          #
########################################

def update_run_stats(dir='.',args=None):
    import numpy as np
    import matplotlib.pyplot as plt
    from diagnostic_tools import file_list,renormalize_shell_spectra,gauss_coef,dipole_latitude,dipole_longitude,\
        polarity_chrons,elike_stats,rms_mean,rms_stdev
    from diagnostic_reading import ShellSpectra, PowerSpectrum
    import os
    import sys
    cwd = os.getcwd()
    pi=np.pi
    
    print("Reading files...")
    initial=None
    final=None
    if args is not None:
        initial=args.initial
        final=args.final
    print(initial,final)
    ### Input parameters
    eps_r=0.35  #r_i/r_o
    r_o=1+1/(1/eps_r-1.)  #dimless outer boundary radius.
    Pm=10.  #need to read this in from main_input?

    ### Shell_Spectra
    files_ss = file_list(initial, final, path=dir+'/Shell_Spectra/')
    #files_ss = file_list(initial, final, 'Shell_Spectra')
    #spec0 = PowerSpectrum(files_ss[0],magnetic=True)
    
    nfiles_ss=len(files_ss)
    if nfiles_ss==0: print('No files found!')
    lmax=8  #number of gauss coef to compute stats for.
    gauss=np.zeros((lmax+1,lmax+1,1),dtype=complex)
    powers=np.zeros((lmax+1,1,3))  #powers(nl,ntimes,3). (0:total,1:m=0, 2:total-m=0 power)
    mpowers=np.zeros((lmax+1,1,3))  #powers(nl,ntimes,3).
    time=np.zeros(1)
    dip_lat=np.zeros(1)
    for ifile,file in enumerate(files_ss):
        sys.stdout.write('file '+str(ifile)+'/'+str(nfiles_ss)+' \r')
        sys.stdout.flush()
        a=ShellSpectra(file,path='')   #note path is already in filename 'file'.
        spec_i = PowerSpectrum(files_ss[ifile],magnetic=True)

        br_index=a.lut[401]
        # Convert Brlm to orthonormal
        brlm_on=renormalize_shell_spectra(a.vals[:lmax+1,:lmax+1,0,br_index,:])
        # Compute Gauss
        gauss_i=gauss_coef(brlm_on)
        i_start=0
        if ifile==0:
            gauss[:,:,0]=gauss_i[:,:,0]  #initial time.
            time[0]=a.time[0]
            powers[:lmax+1,0,:]=spec_i.power[:lmax+1,0,0,:]
            mpowers[:lmax+1,0,:]=spec_i.mpower[:lmax+1,0,0,:]
            i_start=1  #skip this index in next loop only for ifile=0
        gauss=np.append(gauss,gauss_i[:,:,i_start:],axis=2)    #append along time axis
        powers=np.append(powers,spec_i.power[:lmax+1,0,i_start:,:],axis=1)
        mpowers=np.append(mpowers,spec_i.mpower[:lmax+1,0,i_start:,:],axis=1)
        time=np.append(time,a.time[i_start:])
    dip_lat=dipole_latitude(gauss[1,0,:].real,gauss[1,1,:].real,gauss[1,1,:].imag)*180./pi  #[deg]
    # Compute Gauss stats
    nstats=2  #number of statistics to compute.  eg 2 for mean & stdev.
    gauss_stats=np.zeros((lmax+1,lmax+1,nstats),dtype=complex)  #0=mean, 1=stdev
    power_stats=np.zeros((lmax+1,3,nstats))
    mpower_stats=np.zeros((lmax+1,3,nstats))
    imean=0
    istd=1
    for l in range(1,lmax+1):
        for m in range(l+1):
            gauss_stats[l,m,imean]=complex(rms_mean(gauss[l,m,:].real),rms_mean(gauss[l,m,:].imag))  #rms mean
            gauss_stats[l,m,istd]=complex(rms_stdev(gauss[l,m,:].real),rms_stdev(gauss[l,m,:].imag))  #stdev
        for j in range(3):  #loop stat type, all m, m=0, m=!0.
            power_stats[l,j,imean]=np.mean(powers[l,:,j])
            power_stats[l,j,istd]=np.std(powers[l,:,j])
            mpower_stats[l,j,imean]=np.mean(mpowers[l,:,j])
            mpower_stats[l,j,istd]=np.std(mpowers[l,:,j])
    brms_gauss=np.sqrt(np.sum(np.square([gauss_stats[:,:,imean].real,gauss_stats[:,:,imean].imag])))  #brms from gauss.
    diprms=np.sqrt(np.sum(np.square([gauss_stats[1,:,imean].real,gauss_stats[1,:,imean].imag])))  #=sqrt(g10**2+g11**2+h11**2)
    dipolarity=diprms/brms_gauss
    # Note (1/6/18): should the dipolarity calc above be a func of time, and then take the ave of dipolarity(t)?  That's
    # what we do in plot_gauss_ave.py.
    ### Elike stats
    adnad,oddeven,zonality,elike_means=elike_stats(gauss)
    # Times
    time_dip=time/(Pm*(r_o/pi)**2)  #[dipole decay times]
    time_dip_yr=50e3                #[yr/dip_decay]
    time_yr=time_dip*time_dip_yr       #[yr]
    len_time=max(time)-min(time)    #[tau_kappa]
    len_dip=max(time_dip)-min(time_dip)  #[tau_dip]
    len_yr=max(time_yr)-min(time_yr)     #[yr]
    # Reversal stats
    nrev,nexcursions,n_eq_cross,pchrons=polarity_chrons(time_dip,dip_lat)
    
    # Create run_stats.txt
    filename='run_stats.txt'
    f=open(dir+'/'+filename,'w')
    # Print format:
    # pwd
    # glm,hlm
    f.write(cwd+'\n')
    f.write('initial,final '+str(initial)+' '+str(final)+'\n')
    f.write(('length[kappa,dip,kyr] %f %f %f'%(len_time,len_dip,len_yr*1e-3))+'\n')
    f.write(('gauss[brms,diprms,dipolarity] %.2e %.2e %.2e'%(brms_gauss,diprms,dipolarity))+'\n')
    f.write(('AD/NAD,O/E,Z/NZ,chi2 %.2e %.2e %.2e %.2e'%(elike_means[0],elike_means[1],elike_means[2],elike_means[3]))+'\n')
    f.write(('nrev,nexecursions,n_eq_cross %i %i %i'%(nrev,nexcursions,n_eq_cross))+'\n')
    for l in range(1,lmax+1):
        for m in range(l+1):
            if m==0: line='g,h(l=%i) %e %e'%(l,gauss_stats[l,m,0].real,gauss_stats[l,m,0].imag)
            else: line=line+' %e %e'%(gauss_stats[l,m,0].real,gauss_stats[l,m,0].imag)
        f.write(line+'\n')
    line='mpowers(l,allm) %e'%(mpower_stats[1,0,imean])
    for l in range(2,lmax+1): line=line+' %e'%(mpower_stats[l,0,imean])
    f.write(line+'\n')
    line='mpowers(l,m=0) %e'%(mpower_stats[1,1,imean])
    for l in range(2,lmax+1): line=line+' %e'%(mpower_stats[l,1,imean])
    f.write(line+'\n')
    f.close()
    print('Closed '+dir+'/'+filename)
    

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description="""Update run_stats file""")
    parser.add_argument('-d', '--dir', action='store', type=str, dest='dir', required=False, default='.', 
                        help='dir to update run_stats')
    parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False, default=None, 
                    help='initial index to plot from')
    parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False, default=None, 
                    help='final index to plot to')
    args = parser.parse_args()
    dir=args.dir
    update_run_stats(dir,args)

