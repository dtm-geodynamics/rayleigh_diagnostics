#!/usr/bin/env python

###############################################
#
#  Global-Averages (G_Avgs) plotting
#  Plots average KE vs time
#
#  This example routine makes use of the GlobalAverage
#  data structure associated with the G_Avg output.
#  Upon initializing a GlobalAverage object, the 
#  object will contain the following attributes:
#
#    ----------------------------------
#    self.niter                  : number of time steps
#    self.nq                     : number of diagnostic quantities output
#    self.qv[0:nq-1]             : quantity codes for the diagnostics output
#    self.vals[0:niter-1,0:nq-1] : Globally averaged diagnostics as function of time and quantity index
#    self.iters[0:niter-1]       : The time step numbers stored in this output file
#    self.time[0:niter-1]        : The simulation time corresponding to each time step
#    self.lut                    : Lookup table for the different diagnostics output


from diagnostic_reading import GlobalAverage, build_file_list
import matplotlib.pyplot as plt
from matplotlib.pyplot import plot,ion,show
import numpy as np
import argparse
import os 
cwd = os.getcwd()
#ion()

parser = argparse.ArgumentParser( \
                       description="""Plot global averages (G_Avgs): KE vs time.""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False, default=None, 
                    help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False, default=None, 
                    help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
args = parser.parse_args()


# Set saveplot to True to save to a .png file. 
# Set to False to view plots interactively on-screen.
saveplot = args.save
savefile = 'energy_trace.pdf'  #Change .pdf to .png if pdf conversion gives issues

print("initial="+str(args.initial)+" final="+str(args.final))
files = build_file_list(args.initial,args.final,path='G_Avgs')
#The indices associated with our various outputs are stored in a lookup table
#as part of the GlobalAverage data structure.  We define several variables to
#hold those indices here:
a = GlobalAverage(filename=files[0],path='')  # read a file to get the lookup table
ke_index  = a.lut[125]  # Kinetic Energy (KE)
rke_index = a.lut[126]  # KE associated with radial motion
tke_index = a.lut[127]  # KE associated with theta motion
pke_index = a.lut[128]  # KE associated with azimuthal motion
#We also grab some energies associated with the mean (m=0) motions
mrke_index = a.lut[130]  # KE associated with mean radial motion
mtke_index = a.lut[131]  # KE associated with mean theta motion
mpke_index = a.lut[132]  # KE associated with mean azimuthal motion
#Magnetic Energies
me_index  = a.lut[475]  # Magnetic Energy (ME)
rme_index = a.lut[476]  # associated with radial motion
tme_index = a.lut[477]  # associated with theta motion
pme_index = a.lut[478]  # associated with azimuthal motion

#Next, we intialize some empy lists to hold those variables
ke = []
rke = []
tke = []
pke = []
mrke = []
mtke = []
mpke = []
me = []
rme = []
tme = []
pme = []

# We also define an empty list to hold the time.  This time
# will be absolute time since the beginning of the run, 
# not since the beginning of the first output file.  
# In this case, the run was dimensional, so the time is recorded 
# in seconds.  We will change that to days as we go.
alldays = []
timescale=1.  #(3600.0*24.0)

# Next, we loop over all files and grab the desired data
# from the file, storing it in appropriate lists as we go.
for f in files:
    a = GlobalAverage(filename=f,path='')


    days = a.time/timescale
    for i,d in enumerate(days):
        alldays.append(d)
        ke.append(a.vals[i,ke_index])
        rke.append(a.vals[i,rke_index])
        tke.append(a.vals[i,tke_index])
        pke.append(a.vals[i,pke_index])

        mrke.append(a.vals[i,mrke_index])
        mtke.append(a.vals[i,mtke_index])
        mpke.append(a.vals[i,mpke_index])
        
        me.append(a.vals[i,me_index])
        rme.append(a.vals[i,rme_index])
        tme.append(a.vals[i,tme_index])
        pme.append(a.vals[i,pme_index])

#Define times.
times=alldays
ntimes=len(times)
ntimes_stat=ntimes  #ntimes to use for statistics.
tra=max(times)-min(times)  #time range
i_trim=0.  #default, compute stats from t=0.
if tra>5. :
    i_trim=np.min(np.where(times-times[0] > 5.))  #cut out data before i_trim
    ntimes_stat=ntimes-i_trim  #ntimes to use for statistics
ind_stat=np.arange(i_trim,ntimes,dtype=np.uint8)

#Print stats.
print(" ")
print("KE=%8.2e +/- %8.2e  for t=[%4.2f,%4.2f]" %(np.mean(ke),np.std(ke),times[ind_stat[0]],times[max(ind_stat)]))
print("ME=%8.2e +/- %8.2e  for t=[%4.2f,%4.2f]" %(np.mean(me),np.std(me),times[ind_stat[0]],times[max(ind_stat)]))

#Create two plots.
# Plot 1:  Total KE and its breakdown.
# Plot 2:  Breakdown of the mean KE

#Changing the plotting parameter somewhat depending on saveplot
if (saveplot):
    plt.figure(1,figsize=(7.5, 4.0), dpi=300)
    plt.rcParams.update({'font.size': 12})
else:
    plt.figure(1,figsize=(15,5),dpi=100)
    plt.rcParams.update({'font.size': 14})

#Plot Kinetic Energy.
plt.subplot(121)
plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
plt.plot(times,ke,'black', label = 'KE'+r'$_{total}$')
plt.plot(times,rke,'r', label = 'KE'+r'$_{rad}$')
plt.plot(times,tke,'g', label = 'KE'+r'$_\theta$')
plt.plot(times,pke,'b', label = 'KE'+r'$_\phi$')
plt.yscale('log')
plt.xlabel('Time')  # (days)')
plt.ylabel('Energy Density ')  #+r'( erg cm$^{-3}$)')
if (saveplot):
    legend = plt.legend(loc='lower left', shadow=True, ncol = 2, fontsize = 'x-small') 
else:
    legend = plt.legend(loc='lower left', shadow=True, ncol = 2) 
plt.title(cwd,fontsize=10)

#Plot Magnetic Energy.    
plt.subplot(122)
t1='ME'
plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
plt.plot(times,me,'black', label = t1+r'$_{total}$')
plt.plot(times,rme,'r', label = t1+r'$_{rad}$')
plt.plot(times,tme,'g', label = t1+r'$_\theta$')
plt.plot(times,pme,'b', label = t1+r'$_\phi$')
plt.yscale('log')
plt.xlabel('Time')
plt.ylabel('Magnetic Energy Density')
if (saveplot):
    legend = plt.legend(loc='lower left', shadow=True, ncol = 2, fontsize = 'x-small') 
else:
    legend = plt.legend(loc='lower left', shadow=True, ncol = 2) 

plt.tight_layout()



if (saveplot):
    plt.savefig(savefile)
else:
    plt.show()
