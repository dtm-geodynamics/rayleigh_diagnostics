#!/usr/bin/env python

####################################################################################################
#
#  Compute the PSV of a dynamo model
#  Sample Br(CMB,theta,phi), convert it to Bsurf
#  Plot Intensity vs Inclination (tan(Inc)=2tan(lat))
#  Plot histogram of Intensity over time.
#  Use shell_slices to extract Br.
#
#  Shell-Slice (Shell_Slices) plotting example
#  - Reads in a single Shell_Slice file.
#  - Plots vr, vphi, and entropy
#
#  This example routine makes use of the ShellSlice
#  data structure associated with the Shell_Slices output.
#  Upon initializing a ShellSlice object, the 
#  object will contain the following attributes:
#    ----------------------------------
#    self.niter                                    : number of time steps
#    self.nq                                       : number of diagnostic quantities output
#    self.nr                                       : number of shell slices output
#    self.ntheta                                   : number of theta points
#    self.nphi                                     : number of phi points
#    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
#    self.radius[0:nr-1]                           : radii of the shell slices output
#    self.inds[0:nr-1]                             : radial indices of the shell slices output
#    self.costheta[0:ntheta-1]                     : cos(theta grid)
#    self.sintheta[0:ntheta-1]                     : sin(theta grid)
#    self.vals[0:nphi-1,0:ntheta-1,0:nr-1,0:nq-1,0:niter-1] 
#                                                  : The shell slices 
#    self.iters[0:niter-1]                         : The time step numbers stored in this output file
#    self.time[0:niter-1]                          : The simulation time corresponding to each time step
#    self.version                                  : The version code for this particular output (internal use)
#    self.lut                                      : Lookup table for the different diagnostics output
#    -------------------------------------

#import matplotlib
#matplotlib.use("Agg")

import pylab as p 
import numpy as np
from diagnostic_reading import ShellSlice,ShellSpectra
from diagnostic_tools import file_list, calculate_Ylms
from diagnostic_plotting import lutlabels,getlabel
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import ticker
from matplotlib.pyplot import ion,plot,show
import argparse

parser = argparse.ArgumentParser( \
                       description="""Plot PSV.""")
parser.add_argument('-i', '--initial', action='store', type=str, dest='initial', required=False, default=None, 
                    help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=str, dest='final', required=False, default=None, 
                    help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-d', '--default',action='store_true',dest='default',help='use default plotting options',
                    default=False)
args = parser.parse_args()

if args.default: print("default set")

# Set saveplot to True to save to a .png file. 
# Set to False to view plots interactively on-screen.
saveplot = args.save
savefile = 'shell_slice.png'

rnorm = 1./(1-0.35)
path='Shell_Slices'
files = file_list(args.initial, args.final, path=path)
nfiles=len(files)
path='Shell_Spectra'
filespec = file_list(args.initial, args.final, path=path)
nfilespec=len(filespec)

#Read in last shell slice
a = ShellSlice(filename=files[nfiles-1],path='')
s = ShellSpectra(filespec[nfilespec-1],path='')  #vals[0:lmax,0:mmax,0:nr-1,0:nq-1,0:niter-1]

#Default Plotting options.
user_codes = [401,402,403]  #Br,Btheta,Bphi
rad_inds = 1.   #use CMB only.

#NEXT: print the label for each code.  for this we need to create a look up table for labels, like p.labels.
q=lutlabels(user_codes)
print(q.labels)

#Identify the variables indices for vr,vphi, and entropy
var_ind = a.lut[user_codes]

#Tex can be enclosed in dollar signs within a string.  The r in front of the string is necessary for strings enclosing Tex
#units = [r'm s$^{-1}$', r'm s$^{-1}$', r'erg g$^{-1}$ K$^{-1}$']  
#vnames = [r'v$_r$', r'v$_\phi$', "S'"]
vnames=q.labels

# Extract data
#ntheta=a.ntheta
#nphi=a.nphi
ntheta=90
nphi=180
theta = np.linspace(0,180.,ntheta)
phi = np.linspace(0,360.,nphi)
nl = s.nell
nm = s.nm
br=np.zeros([nphi,ntheta,0])  #need to append to iters from other files.
bt=np.zeros([nphi,ntheta,0])  #need to append to iters from other files.
bp=np.zeros([nphi,ntheta,0])
bsr=np.zeros([nl,nm,0])
bst=bsr
bsp=bsr
time = np.zeros([0])
times = np.zeros([0])  #time from shellspectrum
print('Reading in data')
"""
# Read in Shell_Slices
for ifile,f in enumerate(files):
    a=ShellSlice(filename=f,path="")  #returns values at CMB and within domain.
    if ifile==0:
        br=a.vals[:,:,0,var_ind[0],:]
        bt=a.vals[:,:,0,var_ind[1],:]
        bp=a.vals[:,:,0,var_ind[2],:]
        time = a.time
    else:
        br=np.append(br,a.vals[:,:,0,var_ind[0],:],axis=2)
        bt=np.append(bt,a.vals[:,:,0,var_ind[1],:],axis=2)
        bp=np.append(bp,a.vals[:,:,0,var_ind[2],:],axis=2)
        time=np.append(time,a.time)
ntime=len(time)
"""
# Read in Shell_Spectra
for ifile,f in enumerate(filespec):
    s=ShellSpectra(filename=f,path="")  #returns harmonic amplitudes.
    if ifile==0:
        bsr=s.vals[:,:,0,var_ind[0],:]  #complex
        bst=s.vals[:,:,0,var_ind[1],:]
        bsp=s.vals[:,:,0,var_ind[2],:]
        times = s.time
"""    else:
        bsr=np.append(bsr,s.vals[:,:,0,var_ind[0],:],axis=2)  #complex
        bst=np.append(bst,s.vals[:,:,0,var_ind[1],:],axis=2)
        bsp=np.append(bsp,s.vals[:,:,0,var_ind[2],:],axis=2)
        times=np.append(times,s.time)
"""
ntimes=len(times)
#inc = np.arctan(br/bt)  #inclination.

# Extrapolate from CMB to Surface.
r_cmb=3481e3
r_surf=6371e3
nl_surf=32
nm_surf=32
bsr_surf=np.zeros((nl_surf,nm_surf,ntimes),dtype=complex)
bst_surf=np.zeros((nl_surf,nm_surf,ntimes),dtype=complex)
bsp_surf=np.zeros((nl_surf,nm_surf,ntimes),dtype=complex)
for i in range(nl_surf):
    ell=i+1.
    bsr_surf[i,:,:]=bsr[i,:nl_surf,:nm_surf]*(r_cmb/r_surf)**(ell+2)
    bsp_surf[i,:,:]=bsp[i,:nl_surf,:nm_surf]*(r_cmb/r_surf)**(ell+2)
    bst_surf[i,:,:]=bst[i,:nl_surf,:nm_surf]*(r_cmb/r_surf)**(ell+2)
# Convert Bsurf(l,m) to Bsurf(theta,phi)
br_surf=np.zeros((ntheta,nphi,ntimes))
bt_surf=np.zeros((ntheta,nphi,ntimes))
bp_surf=np.zeros((ntheta,nphi,ntimes))
bmag_surf=np.zeros([ntheta,nphi,ntimes])

print('calculating Ylms... this may take some time')
Ylmarray = calculate_Ylms(nl_surf,theta,phi)

print('entering summation')
for itime in range(ntimes):
    print('itime:'+str(itime)+'/'+str(ntimes))
    for itheta,thetai in enumerate(theta):
        #print('itime,itheta='+str(itime)+','+str(itheta))
        for iphi,phii in enumerate(phi):
            #print('itime,itheta,iphi='+str(itime)+','+str(itheta)+','+str(iphi))
            br_surf[itheta,iphi,itime] = np.sum(np.absolute(np.multiply(bsr_surf[:,:,itime], Ylmarray[:,:,itheta,iphi])))
            bt_surf[itheta,iphi,itime] = np.sum(np.absolute(np.multiply(bst_surf[:,:,itime], Ylmarray[:,:,itheta,iphi])))
            bp_surf[itheta,iphi,itime] = np.sum(np.absolute(np.multiply(bsp_surf[:,:,itime], Ylmarray[:,:,itheta,iphi])))
            bmag_surf[itheta,iphi,itime]=np.sqrt(br_surf[itheta,iphi,itime]**2+bt_surf[itheta,iphi,itime]**2+\
                                                 bp_surf[itheta,iphi,itime]**2)

# Save data.
#Ylmarray_save=np.ma.asarray(Ylmarray)
outfile=path+'/B_surf0'
np.savez(outfile,filespec=filespec,times=times,bsr_surf=bsr_surf,bsp_surf=bsp_surf,bst_surf=bst_surf,Ylmarray=Ylmarray,br_surf=br_surf,bt_surf=bt_surf,bp_surf=bp_surf,bmag_surf=bmag_surf)

##################################################################
# Plot comparison of Br,Bt,Bp as (theta,phi) and (l,m)
ion()
nfig=1
nrow=1
ncol=2
panel=1
f1 = p.figure(nfig,figsize=(15,7), dpi=80)
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
panel+=1
twosigma = 2*np.std(br_surf[:,:,0])  #limits are set to +/- twosigma
contour_levels = twosigma*np.linspace(-1,1,256)
# Contour br_surf(theta,phi,t=0)
image1 = ax1.imshow(br_surf[:,:,0],vmin=-twosigma, vmax=twosigma,
    extent=(-np.pi,np.pi,-np.pi/2,np.pi/2), clip_on=False,
    aspect=0.5, interpolation='bicubic')
image1.axes.get_xaxis().set_visible(False)  # Remove the longitude and latitude axis labels
image1.axes.get_yaxis().set_visible(False)
image1.set_cmap('RdYlBu_r')  # Red/Blue map
# Colorbar
cbar = f1.colorbar(image1,orientation='horizontal', shrink=0.5)
tick_locator = ticker.MaxNLocator(nbins=5)
cbar.locator = tick_locator
cbar.update_ticks()

exit()

#########################
# Sample br(theta,phi) at N random locations each time step.
nsamp_t=10
nsamp_tot=nsamp_t*ntimes
br_samp=np.zeros((3,nsamp_tot))  #0=theta,1=phi,2=br
lat_samp=np.zeros(nsamp_tot)
inc_samp=np.zeros(nsamp_tot)
bmap_samp=np.zeros(nsamp_tot)
for i in range(ntimes):
    for j in range(nsamp_t):
        i_theta=np.random.randint(0,ntheta)
        i_phi=np.random.randint(0,nphi)
        br_samp[0,i*nsamp_t+j]=theta[i_theta]
        br_samp[1,i*nsamp_t+j]=phi[i_phi]
        br_samp[2,i*nsamp_t+j]=br[i_phi,i_theta,i]  #all nsamp_t at t(i)
        inc_samp[i*nsamp_t+j]=inc[i_phi,i_theta,i]
        bmag_samp[i_samp_t+j]=bmag[i_phi,i_theta,i]
        """if theta[i_theta]<=90.:
            lat[i*nsamp_t+j]=90.-theta[i_theta]
        if theta[i_theta]>90.:
            lat[i*nsamp_t+j]=theta[i_theta]-90."""
lat_samp=br_samp[0,:]-90.  #co-latitude to latitude
inc_samp
nfig+=nfig
plt.figure(nfig,figsize=(15,7))
plt.subplot(131)
plt.hist(abs(lat_samp),bins=10)
plt.xlabel('abs(latitude)')
plt.subplot(132)
plt.hist(br_samp[1,:],bins=10)
plt.xlabel('phi')
plt.subplot(133)
plt.hist(abs(br_samp[2,:]),bins=10)
plt.xlabel('abs(Br)')
# Plot inclination
nfig+=1
plt.figure(nfig,figsize=(15,7))
plt.subplot(131)
plt.hist(inc_samp,bins=10.)
plt.xlabel('Inclination')
plt.subplot(132)
plot(inc_samp,bmag_samp,'.')
plt.xlabel('Inclination')
plt.ylabel('B')


#Save or display the figure
if (saveplot):
    p.savefig(savefile)  
else:
    plt.show()
