#!/usr/bin/env python
# copied from https://scipython.com/book/chapter-8-scipy/examples/visualizing-the-spherical-harmonics/
#

import matplotlib.pyplot as plt
from matplotlib import cm, colors
#from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.special import sph_harm
plt.ion()

phi = np.linspace(0, np.pi, 100)
theta = np.linspace(0, 2*np.pi, 100)
phi, theta = np.meshgrid(phi, theta)

# The Cartesian coordinates of the unit sphere
x = np.sin(phi) * np.cos(theta)
y = np.sin(phi) * np.sin(theta)
z = np.cos(phi)

m, l = 2, 3

# Calculate the spherical harmonic Y(l,m) and normalize to [0,1]
fcolors = sph_harm(m, l, theta, phi).real
fmax, fmin = fcolors.max(), fcolors.min()
fcolors = (fcolors - fmin)/(fmax - fmin)

# Set the aspect ratio to 1 so our sphere looks spherical
fig = plt.figure(figsize=plt.figaspect(1.))
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(x, y, z,  rstride=1, cstride=1, facecolors=cm.seismic(fcolors))
# Turn off the axis planes
#ax.set_axis_off()
plt.title('real')

# Imaginary
fcolorsi = sph_harm(m, l, theta, phi).imag
fmaxi, fmini = fcolorsi.max(), fcolorsi.min()
fcolorsi = (fcolorsi - fmini)/(fmaxi - fmini)
fig=plt.figure()
ax=fig.add_subplot(111,projection='3d')
ax.plot_surface(x,y,z,rstride=1,cstride=1,facecolors=cm.seismic(fcolorsi))
plt.title('imag')

#Absolute
fcolorsa = np.absolute(sph_harm(m, l, theta, phi))
fmaxa, fmina = fcolorsa.max(), fcolorsa.min()
fcolorsa = (fcolorsa - fmina)/(fmaxa - fmina)
fig=plt.figure()
ax=fig.add_subplot(111,projection='3d')
ax.plot_surface(x,y,z,rstride=1,cstride=1,facecolors=cm.seismic(fcolorsa))
plt.title('abs')

"""# Shifted by cos+isin
fc = sph_harm(m, l, theta, phi)
fc2 = fc.real*np.cos(phi)+fc.imag*np.sin(phi)
fmax, fmin = fc2.max(), fc2.min()
fc2 = (fc2 - fmin)/(fmax - fmin)
fig=plt.figure()
ax=fig.add_subplot(111,projection='3d')
ax.plot_surface(x,y,z,rstride=1,cstride=1,facecolors=cm.seismic(fc2))
plt.title('shifted')
"""
plt.show()
