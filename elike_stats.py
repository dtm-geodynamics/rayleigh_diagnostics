#!/usr/bin/env python

### elike_stats.py 11/28/17 PED ###
# Compute Earth-like dynamo       #
# statistics defined by           #
# Christensen et al (2010).       #
###################################
import numpy as np

def elike_stats(gauss):
    # gauss=[nl,nm,nt].complex
    lmax=len(gauss[:,0,0])
    nt=len(gauss[0,0,:])
    adnad=np.zeros(nt)
    oddeven=np.zeros(nt)
    zonality=np.zeros(nt)
    lmax_stats=8  #max used by CAH2010
    # Compute powers
    p=np.zeros((lmax_stats+1,lmax_stats+1,nt))  #power
    for t in range(nt):
        odd_sum=0.
        even_sum=0.
        zonal_sum=0.
        nonzonal_sum=0.
        for l in range(1,lmax_stats+1):
            for m in range(l+1):
                p[l,m,t]=(l+1)*(gauss[l,m,t].real**2+gauss[l,m,t].imag**2)  #CAH10 eq 2
                #p[l,m,t]=(gauss[l,m,t].real**2+gauss[l,m,t].imag**2)  #CAH10 eq 2. remove (l+1) bc we are at surface?
                if l>1:  # use l=2 and greater
                    if (l+m)%2: even_sum+=p[l,m,t]
                    if not (l+m)%2: odd_sum+=p[l,m,t]
                    if m==0: zonal_sum+=p[l,m,t]
                    if m>0: nonzonal_sum+=p[l,m,t]
        # Compute adnad
        adnad[t]=p[1,0,t]/(p[1,1,t]+np.sum(p[2:,:,t]))  #CAH10 eq 1
        # Compute O/E
        oddeven[t]=odd_sum/even_sum
        # Compute zonality
        zonality[t]=zonal_sum/nonzonal_sum
    # Compute chi^2 stats
    adnad_e=1.4    #CAH10 table 2
    oddeven_e=1.0
    zonality_e=0.15
    sd_adnad_e=2.0
    sd_oddeven_e=2.0
    sd_zonality_e=2.5
    adnad_mean=np.mean(adnad)
    oddeven_mean=np.mean(oddeven)
    zonality_mean=np.mean(zonality)
    chisq=((np.log(adnad_mean)-np.log(adnad_e))/np.log(sd_adnad_e))**2 + \
        ((np.log(oddeven_mean)-np.log(oddeven_e))/np.log(sd_oddeven_e))**2 + \
        ((np.log(zonality_mean)-np.log(zonality_e))/np.log(sd_zonality_e))**2
    stats=[adnad_mean,oddeven_mean,zonality_mean,chisq]
    return(adnad,oddeven,zonality,stats)
