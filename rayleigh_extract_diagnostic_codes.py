#!/usr/bin/python

## Script to extract python dictionaries of Rayleigh variable codes.

import re
import os

def isint(v):
  """ Test if string v can be represented as an integer or not."""
  # modified from: https://stackoverflow.com/a/9859202
  v = str(v).strip()
  return v=='0' or (v if v.find('..') > -1 else v.lstrip('-+').rstrip('0').rstrip('.')).isdigit()

def parse_line(line):
  """Parse a line assuming it is a fortran parameter assignment (or returning None if it can't be)."""
  csplit = line.split('!')                                # split for comments (which may contain =s)
  split = csplit[0].split('=')                            # split around '='
  if len(split) != 2: return None                         # needs to only be 1 '=' left of comments
  lsplit = re.split('::| ', split[0].strip())              # split on spaces and :s
  param_name = lsplit[-1].strip()                         # the word immediately before the '=' should be the parameter name
  param_expression = split[1].strip()                     # the first word right of the '=' and left of the '!' (if present) should be the expression
  param_comment = '!'.join(csplit[1:])                    # comment will be right of first '!' but may itself contain '!'s so re-add them
  param_tex = 'unknown'                                   # fallback if tex not found
  retex = re.search('tex', param_comment, re.IGNORECASE)  # search for the 'tex' flag in the comment
  if retex is not None: param_tex = param_comment[retex.end():].strip(': '+os.linesep) # take everything after 'tex' or 'tex:'
  return param_name, param_expression, param_tex          # return the name, expression and tex
    
def find_offset(filehandle):
  """Find and return the offset parameter name and expression."""
  """Returns the filehandle at the line of the offset declaration."""
  l = filehandle.readline()                                    # read a line from the filehandle
  while re.search('^\s*integer', l, re.IGNORECASE) is None or \
        not len(l.split('!')[0].split('=')) == 2:              # keep going until we find a line that is an integer declaration and that can be parsed
    l = filehandle.readline()
  name, expr, tex = parse_line(l)                              # parse the offset declaration line
  return name, expr                                            # return the offset parameter name and expression (ignore the tex)

def generate_ordered_filenames_offsets(filenames):
  """Generate a topological sort to get filenames in order based on offset dependencies."""
  """Returns a generator."""
  # modified from: https://stackoverflow.com/a/11564323
  pending = []
  for filename in filenames:                                    # create a list of filenames and offsets to be ordered
    f = open(filename, 'r')
    offset_name, offset_expression = find_offset(f)             # find and parse the offset declaration
    f.close()
    expr_split = re.split('\+|-|\*|/', offset_expression)       # split it based on flops
    dependencies = set([s.strip() for s in expr_split if not isint(s)])      # dependencies are assumed to be anything in the 
    pending.append([filename, offset_name, offset_expression, dependencies]) # expression that isn't an int
  emitted = []                                                  # a list of files we've already processed
  while pending:                                                # keep going until we have no files left
    next_pending = []
    next_emitted = []
    for entry in pending:                                       # loop over the files (and offsets and dependencies) that are still pending
      filename, offset_name, offset_expression, dependencies = entry
      dependencies.difference_update(emitted)                   # remove from the dependencies those offset parameters that have
                                                                # already been emitted (as they're already higher in the list so are
                                                                # satisfied)
      if dependencies:                                          # if we still have dependencies we're going to have to come back to this
        next_pending.append(entry)
      else:                                                     # no dependencies, yay! return this result
        yield filename, offset_name, offset_expression
        emitted.append(offset_name)                             # keep track of what we're emitting
        next_emitted.append(offset_name)                        # keep track of whether we're emitting anything on this local loop
    if not next_emitted:                                        # if we're not emitting then we're caught in a loop - throw an error
      raise ValueError("cyclic or missing dependeny detected: %r"%(next_pending,))
    pending = next_pending                                      # save what's left for the next loop
    emitted = next_emitted                                      # emitted should all be gone from dependencies now, so optimize by
                                                                # only worrying about next_emitted

def generate_parameters(filehandle):
  """Generate integer parameters from filehandle"""
  l = filehandle.readline()
  while l:                                                      # keep going until the end of the file
    if re.search('^\s*integer', l, re.IGNORECASE) is not None and \
       len(l.split('!')[0].split('=')) == 2:                    # take lines that start with integer declaration
      yield parse_line(l)
    l = filehandle.readline()
      

strtemplate = """## DO NOT EDIT
## 
## This file was automatically created by rayleigh_extract_diagnostic_codes.py, re-run to update.
##

index_to_string = \\
${names}

string_to_index = \\
${indices}

index_to_tex = \\
${tex}

""" 

if __name__ == "__main__":
  import glob
  import collections
  import pprint
  from string import Template
  import argparse

  # parse options:
  parser = argparse.ArgumentParser( \
                         description="""Parse a set of Rayleigh variable codes from fortran and output
                                        a python module containing dictionaries of their string and tex representations.""")
  parser.add_argument(action='store', type=str, dest='input_filepath', nargs='+',
                      help='path of files to parse - normally *_codes.F in Rayleigh\'s src/Diagnostics folder')
  parser.add_argument('-o', '--output', action='store', type=str, dest='output_filename', required=False, default='rayleigh_codes.py', 
                      help='output filename (defaults to \'rayleigh_codes.py\')')
  args = parser.parse_args()


  filenames = []                     # parse filepaths into filenames
  for ifilepath in args.input_filepath: filenames += glob.glob(ifilepath)

  names   = collections.OrderedDict()  # set up dictionaries
  indices = collections.OrderedDict()
  tex     = collections.OrderedDict()

  if len(filenames) == 1:                        # old style - can have multiple offsets throughout file
    f = open(filenames[0], 'r')
    parameters = generate_parameters(f)          # create a parameter generator and use it to loop
    old_param_name, old_param_expression, old_param_tex = parameters.next()  # get the first parameter
    for param_name, param_expression, param_tex in parameters:
      if re.search(old_param_name, param_expression, re.IGNORECASE):
        exec(old_param_name+"="+old_param_expression) # the old param must be an offset, exec it
      else:                                      # otherwise the old param is just a normal parameter
        exec("old_param_value = "+old_param_expression) # put it into the dictionaries
        names[old_param_value]  = old_param_name
        indices[old_param_name] = old_param_value
        tex[old_param_value]    = old_param_tex
      old_param_name       = param_name
      old_param_expression = param_expression
      old_param_tex        = param_tex
    exec("old_param_value = "+old_param_expression)
    names[old_param_value]  = old_param_name     # we've missed the last one so put it in now (just have to assume it's not an
    indices[old_param_name] = old_param_value    # offset, why would you have an offset last after all?)
    tex[old_param_value]    = old_param_tex
    f.close()
    
  else:                                           # new style - in this case we assume offsets declared at the top of the files
    ordered_filenames_offsets = generate_ordered_filenames_offsets(filenames)  # create generator of ordered files

    for filename, offset_name, offset_expression in ordered_filenames_offsets:
      exec(offset_name+"="+offset_expression)     # declare the offset parameters so that all the later expressions can be parsed
      f = open(filename, 'r')                     # open filehandle
      find_offset(f)                              # use this just to move to the offset line (parsing is unnecessary)
      parameters = generate_parameters(f)         # create a parameter generator and use it to loop
      for param_name, param_expression, param_tex in parameters: 
        exec("param_value = "+param_expression)   # work out the value
        names[param_value]  = param_name           # use it to enter dictionaries
        indices[param_name] = param_value
        tex[param_value]    = param_tex
      f.close()                                   # close file

  ofname = args.output_filename                 # write everything to file using the template above
  if not ofname.endswith('.py'): ofname+'.py'
  f = open(ofname, 'w')
  s = Template(strtemplate)
  f.writelines(s.safe_substitute({"names":pprint.pformat(dict(names), indent=2), \
                                  "indices":pprint.pformat(dict(indices), indent=2), \
                                  "tex":pprint.pformat(dict(tex), indent=2)}))
  f.close()





  
  
