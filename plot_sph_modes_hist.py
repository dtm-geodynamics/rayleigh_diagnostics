#!/usr/bin/env python

###################################################################################
"""  plot_sph_modes_hist.py 4/6/19 PED
     Started from plot_sph_modes.py
     PURPOSE: read in sph_modes data and plot histograms of sph harmonics
"""

#import matplotlib
#matplotlib.use("Agg")

import pylab as p 
import numpy as np
from rayleigh_diagnostics import Shell_Slices,Shell_Spectra,Power_Spectrum,SPH_Modes
from diagnostic_tools import file_list, calculate_Ylms,renormalize_shell_spectra,inverse_transform,norm_lm,dipole_latitude,dipole_longitude,gauss_coef,convert_to_kyr,polarity_chrons
from diagnostic_plotting import lutlabels,getlabel
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import ticker
#matplotlib.use('GTK')
from matplotlib.pyplot import ion,plot,show
import argparse
pi=np.pi

parser = argparse.ArgumentParser( \
                       description="""Plot SPH_MODES.""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial',
                    required=False, default=None, help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final',
                    required=False, default=None, help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True,
                    default=False, required=False,
                    help='save the figure to file instead of plotting interactively')
parser.add_argument('-mcl', '--min_chron_len', action='store', type=float,
                    dest='min_chron_len', required=False, default=1,
                    help='initial index to plot from')
parser.add_argument('-tave', '--timeave',action='store',type=float,
                    dest='timeave',help='time average',default=None)
parser.add_argument('-sn', '--savename', action='store', dest='savename', type=str,
                    default='', required=False, help='suffix name of the figure')
args = parser.parse_args()

# Set saveplot to True to save to a .png file. 
# Set to False to view plots interactively on-screen.
savefig = args.save
savefile = 'sph_modes_hist' + args.savename + '_'
savetype = '.pdf'

if args.timeave: savefile=savefile+'_tave%i'%args.timeave
Pm=10.
Pr=1.
kyr_dip_decay=50.
print('Assume for time scaling: Pm=%i, Pr=%i'%(Pm,Pr))
kyr_kappa=kyr_dip_decay/Pm/(1.538461/pi)**2.  #[kyr/t_kappa]
    
# Outline:
# 1. Read in a sph_modes file.
# 2. Plot.

# 1. Read in a file.
path='SPH_Modes'
print("Reading files...")
files = file_list(args.initial, args.final, path)
nfiles=len(files)
print('Number files found: '+str(nfiles))
sph_modes = SPH_Modes(files[0],path='')
""" SPH_Modes returns:
self.nell
self.lvals
self.vals[m,lvals,rad,qv,iter]
"""
nell=sph_modes.nell; lvals=sph_modes.lvals; niter=sph_modes.niter

# We use the lookup table to find where br, vtheta, and bphi are stored
br_lut=801
bt_lut=802
bp_lut=803
br_index = sph_modes.lut[br_lut]
if bt_lut in sph_modes.qv: bt_index=sph_modes.lut[bt_lut]
if bp_lut in sph_modes.qv: bp_index=sph_modes.lut[bp_lut]
dims=[sph_modes.nell,sph_modes.nr,sph_modes.niter]   #get dims.
nr=sph_modes.nr
niter=sph_modes.niter
rad_index=0  # only use top radius level
#Initialize full arr with first spec file.
times=sph_modes.time
brlm=sph_modes.vals[:,:,rad_index,br_index,:].reshape(nell,max(lvals)+1,niter) #reshape to swap m and l.

### 1b. Restore all remaining files and append data.
for filei in files:
    sph_modesi = SPH_Modes(filei,path='')
    times = np.append(times,sph_modesi.time)
    brlm = np.append(brlm,sph_modesi.vals[:,:,rad_index,br_index,:].reshape(sph_modesi.nell,max(sph_modesi.lvals)+1,sph_modesi.niter),axis=2)
    #print('niter=%d vals.shape=%d time[0,-1]=[%.2f,%.2f] dtime/niter=%.2f'%(sph_modesi.niter,len(sph_modesi.vals[0,0,0,0,:]),sph_modesi.time[0],sph_modesi.time[-1],(sph_modesi.time[-1]-sph_modesi.time[0])/sph_modesi.niter))
times_kyr,times_dip = convert_to_kyr(times)
len_kyr=max(times_kyr)-min(times_kyr)     #[yr]
    
# 2. Compute dipole angles
sqrt2=np.sqrt(2)  #renormalization factor, apply to m>0.
if 1 in sph_modes.lvals:
    # Define Components.
    idip=np.where(sph_modes.lvals==1.)[0]
    g10=brlm[idip,0,:].real.flatten()  #flatten to reduce to 1D
    g11=sqrt2*brlm[idip,1,:].real.flatten()  #flatten to reduce to 1D
    h11=sqrt2*brlm[idip,1,:].imag.flatten()  #flatten to reduce to 1D
    dip_lat=dipole_latitude(g10,g11,h11)*180./pi    #[deg]
    dip_lon=dipole_longitude(g10,g11,h11)*180./pi   #[deg]

# Reversal stats
min_chron_len=args.min_chron_len
print('min_chron_len=%f [dipdecay] %f [kyr]'%(min_chron_len,min_chron_len*kyr_dip_decay))
nrev,nexcursions,n_eq_cross,pchrons=polarity_chrons(times_dip,dip_lat,min_chron_len=min_chron_len)
nrev_myr = nrev/(len_kyr*1e-3)
print('n_eq_cross=%i, %f/Myr. nrev=%i, %f/Myr'%(n_eq_cross,n_eq_cross/(len_kyr*1e-3),nrev,nrev_myr))

# 3. Split arrays into subarrays
sub_kyr = 10.
n_sub = times_kyr[-1]/sub_kyr #number of sub arrays
sub_times_kyr = np.zeros((n_sub,1))  #2 for [istart,iend] of sub array
sub_gauss = np.zeros((n_sub,sph_modes.nell+3,1))  #nell+3 for g10,g11,h11
n = len(times_kyr)
#for i in range(int(n_sub)):
#    istart = int(i*n_sub)
#    iend = int((i+1)*n_sub-1)
#    print(istart,iend)
#    sub_times_kyr[i,0] = times_kyr[istart]
#    np.append(sub_times_kyr[i,0],times_kyr[istart+1:iend],axis=1)
#    sub_gauss[i,0,0] = g10[istart]
#    np.append(sub_gauss[i,0,0],g10[istart+1:iend],axis=2)
#    sub_gauss[i,1,0] = g11[istart]
#    np.append(sub_gauss[i,1,0],g11[istart+1:iend],axis=2)
#    sub_gauss[i,2,0] = h11[istart]
#    np.append(sub_gauss[i,2,0],h11[istart+1:iend],axis=2)
    
##################################################################
# 5. Plot.
ion()
# Begin setting up plots.
nfig=1
nrow=1
ncol=2
panel=1
f1 = p.figure(nfig,figsize=(15,10), dpi=80)
xtitle = 't'
xtitle='kyr'
#title_ft='t=%.2f [kyr] \nr_ratio=%.2f'%(time_title,r_ratio)
#if args.timeave: title_ft=title_ft+' \ntime ave Delta t=%.2f'%(timeave)
#plt.figtext(0.5,0.92,title_ft,ha='center')
plt.subplot(211)
bins=50
num=len(g10.real)
weights=np.zeros(num)+1./num
h11factor=1.
plt.hist(g10,bins=bins,weights=weights,label='g10',alpha=0.5)
plt.hist(g11,bins=bins,weights=weights,label='g11',alpha=0.5)
plt.hist(h11*h11factor,bins=bins,weights=weights,label='h11*%d'%h11factor,alpha=0.5)
plt.legend()
plt.ylabel('Probability %')
plt.xlabel(r'$g_{lm}$ or $h_{lm}$')

axlt=f1.add_subplot(212)
#axlt.plot(times_kyr,dip_lon,'-',label='Dip. Lon.',color='g')
axlt.plot(times_kyr,dip_lat,'-',label='%.2f rev/Myr'%nrev_myr,color='b')
plt.legend(markerscale=0)
#plt.annotate('%.2f rev/Myr'%nrev_myr)
#plt.ylabel('Dip. Lat., Lon. [deg]')
#plt.ylim(0,360)
axlt.set_ylabel('Dipole Co-Latitude [deg]')
axlt.set_xlabel(xtitle)
axlt.set_ylim(0,180)
axlt.set_xlim(min(times_kyr),max(times_kyr))
# Add shaded area for reversed polarity.
for i in range(nrev+1):
    #if beginning of this chron at time+1 has rev polarity:
    if dip_lat[np.where(times_dip==pchrons[i,0])[0]+1][0]>90.: 
        plt.axvspan(pchrons[i,0]*kyr_dip_decay,pchrons[i,1]*kyr_dip_decay,color='0.9')
plt.hlines(90,0,max(times_kyr),alpha=0.5)
         
#plt.tight_layout(True)
if savefig:
    plt.savefig(savefile+'%d'%nfig+savetype)
    print('Saved '+savefile+'%d'%nfig+savetype)


### Zoom in on lat lon
nfig += 1
colors = ['blue','red']
f2 = p.figure(nfig,figsize=(15,10), dpi=80)
istart = 800; iend = 900
plt.plot(times_kyr[istart:iend],dip_lat[istart:iend],'-',color=colors[0],label='Dip lat')
plt.plot(times_kyr[istart:iend],dip_lon[istart:iend],'-',color=colors[1],label='Dip lon')
plt.xlabel('xtitle')
plt.ylabel('Degrees')
plt.legend()

if savefig:
    plt.savefig(savefile+'%d'%nfig+savetype)
    print('Saved '+savefile+'%d'%nfig+savetype)
