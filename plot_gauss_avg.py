#!/usr/bin/env python
########################################################################3
#
#   plot_spectral_trace_gauss.py
#   same as plot_spectra_trace.py but using Gauss coef rather than Br(l,m)
#
#   Plotting Example:  Shell_Spectra
#
#   plot_spectral_trace.py
#   Plot time series (trace) of kinetic and
#   magnetic power spectral elements.
#
#   We plot the velocity power spectrum from one
#   shell spectrum output using the PowerSpectrum
#   class from diagnostic_reading.py.  
#   
#   Ideally, one should loop over several spectra
#   and take a time-average, similar to the approach
#   taken in plot_energy_flux.py.  This routine plots
#   a SINGLE SNAPSHOT of the power.
#
#   When a PowerSpectrum object is initialized, 
#   only power is computed from the Shell_Spectra
#   file and saved.
#  
#   If we want to look at individual m-values,
#   we can use the ShellSpectra class instead.
#   See diagnostic_reading.py for a brief 
#   description of that data structure.
#
#   The power spectrum structure is simpler to
#   work with.  It contains the following attributes:
#    ----------------------------------
#    self.niter                                    : number of time steps
#    self.nr                                       : number of radii at which power spectra are available
#    self.lmax                                     : maximum spherical harmonic degree l
#    self.radius[0:nr-1]                           : radii of the shell slices output
#    self.inds[0:nr-1]                             : radial indices of the shell slices output
#    self.power[0:lmax,0:nr-1,0:niter-1,0:2]       : the velocity power spectrum.  The third
#                                                  : index indicates (0:total,1:m=0, 2:total-m=0 power)
#    self.mpower[0:lmax,0:nr-1,0:niter-1,0:2]      : the magnetic power spectrum
#    self.iters[0:niter-1]                         : The time step numbers stored in this output file
#    self.time[0:niter-1]                          : The simulation time corresponding to each time step
#    self.magnetic                                 : True if mpower exists
#
#    self.vals[0:lmax,0:mmax,0:nr-1,0:nq-1,0:niter-1] 
#                                                  : The complex spectra of the shells output 
#    -------------------------------------
##################################

from rayleigh_diagnostics import Power_Spectrum, Shell_Spectra
from diagnostic_tools import file_list, calculate_Ylms,renormalize_shell_spectra,gauss_coef,\
     dipole_latitude,dipole_longitude,plm_schmidt,fix_times,norm_lm,alpha_95,polarity_chrons
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.pyplot import plot,ion,show
import numpy as np
from math import acos
from scipy.special import lpmv
import sys
import os 
cwd = os.getcwd()
r2d=180./np.pi  #radians/degree
ion()
pi=np.pi
fontsize=20
from matplotlib import rc, rcParams
rcParams['legend.fontsize'] = fontsize
rcParams['axes.labelsize'] = fontsize
rcParams['xtick.labelsize'] = fontsize
rcParams['ytick.labelsize'] = fontsize
rcParams['font.size'] = fontsize
#rc('text', usetex=True)


import argparse
parser = argparse.ArgumentParser( \
                       description="""Plot gauss stats.""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False, default=None, 
                    help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False, default=None, 
                    help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-sn', '--savename', action='store', dest='savename', type=str, default='gauss_ave', 
                    required=False, help='savename of the figure')
parser.add_argument('-ss', '--saveshort', action='store_const', dest='saveshort', const=True, default=False, 
                    required=False, help='In figure 1 only save first and last 2 subplots.')
parser.add_argument('-n', '--noplot', action='store_const', dest='noplot', const=True, default=False, 
                    required=False, help='suppress plotting windows')
parser.add_argument('-t', '--text', action='store_const', dest='text', const=True, default=False, 
                    required=False, help='print stats to text file')
parser.add_argument('-mcl', '--min_chron_len', action='store', type=float, dest='min_chron_len',
                    required=False, default=0.4,help='initial index to plot from')
args = parser.parse_args()

print("initial="+str(args.initial)+" final="+str(args.final))

def set_plot_fontsize(plt,fontsize=None,xlabel=None,ylabel=None):
    plt.ylabel(ylabels[i],{'fontsize':fontsize})
    plt.xlabel(xlabel,{'fontsize':fontsize})
    plt.yticks(size=fontsize)
    plt.xticks(size=fontsize)
    
#Set savefig to True to save to savefile.  Interactive plot otherwise.
savefig = False
#savefig = True
savefig = args.save
#savefile = 'power_spectrum_gauss'
savefile = args.savename
if savefig: print('savefig='+str(savefig))
window_title = 'spectral_trace ('+cwd+')'
initial=args.initial
final=args.final
print("Reading files...")
files = file_list(initial, final, 'Shell_Spectra')
nfiles=len(files)

sspec0 = Shell_Spectra(files[0],path=os.path.join(os.path.curdir,""))
# We use the lookup table to find where br, vtheta, and bphi are stored
br_index = sspec0.lut[801]
dims=[sspec0.lmax,sspec0.nr,sspec0.niter]   #get dims.
sspec = [Power_Spectrum('blank',dims) for i in range(nfiles)]  #define spec as a blank list to get a template object.
sspec[0]=sspec0  #def 1st element.
lmax=sspec0.lmax
lmax_gauss=min([84,lmax])  #limit to <84 bc factorial(170)=inf, so lmax<84. 
nl=lmax+1
nr=sspec0.nr
niter=sspec0.niter
#print(spec0.power.shape)

#rad_index
rad_index=0#.  # only use top radius level?

#Initialize full arr with first spec file.
times=sspec0.time
# Convert Brlm to orthonormal
brlm_ss=np.zeros((nl,nl,sspec0.niter),dtype=complex)
brlm_ss=sspec0.vals[:lmax_gauss,:lmax_gauss,0,br_index,:]  #raw output from ShellSpec
brlm_on=renormalize_shell_spectra(brlm_ss)
brlm=np.zeros((nl,nl,sspec0.niter),dtype=complex)   #orthonormalized.
brlm=brlm_on
# Compute Gauss
gauss=gauss_coef(brlm_on)

for i in range(1,nfiles):  #skip 1st.
    sys.stdout.write('file '+str(i)+'/'+str(nfiles)+' \r')
    sys.stdout.flush()
    sspec_i = Shell_Spectra(files[i],path='')
    times=np.append(times,sspec_i.time)        #extend times arr.
    br_index_i = sspec_i.lut[801]
    sspec[i]=sspec_i                            #contains all spec[i]
    brlm_ss=np.append(brlm_ss,sspec_i.vals[:lmax_gauss,:lmax_gauss,0,br_index_i,:],axis=2)

brlm=renormalize_shell_spectra(brlm_ss)
gauss=gauss_coef(brlm)

print('Compute gauss stats...')

# Define Components.
g10=gauss[1,0,:].real
g11=gauss[1,1,:].real
h11=gauss[1,1,:].imag
dip_lat = dipole_latitude(g10,g11,h11)*180/pi
dip_lon = dipole_longitude(g10,g11,h11)*180/pi

ntimes=len(times)
ntimes_stat=ntimes  #ntimes to use for statistics.
tra=max(times)-min(times)  #time range

# Funcs of time:
gauss_tot=np.sum(np.sum(gauss.real+gauss.imag,axis=0),axis=0) #sum over m (sum over l (real^2+imag^2))
gauss_tot_rms=np.sqrt(np.sum(np.sum(gauss.real**2+gauss.imag**2,axis=0),axis=0)) #sqrt(sum over m (sum over l (real^2+imag^2)))
gauss_nad=gauss_tot-g10
gauss_zonal=np.sum(gauss[:,0,:].real+gauss[:,0,:].imag,axis=0)
lowes=np.sqrt(np.sum(gauss.real**2+gauss.imag**2,axis=1))   #[nl,t] sum over all m for each l.
# Time ave of each glm.
gauss_ave=np.mean(gauss,axis=2)
# Dipolarity
dipolarity = lowes[1,:]/gauss_tot_rms
# Note: this is not the normal dipolarity, it is a Gauss dipolarity, which is not equal to the ratio of field amplitudes.

### PLOTTING ###
time_index = 0  #plot the first record?
# Scale time from kappa units to kyr
Pm=10.
Pr=1.
kyr_dip_decay=50.
print('Assume for time scaling: Pm=%f, Pr=%f'%(Pm,Pr))
times_mag=times/Pm
times_dip=times_mag/(1.538461/pi)**2.
times_kyr=times_dip*kyr_dip_decay
len_kyr=max(times_kyr)-min(times_kyr)     #[yr]
time=sspec0.time
ke_degree=0.  #1.
me_degree=1.
r_cmb=3481e3
r_surf=6371e3
r_ratio=r_cmb/r_surf
# Reversal stats
min_chron_len=args.min_chron_len
nrev,nexcursions,n_eq_cross,pchrons=polarity_chrons(times_dip,dip_lat,min_chron_len=min_chron_len)
print('n_eq_cross=%i, %f/Myr. nrev=%i, %f/Myr'%(n_eq_cross,n_eq_cross/(len_kyr*1e-3),nrev,nrev/(len_kyr*1e-3)))
gnad=gauss_nad
dip=np.sqrt(gauss[1,0,:].real**2+gauss[1,1,:].real**2+gauss[1,1,:].imag**2)
adip=np.sqrt(gauss[1,0,:].real**2)

# Compute E-like stats
from elike_stats import elike_stats
adnad,oddeven,zonality,elike_means=elike_stats(gauss)
# Print stats
#print('ave dipolarity=%.2e, AD/NAD=%.2e, O/E=%.2e, Z/NZ=%.2e'%(np.mean(dipolarity),np.mean(adnad),np.mean(oddeven),np.mean(zonality)))
print('ave dipolarity=%.2e, AD/NAD=%.2e, O/E=%.2e, Z/NZ=%.2e, chi2=%.2e'%(np.mean(dipolarity),elike_means[0],elike_means[1],elike_means[2],elike_means[3]))

#################################
####  Synthetic observations  ###
####  Compute Dec and Inc at a (theta,phi) location and average it over time  ###
## First compute magnetic field components X,Y,Z at (r=surf, theta=pi/2 (equator), phi=0)
x=np.zeros(ntimes_stat)
y=np.zeros(ntimes_stat)
z=np.zeros(ntimes_stat)
horiz=np.zeros(ntimes_stat)
dec=np.zeros(ntimes_stat)
inc=np.zeros(ntimes_stat)
mu=0.  #=cos(theta=pi/2)=0
lmax_obsv=8  #arbitrary degree cutoff for synthetic observations.
plms_schmidt=np.zeros((lmax_obsv+1,lmax_obsv+1))
lpmvs=np.zeros((lmax_obsv+1,lmax_obsv+1))   #at a given mu
norms=np.zeros((lmax_obsv+1,lmax_obsv+1))
for l in range(lmax_obsv+1):   #compute plms
    for m in range(l+1):
        lpmvs[l,m]=lpmv(m,l,mu)
        norms[l,m]=norm_lm(l,m)
        plms_schmidt[l,m]=plm_schmidt(l,m,mu)
print('Computing x,y,z components at equator...')
for i in range(ntimes_stat):
    for l in range(lmax_obsv+1):
        #r_ratio_l=r_ratio**(l+2)  #r_cmb/r_surf.  when using brcmb.
        r_ratio_l=1.   #when using gauss.
        xm=0.
        ym=0.
        zm=0.
        for m in range(l):
            ### Use brlm orthonormal coef.
            #xm += brlm[l,m,i].real/(l+1)*norms[l,m]*lpmvs[l,m+1]  #sum over m.
            #ym += brlm[l,m,i].imag*m/(l+1)*norms[l,m]*lpmvs[l,m]  #sum over m.
            #zm += brlm[l,m,i].real*norms[l,m]*lpmvs[l,m]  #sum over m. 
            ### Use gauss orthonormal coef.
            xm += gauss[l,m,i].real*plms_schmidt[l,m+1]  #sum over m.
            ym += gauss[l,m,i].imag*m*plms_schmidt[l,m]  #sum over m.
            zm += gauss[l,m,i].real*(l+1)*plms_schmidt[l,m]  #sum over m.
        x[i] += xm*r_ratio_l   #sum over l.
        y[i] += ym*r_ratio_l   #sum over l.
        z[i] += zm*r_ratio_l   #sum over l.
y = -y  #apply minus sign.
z = -z  #apply minus sign.
horiz=np.sqrt(x**2+y**2)
Feq=np.sqrt(x**2+y**2+z**2)  #F amplitude at equator.
dec=np.arctan(y/x)
inc=np.arctan(z/horiz)
l_r=np.cos(dec)*np.cos(inc)
m_r=np.sin(dec)*np.cos(inc)
n_r=np.sin(inc)
times_kyr=times_kyr-times_kyr[0]  #zero out times.
times_kyr=fix_times(times_kyr)  #remove backwards time steps
nt=len(times_kyr)
print('Zeroed out times_kyr')
dt_kyr=times_kyr[-1]  #length of run in kyr

print('Computing running averages...')
### Compute running ave over 3 time lengths: (.1,1,10) dipole decay times in kyr
#run_len=np.array([1.,1.5,2.,3.,4.,6.,8.,10.])*kyr_dip_decay  #[kyr]
#run_len=np.array([0.1,0.5,1.,1.5,2.,2.5,3.,4.,5.,10.])*kyr_dip_decay  #[kyr]
run_len=np.array([0.1,0.2,0.4,0.6,0.8,1.,1.2,1.4,1.6,1.8,2.,2.5,3.,3.5,4.,4.5,5.,10.])*kyr_dip_decay  #[kyr]
nrun_len=len(run_len)
nrun_len=np.max(np.where(run_len<times_kyr[-1]/2)[0])+1  #cut out run_len > (len of run)/2.
run_len=run_len[:nrun_len]  #shorten
print('run_len='+' '.join('%i'%x for x in run_len))
#For each run_len compute the run_ave N times where N is
n_sub_run=(dt_kyr/run_len).astype(int)
#n_sub_run=(dt_kyr/run_len).astype(int)-1  #-1 to avoid a partial final subrun
n_sub_run=np.clip(n_sub_run,2,100000)  #ensure that n_sub_run>1 so the "j" loop below always occurs!
ave_g10_run=np.zeros(nrun_len)  #average of the final g10_run for each run_len
std_g10_run=np.zeros(nrun_len)  #std of the final g10_run for each run_len
ave_adip_run=np.zeros(nrun_len)  #adip=sqrt(g10^2)
std_adip_run=np.zeros(nrun_len)  #adip=sqrt(g10^2)
ave_dip_run=np.zeros(nrun_len)  #dip=sqrt(g10^2+g11^2+h11^2)
std_dip_run=np.zeros(nrun_len)  #dip=sqrt(g10^2+g11^2+h11^2)
ave_gnad_run=np.zeros(nrun_len)  #average of the final g10_run for each run_len
std_gnad_run=np.zeros(nrun_len)  #std of the final g10_run for each run_len
ave_dec_run=np.zeros(nrun_len)   #Declination at equator
std_dec_run=np.zeros(nrun_len)
ave_inc_run=np.zeros(nrun_len)
std_inc_run=np.zeros(nrun_len)
ave_R_run=np.zeros(nrun_len)
std_R_run=np.zeros(nrun_len)
ave_a95_run=np.zeros(nrun_len)   #alpha_95 value at equator
std_a95_run=np.zeros(nrun_len)
ave_kfish_run=np.zeros(nrun_len)   #k fisher statistic at equator
std_kfish_run=np.zeros(nrun_len)
ave_Feq_run=np.zeros(nrun_len)  #F amplitude at equator
std_Feq_run=np.zeros(nrun_len)  #F amplitude at equator
nfig=1
if not args.noplot: plt.figure(nfig,figsize=(15,10))
axes=[]
if nrun_len<=10:
    nrows=2  #subplot(sphund), reflects number of rows
if nrun_len>10:
    nrows=3
ncols=nrun_len/nrows
cntsubplot=0  #passed to subplot()
savesubplot=[True]*nrun_len
if args.saveshort:
    nrows=2
    ncols=2
    savesubplot=[None]*nrun_len
    savesubplot[5]=True   #subplot only first and last 2
    savesubplot[10]=True
    savesubplot[-2]=True
    savesubplot[-1]=True

for i in range(nrun_len):        #loop over run_len
    g10_subfin=np.array([0])
    dip_subfin=np.array([0])
    adip_subfin=np.array([0])
    gnad_subfin=np.array([0])
    inc_subfin=np.array([0])
    dec_subfin=np.array([0])
    R_subfin=np.array([0])
    a95_subfin=np.array([0])
    kfish_subfin=np.array([0])
    Feq_subfin=np.array([0])
    axes.append(plt.subplot(nrows,ncols,cntsubplot+1))
    plt.xlabel('kyr')
    plt.ylabel(r'$g_{1,0}$')
    plt.xlim(times_kyr[0],times_kyr[-1])
    plt.locator_params(axis='x',nbins=4)
    j=0
    ind_end=-1  #initialize to -1 so after first loop its 0.
    while(ind_end<nt-1):
        t_subtot=0
        ind_start=ind_end+1
        t_substart=times_kyr[ind_start]
        g10_subrun=np.array([g10[0]]) #(num_subrun,len_subrun)
        dip_subrun=np.array([dip[0]])
        adip_subrun=np.array([adip[0]])
        gnad_subrun=np.array([gnad[0]])
        dec_subrun=np.array([dec[0]])
        inc_subrun=np.array([inc[0]])
        R_subrun=np.array([np.sqrt(l_r[0]**2+m_r[0]**2+n_r[0]**2)])
        Feq_subrun=np.array([Feq[0]])
        k=1
        while(t_subtot<=run_len[i] and ind_start+k < nt):
            #g10_subrun[j,k]=g10_subrun[j,k-1]*(k-1)/k + (1./k)*g10[ind_start+k]
            g10_subrun=np.append(g10_subrun,g10_subrun[k-1]*(k-1)/k + (1./k)*g10[ind_start+k]) #append kth running ave to end of this subrun.
            dip_subrun=np.append(dip_subrun,dip_subrun[k-1]*(k-1)/k + (1./k)*dip[ind_start+k])
            adip_subrun=np.append(adip_subrun,adip_subrun[k-1]*(k-1)/k + (1./k)*adip[ind_start+k])
            gnad_subrun=np.append(gnad_subrun,gnad_subrun[k-1]*(k-1)/k + (1./k)*gnad[ind_start+k])
            dec_subrun=np.append(dec_subrun,dec_subrun[k-1]*(k-1)/k + (1./k)*dec[ind_start+k])
            inc_subrun=np.append(inc_subrun,inc_subrun[k-1]*(k-1)/k + (1./k)*inc[ind_start+k])
#            R_subrun=np.append(R_subrun,np.sqrt(np.sum(l_r[:k])**2+np.sum(m_r[:k])**2+np.sum(n_r[:k])**2))
            R_subrun=np.append(R_subrun,np.sqrt(np.sum(l_r[ind_start:ind_start+k])**2+np.sum(m_r[ind_start:ind_start+k])**2+np.sum(n_r[ind_start:ind_start+k])**2))
            Feq_subrun=np.append(Feq_subrun,Feq_subrun[k-1]*(k-1)/k + (1./k)*Feq[ind_start+k])
            t_subtot=times_kyr[ind_start+k]-t_substart
            if (ind_start+k+1) > nt: break
            k+=1
        ind_end=ind_start+k
        if t_subtot>=run_len[i]:  #if this subrun is full, save it.
            #print(i,j,run_len[i],ind_start,ind_end,t_subtot,g10_subrun[k-1])
            g10_subfin=np.append(g10_subfin,g10_subrun[k-1])  #save last running ave value.
            dip_subfin=np.append(dip_subfin,dip_subrun[k-1])  #save last running ave value.
            adip_subfin=np.append(adip_subfin,adip_subrun[k-1])  #save last running ave value.
            gnad_subfin=np.append(gnad_subfin,gnad_subrun[k-1])  #save last running ave value.
            dec_subfin=np.append(dec_subfin,dec_subrun[k-1])  #save last running ave value.
            inc_subfin=np.append(inc_subfin,inc_subrun[k-1])  #save last running ave value.
            R_subfin=np.append(R_subfin,R_subrun[k-1])
            a95_subfin=np.append(a95_subfin,alpha_95(R_subrun[k-1],k))
            kfish_subfin=np.append(kfish_subfin,(k-1.)/(k-R_subrun[k-1]))
            Feq_subfin=np.append(Feq_subfin,Feq_subrun[k-1])
            if savesubplot[i]:
                plot(times_kyr[ind_start:ind_end],g10[ind_start:ind_end])
                plot(times_kyr[ind_start+1:ind_end-1],g10_subrun[1:-1],color='black',linewidth=1.5)  #exclude end points?
        j+=1
    ave_g10_run[i]=np.mean(g10_subfin[1:])  #append mean(last running ave values)
    std_g10_run[i]=np.std(g10_subfin[1:])
    ave_dip_run[i]=np.mean(dip_subfin[1:])  #append mean(last running ave values)
    std_dip_run[i]=np.std(dip_subfin[1:])
    ave_adip_run[i]=np.mean(adip_subfin[1:])  #append mean(last running ave values)
    std_adip_run[i]=np.std(adip_subfin[1:])
    ave_gnad_run[i]=np.mean(gnad_subfin[1:])
    std_gnad_run[i]=np.std(gnad_subfin[1:])
    ave_dec_run[i]=np.mean(dec_subfin[1:])
    std_dec_run[i]=np.std(dec_subfin[1:])  
    ave_inc_run[i]=np.mean(inc_subfin[1:])
    std_inc_run[i]=np.std(inc_subfin[1:])
    ave_R_run[i]=np.mean(R_subfin[1:])
    std_R_run[i]=np.std(R_subfin[1:])
    ave_a95_run[i]=np.nanmean(a95_subfin[1:])
    std_a95_run[i]=np.nanstd(a95_subfin[1:])
    if np.isnan(ave_a95_run[i]):
        print('ave_a95_run[i]='+str(ave_a95_run[i]))
        exit()
        
    ave_kfish_run[i]=np.mean(kfish_subfin[1:])
    std_kfish_run[i]=np.std(kfish_subfin[1:])
    ave_Feq_run[i]=np.mean(Feq_subfin[1:])
    std_Feq_run[i]=np.std(Feq_subfin[1:])
    #print(i,j,run_len[i],ind_start,ind_end,nt,ave_g10_run[i])  #i+1 bc [0] is initialization of zero.
    #print(g10_subfin[1:])
    #print(i,j,run_len[i],R_subfin[1:],a95_subfin[1:])
    if savesubplot[i]:
#        plt.text(axes[i].get_xlim()[0],axes[i].get_ylim()[1],'\n  run_len=%i\n  n_sub_run=%i'
        plt.text(axes[i].get_xlim()[1]*.75,axes[i].get_ylim()[1]*1.1,'\n  'r'$\tau=%i$ kyr''\n'r'  $N=%i$'
                %(run_len[i],n_sub_run[i]),fontsize=15,va='top')
        axes[i].locator_params(axis='x',nbins=4)
        cntsubplot+=1

# Find time window where std_inc,dec<crit where crit is some critical threshold.
ang_crit=10.  #[deg] critical angle (dec or inc)
if np.max(std_dec_run*180/pi)<ang_crit:
    irun_crit_dec=[0] #if all num < crit then use index 0.
else:
    irun_crit_dec=np.max(np.where(std_dec_run*180/pi>ang_crit))+1  #one after last time that std_dec>crit
if np.max(std_inc_run*180/pi)<ang_crit:
    irun_crit_inc=[0] #if all num < crit then use index 0.
else:
    irun_crit_inc=np.max(np.where(std_inc_run*180/pi>ang_crit))+1  #one after last time that std_dec>crit
irun_crit=np.max([irun_crit_dec,irun_crit_inc])  #use smallest time window where both satisfied.
run_len_crit=run_len[irun_crit]
print('dt where both (std_dec & std_inc) < %i, dt=%i'%(ang_crit,run_len_crit))
print('dt=%i, dec=%f+-%f, inc=%f+-%f, g10=%f+-%f, gnad=%f+-%f'
      %(run_len_crit,ave_dec_run[irun_crit]*180/pi,std_dec_run[irun_crit]*180/pi,ave_inc_run[irun_crit]*180/pi,std_inc_run[irun_crit]*180/pi,ave_g10_run[irun_crit],std_g10_run[irun_crit],ave_gnad_run[irun_crit],std_gnad_run[irun_crit]))
# dt_crit using alpha_95
a95_crit=10.  #[deg]
if np.max(ave_a95_run*180/pi)<a95_crit:
    irun_crit_a95=[0]
else:
    irun_crit_a95=np.max(np.where(ave_a95_run*180/pi>a95_crit))+1
run_len_a95_crit=run_len[irun_crit_a95]    

if (savefig):
    plt.tight_layout()
    #savename=savefile+str(nfig)+'.pdf'
    savename=savefile+str(nfig)+'.eps'
    plt.savefig(savename)
    print('Saved '+savename)

if not args.noplot:
    print('Plotting ...')
    ### Plot running ave versus running lengths.
    xlabel_tau=r'$\tau$ [kyr]'
    nfig+=1
    xlim=(0,run_len[-1]*1.2)
    plt.figure(nfig,figsize=(15,10))
    plt.subplot(221)
    plt.errorbar(run_len,ave_g10_run,yerr=std_g10_run,fmt='o',markersize=20)
    plt.axvline(x=run_len_a95_crit,color='black',linestyle='dashed')  #vertical line at the critical run_len 
    plt.xlim(xlim)
    plt.xlabel(xlabel_tau)
    plt.ylabel(r'$\overline{g_{1,0}}$')
    plt.subplot(222)
    plt.errorbar(run_len,ave_gnad_run,yerr=std_gnad_run,fmt='o',markersize=20)
    plt.axvline(x=run_len_a95_crit,color='black',linestyle='dashed')  #vertical line at the critical run_len 
    plt.xlim(xlim)
    plt.xlabel(xlabel_tau)
    plt.ylabel(r'$\overline{g_{NAD}}$')
    plt.subplot(223)
    plt.errorbar(run_len,ave_dip_run,yerr=std_dip_run,fmt='o',markersize=20,label='Dipole')
    plt.errorbar(run_len,ave_adip_run,yerr=std_adip_run,fmt='o',markersize=20,label='Ax. Dipole')
    plt.xlim(xlim)
    plt.xlabel(xlabel_tau)
    plt.ylabel(r'$\overline{g_D}$')
    plt.legend(numpoints=1)
    plt.subplot(224)
    plt.errorbar(run_len,ave_Feq_run,yerr=std_Feq_run,fmt='o',markersize=20)
    plt.xlim(xlim)
    plt.xlabel(xlabel_tau)
    plt.ylabel(r'$\overline{F_{eq}}$')
    plt.tight_layout()
    if (savefig):
        savename=savefile+str(nfig)+'.pdf'
        plt.savefig(savename)
        print('Saved '+savename)

    ### Plot inc and dec vs tau
    nfig+=1
    plt.figure(nfig,figsize=(15,10))
    plt.subplot(221)
    plt.errorbar(run_len,ave_dec_run*180/pi,yerr=std_dec_run*180/pi,fmt='o',markersize=20)
    plt.axvline(x=run_len_a95_crit,color='black',linestyle='dashed')  #vertical line at the critical run_len 
    plt.xlim(xlim)
    plt.xlabel(xlabel_tau)
    plt.ylabel(r'$\overline{D_{eq}}$ [deg]')
    plt.subplot(222)
    plt.errorbar(run_len,ave_inc_run*180/pi,yerr=std_inc_run*180/pi,fmt='o',markersize=20)
    plt.axvline(x=run_len_a95_crit,color='black',linestyle='dashed')  #vertical line at the critical run_len 
    plt.xlim(xlim)
    plt.xlabel(xlabel_tau)
    plt.ylabel(r'$\overline{I_{eq}}$ [deg]')
    plt.subplot(223)
    plt.errorbar(run_len,ave_a95_run*180/pi,yerr=std_a95_run*180/pi,fmt='o',markersize=20)
    plt.axvline(x=run_len_a95_crit,color='black',linestyle='dashed')  #vertical line at the critical run_len 
    plt.xlim(xlim)
    plt.xlabel(xlabel_tau)
    plt.ylabel(r'$\overline{\alpha_{95}}$ [deg]')
    plt.subplot(224)
    plt.errorbar(run_len,ave_kfish_run,yerr=std_kfish_run,fmt='o',markersize=20)
    plt.axvline(x=run_len_a95_crit,color='black',linestyle='dashed')  #vertical line at the critical run_len 
    plt.xlim(xlim)
    plt.xlabel(xlabel_tau)
    plt.ylabel(r'$\overline{k}$, Fisher')
    if (savefig):
        savename=savefile+str(nfig)+'.pdf'
        plt.savefig(savename)
        print('Saved '+savename)
        
    ### Plot dipole tilt
    nfig+=1
    fig=plt.figure(nfig,figsize=(15,10))
    #plt.figure(nfig).canvas.set_window_title(window_title)
    ax1=fig.add_subplot(211)
    lw = 1.5
    lns1 = ax1.plot(times_kyr,dip_lat,linewidth=lw,label=r'$\theta_{dip}$')
    ax1.set_xlim(times_kyr[0],times_kyr[-1])
    ax1.set_ylim([0,180])
    ax1.set_ylabel(r'$\theta_{dip}$ [deg]')
    ax1.set_xlabel('kyr')
    # Add shaded area for reversed polarity.
    for i in range(nrev+1):
        #if beginning of this chron at time+1 has rev polarity:
        if dip_lat[np.where(times_dip==pchrons[i,0])[0]+1][0]>90.: 
            plt.axvspan(pchrons[i,0]*kyr_dip_decay,pchrons[i,1]*kyr_dip_decay,color='0.8')
    axr = ax1.twinx()
    lns2 = axr.plot(times_kyr,g10,color='green',linewidth=lw,label=r'$g_{1,0}$')
    axr.set_xlim(min(times_kyr),max(times_kyr))
    axr.set_ylabel(r'$g_{1,0}$')
    lns=lns1+lns2
    labs = [l.get_label() for l in lns]
    #ax1.legend(lns,labs,loc='best',fancybox=True,framealpha=1)
    axr.legend(lns,labs,fancybox=True,framealpha=1)
    ax1=fig.add_subplot(212)
    ax1.plot(times_kyr,inc*180/pi,label=r'Inc')
    ax1.plot(times_kyr,dec*180/pi,label=r'Dec')
    ax1.set_xlim(times_kyr[0],times_kyr[-1])
    ax1.set_ylabel(r'$I_{eq}$ , $D_{eq}$ [deg]')
    ax1.set_xlabel('kyr')
    ax1.set_ylim(-90,90)
    ax1.legend(fancybox=True,framealpha=1)
    # Add shaded area for reversed polarity.
    for i in range(nrev+1):
        #if beginning of this chron at time+1 has rev polarity:
        if dip_lat[np.where(times_dip==pchrons[i,0])[0]+1][0]>90.: 
            plt.axvspan(pchrons[i,0]*kyr_dip_decay,pchrons[i,1]*kyr_dip_decay,color='0.8')
    if (savefig):
        plt.savefig(savefile+str(nfig)+'.pdf')
        print('Saved '+savefile)

#################################
# Print summary data to text file.
if args.text:
    filename='gauss_ave.txt'
    f=open(cwd+'/'+filename,'w')
    f.write(cwd+'\n')  #write dir first
    f.write('initial,final '+str(initial)+' '+str(final)+'\n')
    f.write('kyr %f %f\n'%(times_kyr[0],times_kyr[-1]))
    #f.write('dipolarity,AD/NAD,O/E,Z/NZ,chi2 %.2e %.2e %.2e %.2e %.2e\n'%(np.mean(dipolarity),elike_means[0],elike_means[1],elike_means[2],elike_means[3]))
    #I don't trust the above elike stats.  why are they different than from plot_gauss.py??
    f.write('window_lengths '+''.join(str(x)+' ' for x in run_len)+'\n')
    f.write('n_sub_windows '+''.join(str(x)+' ' for x in n_sub_run)+'\n')
    f.write('ave_g10_run '+''.join('%e '%x for x in ave_g10_run)+'\n')
    f.write('std_g10_run '+''.join('%e '%x for x in std_g10_run)+'\n')
    f.write('ave_dip_run '+''.join('%e '%x for x in ave_dip_run)+'\n')
    f.write('std_dip_run '+''.join('%e '%x for x in std_dip_run)+'\n')
    f.write('ave_adip_run '+''.join('%e '%x for x in ave_adip_run)+'\n')
    f.write('std_adip_run '+''.join('%e '%x for x in std_adip_run)+'\n')
    f.write('ave_gnad_run '+''.join('%e '%x for x in ave_gnad_run)+'\n')
    f.write('std_gnad_run '+''.join('%e '%x for x in std_gnad_run)+'\n')
    f.write('ave_dec_run '+''.join('%e '%x for x in ave_dec_run*180/pi)+'[deg]\n')
    f.write('std_dec_run '+''.join('%e '%x for x in std_dec_run*180/pi)+'[deg]\n')
    f.write('ave_inc_run '+''.join('%e '%x for x in ave_inc_run*180/pi)+'[deg]\n')
    f.write('std_inc_run '+''.join('%e '%x for x in std_inc_run*180/pi)+'[deg]\n')
    f.write('ave_R_run '+''.join('%e '%x for x in ave_R_run)+'\n')
    f.write('std_R_run '+''.join('%e '%x for x in std_R_run)+'\n')
    f.write('ave_a95_run '+''.join('%e '%x for x in ave_a95_run*180/pi)+'[deg]\n')
    f.write('std_a95_run '+''.join('%e '%x for x in std_a95_run*180/pi)+'[deg]\n')
    f.write('ave_kfish_run '+''.join('%e '%x for x in ave_kfish_run)+'\n')
    f.write('std_kfish_run '+''.join('%e '%x for x in std_kfish_run)+'\n')
    f.write('ave_Feq_run '+''.join('%e '%x for x in ave_Feq_run)+'\n')
    f.write('std_Feq_run '+''.join('%e '%x for x in std_Feq_run)+'\n')
    f.write('dt_crit,dec,inc,g10,gnad %i %e (%e) %e (%e) %e (%e) %e (%e)'
            %(run_len[irun_crit],ave_dec_run[irun_crit]*180/pi,std_dec_run[irun_crit]*180/pi,ave_inc_run[irun_crit]*180/pi,std_inc_run[irun_crit]*180/pi,ave_g10_run[irun_crit],std_g10_run[irun_crit],ave_gnad_run[irun_crit],std_gnad_run[irun_crit]))
    f.close()
    print('Closed '+cwd+'/'+filename)
    
#exit()

#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------
#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------
#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------
#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------
#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------
#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------
#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------
#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------
#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------
#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------
#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------
#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------#####-----------




