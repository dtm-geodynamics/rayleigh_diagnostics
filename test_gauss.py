#!/usr/bin/env python

####################################################################################################
#
#  Compute the PSV of a dynamo model
#  Sample Br(CMB,theta,phi), convert it to Bsurf
#  Plot Intensity vs Inclination (tan(Inc)=2tan(lat))
#  Plot histogram of Intensity over time.
#  Use shell_slices to extract Br.
#
#  Shell-Slice (Shell_Slices) plotting example
#  - Reads in a single Shell_Slice file.
#  - Plots vr, vphi, and entropy
#
#  This example routine makes use of the ShellSlice
#  data structure associated with the Shell_Slices output.
#  Upon initializing a ShellSlice object, the 
#  object will contain the following attributes:
#    ----------------------------------
#    self.niter                                    : number of time steps
#    self.nq                                       : number of diagnostic quantities output
#    self.nr                                       : number of shell slices output
#    self.ntheta                                   : number of theta points
#    self.nphi                                     : number of phi points
#    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
#    self.radius[0:nr-1]                           : radii of the shell slices output
#    self.inds[0:nr-1]                             : radial indices of the shell slices output
#    self.costheta[0:ntheta-1]                     : cos(theta grid)
#    self.sintheta[0:ntheta-1]                     : sin(theta grid)
#    self.vals[0:nphi-1,0:ntheta-1,0:nr-1,0:nq-1,0:niter-1] 
#                                                  : The shell slices 
#    self.iters[0:niter-1]                         : The time step numbers stored in this output file
#    self.time[0:niter-1]                          : The simulation time corresponding to each time step
#    self.version                                  : The version code for this particular output (internal use)
#    self.lut                                      : Lookup table for the different diagnostics output
#    -------------------------------------

#import matplotlib
#matplotlib.use("Agg")

import pylab as p 
import numpy as np
from diagnostic_reading import ShellSlice,ShellSpectra
from diagnostic_tools import file_list, calculate_Ylms, gauss_coef, norm_lm, norm_lm_schmidt, harmonic_transform, inverse_transform, inverse_transform_cian
from diagnostic_plotting import lutlabels,getlabel
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import ticker
from matplotlib.pyplot import ion,plot,show
import argparse
from scipy.special import lpmv, legendre, sph_harm
import sys
pi=np.pi
plt.ion()
r_surf=6371e3
r_cmb=3481e3

# User defined Gauss Coefficient amplitudes
nl=10
gauss_in=np.zeros((nl,nl),dtype='complex')
gauss_in[0,0]=complex(1.,0.)
gauss_in[1,0]=complex(0.,0)
gauss_in[1,1]=complex(1.,0.)
gauss_in[2,0]=complex(0.,0.)
gauss_in[2,1]=complex(1.,0.)
gauss_in[2,2]=complex(pi,0.)

# Compute orthonormal Brlm_cmb from gauss.
print('Compute orthonormal brlm_cmb from gauss')
brlm_cmb=np.zeros((nl,nl),dtype='complex')
for ell in xrange(nl):
    for m in xrange(ell+1):
        # Convert potential to B with -(ell+1), extrapolate surf to cmb, and 
        # convert from schmidt to orthonormal harmonics.
        factor=(-1.)*(ell+1)*(r_surf/r_cmb)**(ell+2)*norm_lm(ell,m)/norm_lm_schmidt(ell,m)
        brlm_cmb[ell,m]=gauss_in[ell,m]*factor

# Convert brlm_cmb to brtp_cmb(theta,phi)
print('Convert brlm_cmb to brtp_cmb')
ntheta=90
nphi=ntheta*2
#costheta=np.linspace(-1,1,ntheta)
costheta, weights = np.polynomial.legendre.leggauss(ntheta)
theta=np.arccos(costheta)
#phi=np.linspace(0,2*pi,nphi+1)
#phi=phi[0:nphi-1]  #remove last value.
phi=np.linspace(0,2*np.pi,nphi+1)[:-1]
brtp_cmb=np.zeros((ntheta,nphi))
brtp_cmb=inverse_transform(brlm_cmb,theta,nphi)
plt.figure()
plt.contourf(brtp_cmb)
plt.colorbar()
plt.title('brtp_cmb')
            
# Convert brtp_cmb to brlm_cmb
print('Convert brtp_cmb to brlm_cmb')
brlm_cmb_transformed=np.zeros((nl,nl),dtype='complex')
brlm_cmb_transformed=harmonic_transform(nl,costheta,phi,brtp_cmb,weights)  #transform btp to blm
"""print('l,m,(brlm_cmb),(brlm_cmb_transformed):')
for ell in xrange(nl):
    for m in xrange(ell+1):
        print(str(ell)+','+str(m)+',(%.4f,%.4f),(%.4f,%.4f)'
              %(brlm_cmb[ell,m].real,brlm_cmb[ell,m].imag,
                brlm_cmb_transformed[ell,m].real,brlm_cmb_transformed[ell,m].imag))"""

# Convert brlm_cmb to gauss_out
print('Convert brlm_cmb to gauss')
gauss_out=gauss_coef(brlm_cmb_transformed)

# Compare gauss_in to gauss_out
gauss_diff=gauss_out-gauss_in
print('l,m,(g_in,h_in),(g_out,h_out)')
for ell in xrange(3):
    for m in xrange(ell+1):
        print(str(ell)+','+str(m)+',('+str(gauss_in[ell,m].real)+','+str(gauss_in[ell,m].imag)+'),('+
              str(gauss_out[ell,m].real)+','+str(gauss_out[ell,m].imag)+')')

        
