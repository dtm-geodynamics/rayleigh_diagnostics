#!/usr/bin/env python

###############################################
#
#  Copied from plot_energy_distro.py  (8/30/17)
#
#  Shell-Averages (Shell_Avgs) plotting example
#  Reads in time steps 3 million through 3.3 million
#  Plots average KE and v' vs. radius
#
#  This example routine makes use of the ShellAverage
#  data structure associated with the Shell_Avg output.
#  Upon initializing a ShellAverage object, the 
#  object will contain the following attributes:
#
#    ----------------------------------
#    self.niter                         : number of time steps
#    self.nq                            : number of diagnostic quantities output
#    self.nr                            : number of radial points
#    self.qv[0:nq-1]                    : quantity codes for the diagnostics output
#    self.radius[0:nr-1]                : radial grid
#
#    self.vals[0:nr-1,0:3,0:nq-1,0:niter-1] : The spherically averaged diagnostics
#                                             0-3 refers to moments (index 0 is mean, index 3 is kurtosis)    
#    self.iters[0:niter-1]              : The time step numbers stored in this output file
#    self.time[0:niter-1]               : The simulation time corresponding to each time step
#    self.version                       : The version code for this particular output (internal use)
#    self.lut                           : Lookup table for the different diagnostics output
#   -------------------------------------

#from rayleigh_diagnostics import ShellAverage
from rayleigh_diagnostics import Shell_Avgs
from diagnostic_plotting import getlabel,momentlabel
from diagnostic_tools import file_list
import matplotlib.pyplot as plt
from matplotlib.pyplot import show,ion,plot
import numpy as np
import argparse
import os
import rayleigh_codes
cwd = os.getcwd()
import matplotlib as mpl
mpl.rc('text', usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
### Check python version
import sys
if sys.version_info[0]==3:
    raw_input=input

parser = argparse.ArgumentParser(description="""Plot radial profiles (Shell_Avgs).""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False,
                    default=None, help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False,
                    default=None, help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-savename',dest='savename',type=str,default='radial_profile.pdf')
parser.add_argument('-q1', '--quantities1', action='store', dest='quantities1', type=int, nargs='*',
                    default=[1,2,3], required=False, help='quantities in panel 1')
parser.add_argument('-q2', '--quantities2', action='store', dest='quantities2', type=int, nargs='*',
                    default=[401], required=False, help='quantities in panel 2')
parser.add_argument('-d', '--default',action='store_true',dest='default',
                    help='use default plotting options',default=False)  #otherwise queue user.
args = parser.parse_args()

# Set saveplot to True to save to a file. 
# Set to False to view plots interactively on-screen.
saveplot = args.save  #False
if not saveplot: plt.ion()
savefile = args.savename #'radial_profile.pdf'  #If pdf gives issues, try .png instead.
window_title = 'radial_profile ('+cwd+')'
# Get file list
files = file_list(path='Shell_Avgs',initial=args.initial)
nfiles = len(files)
print('%i files found'%nfiles)
#a = ShellAverage(files[0],path='')  #a.vals[nr,moment,nq,niter]
a = Shell_Avgs(files[0],path='')  #a.vals[nr,moment,nq,niter]
pi=np.pi
radius = a.radius
rnorm = radius/radius[0] # normalize radius so that upper boundary is r=1
area=4*pi*radius**2
# Get quantities to plot
if not args.default:    #then ask user.
    print('Iterations:'+str(a.niter)+' Quantities:'+str(a.nq)+' Codes:'+str(a.qv))
    user_codes1_in = raw_input('Choose up to 3 codes for first panel (eg 1 2 3):').split(' ')  
    user_codes2_in = raw_input('Choose up to 3 codes for second panel (eg 1 2 3):').split(' ') 
    q1 = [int(num) for num in user_codes1_in]   #make int array.
    q2 = [int(num) for num in user_codes2_in]   #make int array.
    # Choose time
    user_times = raw_input('Enter index of time(s) to plot (ntimes=%i): '%ntimes).split(' ')
    ut = [int(num) for num in user_times]
    print_times=[time0[i] for i in ut]
    print('Plotting at t = %s'%(", ".join([str(time0[i]) for i in ut])))
    # Ask to mult by Area
    area_switch = raw_input('Multiply by area? (n=0,y=1): ')
    if int(area_switch):
        q_coef=area
        label_pref='A*'
else:  #use defaults
    q1 = args.quantities1  
    q2 = args.quantities2
    itime0 = -1; ut=[-1]
    q_coef = 1.; label_pref=''

# Define data array
data = {}  #dictionary
time = {}
icount = 0
# Loop to import data
for i,f in enumerate(files):
    #a = ShellAverage(f,path='')  #a.vals[nr,moment,nq,niter]
    a = Shell_Avgs(f,path='')  #a.vals[nr,moment,nq,niter]
    for iq in a.qv:
        if iq not in data:
            data[iq]=[]
            time[iq]=[]
    for i,d in enumerate(a.time):
        for iq in a.qv:
            data[iq].append(a.vals[:,:,a.lut[iq],i])
            time[iq].append(d)
time0 = time[a.qv[0]]  #get the time array for the first qv.
ntimes = len(time[iq])  #number of time steps outputted.

# Choose moment
#q_moment = 1  #moment of quantity to plot.
#user_moment_in = input('Choose moment to plot (0=mean,1=rms):')
#q_moment = user_moment_in
q1_moment={}
q2_moment={}
for iq in q1:
    if iq < 10: q1_moment[iq]=1  #use rms for vel components.
    else: q1_moment[iq]=0        #use mean for else
for iq in q2:
    if iq < 10: q2_moment[iq]=1  #use rms for vel components.
    else: q2_moment[iq]=0        #use mean for else


# Linewidth
lw = 1.5

if (saveplot):
    plt.figure(1,figsize=(7.5, 4.0), dpi=300)
    plt.rcParams.update({'font.size': 12})
else:
    plt.figure(1,figsize=(15,5),dpi=100)
    plt.rcParams.update({'font.size': 14})

plt.figure(1).canvas.set_window_title(window_title)
xlabel='Radius '+r'(r/r$_{o}$)'

#Plot 1: 
plt.subplot(121)
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
q1_found=0
for iq in q1:
    if iq in data:
        q1_found=1
        for itime in ut:
            plot(rnorm,q_coef*data[iq][itime][:,q1_moment[iq]],label='$\mathrm{'+momentlabel(q1_moment[iq])+'}$ '+label_pref+rayleigh_codes.index_to_tex[iq]+' $\mathrm{'+getlabel(iq).replace("_","~")+'}~(t=%.2f'%(time0[itime])+')$')
if q1_found:
    plt.xlabel(xlabel)
    plt.legend(fontsize=8)
else:
    print('No '+str(getlabel(q1))+' found.')

#Plot 2:
plt.subplot(122)
q2_found=0
for iq in q2:
    if iq in data:
        q2_found=1
        for itime in ut:
            plot(rnorm,q_coef*data[iq][itime][:,q2_moment[iq]],label='$\mathrm{'+momentlabel(q2_moment[iq])+'}$ '+label_pref+rayleigh_codes.index_to_tex[iq]+' $\mathrm{'+getlabel(iq).replace("_","~")+'}~(t=%.2f'%(time0[itime])+')$')
if q2_found:
    plt.xlabel(xlabel)
    plt.legend(fontsize=8)
    #plt.tight_layout()
else:
    print('No '+str(getlabel(q2))+' found.')

if (saveplot):
    plt.savefig(savefile)
    print('Saved '+savefile)
else:
    plt.show()


