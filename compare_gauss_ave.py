#!/usr/bin/env python

### compare_gauss_ave.py 1/4/18  PED       ###
#PURPOSE: compare multiple rayleigh runs #
# using data the run_stats files.        #

import numpy as np
import matplotlib
matplotlib.use('TkAgg')    #this staggers plotting windows.
import matplotlib.pyplot as plt
from matplotlib.pyplot import plot
from diagnostic_tools import read_text_gauss,read_text_gauss_ave,read_text_gavg
import argparse
import os 
cwd = os.getcwd()

r_surf=6371e3
r_core=3481e3
pi=np.pi
mu0=4*pi*1e-7
r_ratio_nd=0.35
r_icb_nd=1/(1./r_ratio_nd-1.)
r_core_nd=1.+r_icb_nd
r_surf_nd=r_core_nd*(r_surf/r_core)
pm=10.

parser = argparse.ArgumentParser(description="""Compare Davies dynamo stats.""")
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-t', '--text', action='store_const', dest='text', const=True, default=False, 
                    required=False, help='print stats to text file')
args = parser.parse_args()
savename='compare_gauss'

def set_plot_fontsize(plt,fontsize=None,xlabel=None,ylabel=None):
    plt.ylabel(ylabels[i],{'fontsize':fontsize})
    plt.xlabel(xlabel,{'fontsize':fontsize})
    plt.yticks(size=fontsize)
    plt.xticks(size=fontsize)

### Read run_stats.txt into data
#'ek1e-3/ra1e6/flux/bot-00.8',\
#,'ek1e-3/ra1e6/flux/bot-02'
dir=['ek1e-3/ra1e6/flux/bot-00.8','ek1e-3/ra1e6/flux/bot-01','ek1e-3/ra1e6/flux/bot-02',\
         'ek1e-3/ra1e6/flux/bot-03','ek1e-3/ra1e6/flux/bot-04','ek1e-3/ra1e6/flux/bot-05',\
         'ek1e-3/ra1e6/flux/bot-06','ek1e-3/ra1e6/flux/bot-07','ek1e-3/ra1e6/flux/bot-08',\
         'ek1e-3/ra1e6/flux/bot-09','ek1e-3/ra1e6/flux/bot-10',\
         'ek1e-3/ra1e6/flux/bot-12']
#files=['/Users/pdriscoll/memex/rayleigh/rayleigh_git/rayleigh/runs/dynamos/maginit7/'+s+'/gauss_ave.txt' for s in dir]
nfiles=len(dir)
# Use function to read text file into structure "a"
a=read_text_gauss_ave(dir)
b=read_text_gauss(dir)
c=read_text_gavg(dir)

nwindows=len(a.win_len[0,:])
n_eq_x_myr=b.n_eq_cross/(b.len_kyr*1e-3)
n_rev_myr=b.nrev/(b.len_kyr*1e-3)
#adipolarity=np.zeros(nfiles)
g10_rms=b.gauss[:,1,0].real
#adipolarity=g10_rms/b.brms
rm=2.*np.sqrt(c.ave_ke)*pm   #magnetic reynolds number
# Compute VDM's.  will be funcs of [nfiles,nruns]
#p_axidip=4*pi*r_surf**3/mu0*np.sqrt(b.gauss[:,1,0].real**2)
p_adip=4.*pi*r_surf_nd**3*a.ave_adip_run   #adip is rms ax dip.
#p_dip=4*pi*r_surf**3/mu0*np.sqrt(b.gauss[:,1,0].real**2+b.gauss[:,1,1].real**2+b.gauss[:,1,1].imag**2)
p_dip=4.*pi*r_surf_nd**3*a.ave_dip_run   #dip is rms dip.  p in units of [1/mu0]?
p_vdm=4.*pi*r_surf_nd**3/2*a.ave_Feq_run*np.sqrt(1+3*np.cos(a.ave_inc_run*pi/180)**2)
colat_dip=np.arctan(2/np.tan(a.ave_inc_run*pi/180))  #using (3.3.3) from Merrill.
#p_vadm=4.*pi*r_surf_nd**3*a.ave_Feq_run/np.sqrt(1+3*np.cos(colat_dip)**2)
p_vadm=4.*pi*r_surf_nd**3*a.ave_Feq_run
# Compute dt_crit for critical alpha_95.
a95_crit=[5,10]
n_a95_crit=len(a95_crit)
dt_a95_crit=np.zeros((n_a95_crit,nfiles))
a95_a95_crit=np.zeros((n_a95_crit,nfiles))
kfish_a95_crit=np.zeros((n_a95_crit,nfiles))
g10_a95_crit=np.zeros((n_a95_crit,nfiles))
dip_a95_crit=np.zeros((n_a95_crit,nfiles))
adip_a95_crit=np.zeros((n_a95_crit,nfiles))
gnad_a95_crit=np.zeros((n_a95_crit,nfiles))
Feq_a95_crit=np.zeros((n_a95_crit,nfiles))
ind_a95_crit=np.zeros((n_a95_crit,nfiles),dtype=int)
for i in range(n_a95_crit):
    for j in range(nfiles):
        if np.min(a.ave_a95_run[j,:])>a95_crit[i]:  #if lowest a95>crit then take index 0.
            ind_a95_crit[i,j]=0
        else:
            ind_a95_crit[i,j]=np.min([np.max(np.where(a.ave_a95_run[j,:]>a95_crit[i]))+1,nwindows-1])  #min ensures < nfiles.
        a95_a95_crit[i,j]=a.ave_a95_run[j,ind_a95_crit[i,j]]
        kfish_a95_crit[i,j]=a.ave_kfish_run[j,ind_a95_crit[i,j]]
        g10_a95_crit[i,j]=a.ave_g10_run[j,ind_a95_crit[i,j]]
        dip_a95_crit[i,j]=a.ave_dip_run[j,ind_a95_crit[i,j]]
        adip_a95_crit[i,j]=a.ave_adip_run[j,ind_a95_crit[i,j]]
        gnad_a95_crit[i,j]=a.ave_gnad_run[j,ind_a95_crit[i,j]]
        Feq_a95_crit[i,j]=a.ave_Feq_run[j,ind_a95_crit[i,j]]
        dt_a95_crit[i,j]=a.win_len[j,ind_a95_crit[i,j]]
# Parse ekman number
ind_ek1e3=np.where(a.ekman==1e-3)[0]
ind_ek3e4=np.where(a.ekman==3e-4)[0]
sym=np.chararray(nfiles) ### Define symbol by Ekman number
sym[ind_ek1e3]='o'
sym[ind_ek3e4]='s'
colors=np.chararray(nfiles)
colors[ind_ek1e3]='r'
colors[ind_ek3e4]='b'

########################################################
### Plot data
plt.ion()
ms=10.  #markersize
nfig=1
fs=(15,10)
fsl=12  #fontsize of legend
xlabel='dT/dr bottom'
fig=plt.figure(nfig,figsize=fs)
plt.subplot(221)
#plots vs bot: KE,ME. Brms. g10. d. ad. Nrev. Neqx. a95. k. inc. dec. F. dt(a95_crit). k(a95_crit)
#plots vs dt_crit?
plt.errorbar(a.bot,c.ave_ke,yerr=c.std_ke,marker='o',markersize=ms,label='KE')
plt.errorbar(a.bot,c.ave_me,yerr=c.std_me,marker='o',markersize=ms,label='ME')
plt.legend(numpoints=1,fontsize=12)
plt.xlabel(xlabel)
plt.ylabel('Energy, vol. rms')
plt.xlim(a.bot[0],a.bot[-1])
plt.subplot(222)
#plot(a.bot,b.brms,'-o',markersize=ms,label='Brms')
plot(a.bot,g10_rms,'-o',markersize=ms,label='g10') #g10 ave over entire run.
plt.xlabel(xlabel)
plt.ylabel('g10 rms')
#plt.subplot(223)
ax1=fig.add_subplot(223)
lns1 = ax1.plot(a.bot,n_eq_x_myr,'-o',markersize=ms,label='eq. crossings')
ax1.set_ylabel('N eq crossing / Myr')
ax1.set_xlabel(xlabel)
axr = ax1.twinx()
lns2 = axr.plot(a.bot,b.nrev/(b.len_kyr*1e-3),'-o',color='g',markersize=ms,label='reversals')
axr.set_ylabel('N reversals / Myr')
lns=lns1+lns2
labs = [l.get_label() for l in lns]
ax1.legend(lns,labs,loc='upper left',numpoints=1,fontsize=12)
plt.subplot(224)
plot(a.bot,b.dipolarity,'-o',markersize=ms,label='full d')
plot(a.bot,b.adipolarity,'-o',markersize=ms,label='axial d')
plt.ylabel('dipolarity (surface)')
plt.xlabel(xlabel)
plt.legend(numpoints=1,fontsize=fsl)
plt.rcParams.update({'font.size': 15})

if (args.save):
    fontsize=20.  #fontsize
    ticksize=20.   #tick label size
    plt.rcParams.update({'font.size': fontsize})
    plt.tight_layout()
    plt.savefig(savename+'_%i.pdf'%nfig)

# Plot Number reverals, eq crossing, length of run
nfig+=1
plt.figure(nfig,figsize=fs)
plt.subplot(221)
plot(a.bot,n_eq_x_myr,'-o',markersize=ms)
plt.xlabel(xlabel)
plt.ylabel('N eq cross / Myr')
plt.subplot(222)
plot(a.bot,n_rev_myr,'-o',markersize=ms)
plt.xlabel(xlabel)
plt.ylabel('N reversals / Myr')
plt.subplot(223)
plot(a.bot,a.times_ra[:,1],'-o',markersize=ms)
plt.xlabel(xlabel)
plt.ylabel('len [kyr]')
if (args.save):
    plt.tight_layout()
    plt.savefig(savename+'_%i.pdf'%nfig)    
        
nfig+=1
plt.figure(nfig,figsize=fs)
#plot all a95 data vs win_len
for i in range(nfiles):
    plt.errorbar(a.win_len[i,:],a.ave_a95_run[i,:],yerr=a.std_a95_run[i,:],marker='o',label='bot-'+str(a.bot[i]),markersize=ms)
plt.legend(fontsize=fsl)
plt.xlabel(r'$\tau$ [kyr]')
plt.ylabel(r'$\alpha_{95}$ [deg]')
plt.ylim(0,np.nanmax(a.ave_a95_run+a.std_a95_run)*1.2)
if (args.save):
    plt.tight_layout()
    plt.savefig(savename+'_%i.pdf'%nfig)
    
nfig+=1
plt.figure(nfig,figsize=fs)
# Results with a critical a95.
plt.subplot(221)
for i in range(n_a95_crit):
    plot(a.bot,dt_a95_crit[i,:],'-o',markersize=ms,label=r'$\alpha_{95}=%i$'%a95_crit[i])
plt.legend(numpoints=1,loc='upper left')#,fontsize=fsl)
plt.xlabel(xlabel)
plt.ylabel(r'$\tau_{crit}$ [kyr]')
plt.ylim(0,np.max(dt_a95_crit)*1.2)
plt.subplot(222)
for i in range(n_a95_crit):
    plot(a.bot,kfish_a95_crit[i,:],'-o',markersize=ms,label=r'$\alpha_{95}=%i$'%a95_crit[i])
plt.legend(numpoints=1,loc='upper right')#,fontsize=fsl)
plt.xlabel(xlabel)
#plt.ylabel(r'k fisher ($\alpha_{95,crit}$)')
plt.ylabel(r'$k (\tau_{crit})$')
plt.subplot(223)
for i in range(n_a95_crit):
    plot(a.bot,g10_a95_crit[i,:],'-o',markersize=ms,label=r'$\alpha_{95}=%i$'%a95_crit[i])
plt.legend(numpoints=1,loc='upper right')#,fontsize=fsl)
plt.xlabel(xlabel)
#plt.ylabel(r'g10 ($\alpha_{95,crit}$)')
plt.ylabel(r'$g_{1,0} (\tau_{crit})$')
plt.subplot(224)
for i in range(n_a95_crit):
    plot(a.bot,gnad_a95_crit[i,:],'-o',markersize=ms,label=r'$\alpha_{95}=%i$'%a95_crit[i])
plt.legend(numpoints=1,loc='upper right')#,fontsize=fsl)
plt.xlabel(xlabel)
#plt.ylabel(r'gnad ($\alpha_{95,crit}$)')
plt.ylabel(r'$g_{NAD} (\tau_{crit})$')

if (args.save):
    plt.tight_layout()
    plt.savefig(savename+'_%i.pdf'%nfig)


# Plot Feq, VADM, and VDM.  where VDM uses observed inc?  VADM assumes observation at equator?
nfig+=1
plt.figure(nfig,figsize=fs)
plt.subplot(221)
for i in range(n_a95_crit):
    plot(a.bot,Feq_a95_crit[i,:],'-o',markersize=ms,label=r'$\alpha_{95}=%i$'%a95_crit[i])
plt.xlabel(xlabel)
#plt.ylabel(r'F at eq. ($\alpha_{95,crit}$)')
plt.ylabel(r'F at eq. ($\tau_{crit}$)')
plt.legend(numpoints=1,loc='upper right')#,fontsize=fsl)
plt.subplot(222)
for i in range(n_a95_crit):
    plot(a.bot,dip_a95_crit[i,:],'-o',markersize=ms,label=r'$\alpha_{95}=%i$'%a95_crit[i])
plt.xlabel(xlabel)
#plt.ylabel(r'rms dip ($\alpha_{95,crit}$)')
plt.ylabel(r'rms dip ($\tau_{crit}$)')
plt.legend(numpoints=1,loc='upper right')#,fontsize=fsl)
plt.subplot(223)
for i in range(n_a95_crit):
    plot(a.bot,adip_a95_crit[i,:],'-o',markersize=ms,label=r'$\alpha_{95}=%i$'%a95_crit[i])
plt.xlabel(xlabel)
#plt.ylabel(r'rms ax dip ($\alpha_{95,crit}$)')
plt.ylabel(r'rms ax dip ($\tau_{crit}$)')
plt.legend(numpoints=1,loc='upper right')#,fontsize=fsl)
if (args.save):
    plt.tight_layout()
    plt.savefig(savename+'_%i.pdf'%nfig)

# Plot VDM all with dt=50 kyr
ind_50=np.where(a.win_len[0,:]==50.)[0][0]
ind_100=np.where(a.win_len[0,:]==100.)[0][0]
ind_500=np.where(a.win_len[0,:]==500.)[0][0]
nfig+=1
plt.figure(nfig,figsize=fs)
plt.subplot(221)
plot(a.bot,p_dip[:,ind_50],'-o',markersize=ms,label='DM')  #known value
plot(a.bot,p_vdm[:,ind_50],'-o',markersize=ms,label='VDM')  #inferred value
plt.xlabel(xlabel)
plt.ylabel(r'Dipole Moment') # $p_{DM}$')
plt.legend(numpoints=1,fontsize=fsl)
plt.subplot(222)
plot(a.bot,p_adip[:,ind_50],'-o',markersize=ms,label='ADM')
plot(a.bot,p_vadm[:,ind_50],'-o',markersize=ms,label='VADM')
plt.xlabel(xlabel)
plt.ylabel(r'Ax. Dipole Moment') #$p_{ADM}$')
plt.legend(numpoints=1,fontsize=fsl)
# Plot ratios: inferred/known
plt.subplot(223)
#plt.rc('text', usetex=True)
plot(a.bot,p_vdm[:,ind_50]/p_dip[:,ind_50],'-o',markersize=ms,label=r'$\tau=50$ kyr')
plot(a.bot,p_vdm[:,ind_100]/p_dip[:,ind_100],'-o',markersize=ms,label=r'$\tau=100$ kyr')
plot(a.bot,p_vdm[:,ind_500]/p_dip[:,ind_500],'-o',markersize=ms,label=r'$\tau=500$ kyr')
plt.axhline(y=1,color='gray')
plt.xlabel(xlabel)
plt.ylabel(r'$p_{VDM}/p_{DM}$')
plt.ylim(0.5,1.5)
plt.legend(numpoints=1,fontsize=fsl)
plt.subplot(224)
plot(a.bot,p_vadm[:,ind_50]/p_adip[:,ind_50],'-o',markersize=ms,label=r'$\tau=50$ kyr')
plot(a.bot,p_vadm[:,ind_100]/p_adip[:,ind_100],'-o',markersize=ms,label=r'$\tau=100$ kyr')
plot(a.bot,p_vadm[:,ind_500]/p_adip[:,ind_500],'-o',markersize=ms,label=r'$\tau=500$ kyr')
plt.axhline(y=1,color='gray')
plt.xlabel(xlabel)
plt.ylabel(r'$p_{VADM}/p_{ADM}$')
plt.ylim(0.5,1.5)
plt.legend(numpoints=1,fontsize=fsl)
if (args.save):
    plt.tight_layout()
    plt.savefig(savename+'_%i.pdf'%nfig)


## Compare dip,adip,Feq ##
nfig+=1
plt.figure(nfig,figsize=fs)
plot(a.bot,a.ave_dip_run[:,ind_50],'-o',markersize=ms,label='dip')
plot(a.bot,a.ave_adip_run[:,ind_50],'-o',markersize=ms,label='adip')
plot(a.bot,a.ave_Feq_run[:,ind_50],'-o',markersize=ms,label='Feq')
plt.legend(numpoints=1)


## 4 Panel all versus N_rev ##
nfig+=1
plt.figure(nfig,figsize=fs)
plt.subplot(221)
xlabel='N reversals / Myr'  #'N eq cross / Myr'  #'N rev / Myr'
xvar=n_rev_myr  #n_eq_x_myr    #n_rev_myr
for i in range(n_a95_crit):
    plot(xvar,dt_a95_crit[i,:],'o',markersize=ms,label=r'$\alpha_{95}=%i$'%a95_crit[i])
plt.legend(numpoints=1,loc='upper left')#,fontsize=fsl)
plt.xlabel(xlabel)
plt.ylabel(r'$\tau_{crit}$ [kyr]')
plt.ylim(0,np.max(dt_a95_crit)*1.2)
plt.subplot(222)
plot(xvar,a.ave_kfish_run[:,ind_50],'o',markersize=ms)
plt.xlabel(xlabel)
plt.ylabel(r'$k$ ($\tau=50$ kyr)')
plt.subplot(223)
ratio_dm=p_vdm/p_dip
ratio_adm=p_vadm/p_adip
# Linear fit: VDM/DM=a+b*(rev_freq)
fit_dm=np.polyfit(n_rev_myr,ratio_dm[:,ind_50],1)
fit_adm=np.polyfit(n_rev_myr,ratio_adm[:,ind_50],1)
plot(xvar,ratio_dm[:,ind_50],'o',markersize=ms)
plt.axhline(y=1,color='gray')
plt.xlabel(xlabel)
plt.ylabel(r'$p_{VDM}/p_{DM}$ ($\tau=50$ kyr)')
plt.ylim(0.5,1.5)
x=np.arange(0,10,1)  #range of rev_freq
plot(x,fit_dm[1]+fit_dm[0]*x,label='a=%.2f, b=%.2f'%(fit_dm[1],fit_dm[0]))
plt.legend(loc='upper left')
plt.subplot(224)
plot(xvar,p_vadm[:,ind_50]/p_adip[:,ind_50],'o',markersize=ms)
plt.axhline(y=1,color='gray')
plt.xlabel(xlabel)
plt.ylabel(r'$p_{VADM}/p_{ADM}$ ($\tau=50$ kyr)')
plt.ylim(0.5,1.5)
plot(x,fit_adm[1]+fit_adm[0]*x,label='a=%.2f, b=%.2f'%(fit_adm[1],fit_adm[0]))
plt.legend(loc='upper left')
if (args.save):
    plt.tight_layout()
    plt.savefig(savename+'_%i.pdf'%nfig)


## 4 Panel Rm vs tau_ave, dTdr_bot ##
nfig+=1
plt.figure(nfig,figsize=fs)
plt.subplot(221)
for i in range(n_a95_crit):
    plot(rm,dt_a95_crit[i,:],'-o',markersize=ms,label=r'$\alpha_{95}=%i$'%a95_crit[i])
plt.xlabel(r'$Rm$')
plt.ylabel(r'$\tau_{crit}$ [kyr]')
plt.legend(numpoints=1,loc='upper left')
plt.subplot(222)
plot(a.bot,rm,'-o',markersize=ms)
plt.xlabel(r'$dT/dr_{bot}$')
plt.ylabel(r'$Rm$')

### Print stats to text file ####
if args.text:
    textfile='compare_gauss_ave.txt'
    f=open(cwd+'/'+textfile,'w')
    f.write(cwd+' \\\ \n')  #write dir first
    f.write(r'$dT/dr_i$ & $KE$ & $ME$ & kyr & $N_{eq}$ & $N_{rev}$ & $\tau_{crit}(\alpha_{95}<10)$ & $VDM/DM$ & $VADM/ADM$ \\'+' \n')
    for i in range(nfiles):
        f.write(r'%.1f & %i & %i & %i & %i & %i & %i & %.2f & %.2f'%(a.bot[i],c.ave_ke[i],c.ave_me[i],\
                 b.len_kyr[i],b.n_eq_cross[i],b.nrev[i],dt_a95_crit[1,i],\
                 p_vdm[i,ind_50]/p_dip[i,ind_50],p_vadm[i,ind_50]/p_adip[i,ind_50]))
        if i<nfiles-1:
            f.write(r' \\ ')
        f.write('\n')
    f.close()
    print('Closed '+cwd+'/'+textfile+'.')
