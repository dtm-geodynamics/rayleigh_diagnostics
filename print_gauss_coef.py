#!/usr/bin/env python

####################################################################################################
#
# print_gauss_coef.py
# 1) Read in spectral components from ShellSpectra.
# 2) Extrapolate from CMB to surface.
# 3) Print Gauss coefficients to text file.
#
####################################################################################################

import pylab as p 
import numpy as np
from diagnostic_reading import ShellSlice,ShellSpectra
from diagnostic_tools import file_list, calculate_Ylms,renormalize_shell_spectra,gauss_coef
from diagnostic_plotting import lutlabels,getlabel
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import ticker
from matplotlib.pyplot import ion,plot,show
import argparse
import os
import shutil
ion()

path='Shell_Spectra'
filespec = file_list(path=path)
nfilespec=len(filespec)

#Read in last shell slice
s = ShellSpectra(filespec[nfilespec-1],path='')  #vals[0:lmax,0:mmax,0:nr-1,0:nq-1,0:niter-1]

#Default Plotting options.
user_codes = [401,402,403]  #Br,Btheta,Bphi
rad_inds = 1.   #use CMB only.

#NEXT: print the label for each code.  for this we need to create a look up table for labels, like p.labels.
q=lutlabels(user_codes)
print(q.labels)

#Identify the variables indices for vr,vphi, and entropy
var_ind = s.lut[user_codes]
vnames=q.labels

# Extract data
nl = s.nell
nm = s.nm
bsr=np.zeros([nl,nm,0])
bst=bsr
bsp=bsr
time = np.zeros([0])
times = np.zeros([0])  #time from shellspectrum
print('Reading in data')
# Read in Shell_Spectra
for ifile,f in enumerate(filespec):
    s=ShellSpectra(filename=f,path="")  #returns harmonic amplitudes.
    if ifile==0:
        bsr=s.vals[:,:,0,var_ind[0],:]  #complex
        bst=s.vals[:,:,0,var_ind[1],:]
        bsp=s.vals[:,:,0,var_ind[2],:]
        times = s.time
    else:
        bsr=np.append(bsr,s.vals[:,:,0,var_ind[0],:],axis=2)  #complex
        bst=np.append(bst,s.vals[:,:,0,var_ind[1],:],axis=2)
        bsp=np.append(bsp,s.vals[:,:,0,var_ind[2],:],axis=2)
        times=np.append(times,s.time)               
ntimes=len(times)
#inc = np.arctan(br/bt)  #inclination.
pm=10.
print('Scaling time to mag diff assuming Pm='+str(pm))
times_mag=times/pm
ntimes_mag=ntimes   #shorten times here for testing.
times_mag=times_mag[:ntimes_mag]
print('Computing Gauss for ntimes='+str(ntimes_mag))
bsr_on=np.zeros((nl,nm,ntimes_mag),dtype=complex)
glm=np.zeros((nl,nm,ntimes_mag),dtype=complex)
for itime in range(ntimes_mag):
    # Convert from Rayleigh harmonics to orthonormal
    bsr_on[:,:,itime]=renormalize_shell_spectra(bsr[:,:,itime])
    # compute Gauss coef
    glm[:,:,itime] = gauss_coef(bsr_on[:,:,itime])

print('Writing to File')
# Print to local File.
nl_write=32  #number of ell to write to file.
gauss_file='gauss_coef.txt'
time_file='gauss_time.txt'
fg=open(path+'/'+gauss_file,'w')
ft=open(path+'/'+time_file,'w')
# Print format:
# time
# l m glm
for itime,t in enumerate(times_mag):
    ft.write('%e \n'%(t))
    for ell in range(1,nl_write+1):
        for m in range(ell+1):
            fg.write('%i %i %e \n'%(ell,m,glm[ell,m,itime].real))
            if m>0: fg.write('%i %i %e \n'%(ell,m,glm[ell,m,itime].imag))

fg.close()
ft.close()
print('Local files closed.')

print('Copying to dropbox.')
# Copy over to another directory in dropbox.
# Parse pwd into parameters
pwd=os.getcwd()
pwd_list=os.getcwd().split('/')[12:]
ffff=0.  #flag for fixed flux
if any('flux' in s for s in pwd_list):  # if 'flux' is in path then its ffff
    ffff=1
# Copy files to Dropbox for Chris.
path_dbox='/Users/pdriscoll/Dropbox/research_dropbox/dynamos/iceland/rayleigh/'
dtdr_bot=''  #string for dTdr_bot.
dtdr_bot_crit_ffff=-0.007  #make sure this is right!
dtdr_bot_crit_fft=0.       #need to compute this.
supercrit_factor=28.57        #dtdr_supercrit/ra_supercrit factor
if ffff:   #if 'flux' in pwd_list then filestructure is ra5e6/flux/botX...
    dtdr_bot=float(pwd_list[1].split('bot')[1])  #extract X from botX
    ra_super='%.2f'%(dtdr_bot/dtdr_bot_crit_ffff/supercrit_factor)
    path_run='E=3e-4/Pm=10/Pr=1/NS/FFFF/BH/eps=1/Ra='+str(ra_super)
    # NS stands for no-slip.  FFFF are the thermal BC's on each boundary (FF on both in this case).  BH = bottom heating
if not ffff:     #if 'flux' not in pwd_list then filestructure is ra5e6/botX...
    dtdr_bot=float(pwd_list[0].split('bot')[1])   #extract X from botX
    ra_super=str(dtdr_bot/dtdr_bot_crit_fft)
    path_run='E=3e-4/Pm=10/Pr=1/NS/FFT/BH/eps=0/Ra='+str(ra_super)
full_path=path_dbox+path_run
# Check if path exists, if not create it.
if not os.path.exists(full_path):
    os.makedirs(full_path)
# Print run info to gauss_info.txt
info_file='gauss_info.txt'
finfo=open(path+'/'+info_file,'w')
finfo.write('data path: '+pwd+' \n')
finfo.write('gauss path: '+full_path)
finfo.close()
# Copy files over to gauss path
shutil.copy(path+'/'+gauss_file,full_path+'/'+gauss_file)
shutil.copy(path+'/'+time_file,full_path+'/'+time_file)
shutil.copy(path+'/'+info_file,full_path+'/'+info_file)
print('copied '+path+'/'+gauss_file+' to '+full_path+'/'+gauss_file)
print('copied '+path+'/'+time_file+' to '+full_path+'/'+time_file)
print('copied '+path+'/'+info_file+' to '+full_path+'/'+info_file)
