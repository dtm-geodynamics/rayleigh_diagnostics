! A test program to print out Rayleigh's Plms

program print_plms
  use legendre_polynomials
  implicit none
  integer :: ntheta = 120
  integer, parameter :: lmax = 79
  integer :: i, m, l, iodd, ieven
  integer :: mval(lmax+1)
  logical :: parity_in = .true.

  mval = (/(i,i=0,lmax)/)

  call initialize_legendre(ntheta, lmax, mval, parity_in)

  open (unit=7, file="plms.txt")
  do m = 1,lmax+1
    do l = 1,m-1
      do i = 1, ntheta
        write(7, '(E22.15E3A$)') 0.0, " "
      end do
      write(7, '(A)') ""
    end do
    iodd = 1
    ieven = 1
    do l = m,lmax+1
      if(mod(l-m,2).eq.1) then
        ! odd
        do i = 1, ntheta/2
          write(7, '(E22.15E3A$)') p_lm_odd(m)%data(iodd, i), " "
        end do
        do i = 1, ntheta/2
          write(7, '(E22.15E3A$)') -1.0d0*p_lm_odd(m)%data(iodd, ntheta/2-i+1), " "
        end do
        iodd = iodd + 1
      else
        ! even
        do i = 1, ntheta/2
          write(7, '(E22.15E3A$)') p_lm_even(m)%data(ieven, i), " "
        end do
        do i = 1, ntheta/2
          write(7, '(E22.15E3A$)') p_lm_even(m)%data(ieven, ntheta/2-i+1), " "
        end do
        ieven = ieven + 1
      end if
      write(7, '(A)') ""
    end do
  end do

  call finalize_legendre()

end program print_plms

