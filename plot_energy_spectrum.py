#!/usr/bin/env python

###############################################
#
#  Global-Averages (G_Avgs) plotting
#  Plots frequency spectrum of KE and ME
#
#  This example routine makes use of the GlobalAverage
#  data structure associated with the G_Avg output.
#  Upon initializing a GlobalAverage object, the 
#  object will contain the following attributes:
#
#    ----------------------------------
#    self.niter                  : number of time steps
#    self.nq                     : number of diagnostic quantities output
#    self.qv[0:nq-1]             : quantity codes for the diagnostics output
#    self.vals[0:niter-1,0:nq-1] : Globally averaged diagnostics as function of time and quantity index
#    self.iters[0:niter-1]       : The time step numbers stored in this output file
#    self.time[0:niter-1]        : The simulation time corresponding to each time step
#    self.lut                    : Lookup table for the different diagnostics output


from diagnostic_reading import GlobalAverage
from diagnostic_tools import file_list
import matplotlib.pyplot as plt
from matplotlib.pyplot import plot,ion,show
import numpy as np
import argparse

parser = argparse.ArgumentParser( \
                       description="""Plot global averages (G_Avgs):frequency spectrium KE and ME.""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False, default=None, 
                    help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False, default=None, 
                    help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
args = parser.parse_args()


# Set saveplot to True to save to a .png file. 
# Set to False to view plots interactively on-screen.
saveplot = args.save
savefile = 'energy_spectrum.pdf'  #Change .pdf to .png if pdf conversion gives issues

# Get filenames
files = file_list(args.initial, args.final, path='G_Avgs')
# If all files used (ie args.initial and .final not set) then
# exclude first file (assuming it's initial adjustment period).
nfiles=len(files)
if nfiles>1:
    if not args.initial and not args.final:
        files=files[1:]

#The indices associated with our various outputs are stored in a lookup table
#as part of the GlobalAverage data structure.  We define several variables to
#hold those indices here:
a = GlobalAverage(filename=files[0],path='')  # read a file to get the lookup table
ke_index  = a.lut[125]  # Kinetic Energy (KE)
rke_index = a.lut[126]  # KE associated with radial motion
tke_index = a.lut[127]  # KE associated with theta motion
pke_index = a.lut[128]  # KE associated with azimuthal motion
#We also grab some energies associated with the mean (m=0) motions
mrke_index = a.lut[130]  # KE associated with mean radial motion
mtke_index = a.lut[131]  # KE associated with mean theta motion
mpke_index = a.lut[132]  # KE associated with mean azimuthal motion
#Magnetic Energies
me_index  = a.lut[475]  # Magnetic Energy (ME)
rme_index = a.lut[476]  # associated with radial motion
tme_index = a.lut[477]  # associated with theta motion
pme_index = a.lut[478]  # associated with azimuthal motion

#Next, we intialize some empy lists to hold those variables
ke = []
rke = []
tke = []
pke = []
mrke = []
mtke = []
mpke = []
me = []
rme = []
tme = []
pme = []

# We also define an empty list to hold the time.  This time
# will be absolute time since the beginning of the run, 
# not since the beginning of the first output file.  
# In this case, the run was dimensional, so the time is recorded 
# in seconds.  We will change that to days as we go.
alldays = []
timescale=1.  #(3600.0*24.0)

# Next, we loop over all files and grab the desired data
# from the file, storing it in appropriate lists as we go.
for f in files:
    a = GlobalAverage(filename=f,path='')


    days = a.time/timescale
    for i,d in enumerate(days):
        alldays.append(d)
        ke.append(a.vals[i,ke_index])
        rke.append(a.vals[i,rke_index])
        tke.append(a.vals[i,tke_index])
        pke.append(a.vals[i,pke_index])

        mrke.append(a.vals[i,mrke_index])
        mtke.append(a.vals[i,mtke_index])
        mpke.append(a.vals[i,mpke_index])
        
        me.append(a.vals[i,me_index])
        rme.append(a.vals[i,rme_index])
        tme.append(a.vals[i,tme_index])
        pme.append(a.vals[i,pme_index])

time=np.asarray(alldays)
t_min=min(time)
t_max=max(time)
t_ra=t_max-t_min
#Ask user to choose number freq.
#user_nfreq_str_arr = raw_input('Enter number of freq:')
#user_nfreq = int(user_nfreq_str_arr)
#user_nfreq=10
#print("nfreq:"+str(user_nfreq))
#fft_timestep=t_ra/user_nfreq
        
# Create Spectrum using FFT
nt=len(time)
fft_dt=t_ra/nt  #ave time step
fft_dt_max=max(time[1:]-time[:-1])
freq_fft_dt=1./fft_dt   #freq of ave time step
freq_fft_dt_max=1./fft_dt_max   #freq of ave time step
fft_ke=abs(np.fft.fft(ke))
fft_ke_norm=fft_ke/sum(fft_ke)
fft_me=abs(np.fft.fft(me))
fft_me_norm=fft_me/sum(fft_me)
freq=np.fft.fftfreq(nt,fft_dt)
freq_min=freq[1]  #set min freq to first non zero value?
#Find nearest freq to max.
i_fmax=(np.abs(freq-freq_fft_dt_max)).argmin()
i_fmin=(np.abs(freq-freq_min)).argmin()
print('(fmin,fmax)=(%8.2e,%8.2e)'%(freq_min,freq_fft_dt_max))
print('KE(fmax)/KE(fmin)=%8.2e , ME(fmax)/ME(fmin)=%8.2e'%(fft_ke_norm[i_fmax]/fft_ke_norm[i_fmin],fft_me_norm[i_fmax]/fft_me_norm[i_fmin]))

#Create two plots.
# Plot 1:  Total KE and its breakdown.
# Plot 2:  Breakdown of the mean KE

#Changing the plotting parameter somewhat depending on saveplot
if (saveplot):
    plt.figure(1,figsize=(7.5, 4.0), dpi=300)
    plt.rcParams.update({'font.size': 12})
else:
    plt.figure(1,figsize=(15,5),dpi=100)
    plt.rcParams.update({'font.size': 14})

plt.subplot(121)
plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
plt.plot(freq[0:nt/2.],fft_ke_norm[0:nt/2.],'black', label = 'KE'+r'$_{total}$')
#plt.plot(alldays,rke,'r', label = 'KE'+r'$_{rad}$')
#plt.plot(alldays,tke,'g', label = 'KE'+r'$_\theta$')
#plt.plot(alldays,pke,'b', label = 'KE'+r'$_\phi$')
plt.xscale('log')
plt.yscale('log')
#plt.xlim([1,freq_fft_dt])
axes = plt.gca()
#axes.set_xlim([1,freq_fft_dt_max])
axes.set_xlim([freq_min,freq_fft_dt_max])
plt.xlabel('1/Time')  # (days)')
plt.ylabel('Energy Density ')  #+r'( erg cm$^{-3}$)')
#if (saveplot):
#    legend = plt.legend(loc='upper right', shadow=True, ncol = 2, fontsize = 'x-small') 
#else:
#    legend = plt.legend(loc='upper right', shadow=True, ncol = 2) 

# Plot Magnetic Energy Spectrum
plt.subplot(122)
plt.plot(freq[0:nt/2.],fft_me_norm[0:nt/2.],'black', label = 'ME'+r'$_{total}$')
plt.xscale('log')
plt.yscale('log')
axes = plt.gca()
#axes.set_xlim([1,freq_fft_dt_max])
axes.set_xlim([freq_min,freq_fft_dt_max])
plt.xlabel('1/Time')  # (days)')
plt.ylabel('Magnetic Energy Density ')
#if (saveplot):
#    legend = plt.legend(loc='upper right', shadow=True, ncol = 2, fontsize = 'x-small') 
#else:
#    legend = plt.legend(loc='upper right', shadow=True, ncol = 2) 

plt.tight_layout()

if (saveplot):
    plt.savefig(savefile)
else:
    plt.show()
