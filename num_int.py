#!/usr/bin/env python

import numpy as np
from scipy.integrate import quad, dblquad
import scipy.integrate as integrate
from scipy.special import lpmv, sph_harm
import scipy.misc
from diagnostic_tools import Ylmtp, norm_lm
factorial=scipy.misc.factorial
pi=np.pi

def int_sintheta(theta,a,b):
    return np.sin(theta)
def int_phi(phi):
    return 1

a=2
b=1
I_theta=quad(int_sintheta,0,pi,args=(a,b))
print(I_theta)
I_phi=quad(int_phi,0,2*pi)
I_product=I_theta[0]*I_phi[0]
print(I_phi)
print('I_theta*I_phi='+str(I_product)+', 4pi='+str(4*pi)+', diff='+str(I_product-4*pi))

def f(x,y,a,b):
    return a*np.sin(b*x)*1

D_int=integrate.nquad(f,[[0,pi],[0,2*pi]],args=(2,1))
print(D_int)
print(a*4*pi)

# Romberg
R_int=integrate.romberg(int_sintheta,0,pi,args=(1,1))
print(R_int)

# Simpsons
nphi=180+1
phi=np.linspace(0,2*pi,nphi)
dphi=2*pi/nphi
def eimphi(phi,m):
    res=np.cos(m*phi)+1j*np.sin(m*phi)
    return res
def eimphi2(phi,m):
    res=eimphi(phi,m)*eimphi(phi,-m)
    return res
f0=eimphi(phi,0)
print('m=0:'+str(integrate.simps(eimphi(phi,0),dx=dphi)))
print('m=1:'+str(integrate.simps(eimphi(phi,1),dx=dphi)))
print('m=2:'+str(integrate.simps(eimphi(phi,2),dx=dphi)))
print('m=+-1:'+str(integrate.simps(eimphi2(phi,1),dx=dphi)))
print('m=+-2:'+str(integrate.simps(eimphi2(phi,2),dx=dphi)))

# Legendre part
def plm2(l,m,costheta):
    res=lpmv(m,l,costheta)*lpmv(m,l,costheta)   #NOTE: no -m here.
    return res
def plm2_expected(l,m,costheta):
    return factorial(l+m)/factorial(l-m)*2./(2*l+1)  #Lea (8.55)
ntheta=90
dcostheta=2./ntheta  #??????
costheta=np.linspace(-1,1,ntheta)
theta=np.arccos(costheta)
nphi=2*ntheta
phi=np.linspace(0,2*pi,nphi+1)
print('Legendre: l,m,int(plm*plm),expected')
for ell in xrange(3):
    for m in xrange(ell+1):
        print(str(ell)+','+str(m)+','+str(integrate.simps(plm2(ell,m,costheta),x=costheta))+','+str(plm2_expected(ell,m,costheta)))
        

def legendre_transform(l,m,costheta,gt):
    # Legendre transform
    # gt is an arbitrary function of theta.
    return np.sqrt((2.*l+1)/2*factorial(l-m)/factorial(l+m))*lpmv(m,l,costheta)*gt
def norm_alp(l,m):           
    #Norm of associated Legendre polynomials.
    #1/sqrt of Lea 8.55.  this is the theta bit of the Ylm orthonormalization.
    return np.sqrt((2.*l+1)/2*factorial(l-m)/factorial(l+m))
def fourier_transform(m,phi,gp):
    # Fourier transform
    # gp is an arbitrary function of phi.
    return (1./np.sqrt(2*pi))*(np.cos(m*phi)+1j*np.sin(m*phi))*gp
def harmonic_transform(nl,costheta,phi,gtp):
    # Transform arbitrary function gtp[ntheta,nphi]
    # to glm[nl,nl]
    ntheta=len(costheta)
    theta=np.arccos(costheta)
    nphi=len(phi)
    sintheta=np.sin(theta)
    glm=np.zeros((nl,nl),dtype='complex')
    for ell in xrange(nl):
        for m in xrange(ell+1):
            I_theta=np.zeros(nphi,dtype=complex)
            for iphi in xrange(nphi):
                gt=np.zeros(ntheta,dtype=complex)
                gt=gtp[:,iphi]
                I_theta[iphi]=integrate.simps(legendre_transform(ell,m,costheta,gt),x=costheta)
            glm[ell,m]=integrate.simps(fourier_transform(m,phi,I_theta),x=phi)
    return glm

print('Test Orthonormality of Transforms: l,m,Legendre,Fourier')
for ell in xrange(3):
    for m in xrange(ell+1):
        gt=lpmv(m,ell,costheta)*norm_alp(ell,m)
        leg_res=integrate.simps(legendre_transform(ell,m,costheta,gt),x=costheta)
        gp=eimphi(phi,-m)/np.sqrt(2*pi)
        fou_res=integrate.simps(fourier_transform(m,phi,gp),x=phi)
        print(str(ell)+','+str(m)+','+str(leg_res)+','+str(fou_res))

print('Test orthonormality of harmonic_transform:')
for ell in xrange(3):
    for m in xrange(ell+1):
        gtp=complex(1.,1.)*Ylmtp(ell,-m,theta,phi)
        harm_res=harmonic_transform(5,costheta,phi,gtp)
        print(str(ell)+','+str(m)+','+str(harm_res[ell,m]))
