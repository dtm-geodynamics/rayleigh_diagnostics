#!/usr/bin/env python

####################################################################################################
#
#  Compute the PSV of a dynamo model
#  Sample Br(CMB,theta,phi), convert it to Bsurf
#  Plot Intensity vs Inclination (tan(Inc)=2tan(lat))
#  Plot histogram of Intensity over time.
#  Use shell_slices to extract Br.
#
#  Shell-Slice (Shell_Slices) plotting example
#  - Reads in a single Shell_Slice file.
#  - Plots vr, vphi, and entropy
#
#  This example routine makes use of the ShellSlice
#  data structure associated with the Shell_Slices output.
#  Upon initializing a ShellSlice object, the 
#  object will contain the following attributes:
#    ----------------------------------
#    self.niter                                    : number of time steps
#    self.nq                                       : number of diagnostic quantities output
#    self.nr                                       : number of shell slices output
#    self.ntheta                                   : number of theta points
#    self.nphi                                     : number of phi points
#    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
#    self.radius[0:nr-1]                           : radii of the shell slices output
#    self.inds[0:nr-1]                             : radial indices of the shell slices output
#    self.costheta[0:ntheta-1]                     : cos(theta grid)
#    self.sintheta[0:ntheta-1]                     : sin(theta grid)
#    self.vals[0:nphi-1,0:ntheta-1,0:nr-1,0:nq-1,0:niter-1] 
#                                                  : The shell slices 
#    self.iters[0:niter-1]                         : The time step numbers stored in this output file
#    self.time[0:niter-1]                          : The simulation time corresponding to each time step
#    self.version                                  : The version code for this particular output (internal use)
#    self.lut                                      : Lookup table for the different diagnostics output
#    -------------------------------------

#import matplotlib
#matplotlib.use("Agg")

import pylab as p 
import numpy as np
from diagnostic_reading import ShellSlice,ShellSpectra
from diagnostic_tools import file_list, calculate_Ylms, gauss_coef, norm_lm, norm_lm_schmidt, harmonic_transform, inverse_transform, inverse_transform_cian, renormalize_shell_spectra
from diagnostic_plotting import lutlabels,getlabel
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import ticker
from matplotlib.pyplot import ion,plot,show
import argparse
from scipy.special import lpmv, legendre
pi=np.pi
plt.ion()

parser = argparse.ArgumentParser( \
                       description="""Plot PSV.""")
parser.add_argument('-i', '--initial', action='store', type=str, dest='initial', required=False, default=None, 
                    help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=str, dest='final', required=False, default=None, 
                    help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-d', '--default',action='store_true',dest='default',help='use default plotting options',
                    default=False)
args = parser.parse_args()

if args.default: print("default set")

# Set saveplot to True to save to a .png file. 
# Set to False to view plots interactively on-screen.
saveplot = args.save
savefile = 'shell_slice.png'

rnorm = 1./(1-0.35)
path='Shell_Slices'
files = file_list(args.initial, args.final, path=path)
nfiles=len(files)
path='Shell_Spectra'
filespec = file_list(args.initial, args.final, path=path)
nfilespec=len(filespec)

#Read in last shell slice
a=ShellSlice('/01000000',path='Shell_Slices')
#a = ShellSlice(filename=files[nfiles-1],path='')
# Read in Shell_Spectra
s=ShellSpectra(filename='/01000000',path='Shell_Spectra')  #vals[0:lmax,0:mmax,0:nr-1,0:nq-1,0:niter-1]

#Default Plotting options.
user_codes = [401,402,403]  #Br,Btheta,Bphi
rad_inds = 1.   #use CMB only.
q=lutlabels(user_codes)
print(q.labels)
var_ind = a.lut[user_codes]
vnames=q.labels
br=a.vals[:,:,0,var_ind[0],:]  #self.vals[0:nphi-1,0:ntheta-1,0:nr-1,0:nq-1,0:niter-1] 
bt=a.vals[:,:,0,var_ind[1],:]
bp=a.vals[:,:,0,var_ind[2],:]
time = a.time

# Extract data
ntheta=a.ntheta
nphi=a.nphi
nl = s.nell
nm = s.nm
#br=np.zeros([nphi,ntheta,0])  #need to append to iters from other files.
#bt=np.zeros([nphi,ntheta,0])  #need to append to iters from other files.
#bp=np.zeros([nphi,ntheta,0])
bsr=np.zeros([nl,nm,0])
bst=bsr
bsp=bsr
time = np.zeros([0])
times = np.zeros([0])  #time from shellspectrum
print('Reading in data')

var_ind = s.lut[user_codes]
bsr=s.vals[:,:,0,var_ind[0],:]  #self.vals[0:lmax,0:mmax,0:nr-1,0:nq-1,0:niter-1]
bst=s.vals[:,:,0,var_ind[1],:]
bsp=s.vals[:,:,0,var_ind[2],:]
times = s.time
ntimes=len(times)
#inc = np.arctan(br/bt)  #inclination.
r_cmb=3481e3
r_surf=6371e3
nl_surf=nl
nm_surf=nm
ntimes_surf=10
ntheta=a.ntheta
nphi=a.nphi
#theta = np.linspace(pi,0,ntheta)
theta=np.arccos(a.costheta)
phi = np.linspace(0,2*pi,nphi)
br_cmb=np.zeros((ntheta,nphi))  #this is the transformed br(theta,phi) from blm
#itime=9
itime=np.argmin(a.time-s.time)
print('times: slice,spec,diff ='+str(a.time[0])+','+str(s.time[itime])+','+str(a.time[0]-s.time[itime]))

# Compute Br(cmb,theta,phi) and compare to output of Br(cmb,theta,phi)
brlmtp=np.zeros((ntheta,nphi))
print('Computing Br from Blm')
brlm=bsr[:,:,itime]
brlm_onorm=renormalize_shell_spectra(brlm)
brlmtp=inverse_transform(brlm_onorm,theta,nphi)
# Plot Br maps
plt.figure(figsize=(15,5))
plt.subplot(131)
plt.contourf(brlmtp)
plt.colorbar()
plt.title('brlmtp')
plt.subplot(132)
plt.contourf(np.transpose(br[:,:,0]))
plt.colorbar()
plt.title('br')
plt.subplot(133)
plt.contourf(np.transpose(br[:,:,0])-brlmtp)
plt.colorbar()
plt.title('br-brlmtp')
"""
print('Computing Cians inverse...')
brlmtp_cian=inverse_transform_cian(brlm,theta,nphi)
plt.title('br-brlmtp')
plt.subplot(223)
plt.contourf(brlmtp_cian)
plt.colorbar()
plt.title('brlmtp_cian')
plt.subplot(224)
plt.contourf(np.transpose(br[:,:,0])-brlmtp_cian)
plt.colorbar()
plt.title('br-brlmtp_cian')
"""

# Compute Gauss Coefficients
print('Computing Gauss Coefficients')
gauss=gauss_coef(brlm_onorm)
print('l,m,g,h')
for ell in range(3):
    for m in range(ell+1):
        print(str(ell)+','+str(m)+','+str(gauss[ell,m].real)+','+str(gauss[ell,m].imag))

exit()

############################################################
# Extrapolate from CMB to Surface.

print('Computing Gauss')
# Compute Gauss coef at cmb.
g_lm_cmb=np.zeros((nl_surf,nm_surf,ntimes_surf),dtype='complex')
for t in range(ntimes_surf):
    g_lm_cmb[:,:,t]=gauss_coef(bsr[:nl_surf,:nm_surf,t])
# Compute harmonic stuff to speed up calculations.
lpmvs=np.zeros((nm_surf+1,nl_surf+1,ntheta))
norms=np.zeros((nl_surf+1,nm_surf+1))
costheta=np.cos(theta)
for ell in range(1,nl_surf):
    for m in range(ell+1):
        lpmvs[m,ell,:]=lpmv(m,ell,costheta)
        #lpmvs[m,ell,:]=legendre(ell)[costheta]
        norms[ell,m]=norm_lm(ell,m)
#        norms[ell,m]=norm_lm_schmidt(ell,m) 
"""
# Compute Br(cmb,theta,phi) and compare to output of Br(cmb,theta,phi)
print('Computing Br from g_lm')
for iphi,phii in enumerate(phi):
    for ell in range(1,nl_surf):
        br_cmb[:,iphi]=br_cmb[:,iphi]+norms[ell,0]*lpmvs[0,ell,:]*(ell+1.)*(g_lm_cmb[ell,0,itime].real)
        for m in range(1,ell+1):
            br_cmb[:,iphi]=br_cmb[:,iphi]+1.0*norms[ell,m]*lpmvs[m,ell,:]*(ell+1.)*(
                g_lm_cmb[ell,m,itime].real*np.cos(m*phii)-g_lm_cmb[ell,m,itime].imag*np.sin(m*phii))

# Plot comparison of Br,Bt,Bp as (theta,phi) and (l,m)
ion()
nfig=1
nrow=1
ncol=3
f1 = p.figure(nfig,figsize=(15,5), dpi=80)
# Contour br_surf(theta,phi,t=0)
for panel in range(1,ncol+1):
    if panel==1:
        surf=np.transpose(br[:,:,0])  #.reshape(nphi,ntheta)
        title='Br output'
    if panel==2:
        surf=br_cmb[:,:]
        title='Br transformed'
    if panel==3:
        surf=np.transpose(br[:,:,0])-br_cmb
        title='Diff'
    ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
    twosigma = 2*np.std(surf)  #limits are set to +/- twosigma
    contour_levels = twosigma*np.linspace(-1,1,256)
    image1 = ax1.imshow(surf,vmin=-twosigma, vmax=twosigma,
                        extent=(-pi,pi,-pi/2,pi/2), clip_on=False,
                        aspect=0.5, interpolation='bicubic')
    plt.title(title)
    image1.axes.get_xaxis().set_visible(False)  # Remove the longitude and latitude axis labels
    image1.axes.get_yaxis().set_visible(False)
    image1.set_cmap('RdYlBu_r')  # Red/Blue map
    # Colorbar
    cbar = f1.colorbar(image1,orientation='horizontal', shrink=0.5)
    tick_locator = ticker.MaxNLocator(nbins=5)
    cbar.locator = tick_locator
    cbar.update_ticks()
print('max,min diff='+str(np.max(np.transpose(br[:,:,0])-br_cmb))+','+str(np.min(np.transpose(br[:,:,0])-br_cmb)))

exit()
"""
###############################

restore=1
if not restore:
    # Compute Br(l,m) from Br(theta,phi)
    print('Computing Blm')
    brlm=np.zeros((nm_surf,nl_surf),dtype='complex')
    dtheta=theta[0]-theta[1]  #theta is backwards in index.
    dphi=phi[1]-phi[0]
    sintheta=np.sin(theta)

    brtp = np.transpose(br[:,:,0])
    brlm = harmonic_transform(brtp,nl_surf)
        
    outfile=path+'/Blm'
    np.savez(outfile,brlm=brlm,br=br,theta=theta,phi=phi,norms=norms,lpmvs=lpmvs)
    print('saved '+outfile)
if restore:
    print('restoring Blm.npz')
    data=np.load(path+'/Blm.npz') # restores ['phi', 'br', 'lpmvs', 'norms', 'brlm', 'theta']
    brlm=data['brlm']
    
diff_lm_real=bsr[:,:,itime].real-(brlm.real)
diff_lm_imag=bsr[:,:,itime].imag-(brlm.imag)
# Plot diff_lm_real
plt.figure(figsize=(15,7))
plt.subplot(121)
plt.contourf(diff_lm_real)
plt.colorbar()
plt.title('blm-blm(transf) .real')
plt.subplot(122)
plt.contourf(diff_lm_real)
plt.xlim(0,10)
plt.ylim(0,10)
# Plot diff_lm_imag
plt.figure(figsize=(15,7))
plt.subplot(121)
plt.contourf(diff_lm_imag)
plt.colorbar()
plt.title('blm-blm(transf) .imag')
plt.subplot(122)
plt.contourf(diff_lm_imag)
plt.xlim(0,10)
plt.ylim(0,10)
# Print out a few ratios
l_check=[3,5,7,9,5,7,9]
m_check=[2,2,2,2,4,4,4]
for il,ell in enumerate(l_check):
    m=m_check[il]
    print(str(ell)+','+str(m)+','+str(brlm[ell,m]/bsr[ell,m,itime])+','+str(norm_lm(ell,m)))


