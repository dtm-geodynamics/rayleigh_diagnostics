
import numpy as np
from diagnostic_reading import ShellSlice, ShellSpectra
from diagnostic_tools import file_list
import matplotlib.pyplot as plt
from scipy.special import lpmv
import math
plt.ion()

#sslicefiles = file_list(None, None, path='Shell_Slices')
#sslice = ShellSlice(filename=sslicefiles[-1],path='')

sslicefilename = 'Shell_Slices/01000000'
sslice = ShellSlice(filename=sslicefilename,path='')

sslicetindex = -1
sslicetime = sslice.time[sslicetindex]

f1 = plt.figure(figsize=(5.5*3, 5*1), dpi=80)
f1.canvas.set_window_title('test_spectra')

sslicevals = np.transpose(sslice.vals[:,:,0,sslice.lut[401],sslicetindex].reshape(sslice.nphi, sslice.ntheta))
twosigma = 2*np.std(sslicevals)

ax1 = f1.add_subplot(1,3,1,projection="mollweide")
image1 = ax1.imshow(sslicevals, #vmin=-twosigma, vmax=twosigma,
                    extent=(-np.pi,np.pi,-np.pi/2,np.pi/2), 
                    clip_on=True, aspect=0.5, 
                    interpolation='bicubic')
image1.axes.get_xaxis().set_visible(False)  # Remove the longitude and latitude axis labels
image1.axes.get_yaxis().set_visible(False)
image1.set_cmap('RdYlBu_r')
cbar = f1.colorbar(image1,orientation='horizontal', shrink=0.5)


sspectrafiles = file_list(None, None, path='Shell_Spectra')

# search for the shell spectra with the minimum time difference to our selected shell slice
timedifference = 1.e24
for sspectrafile in sspectrafiles[::-1]:
  sspectra = ShellSpectra(filename=sspectrafile,path='')
  reltime = abs(sspectra.time-sslicetime)
  tmpsspectratindex = np.argmin(reltime)
  if reltime[tmpsspectratindex] > timedifference:
    break
  else:
    sspectrafilename = sspectrafile
    sspectratindex = tmpsspectratindex
    timedifference = reltime[tmpsspectratindex]

sspectra = ShellSpectra(filename=sspectrafilename,path='')
sspectratime = sspectra.time[sspectratindex]
print("shell slice time, shell spectra time, difference = %f, %f, %e"%(sslicetime, sspectratime, sspectratime-sslicetime))


def compute_lpmvs_rayleigh(lmax, costhetas):
  mg0corrn = (2**0.5)  # this factor is not in the Rayleigh complute_plm code but was found to be necessary to correct
  lpmvs = np.zeros((lmax+1, lmax+1, len(costhetas)))
  def compute_factorial_ratio(m):
    ratio = 1.0
    for i in xrange(1,m+1):
      ratio = ratio*((i-0.5)/i)**0.5
    return ratio
  for m in xrange(lmax+1):
    factorial_ratio = compute_factorial_ratio(m)
    amp = ((m+0.5)/(2.0*np.pi))**0.5
    amp = amp*factorial_ratio
    if m>0: amp = amp*mg0corrn
    tmp = 1.0 - costhetas*costhetas
    if np.mod(m,2)==1:
      lpmvs[m,m,:] = -amp*tmp**(m/2+0.5)
    else:
      lpmvs[m,m,:] = amp*tmp**(m/2)
    if (m < lmax):
      lpmvs[m,m+1,:] = lpmvs[m,m,:]*costhetas*(2.0*m+3.0)**0.5
    for l in xrange(m+2,lmax+1):
      amp = (l-1)**2-m*m
      amp = amp/(4.0*(l-1)**2 - 1.0)
      amp = amp**0.5
      tmp = lpmvs[m,l-1,:]*costhetas - amp*lpmvs[m,l-2,:]
      amp = (4.0*l*l - 1.0)/(l*l - m*m)
      lpmvs[m,l,:] = tmp*amp**0.5
  return lpmvs
      

def compute_lpmvs_python(lmax, costhetas):
  mg0corrn = (2**0.5)  # this factor is not in the Rayleigh complute_plm code but was found to be necessary to correct
  lpmvs = np.zeros((lmax+1, lmax+1, len(costhetas)))
  for l in xrange(lmax+1):
    norm = np.sqrt((2.*l+1.)/(4.*np.pi))
    lpmvs[0,l,:] = norm*lpmv(0,l,costhetas)
    for m in xrange(1,l+1):
      norm = np.sqrt(((2.*l+1.)/(4.*np.pi))*(float(math.factorial(l-m))/float(math.factorial(l+m))))
      lpmvs[m,l,:] = mg0corrn*norm*lpmv(m,l,costhetas)
  return lpmvs

lpmvs = compute_lpmvs_python(sspectra.lmax, sslice.costheta)

phis = np.linspace(0.0, 2*np.pi, sslice.nphi+1)
convertedvals = np.zeros(sslicevals.shape)
for iphi in xrange(sslice.nphi):
  for l in xrange(sspectra.lmax):
    # differences in scaling between m==0 and m>0 are incorporated into the lpmvs above
    for m in xrange(l+1):
      Brlm = sspectra.vals[l, m, 0, sspectra.lut[401], sspectratindex]
      convertedvals[:,iphi] += (Brlm.real*np.cos(m*phis[iphi]) - Brlm.imag*np.sin(m*phis[iphi]))*lpmvs[m,l,:]


convertedtwosigma = 2*np.std(convertedvals)
ax2 = f1.add_subplot(1,3,2,projection="mollweide")
image2 = ax2.imshow(convertedvals, #vmin=-convertedtwosigma, vmax=convertedtwosigma,
                    extent=(-np.pi,np.pi,-np.pi/2,np.pi/2), 
                    clip_on=True, aspect=0.5, 
                    interpolation='bicubic')
image2.axes.get_xaxis().set_visible(False)  # Remove the longitude and latitude axis labels
image2.axes.get_yaxis().set_visible(False)
image2.set_cmap('RdYlBu_r')
cbar = f1.colorbar(image2,orientation='horizontal', shrink=0.5)

ax3 = f1.add_subplot(1,3,3,projection="mollweide")
image3 = ax3.imshow(sslicevals-convertedvals, #vmin=-convertedtwosigma, vmax=convertedtwosigma,
                    extent=(-np.pi,np.pi,-np.pi/2,np.pi/2), 
                    clip_on=True, aspect=0.5, 
                    interpolation='bicubic')
image3.axes.get_xaxis().set_visible(False)  # Remove the longitude and latitude axis labels
image3.axes.get_yaxis().set_visible(False)
image3.set_cmap('RdYlBu_r')
cbar = f1.colorbar(image3,orientation='horizontal', shrink=0.5)

print("max, min difference = %e, %e"%(np.max(sslicevals-convertedvals), np.min(sslicevals-convertedvals)))

# Next, set various parameters that control the plot layout
# These parameters can also be set interactively when using plt.show()
pbottom = 0.05
pright = 0.95
pleft = 0.05
ptop = 0.95
phspace = 0.03
pwspace = 0.03

plt.subplots_adjust(left = pleft, bottom = pbottom, right = pright, top = ptop, wspace=pwspace,hspace=phspace) 



plt.show()


