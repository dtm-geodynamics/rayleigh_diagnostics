### compare_k.py ###
### PED 2020 ###
### Purpose: plot Ra vs k for all runs in
### memex_kluo/.../mixed

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib as mpl
plt.ion()
pi=np.pi
import sys
mpl.rcParams.update({'font.size': 15})

dirs_kappa=['kappa-1','kappa-1.2','kappa-1.4','kappa-1.6','kappa-1.8',#'kappa-2',
                'kappa-2.2','kappa-2.4','kappa-3','kappa-3.5','kappa-4']
dirs_linear='kappa-linear'
dirs_manual='manual/convectinit/convectinit-rot-nonmag'
nkappa=len(dirs_kappa)
dirs=dirs_kappa
dirs.append(dirs_linear)
dirs.append(dirs_manual)
i_linear=nkappa
i_manual=i_linear+1
nmanual=1
nlinear=1
markers=['o' for i in range(nkappa)]
markers.append('^')
markers.append('D') #one for each dir.
kappa_arr=np.array([dirs_kappa[i].split('-')[1] for i in range(nkappa)],dtype='float')

### Read in stats
ndirs=len(dirs)
nRa=50  #arbitrarily large number of Ra runs.
nstats=5
stats=np.zeros((ndirs,nRa,nstats))
i_Ra=0; i_Ra_Rac=1; i_k_mean=2; i_k_max=3; i_KE_mean=4
nRa=np.zeros(ndirs).astype('int')
for idir in range(ndirs):
    lines=[]
    for line in open(dirs[idir]+'/stats.txt','r'):
        lines.append(line)
    for iline,line in enumerate(lines):
        if iline >0: stats[idir,iline-1,:]=line.split()
    nRa[idir]=len(lines)-1
### Print number of Ra cases for each kappa
for ik in range(nkappa): print('nRa(kappa=%.1f)=%i'%(kappa_arr[ik],nRa[ik]))
print('nRa(kappa=const)=%i'%(sum(nRa[:i_linear])))
print('nRa(kappa=linear)=%i'%(nRa[i_linear]))
print('nRa(kappa=manual)=%i'%(nRa[i_manual]))
### Find Ra_crit
i_Ra_crit=np.zeros(nkappa,dtype='int')
kappa_arr_fit=kappa_arr[:]
nkappa_fit=len(kappa_arr_fit)
Ra_crit=np.zeros(nkappa_fit)
KE_crit=1.  #critical KE to denote "convective"
for ik in range(nkappa_fit):
    #index of first convective Ra
    i_Ra_crit[ik]=np.where(stats[ik,:,i_KE_mean]>KE_crit)[0].min()
    #Ra of first convective case
    Ra_crit[ik]=stats[ik,i_Ra_crit[ik],i_Ra]
### Fit to Ra_crit
coef=np.polyfit(kappa_arr_fit,Ra_crit,1)
### Ra_crit of linear case
Ra_crit_linear=stats[i_linear,np.where(stats[i_linear,:,i_KE_mean]>KE_crit)[0].min(),i_Ra]
### Ra_crit of manual case
Ra_crit_manual=stats[i_manual,np.where(stats[i_manual,:,i_KE_mean]>KE_crit)[0].min(),i_Ra]
### print effective constant k for linear and manual cases
print('For linear k, effective constant k=%e.'%((Ra_crit_linear-coef[1])/coef[0]))
print('For empirical k, effective constant k=%e.'%((Ra_crit_manual-coef[1])/coef[0]))
### print scaled k fit coefs
print('Fit coef Ra_crit=Ra_0+Ra_1*k: Ra_0=%f Ra_1=%f'%(coef[1],coef[0]))
print('Fit coef scaled: Ra_0/Ra_c(k=1)=%f Ra_1/Ra_c(k=1)=%f'%(coef[1]/Ra_crit[0],coef[0]/Ra_crit[0]))

### Plot
plt.figure(figsize=(10,8))
color_nums=stats[:,:,i_KE_mean]/stats[:,:,i_KE_mean].max()
def color_switch(KE):
    if KE<KE_crit:
        return('white')
    else:
        return('black')
cmap=plt.cm.YlOrRd
for idir in range(ndirs):
    for i in range(nRa[idir]):
        ira=nRa[idir]-1-i  #reverse order?
        plt.plot(stats[idir,ira,i_k_max],stats[idir,ira,i_Ra],marker=markers[idir],
                     markerfacecolor=color_switch(stats[idir,ira,i_KE_mean]),
                     markeredgecolor='black',markersize=20)
                     #color=cmap(color_nums[idir,i]))
### Overplot linear fit
plt.plot(np.linspace(1,4,100),coef[1]+coef[0]*np.linspace(1,4,100),color='blue')
plt.xlabel('k')
plt.ylabel('Ra')
plt.ylim(0,50e3)
plt.gca().add_artist(plt.legend(handles=[
    Line2D([0],[0],marker='o',label='k constant',markersize=15,lw=0,markerfacecolor='white',color='k'),
    Line2D([0],[0],marker='^',label='k linear',markersize=15,lw=0,markerfacecolor='white',color='k'),
    Line2D([0],[0],marker='D',label='k empirical',markersize=15,lw=0,markerfacecolor='white',color='k'),
    Line2D([0],[0],marker='o',label='convecting',markersize=15,lw=0,color='k'),
    Line2D([0],[0],marker='o',label='not convecting',markersize=15,lw=0,markerfacecolor='white',color='k')],
                                    labelspacing=1.2,loc=4))
plt.legend(handles=[Line2D([1],[1],label='%e + %e k'%(coef[1],coef[0]),color='blue')],loc=3)
plt.grid(True)

if '-s' in sys.argv:
    plt.savefig('Ra_k.jpg')
    print('saved Ra_k.jpg')

### Figure 2: KE vs Ra/Ra_crit
cmap_k = mpl.cm.get_cmap('rainbow') #  #tab20
k_max_vals = stats[:,0,i_k_max]  #k_max for each k case.
k_max = k_max_vals.max()         #max of k_max vals, used for coloring.
i_k_sort = np.argsort(k_max_vals)  #index of k_max_vals that gives a sorted k arr
plt.figure(figsize=(10,8))
ax = plt.subplot(1,1,1)
for idir in range(ndirs):
    plt.plot(stats[idir,:,i_Ra_Rac],stats[idir,:,i_KE_mean],marker=markers[idir],linestyle="None",
                 markerfacecolor=cmap_k(k_max_vals[idir]/k_max),color='k',
                 markersize=10,label='%.1f'%stats[idir,0,i_k_max])
plt.xlabel(r'$Ra/Ra_{crit}$')
plt.ylabel('Kinetic Energy')
plt.ylim(0,50)
plt.xlim(1,3)
plt.grid(True)
plt.gca().add_artist(plt.legend(handles=[
    Line2D([0],[0],marker='o',label='k constant',markersize=15,lw=0,markerfacecolor='white',color='k'),
    Line2D([0],[0],marker='^',label='k linear',markersize=15,lw=0,markerfacecolor='white',color='k'),
    Line2D([0],[0],marker='D',label='k empirical',markersize=15,lw=0,markerfacecolor='white',color='k')],                                    labelspacing=1.2,loc=4))
### Color Legend
#plt.legend(title='k',loc='upper left')
handles, labels = ax.get_legend_handles_labels()
i_labels_sort = np.argsort(labels)  #sort by labels 'k' values
handles = np.asarray(handles)[i_labels_sort].tolist()  #sort by labels 'k' values
labels = np.asarray(labels)[i_labels_sort].tolist()    #sort by labels 'k' values
plt.legend(handles,labels,title='k',loc='upper right')

if '-s' in sys.argv:
    savename = 'KE_Rasuper.jpg'
    plt.savefig(savename)
    print('saved '+savename)

### Figure 3: (log,log) KE vs Ra/Ra_crit
plt.figure(figsize=(10,8))
for idir in range(ndirs):
    plt.loglog(stats[idir,:,i_Ra_Rac],stats[idir,:,i_KE_mean],marker=markers[idir],linestyle="None",
                   markerfacecolor=cmap_k(k_max_vals[idir]/k_max),color='k',
                   markersize=10,label='%.1f'%stats[idir,0,i_k_max])
plt.xlabel(r'$Ra/Ra_{crit}$')
plt.ylabel('Kinetic Energy')
plt.ylim(1e0,1e2)
plt.xlim(0.5,10)
plt.grid(True)
### Symbol Legend
plt.gca().add_artist(plt.legend(handles=[
    Line2D([0],[0],marker='o',label='k constant',markersize=15,lw=0,markerfacecolor='white',color='k'),
    Line2D([0],[0],marker='^',label='k linear',markersize=15,lw=0,markerfacecolor='white',color='k'),
    Line2D([0],[0],marker='D',label='k empirical',markersize=15,lw=0,markerfacecolor='white',color='k')],                                    labelspacing=1.2,loc=4))
### Color Legend
plt.legend(handles,labels,title='k',loc='upper right')
if '-s' in sys.argv:
    savename = 'KE_Rasuper_log.jpg'
    plt.savefig(savename)
    print('saved '+savename)
