#!/usr/bin/env python

import numpy as np
from plot_blines import *
import matplotlib.pyplot as plt

n=101
mu_arr = np.linspace(-1,+1,n)
theta_arr = np.arccos(mu_arr)
phi = 0.
r = 1.5
asg = 1.0
l = 2.
m = 2.
glm = .1
hlm = .1
x_arr = r*np.sin(theta_arr)*np.cos(phi)
y_arr = r*np.sin(theta_arr)*np.sin(phi)
z_arr = r*np.cos(theta_arr)
components = [gauss_component(l,m,glm,hlm,asg)]
Bx = np.zeros(n)
By = np.zeros(n)
Bz = np.zeros(n)

for i in range(n):
    Bx[i],By[i],Bz[i] = B_total(x_arr[i],y_arr[i],z_arr[i],components)



plt.figure()
plt.ion()
plt.plot(theta_arr,Bx,label='Bx')
plt.plot(theta_arr,By,label='By')
plt.plot(theta_arr,Bz,label='Bz')
plt.legend()
