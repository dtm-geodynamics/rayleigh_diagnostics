#!/usr/bin/env python

#Equatorial Slice Movie Example.
#Displays a single variable from the equatorial_slice data structure and stitches files together to create a movie.
#
# Equatorial Slice Data stucture format:
#    """Rayleigh Equatorial Slice Structure
#    ----------------------------------
#    self.niter                                    : number of time steps
#    self.nq                                       : number of diagnostic quantities output
#    self.nr                                       : number of radial points
#    self.nphi                                     : number of phi points
#    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
#    self.radius[0:nr-1]                           : radial grid
#    self.vals[0:phi-1,0:nr-1,0:nq-1,0:niter-1]    : The equatorial_slices
#    self.phi[0:nphi-1]                            : phi values (in radians)
#    self.iters[0:niter-1]                         : The time step numbers stored in this output file
#    self.time[0:niter-1]                          : The simulation time corresponding to each time step
#    self.version                                  : The version code for this particular output (internal use)
#    self.lut                                      : Lookup table for the different diagnostics output
#    """

from rayleigh_diagnostics import Equatorial_Slices
from rayleigh_update_input import old_to_new_code
from rayleigh_legacy_codes import string_to_index
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.pyplot import ion,plot
from diagnostic_plotting import lutlabels,getlabel
from diagnostic_tools import file_list
from diagnostic_tools import convert_to_kyr
import argparse
import matplotlib.animation as animation
import matplotlib.pyplot as plt
from matplotlib import ticker

from sys import stdout
from time import sleep

parser = argparse.ArgumentParser(description="""Create movie of all equatorial slices""")
parser.add_argument('-f', '--final', action='store', type=int, dest='final', required=False,
                    default=None, help='final ts to plot')
parser.add_argument('-i', '--initial', action='store', type=int, dest='initial', required=False,
                    default=None, help='initial ts to plot')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-sn', '--savename', action='store', type=str, dest='savename', required=False,
                    default=False, help='name of file to save')
parser.add_argument('-q', '--quantity', action='store', dest='quantity', type=int, nargs='*',
                    default=[501], required=False, help='quantity')
parser.add_argument('-d', '--default',action='store_true',dest='default',
                    help='use default plotting options',default=False)  #otherwise queue user.
parser.add_argument('-v', '--version', action='store', type=int, dest='version',
                    required=False, default=2, 
                    help='version of data (ex. new codes (default) or old codes (v1)')
parser.add_argument('-fps', '--fps', action='store', type=float, dest='fps', required=False,
                    default=20, help='movie frames per second')
parser.add_argument('-dpi', '--dpi', action='store', type=float, dest='dpi', required=False,
                    default=200, help='dpi resolution of images')
args = parser.parse_args()

    
path='Equatorial_Slices'
initial=args.initial
final=args.final
files = file_list(initial,final,path=path)
nfiles=len(files)

q_code = 501 #default q_code
ifilestart=0
ifileend=nfiles-1

a = Equatorial_Slices(filename=files[0],path='')

if args.version == 1:
    print('old version selected')
    for i in range(len(a.qv)): #updated so that movies can be made from old data (old codes: 64 -> 501)
        a.qv[i] = old_to_new_code(a.qv[i])
    
if not args.default:
    print('Codes: '+str(a.qv))
    
    user_codes = raw_input('Enter a quantity code: ').split(' ')
    q_codes = [int(num) for num in user_codes]
    q_code = q_codes[0] #disregards excess codes

    print('\nFiles found: %d' %(nfiles))
    if args.initial is None:
        choice = raw_input('Range of files or all files? Select 0 or 1: ')

        #user chooses range of files from which to stitch the movie
        if(choice == '0'):
            ifilestart = int(raw_input('Choose starting index (min 0): '))
            ifileend = int(raw_input('Choose ending index (max %i):'%(nfiles-1)))
        nfiles = ifileend-ifilestart

#Set up the grid
nr = a.nr
nphi = a.nphi
r = a.radius/np.max(a.radius)
phi = np.zeros(nphi+1,dtype='float64')
phi[0:nphi] = a.phi
phi[nphi] = np.pi*2  # For display purposes, it is best to have a redunant data point at 0,2pi
radius_matrix, phi_matrix = np.meshgrid(r,phi)
X = radius_matrix * np.cos(phi_matrix) 
Y = radius_matrix * np.sin(phi_matrix)
#Extract fields to plot

#qindex = a.lut[q_code]
qtitle = getlabel(q_code)
qindex = np.where(a.qv==q_code)
print('variable selected: ' + qtitle)
time_in_kyr = raw_input('Time units in kyr? (y/n): ')
eqSlices = []
times=[] #keeps track of times to update time stamp on each frame of movie

print('Loading files... ')
for i in range(ifilestart, ifileend):

    #dynamically update progress of files that are being processed
    stdout.write('\r%d/%d' %(i-ifilestart,nfiles)) 
    stdout.flush()

    currSlice = Equatorial_Slices(filename=files[i],path='')
    eqSlices.append(currSlice)

    for j in range(len(currSlice.time)):        
        times.append(currSlice.time[j])

stdout.write('\r%d \r\n' %nfiles) #prints final number of files processed

#converts time in thermal diffusion units to thousands of years
if(time_in_kyr == 'y'):
    (times_kyr, times_dip) = convert_to_kyr(times) #method returns times in kyr and dip
    times = times_kyr
    #times = times.tolist()


############### begin plotting #################

totalIter = 0

#counts total number of frames (for all slices)
for a in eqSlices:
    totalIter = totalIter + a.niter #a.niter = number of frames for slice "a"
  
eqslice_array = np.zeros((a.nphi+1, a.nr, totalIter)) #holds all time iters for all files

istart = 0
iend = 0

#reshape the eqslice_array into phi r niter
for s in eqSlices:
    
    istart = iend
    iend = istart + s.niter
    
    eqslice_array[0:nphi, :, istart:iend] = s.vals[:,:,qindex,:].reshape(s.nphi,s.nr,s.niter)
    eqslice_array[nphi,:] = eqslice_array[0,:] #need redundant data point

ntimes = np.size(eqslice_array, axis=2)

#if entropy, subtract mean so colorbar has smaller range
if (q_code == 501):  
    #eqslice_tempavg = np.mean(eqslice_array, axis=2)
    eqslice_tempavg = np.mean(eqslice_array)
    
    for i in range(ntimes):
        eqslice_array[:,:,i] = eqslice_array[:, :, i] - eqslice_tempavg

#set up the frame
f1 = plt.figure(dpi=900)
pleft = 0.15
pright = 0.95
ptop = 0.95
cspace = (pright-pleft)
ypos = ptop
xpos = 0.45
fs=20 #font size


# Set the projection
ax1 = f1.add_subplot(1,1,1, projection="rectilinear") 
twosigma = 2*np.std(eqslice_array)  #limits are set to +/- twosigma
contour_levels = twosigma*np.linspace(-1,1,256)


def animate(i):
    
    f1.clear()
    ax1 = f1.add_subplot(1,1,1, projection="rectilinear")

    eqslices = eqslice_array[:, :, i].reshape(a.nphi+1, a.nr) #NO transpose!

    #creates the image
    img = ax1.pcolormesh(X,Y,eqslices[:,:],cmap='jet',shading='gouraud',edgecolors=None, vmin=-twosigma, vmax=twosigma)
    f1.colorbar(img, label=qtitle, orientation='vertical')

    #updates time stamp
    time = times[i]
    time_str = 't=%.3f'%(time)
    
    
    plt.title(time_str)
    plt.axis('off') 

    return ax1 

# Make Movie.
print('Making movie...')
movie_name=qtitle+'_eqslice_'+str('t%.3f'%max(a.time))+'_'+str('n%i'%(totalIter))+'.mp4'
if args.savename: movie_name=args.savename+'.mp4'
anim = animation.FuncAnimation(f1, animate, frames=totalIter, repeat=False,interval=1)
#anim.save(movie_name, writer=animation.FFMpegWriter(bitrate=500),fps=args.fps)
#anim.save(movie_name, writer=animation.FFMpegWriter(bitrate=500,fps=args.fps))
anim.save(movie_name, writer=animation.FFMpegWriter(fps=args.fps),dpi=args.dpi)
print('Saved '+movie_name)
