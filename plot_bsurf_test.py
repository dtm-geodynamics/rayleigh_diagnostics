#!/usr/bin/env python

####################################################################################################
#

#import matplotlib
#matplotlib.use("Agg")

import pylab as p 
import numpy as np
from diagnostic_reading import ShellSlice,ShellSpectra,PowerSpectrum
from diagnostic_tools import file_list, calculate_Ylms,renormalize_shell_spectra,inverse_transform,norm_lm
from diagnostic_plotting import lutlabels,getlabel
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import ticker
#matplotlib.use('GTK')
from matplotlib.pyplot import ion,plot,show
import argparse
pi=np.pi

path='Shell_Spectra'
print("Reading files...")
initial=None
final=None
files = file_list(initial, final, path)
nfiles=len(files)

sspec0 = ShellSpectra(files[1],path='')

# We use the lookup table to find where br, vtheta, and bphi are stored
br_index = sspec0.lut[401]
dims=[sspec0.lmax,sspec0.nr,sspec0.niter]   #get dims.
lmax=sspec0.lmax
lmax_gauss=min([84,lmax])  #limit to <84 bc factorial(170)=inf, so lmax<84. 
nl=lmax+1
nr=sspec0.nr
niter=sspec0.niter
sspec = [PowerSpectrum('blank',dims) for i in range(nfiles)]  #define spec as a blank list to get a template object.
sspec[0]=sspec0  #def 1st element.
# Read all files next???

#rad_index
rad_index=0  # only use top radius level

#Initialize full arr with first spec file.
times=sspec0.time
# Convert Brlm to orthonormal
brlm_on=renormalize_shell_spectra(sspec0.vals[:lmax_gauss+1,:lmax_gauss+1,0,br_index,:])

import sys
import scipy.special
from scipy.special import lpmv
itime=-1  #iteration to plot
blm=brlm_on[:,:,itime]
sslice_files=file_list(initial,final,'Shell_Slices')
sslice0 = ShellSlice(filename=sslice_files[1],path='')    #    self.vals[0:nphi-1,0:ntheta-1,0:nr-1,0:nq-1,0:niter-1] 
brcmb0 = sslice0.vals[:,:,0,sslice0.lut[401],itime].reshape(sslice0.nphi,sslice0.ntheta)  #    self.vals[0:nphi-1,0:ntheta-1,0:nr-1,0:nq-1,0:niter-1]
print('shell_spec time=%f , shell_slice time=%f'%(times[itime],sslice0.time[itime]))

# Extrapolate orthonormal harmonics to surf and Compute Brtp(surface)
r_cmb=3481e3
r_surf=6371e3
#r_ratio=r_cmb/r_surf
r_ratio=1.  #test to see if Brtp_cmb is same as in shell_slices.
ntheta=sslice0.ntheta #90  #this should be a func of lmax?
nphi=sslice0.nphi #ntheta*2
costheta, weights = np.polynomial.legendre.leggauss(ntheta)
theta=np.arccos(costheta)
# Compute brtp from brlm_on
brtp_trans=inverse_transform(brlm_on[:,:,itime],theta,nphi,r_ratio)

##################################################################
# Plot comparison of Br,Bt,Bp as (theta,phi) and (l,m)
ion()
nfig=1
nrow=1
ncol=3
panel=1
f1 = p.figure(nfig,figsize=(15,7), dpi=80)
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
panel+=1
surf=brcmb0
surf=np.transpose(surf)
twosigma = 2*np.std(surf)  #limits are set to +/- twosigma
contour_levels = twosigma*np.linspace(-1,1,256)
# Contour br_surf(theta,phi,t=0)
image1 = ax1.imshow(surf,vmin=-twosigma, vmax=twosigma,
    extent=(-np.pi,np.pi,-np.pi/2,np.pi/2), clip_on=False,
    aspect=0.5, interpolation='bicubic')
image1.axes.get_xaxis().set_visible(False)  # Remove the longitude and latitude axis labels
image1.axes.get_yaxis().set_visible(False)
image1.set_cmap('RdYlBu_r')  # Red/Blue map
plt.title('Br cmb slice')
# Colorbar
cbar = f1.colorbar(image1,orientation='horizontal', shrink=0.5)
tick_locator = ticker.MaxNLocator(nbins=5)
cbar.locator = tick_locator
cbar.update_ticks()

### Plot transformed Brtp ###
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
panel+=1
surf=brtp_trans
#surf=np.transpose(surf)
twosigma = 2*np.std(surf)  #limits are set to +/- twosigma
contour_levels = twosigma*np.linspace(-1,1,256)
# Contour br_surf(theta,phi,t=0)
image1 = ax1.imshow(surf,vmin=-twosigma, vmax=twosigma,
    extent=(-np.pi,np.pi,-np.pi/2,np.pi/2), clip_on=False,
    aspect=0.5, interpolation='bicubic')
image1.axes.get_xaxis().set_visible(False)  # Remove the longitude and latitude axis labels
image1.axes.get_yaxis().set_visible(False)
image1.set_cmap('RdYlBu_r')  # Red/Blue map
plt.title('Br cmb transformed')
# Colorbar
cbar = f1.colorbar(image1,orientation='horizontal', shrink=0.5)
tick_locator = ticker.MaxNLocator(nbins=5)
cbar.locator = tick_locator
cbar.update_ticks()

### Plot difference: brcmb_slice-brcmb_trans ###
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
panel+=1
surf=np.transpose(brcmb0)-brtp_trans   #difference map
twosigma = 2*np.std(surf)  #limits are set to +/- twosigma
contour_levels = twosigma*np.linspace(-1,1,256)
# Contour br_surf(theta,phi,t=0)
image1 = ax1.imshow(surf,vmin=-twosigma, vmax=twosigma,
    extent=(-np.pi,np.pi,-np.pi/2,np.pi/2), clip_on=False,
    aspect=0.5, interpolation='bicubic')
image1.axes.get_xaxis().set_visible(False)  # Remove the longitude and latitude axis labels
image1.axes.get_yaxis().set_visible(False)
image1.set_cmap('RdYlBu_r')  # Red/Blue map
plt.title('slice-transformed')
# Colorbar
cbar = f1.colorbar(image1,orientation='horizontal', shrink=0.5)
tick_locator = ticker.MaxNLocator(nbins=5)
cbar.locator = tick_locator
cbar.update_ticks()


