#!/usr/bin/env python
import pylab as p 
import numpy as np
from diagnostic_reading import ShellSlice,ShellSpectra
from diagnostic_tools import file_list, calculate_Ylms, gauss_coef, norm_lm, norm_lm_schmidt, harmonic_transform, inverse_transform, inverse_transform_cian, harmonic_discrete_mu_real
from diagnostic_plotting import lutlabels,getlabel
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import ticker
from matplotlib.pyplot import ion,plot,show
import argparse
from scipy.special import lpmv, legendre, sph_harm
import sys
pi=np.pi
plt.ion()
r_surf=6371e3
r_cmb=3481e3

# Test inverse_transform

# Define gtp
ntheta=90
nphi=ntheta*2
costheta, weights = np.polynomial.legendre.leggauss(ntheta)
#costheta=np.linspace(-1,1,ntheta)
theta=np.arccos(costheta)
#phi=np.linspace(0,2*pi,nphi+1)
#phi=phi[0:nphi-1]
#phi=np.linspace(0,2*pi,nphi)
phi=np.linspace(0,2*np.pi,nphi+1)[:-1]
gtp=np.zeros((ntheta,nphi))
amp=2.
gtp=gtp+amp  #constant amplitude (l=m=0)

# harmonic_transform to get glm
nl=10
glm=harmonic_transform(nl,costheta,phi,gtp,weights)
glm_cian=harmonic_discrete_mu_real(gtp,nl)
print('l,m,glm,glm_cian,amp/Nlm')
for ell in xrange(5):
    for m in xrange(ell+1):
        print(str(ell)+','+str(m)+','+str(glm[ell,m])+','+str(glm_cian[ell,m])+','+str(amp/norm_lm(ell,m)))

# inverse_transform to get gtp
gtp_t=inverse_transform(glm,theta,nphi)
gtp_t_cian=inverse_transform(glm_cian,theta,nphi)

# compare
plt.figure(figsize=(15,10))
plt.subplot(231)
plt.contourf(gtp)
plt.title('gtp')
plt.colorbar()
plt.subplot(232)
plt.contourf(gtp_t)
plt.colorbar()
plt.title('gtp_t')
plt.subplot(233)
plt.contourf(gtp-gtp_t)
plt.colorbar()
plt.title('gtp-gtp_t')

plt.subplot(234)
plt.contourf(gtp)
plt.title('gtp')
plt.colorbar()
plt.subplot(235)
plt.contourf(gtp_t_cian)
plt.colorbar()
plt.title('gtp_t_cian')
plt.subplot(236)
plt.contourf(gtp-gtp_t_cian)
plt.colorbar()
plt.title('gtp-gtp_t_cian')
