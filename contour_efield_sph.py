### contour_efield.py 10/17/18 PED ###
### Code copied from:
# https://pythonmatplotlibtips.blogspot.com/2017/12/draw-beautiful-electric-field-lines.html

import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import numpy as np
from scipy.integrate import ode as ode
from matplotlib import cm
from itertools import product
from diagnostic_tools import *
import sys
pi=np.pi
plt.ion()

class charge:
    def __init__(self, q, pos,l=0,m=0,glm=0,hlm=0,a=0):
        self.q=q
        self.pos=pos
        self.glm=glm
        self.hlm=hlm
        self.l=l
        self.m=m
        self.a=a

class gauss:
    def __init__(self, l=0,m=0,glm=0,hlm=0,a=0):
        self.glm=glm
        self.hlm=hlm
        self.l=l
        self.m=m
        self.a=a
 
def B_gauss(l,m,glm,hlm, a, r,theta,phi):
    ### Given glm,hlm,a,r,theta,phi return Bx,By,Bz(r,theta,phi)
    mu = np.cos(theta)
    Plm = plm_schmidt(l,m,mu); Plmp1 = plm_schmidt(l,m+1,mu)
    B_x = (a/r)**(l+2.)*(m*mu/np.sin(theta)*Plm+Plmp1)*(glm*np.cos(m*phi)+hlm*np.sin(m*phi))
    B_y = 1./(np.sin(theta))*(a/r)**(l+2.)*m*Plm*(glm*np.sin(m*phi)-hlm*np.cos(m*phi))
    B_z = -1*(a/r)**(l+2.)*(l+1.)*Plm*(glm*np.cos(m*phi)+hlm*np.sin(m*phi))
    B_r = -B_z
    B_theta = -B_x
    B_phi = B_y
    B_x2 = np.sin(theta)*np.cos(phi)*B_r + np.cos(theta)*np.cos(phi)*B_theta + np.sin(phi)*B_phi
    B_y2 = np.sin(theta)*np.sin(phi)*B_r + np.cos(theta)*np.sin(phi)*B_theta + np.cos(phi)*B_phi
    B_z2 = np.cos(theta)*B_r - np.sin(theta)*B_theta
    #print('l=%d m=%d glm=%.2f hlm=%.2f a=%.2f r=%.4f theta=%.4f phi=%.4f Plm=%e Plmp1=%e Bx=%e By=%e Bz=%e'%(l,m,glm,hlm,a,r,theta,phi,Plm,Plmp1,B_x2,B_y2,B_z2))
    #sys.exit(0)
    return(B_x2,B_y2,B_z2)

def E_point_charge(q, a, x, y):
    return q*(x-a[0])/((x-a[0])**2+(y-a[1])**2)**(1.5), \
        q*(y-a[1])/((x-a[0])**2+(y-a[1])**2)**(1.5)  #returns(Ex,Ey)

def B_total(x,y,z,components):
    # Compute a,r,theta,phi
    #z = 0.
    r = np.sqrt(x**2+y**2+z**2)
    phi = np.arctan2(y,x)
    theta = np.arctan2(np.sqrt(x**2+y**2),z)
    Bx,By,Bz = 0,0,0
    for C in components:
        #a = np.sqrt(C.pos[0]**2+C.pos[1]**2)  #radius of source (location of glms)
        # Compute source contribution to field.
        B=B_gauss(C.l,C.m,C.glm,C.hlm, C.a, r,theta,phi)
        Bx=Bx+B[0]
        By=By+B[1]
        Bz=Bz+B[2]
    #print('B_total: r=%f theta=%f phi=%f Bx=%f By=%f Bz=%f'%(r,theta,phi,Bx,By,Bz))
    return [Bx,By,Bz]

def E_total(x, y, charges):
    Ex, Ey=0, 0
    for C in charges:
        # Compute source contribution to field.
        E=E_point_charge(C.q, C.pos, x, y)
        Ex=Ex+E[0]
        Ey=Ey+E[1]
    return [ Ex, Ey ]

def B_dir(t, y, components):
    #print('B_dir: t=%f x=%f y=%f z=%f'%(t,y[0],y[1],y[2]))
    Bx,By,Bz = B_total(y[0],y[1],y[2], components)
    #print('B_dir: t=%f x=%f y=%f z=%f Bx=%f By=%f Bz=%f'%(t,y[0],y[1],y[2],Bx,By,Bz))
    n=np.sqrt(Bx**2+By**2+Bz**2)
    #if n == 0.: exit()
    return [Bx/n,By/n,Bz/n]  #normalized unit vector directions?  New coordinates?

def E_dir(t, y, charges):
    Ex, Ey=E_total(y[0], y[1], charges)
    n=np.sqrt(Ex**2+Ey**2)
    return [Ex/n, Ey/n]

def V_gauss(l,m,glm,hlm,a,r,theta,phi):
    ### Compute mag potential(r,theta,phi)
    mu = np.cos(theta)
    Plm = plm_schmidt(l,m,mu); Plmp1 = plm_schmidt(l,m+1,mu)
    phi = a*(a/r)**(l+1.)*Plm*(glm*np.cos(m*phi)+hlm*np.sin(m*phi))
    return(phi)

def V_point_charge(q, a, x, y):
    return q/((x-a[0])**2+(y-a[1])**2)**(0.5)

def Phi_total(x, y, z, components):
    # Compute a,r,theta,phi
    #z = 0.
    r = np.sqrt(x**2+y**2+z**2)
    phi = np.arctan2(y,x)
    theta = np.arctan2(np.sqrt(x**2+y**2),z)
    V=0
    for C in components:
        #Vp=V_point_charge(C.q, C.pos, x, y)
        #a = np.sqrt(C.pos[0]**2+C.pos[1]**2)  #radius of source (location of glms)
        Vp = V_gauss(C.l, C.m,C.glm,C.hlm,C.a,r,theta,phi)
        V = V+Vp
    return V

def V_total(x, y, components):
    V=0
    for C in components:
        Vp=V_point_component(C.q, C.pos, x, y)
        V = V+Vp
    return V

# charges and positions
#charges=[ charge(0,0,1.,0,1.,0,1.)]#  , charge(0,0,2,0,-1,0,1)] #, charge(1, [0, 1]), charge(1, [0, -1]) ]
components=[ gauss(1.,0,1.,0,1.)]
 
# calculate field lines
x0, x1=-3, 3
y0, y1=-3, 3
z0, z1=-3, 3
#R=0.01
dt=0.08*1.
eps = 0.1
# loop over all charges
xs,ys,zs = [],[],[]
# trace angles: alpha in x-y plane, beta in y-z plane.
#beta=pi/2.  #eq plane?
beta = pi/10.
alpha = np.arctan2(0,1)  #phi value of plane normal to view.
nbeta=40; beta_max=2.*pi; beta_step = beta_max/(nbeta+1.)
for C in components:
    #for alpha in np.linspace(0, 2*np.pi*15/16, 16):
    for beta in np.linspace(beta_step, beta_max-beta_step, nbeta):
        r=ode(B_dir); r.set_integrator('vode').set_f_params(components)
        x = [np.sqrt(C.a**2+eps**2)*np.sin(beta)*np.cos(alpha)]  #start curve right at source r=a?
        y = [np.sqrt(C.a**2+eps**2)*np.sin(beta)*np.sin(alpha)]
        z = [np.sqrt(C.a**2+eps**2)*np.cos(beta)]
        r.set_initial_value([x[0], y[0], z[0]], 0)
        print('beta=%f x=%f y=%f z=%f'%(beta,x[0],y[0],z[0]))
        while r.successful():
            r.integrate(r.t+dt)
            x.append(r.y[0]); y.append(r.y[1]); z.append(r.y[2])
            print('t=%f x=%f y=%f z=%f'%(r.t,r.y[0],r.y[1],r.y[2]))
            #if r.t >= 1.: sys.exit(0)
            hit_charge=False
            ## check if field line left drwaing area or ends in some charge
            for C2 in components:
                if np.sqrt(r.y[0]**2+r.y[1]**2+r.y[2]**2)<C2.a:
                    hit_charge=True
            if hit_charge or (not (x0<r.y[0] and r.y[0]<x1)) or \
                    (not (y0<r.y[1] and r.y[1]<y1)) or (not (z0<r.y[2] and r.y[2]<z1)):
                print('Break: hit_charge=%d x=%f y=%f z=%f'%(hit_charge,r.y[0],r.y[1],r.y[2]))
                break
        xs.append(x)
        ys.append(y)
        zs.append(z)

# calculate electric potential
vvs = []
xxs = []
yys = []
zzs = []
numcalcv = 10
for xx,yy,zz in product(np.linspace(x0,x1,numcalcv),np.linspace(y0,y1,numcalcv),np.linspace(z0,z1,numcalcv)):
    xxs.append(xx)
    yys.append(yy)
    zzs.append(zz)
    #vvs.append(V_total(xx,yy,charges))
    vvs.append(Phi_total(xx,yy,zz,components))
xxs = np.array(xxs)
yys = np.array(yys)
zzs = np.array(zzs)
vvs = np.array(vvs)

plt.figure(figsize=(15, 10),facecolor="w")

# plot field line
for x, z in zip(xs,zs):
    plt.plot(x, z, color="k")
c1=plt.Circle((0,0),components[0].a)
ax=plt.gca()
ax.add_patch(c1)

## plot point charges
#for C in components:
#    if C.q>0:
#        plt.plot(C.pos[0], C.pos[1], 'ro', ms=8*np.sqrt(C.q))
#    if C.q<0:
#        plt.plot(C.pos[0], C.pos[1], 'bo', ms=8*np.sqrt(-C.q))

# plot electric potential
clim0,clim1 = -2,2
vvs[np.where(vvs<clim0)] = clim0*0.999999 # to avoid error
vvs[np.where(vvs>clim1)] = clim1*0.999999 # to avoid error
plt.tricontour(xxs,zzs,vvs,10,colors="0.3")
plt.tricontourf(xxs,zzs,vvs,100,cmap=cm.jet)
cbar = plt.colorbar()
cbar.set_clim(clim0,clim1)
cbar.set_ticks([-2,-1.5,-1,-0.5,0,0.5,1,1.5,2])
cbar.set_label("Electric Potential")
plt.xlabel('$x$')
plt.ylabel('$z$')
plt.xlim(x0, x1)
plt.ylim(y0, y1)
plt.axes().set_aspect('equal','datalim')
plt.savefig('electric_force_lines_1.png',dpi=250,bbox_inches="tight",pad_inches=0.02)
plt.show()
