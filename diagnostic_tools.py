import os
import glob
import numpy as np
import sys
import scipy.special
from scipy.special import lpmv
import scipy.misc
import scipy.integrate as integrate
pi=np.pi
#factorial=scipy.misc.factorial
r_surf = 6371e3
r_cmb = 3481e3

def factorial(x):
    return float(scipy.misc.factorial(x))
def rms_mean(x):   #compute rms mean
    return(np.sqrt(np.mean(np.square(x))))
def rms_stdev(x):  #compute rms stdev
    mean=rms_mean(x)
    return(np.sqrt(np.mean(np.square(x-mean))))
def rms_sum(x):
    return(np.sqrt(np.sum(np.square(x))))

def file_list(initial=None, final=None, path=os.path.curdir):
  files = glob.glob1(path, "*")
  files = [os.path.join(path, f) for f in files if f.isdigit() and (initial is None or int(f) >= initial) and (final is None or int(f) <= final)]
#  files = [f for f in files if f.isdigit()]
#  files = [os.path.join(path, f) for f in files if (initial is None or f >= initial) and (final is None or f <= final)]
  files.sort()
  return files

#converts thermal diffusion to kyr
def convert_to_kyr(times,Pm=10.,Pr=1.,kyr_dip_decay=50.):
    times = np.asarray(times)
    #print('Assume for time scaling: Pm=%f, Pr=%f'%(Pm,Pr))
    times_mag=times/Pm
    times_dip=times_mag/(1.538461/np.pi)**2.
    times_kyr= times_dip*kyr_dip_decay #time in kyr
    return (times_kyr, times_dip)

### Dipole lat,lon
def dipole_latitude(g10,g11,h11):
    return(np.arccos(-g10/np.sqrt(g10**2+g11**2+h11**2)))
def dipole_longitude(g10,g11,h11):
    #lon_shift = np.asarray([np.pi if g11[k] < 0 and h11[k] <0 else 0.0 for k in xrange(len(g11))])
    #return(np.arccos(g11/np.sqrt(g11**2+h11**2)) + lon_shift)
    return(np.arctan2(h11,g11))
def remove_longitude_jumps(dip_lon,dip_lat):
    ### Purpose: given an array of longitudes from 180 to -180, find the indicies where
    ### the longitude crosses the 180 to -180 meridian, and return a list of arrays
    ### that break the given full array into subarrays.
    dip_lon_mult=dip_lon[:-1]*dip_lon[1:]
    ind_crossing=np.where(dip_lon_mult < -1e4)[0] #-1e4 is roughly 180*(-180)
    n_crossing=len(ind_crossing)
    dip_lon_split=[dip_lon[:ind_crossing[0]]]
    dip_lat_split=[dip_lat[:ind_crossing[0]]]
    for i in range(n_crossing-1):
        dip_lon_split.append(dip_lon[ind_crossing[i]+1:ind_crossing[i+1]])
        dip_lat_split.append(dip_lat[ind_crossing[i]+1:ind_crossing[i+1]])
    return(dip_lon_split,dip_lat_split)

### Reversals ###
def polarity_chrons(time_dip,dip_lat,min_chron_len=1.):
    #Compute the beginning and end of polarity chrons.
    #Note: time_dip is time in dipole decay times. dip_lat is latitude in degrees.
    #Note: dip_lat is in units of degrees.
    #min_chron_len is the minimum length of a stable polarity chron in units of dipole decay time.
    ### Goes into polarity_chrons func ###
    polarity=np.sign(dip_lat-90.)     #= +1 N, -1 R
    i_eq_cross=np.where(np.diff(np.sign(dip_lat-90.)))[0]
    n_eq_cross=len(i_eq_cross)
    i_chrons=[0]+np.ndarray.tolist(i_eq_cross)+[-1]  #[0,-1] to add beginning and end times.
    time_cross=time_dip[i_chrons]  #include end of run
    nchrons=len(i_chrons)-1  #-1 to exclude after end.
    pol_chron=polarity[i_chrons[1:]]  #polarity of each chron before eq crossing. [1:] to remove first element?
    chron_len=np.diff(time_dip[i_chrons])
    pchrons=np.zeros((1,2))  #[npolchrons,[start,end]]
    pchrons[0,:]=[time_dip[0],time_cross[0]]  #time to first crossing
    nrev=0
    nexcursions=0
    current_pol=polarity[0]
    for i in range(1,nchrons):
        #if chron_len[i]>1.:
        if chron_len[i]>min_chron_len:  #chron is longer than min requirement.
            #if polarity is same then extend this chron, count as excursion
            if pol_chron[i]==current_pol:
                pchrons[nrev,:]=[pchrons[nrev,0],time_cross[i+1]] #extend len of this nrev chron
                nexcursions+=1
            # if polarity is diff then start new chron
            if pol_chron[i]+current_pol==0:
                nrev+=1
                pchrons=np.append(pchrons,[[time_cross[i],time_cross[i+1]]],axis=0)
                current_pol=pol_chron[i]     #update "current" pol
        else: pchrons[nrev,1]=time_cross[i]  #extend current pol chron to include this short one
    ### End Goes into polarity_chrons func ###
    return(nrev,nexcursions,n_eq_cross,pchrons)

### Spherical Harmonics ###
def Ylm(l,m,theta,phi):
    # return single Ylm[itheta,iphi].
    return scipy.special.sph_harm(m,l,phi,theta)

def Ylmtp(l,m,theta,phi):
    # returns all Ylm[ntheta,nphi] given single (l,m).
    #Note: documentation of sph_harm has phi and theta switched from normal convention.
    ylmtp=np.zeros((len(theta),len(phi)),dtype=complex)
    for iphi in xrange(len(phi)):
        ylmtp[:,iphi] = scipy.special.sph_harm(m,l,phi[iphi],theta)
    return ylmtp

def calculate_Ylms(nl,theta,phi):
#  Ylmarray = np.ma.masked_all([nl,nl,len(theta),len(phi)], dtype=complex)
  Ylmarray = np.zeros([nl,nl,len(theta),len(phi)], dtype=complex)
  for iphi,phii in enumerate(phi):
      #print('iphi:'+str(iphi)+'/'+str(len(phi)))
      for ell in range(1,nl):  #-1 bc we don't need l=0 (monopole=0).
          for m in range(ell):  #loop m=0:ell. imag component is (-m).
              Ylmarray[ell,m,:,iphi] = Ylm(ell,m,theta,phii)
  return Ylmarray

### Harmonic Normalizations ###
def norm_lm(l,m):
    l=float(l)
    m=float(m)
    return np.sqrt( (2*l+1.)/(4*pi)*factorial(l-m)/factorial(l+m))

# Schmidt normalization
def norm_lm_schmidt(l,m):
    if m>0:
        return np.sqrt(2.*factorial(l-m)/factorial(l+m))
    if m==0:
        return 1.   # Merrill (B.4.5)
        #return np.sqrt(1./(2.*l+1.))
    if m<0:
        return 0.
    
# Convert orthonormal to schmidt harmonics
def convert_to_schmidt_norm(blm):
    blm_schmidt=np.zeros(blm.shape,dtype='complex')
    for ell in xrange(len(blm[:,0])):
        for m in xrange(ell+1):
            blm_schmidt[ell,m] = blm[ell,m]/norm_lm(ell,m)*norm_lm_schmidt(ell,m)
    return blm_schmidt

# Schmidt normalized Legendre Polynomials
def plm_schmidt(l,m,mu):
    return norm_lm_schmidt(l,m)*lpmv(m,l,mu)

### Compute Gauss Coefficients from orthonormal harmonics blm ###
# Input: Blm at CMB.  Output: g_lm at surface.
# blm are assumed to be complex.
def gauss_coef(blm_cmb):
    nl=len(blm_cmb[:,0])
    #blm_cmb_onorm=renormalize_shell_spectra(blm_cmb)  #orthonormal
    blm_cmb_schmidt=convert_to_schmidt_norm(blm_cmb)  #onormal to schmidt
    g_lm_surf=np.zeros(blm_cmb.shape,dtype='complex')
    for ell in xrange(nl):
        # Convert B to potential with 1/(l+1), and extrapolate to r_surf.
        g_lm_surf[ell,:]=blm_cmb_schmidt[ell,:]/(ell+1.)*(r_cmb/r_surf)**(ell+2.)
    return g_lm_surf
        

### Compute harmonic transform to get blm from b(theta,phi) ###
def harmonic_transform(nl,costheta,phi,gtp,weights):
    # Transform arbitrary function gtp[ntheta,nphi]
    # to glm[nl,nl]
    ntheta=len(costheta)
    theta=np.arccos(costheta)
    dcostheta=[costheta[0]+1,-np.diff(costheta)]
    nphi=len(phi)
    dphi=phi[1]-phi[0]
    sintheta=np.sin(theta)
    #glm=np.zeros((nl+1,nl+1),dtype='complex')
    glm=np.zeros((nl,nl),dtype='complex')
    for ell in xrange(nl):
        for m in xrange(ell+1):
            norm_leg=np.sqrt((2.*ell+1)/2*factorial(ell-m)/factorial(ell+m))
            I_theta=np.zeros(nphi,dtype=complex)
            for iphi in xrange(nphi):
                gt=np.zeros(ntheta,dtype=complex)
                gt=gtp[:,iphi]
                I_theta[iphi]=sum(norm_leg*lpmv(m,ell,costheta)*gt*weights)
            glm[ell,m]=sum( (1./np.sqrt(2*pi))*(np.cos(m*phi)+1j*np.sin(m*phi))*I_theta*dphi )
    glm[:,1:]=2.*glm[:,1:]  #for m>0, factor of 2 from m=+/-m terms.
    return glm

def legendre_transform(l,m,costheta,gt,weights):
    # Legendre transform
    # gt is an arbitrary function of theta.
    return np.sqrt((2.*l+1)/2*factorial(l-m)/factorial(l+m))*lpmv(m,l,costheta)*gt*weights
def fourier_transform(m,phi,gp):
    # Fourier transform
    # gp is an arbitrary function of phi.
    return (1./np.sqrt(2*pi))*(np.cos(m*phi)+1j*np.sin(m*phi))*gp

def harmonic_discrete_mu_real(data,lmax):  # requires real input
    ntheta=len(data[:,0])
    nphi=len(data[0,:])
    costheta, weights = np.polynomial.legendre.leggauss(ntheta)
    fft_out = np.fft.rfft(data)/nphi
    fft_out[:,1:] = 2.*fft_out[:,1:]  # done in rayleigh
    lpmvs = compute_lpmvs_python(lmax, costheta)
    transformed = np.zeros((lmax+1,lmax+1), dtype='complex')
    for l in xrange(lmax+1):
        for m in xrange(l+1):
            transformed[l, m] = sum(2.*np.pi*fft_out[:,m]*lpmvs[m,l,:]*weights)
    return transformed

def compute_lpmvs_python(lmax, costhetas):
    mg0corrn = 1.#(2**0.5)
    # this factor is not in the Rayleigh complute_plm code but was found to be necessary to correct
    lpmvs = np.zeros((lmax+1, lmax+1, len(costhetas)))
    for l in xrange(lmax+1):
      norm = np.sqrt((2.*l+1.)/(4.*np.pi))
      lpmvs[0,l,:] = norm*lpmv(0,l,costhetas)
      for m in xrange(1,l+1):
        norm = np.sqrt(((2.*l+1.)/(4.*np.pi))*(float(factorial(l-m))/float(factorial(l+m))))
        lpmvs[m,l,:] = mg0corrn*norm*lpmv(m,l,costhetas)
    return lpmvs

# Compute B(theta,phi) from B(l,m).  blm should be orthonormal.
# for vector fields: brlm is the radial component spectral amplitudes.
# the vector "component" options are: 'r' for radial, 't' theta, 'p' phi.  Default is 'r'.
def inverse_transform(brlm,theta,nphi,r_ratio=1.,component='r'):
    ntheta=len(theta)
    phi=np.linspace(0,2*pi,nphi+1)
    costheta=np.cos(theta)
    sintheta=np.sin(theta)
    nl=len(brlm[:,0])
    nm=len(brlm[0,:])
    btp=np.zeros((ntheta,nphi))
    # Compute harmonic stuff to speed up calculations.
    lpmvn=np.zeros((nm+1,nl,ntheta))   #nm+1 to have value of zero for derivative terms.
    lpmvs=np.zeros((nm+1,nl,ntheta))   #nm+1 to have value of zero for derivative terms.
    norms=np.zeros((nm+1,nl,ntheta))   
    for ell in xrange(nl):
        for m in xrange(ell+1):
            lpmvs[m,ell,:]=lpmv(m,ell,costheta)   #unnormalized
            norms[ell,m]=norm_lm(ell,m)           # normalizations
            lpmvn[m,ell,:]=norms[ell,m]*lpmv(m,ell,costheta)  #fully normalized.
    # Compute inverse transform.
    if component=='r':   #Return Radial component.
        for iphi in xrange(nphi):
            for ell in xrange(nl):
                r_ratio_l = r_ratio**(ell+2)
                btpm=np.zeros(ntheta)
                for m in xrange(ell+1):
                    btpm += lpmvn[m,ell,:]*(brlm[ell,m].real*np.cos(m*phi[iphi])-brlm[ell,m].imag*np.sin(m*phi[iphi]))
                btp[:,iphi] += btpm*r_ratio_l  #extrapolate across r_ratio=r_source/r_destination. (1/8/18)
            sys.stdout.write(str(iphi)+'/'+str(nphi)+' \r')
            sys.stdout.flush()
    elif component=='t':   #Return theta component.
        for iphi in xrange(nphi):
            for ell in xrange(nl):
                r_ratio_l = r_ratio**(ell+2)/(ell+1)
                btpm=np.zeros(ntheta)
                for m in xrange(ell+1):  
                    btpm += (1)*(m*costheta/sintheta*lpmvn[m,ell,:]+norms[ell,m]*lpmvs[m+1,ell,:])*(brlm[ell,m].real*np.cos(m*phi[iphi])-brlm[ell,m].imag*np.sin(m*phi[iphi]))
                btp[:,iphi] += btpm*r_ratio_l  #extrapolate across r_ratio=r_source/r_destination. (1/8/18)
            sys.stdout.write(str(iphi)+'/'+str(nphi)+' \r')
            sys.stdout.flush()
        btp=-btp  #apply - sign.
    elif component=='p':   #Return Phi component.
        for iphi in xrange(nphi):
            for ell in xrange(nl):
                r_ratio_l = r_ratio**(ell+2)
                btpm=np.zeros(ntheta)
                for m in xrange(ell+1):
                    btpm += lpmvn[m,ell,:]*m*(brlm[ell,m].real*np.sin(m*phi[iphi])+brlm[ell,m].imag*np.cos(m*phi[iphi]))
                btp[:,iphi] += btpm*r_ratio_l  #extrapolate across r_ratio=r_source/r_destination. (1/8/18)
            sys.stdout.write(str(iphi)+'/'+str(nphi)+' \r')
            sys.stdout.flush()
            btp[:,iphi] = btp[:,iphi]/sintheta
    else: print('ERROR: component must be r, t, or p.')
    return btp

# Compute B(theta,phi) from B(l,m)
def inverse_transform_cian(blm,theta,nphi):
    #    theta=np.linspace(pi,0,ntheta)
    ntheta=len(theta)
    phi=np.linspace(0,2*pi,nphi+1)
    costheta=np.cos(theta)
    sintheta=np.sin(theta)
    nl=len(blm[:,0])
    nm=len(blm[0,:])
    btp=np.zeros((ntheta,nphi))
    # Compute harmonic stuff to speed up calculations.
    lpmvn=np.zeros((nm+1,nl+1,ntheta))
    for ell in xrange(nl+1):
        lpmvn[0,ell,:]=norm_lm(ell,0)*lpmv(0,ell,costheta)
        for m in xrange(1,ell+1):
            lpmvn[m,ell,:]=(2**0.5)*norm_lm(ell,m)*lpmv(m,ell,costheta)
    # Compute inverse transform.
    for iphi in xrange(nphi):
        for ell in xrange(nl):
            for m in range(ell+1):
                btp[:,iphi] += (blm[ell,m].real*np.cos(m*phi[iphi])-blm[ell,m].imag*np.sin(m*phi[iphi]))*lpmvn[m,ell,:]
        sys.stdout.write(str(iphi)+'/'+str(nphi)+' \r')
        sys.stdout.flush()
    return btp

# Renormalize Shell_Spectra to orthonormal.
def renormalize_shell_spectra(blm):
    blm_norm=np.zeros(blm.shape,dtype='complex')
    for ell in xrange(len(blm[:,0])):
        blm_norm[ell,0]=blm[ell,0]
        for m in xrange(1,ell+1):
            blm_norm[ell,m]=(2**(+0.5))*blm[ell,m]
    return blm_norm

### E-like stats
def elike_stats(gauss):
    # gauss=[nl,nm,nt].complex.  Gauss are at Earth's surface.
    r_ratio=6371./3481  #surf/cmb
    lmax=len(gauss[:,0,0])
    nt=len(gauss[0,0,:])
    adnad=np.zeros(nt)
    oddeven=np.zeros(nt)
    zonality=np.zeros(nt)
    lmax_stats=8  #max used by CAH2010
    # Compute powers
    p=np.zeros((lmax_stats+1,lmax_stats+1,nt))  #power
    for t in range(nt):
        odd_sum=0.
        even_sum=0.
        zonal_sum=0.
        nonzonal_sum=0.
        for l in range(1,lmax_stats+1):
            for m in range(l+1):
                #p[l,m,t]=(gauss[l,m,t].real**2+gauss[l,m,t].imag**2)  #CAH10 eq 2. remove (l+1) bc we are at surface?
                #p[l,m,t]=(l+1)*(gauss[l,m,t].real**2+gauss[l,m,t].imag**2)  #CAH10 eq 2
                p[l,m,t]=(l+1)*(r_ratio)**(2.*(l+2))*(gauss[l,m,t].real**2+gauss[l,m,t].imag**2)  #Davies2014
                if l>1:  # use l=2 and greater
                    if (l+m)%2: even_sum+=p[l,m,t]
                    if not (l+m)%2: odd_sum+=p[l,m,t]
                    if m==0: zonal_sum+=p[l,m,t]
                    if m>0: nonzonal_sum+=p[l,m,t]
        # Compute adnad
        #adnad[t]=p[1,0,t]/(p[1,1,t]+np.sum(p[2:,:,t]))  #CAH10 eq 1
        adnad[t]=p[1,0,t]/(p[1,1,t]+np.sum(p[2:,:,t]))  #CAH10 eq 1
        # Compute O/E
        oddeven[t]=odd_sum/even_sum
        # Compute zonality
        zonality[t]=zonal_sum/nonzonal_sum
    # Compute chi^2 stats
    adnad_e=1.4    #CAH10 table 2
    oddeven_e=1.0
    zonality_e=0.15
    sd_adnad_e=2.0
    sd_oddeven_e=2.0
    sd_zonality_e=2.5
    adnad_mean=np.mean(adnad)
    oddeven_mean=np.mean(oddeven)
    zonality_mean=np.mean(zonality)
    chisq=((np.log(adnad_mean)-np.log(adnad_e))/np.log(sd_adnad_e))**2 + \
        ((np.log(oddeven_mean)-np.log(oddeven_e))/np.log(sd_oddeven_e))**2 + \
        ((np.log(zonality_mean)-np.log(zonality_e))/np.log(sd_zonality_e))**2
    stats=[adnad_mean,oddeven_mean,zonality_mean,chisq]
    return(adnad,oddeven,zonality,stats)

def fix_times(time):
    ind=np.where(np.diff(time)<0.) #indexes where time decreases
    # note: ind is actually the index 1 before where time decreases, so use +1 below.
    if min([0,ind])>0:  #if time decreases after index=0
        n_ind=len(ind)
        for i in range(n_ind):
            time[ind[i]+1]=(time[ind[i]+2]-time[ind[i]])/2. #reset time as ave of surrounding.
    return(time)

def alpha_95(R,N):
    if N==1: print('In alpha_95: N=1.  This is bad!')
    return np.arccos(1.-(N-R)/R*( (1./0.05)**(1./(N-1.)) - 1.))
    

class read_text_gauss:
    ### Read run_stats.txt files and return data in "a".
    def __init__(a,dir=dir):
        #files=['/Users/pdriscoll/memex/rayleigh/rayleigh_git/rayleigh/runs/dynamos/maginit7/'+s+'/run_stats.txt' for s in dir]
        files=['/Users/pdriscoll/memex/rayleigh/rayleigh_git/rayleigh/runs/dynamos/maginit7/'+s+'/gauss.txt' for s in dir]
        a.files=files
        nfiles=len(files)
        data={}  #a dict
        a.ekman=np.zeros(len(files))
        for ifile,file in enumerate(files):
            a.ekman[ifile]=float(dir[ifile].split('/')[0][2:])
            f=open(file,'r')
            next(f)  #skip first line (header of dir)
            if ifile==0:  #define elements of dict by first element in each line.
                for line in f:
                    data[line.split()[0]]=[line.split()[1:]]

            else:         #enter values where they should go in dict.
                for line in f:
                    data[line.split()[0]].append(line.split()[1:])
        # Parse data into arrays
        keys=data.keys()
        a.nrev=np.array(data['nrev,nexecursions,n_eq_cross,min_chron_len'])[:,0].astype(int)
        a.nexcursions=np.array(data['nrev,nexecursions,n_eq_cross,min_chron_len'])[:,1].astype(int)
        a.n_eq_cross=np.array(data['nrev,nexecursions,n_eq_cross,min_chron_len'])[:,2].astype(int)
        a.adnad=np.array(data['AD/NAD,O/E,Z/NZ,chi2'])[:,0].astype(float)
        a.oe=np.array(data['AD/NAD,O/E,Z/NZ,chi2'])[:,1].astype(float)
        a.znz=np.array(data['AD/NAD,O/E,Z/NZ,chi2'])[:,2].astype(float)
        a.chi2=np.array(data['AD/NAD,O/E,Z/NZ,chi2'])[:,3].astype(float)
        a.brms=np.array(data['gauss[brms,diprms,dipolarity,adipolarity]'])[:,0].astype(float)
        a.diprms=np.array(data['gauss[brms,diprms,dipolarity,adipolarity]'])[:,1].astype(float)
        a.dipolarity=np.array(data['gauss[brms,diprms,dipolarity,adipolarity]'])[:,2].astype(float)
        a.adipolarity=np.array(data['gauss[brms,diprms,dipolarity,adipolarity]'])[:,3].astype(float)
        a.len_kappa=np.array(data['length[kappa,dip,kyr]'])[:,0].astype(float)
        a.len_dip=np.array(data['length[kappa,dip,kyr]'])[:,1].astype(float)
        a.len_kyr=np.array(data['length[kappa,dip,kyr]'])[:,2].astype(float)
        a.lmax=8
        a.gauss=np.zeros((len(files),a.lmax+1,a.lmax+1),dtype=complex)
        for i in range(len(files)):
            for l in range(1,a.lmax+1):
                keylm='g,h(l='+str(l)+')'
                for m in range(l+1):
                    a.gauss[i,l,m]=complex(np.array(data[keylm])[i,m*2].astype(float),np.array(data[keylm])[i,m*2+1].astype(float))
        
class read_text_gauss_ave:
    def __init__(a,dir=dir):
        ### Read gauss_ave.txt files.
        path='/Users/pdriscoll/memex/rayleigh/rayleigh_git/rayleigh/runs/dynamos/maginit7/'
        files=[path+s+'/gauss_ave.txt' for s in dir]
        a.files=files
        nfiles=len(files)
        data={}  #a dict
        a.ekman=np.zeros(len(files))
        a.bot=np.zeros(len(files))
        for ifile,file in enumerate(files):
            #print(file)
            a.ekman[ifile]=float(dir[ifile].split('/')[0][2:])  #0th sub-dir, skip first 2 chars
            a.bot[ifile]=float(dir[ifile].split('/')[3][4:])
            f=open(file,'r')
            next(f)  #skip first line (header of dir)
            if ifile==0:  #define elements of dict by first element in each line.
                for line in f:
                    data[line.split()[0]]=[line.split()[1:]]
            else:         #enter values where they should go in dict.
                for line in f:
                    data[line.split()[0]].append(line.split()[1:])
                    #print(line)
        # Parse data into arrays
        keys=data.keys()
        a.times_ra=np.array(data['kyr']).astype(float)
        a.dipolarity=np.array(data['dipolarity,AD/NAD,O/E,Z/NZ,chi2'])[:,0].astype(float)
        a.adnad=np.array(data['dipolarity,AD/NAD,O/E,Z/NZ,chi2'])[:,1].astype(float)
        a.oe=np.array(data['dipolarity,AD/NAD,O/E,Z/NZ,chi2'])[:,2].astype(float)
        a.znz=np.array(data['dipolarity,AD/NAD,O/E,Z/NZ,chi2'])[:,3].astype(float)
        a.chi2=np.array(data['dipolarity,AD/NAD,O/E,Z/NZ,chi2'])[:,4].astype(float)
        a.win_len=np.array(data['window_lengths']).astype(float)
        a.nwin=len(a.win_len[0])  #num windows.  assume same in each file.
        nwin=a.nwin
        a.n_sub_win=np.array(data['n_sub_windows']).astype(float)
        #a.n_ind_sub_win=np.array(data['n_ind_sub_windows']).astype(float)
        a.ave_g10_run=np.array(data['ave_g10_run']).astype(float)
        a.std_g10_run=np.array(data['std_g10_run']).astype(float)
        a.ave_dip_run=np.array(data['ave_dip_run']).astype(float)
        a.std_dip_run=np.array(data['std_dip_run']).astype(float)
        a.ave_adip_run=np.array(data['ave_adip_run']).astype(float)
        a.std_adip_run=np.array(data['std_adip_run']).astype(float)
        a.ave_gnad_run=np.array(data['ave_gnad_run']).astype(float)
        a.std_gnad_run=np.array(data['std_gnad_run']).astype(float)
        a.ave_dec_run=np.array(data['ave_dec_run'])[:,:nwin].astype(float)  #ignore last string '[deg]'
        a.std_dec_run=np.array(data['std_dec_run'])[:,:nwin].astype(float)
        a.ave_inc_run=np.array(data['ave_inc_run'])[:,:nwin].astype(float)
        a.std_inc_run=np.array(data['std_inc_run'])[:,:nwin].astype(float)

        a.ave_R_run=np.array(data['ave_R_run'])[:,:nwin].astype(float)
        a.std_R_run=np.array(data['std_R_run'])[:,:nwin].astype(float)
        a.ave_a95_run=np.array(data['ave_a95_run'])[:,:nwin].astype(float)
        a.std_a95_run=np.array(data['std_a95_run'])[:,:nwin].astype(float)
        a.ave_kfish_run=np.array(data['ave_kfish_run'])[:,:nwin].astype(float)
        a.std_kfish_run=np.array(data['std_kfish_run'])[:,:nwin].astype(float)
        a.ave_Feq_run=np.array(data['ave_Feq_run'])[:,:nwin].astype(float)
        a.std_Feq_run=np.array(data['std_Feq_run'])[:,:nwin].astype(float)
        a.dt_crit=np.array(data['dt_crit,dec,inc,g10,gnad'])[:,0].astype(float)
        # Find ind of each dt_crit
        a.ind=np.zeros(nfiles,dtype=int)
        a.ave_dec_crit=np.zeros(nfiles)
        a.ave_inc_crit=np.zeros(nfiles)
        a.ave_g10_crit=np.zeros(nfiles)
        a.ave_gnad_crit=np.zeros(nfiles)
        a.ave_a95_crit=np.zeros(nfiles)
        a.ave_kfish_crit=np.zeros(nfiles)
        a.std_dec_crit=np.zeros(nfiles)
        a.std_inc_crit=np.zeros(nfiles)
        a.std_g10_crit=np.zeros(nfiles)
        a.std_gnad_crit=np.zeros(nfiles)
        a.std_a95_crit=np.zeros(nfiles)
        a.std_kfish_crit=np.zeros(nfiles)
        for i in range(nfiles):
            a.ind[i]=np.where(a.win_len[i]==a.dt_crit[i])[0]
            a.ave_dec_crit[i]=a.ave_dec_run[i,a.ind[i]]
            a.ave_inc_crit[i]=a.ave_inc_run[i,a.ind[i]]
            a.ave_g10_crit[i]=a.ave_g10_run[i,a.ind[i]]
            a.ave_gnad_crit[i]=a.ave_gnad_run[i,a.ind[i]]
            a.ave_a95_crit[i]=a.ave_a95_run[i,a.ind[i]]
            a.ave_kfish_crit[i]=a.ave_kfish_run[i,a.ind[i]]
            a.std_dec_crit[i]=a.std_dec_run[i,a.ind[i]]
            a.std_inc_crit[i]=a.std_inc_run[i,a.ind[i]]
            a.std_g10_crit[i]=a.std_g10_run[i,a.ind[i]]
            a.std_gnad_crit[i]=a.std_gnad_run[i,a.ind[i]]
            a.std_a95_crit[i]=a.std_a95_run[i,a.ind[i]]
            a.std_kfish_crit[i]=a.std_kfish_run[i,a.ind[i]]


class read_text_gavg:
    def __init__(a,dir=dir):
        ### Read global_avgs.txt files.
        path='/Users/pdriscoll/memex/rayleigh/rayleigh_git/rayleigh/runs/dynamos/maginit7/'
        files=[path+s+'/global_avgs.txt' for s in dir]
        a.files=files
        nfiles=len(files)
        data={}  #a dict
        a.ekman=np.zeros(len(files))
        a.bot=np.zeros(len(files))
        for ifile,file in enumerate(files):
            #print(file)
            a.ekman[ifile]=float(dir[ifile].split('/')[0][2:])  #0th sub-dir, skip first 2 chars
            a.bot[ifile]=float(dir[ifile].split('/')[3][4:])
            f=open(file,'r')
            next(f)  #skip first line (header of dir)
            if ifile==0:  #define elements of dict by first element in each line.
                for line in f:
                    data[line.split()[0]]=[line.split()[1:]]
            else:         #enter values where they should go in dict.
                for line in f:
                    data[line.split()[0]].append(line.split()[1:])
                    #print(line)
        # Parse data into arrays
        keys=data.keys()
        a.len_kappa=np.array(data['length[kappa,dip,kyr]'])[:,0].astype(float)
        a.len_dip=np.array(data['length[kappa,dip,kyr]'])[:,1].astype(float)
        a.len_kyr=np.array(data['length[kappa,dip,kyr]'])[:,2].astype(float)
        a.ave_ke=np.array(data['KE'])[:,0].astype(float)
        a.std_ke=np.array(data['KE'])[:,1].astype(float)
        a.ave_me=np.array(data['ME'])[:,0].astype(float)
        a.std_me=np.array(data['ME'])[:,1].astype(float)
