#!/usr/bin/env python
####################################################
#
#  Diagnostics for a dynamo run.
#  Run: plot_global_avgs.py and plot_spectral_trace.py
#
####################################################

import sys
import os

rayleighscripts='/Users/pdriscoll/RESEARCH/rayleigh/bitbucket/rayleigh_diagnostics2/rayleigh_diagnostics'
#print(rayleighscripts)

os.system(rayleighscripts+"/plot_global_avgs.py -d &")
os.system(rayleighscripts+"/plot_spectral_trace.py &")
#os.system(rayleighscripts+"/plot_spectral_trace_gauss.py &")
