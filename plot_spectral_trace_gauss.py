#!/usr/bin/env python
########################################################################3
#
#   plot_spectral_trace_gauss.py
#   same as plot_spectra_trace.py but using Gauss coef rather than Br(l,m)
#
#   Plotting Example:  Shell_Spectra
#
#   plot_spectral_trace.py
#   Plot time series (trace) of kinetic and
#   magnetic power spectral elements.
#
#   We plot the velocity power spectrum from one
#   shell spectrum output using the PowerSpectrum
#   class from diagnostic_reading.py.  
#   
#   Ideally, one should loop over several spectra
#   and take a time-average, similar to the approach
#   taken in plot_energy_flux.py.  This routine plots
#   a SINGLE SNAPSHOT of the power.
#
#   When a PowerSpectrum object is initialized, 
#   only power is computed from the Shell_Spectra
#   file and saved.
#  
#   If we want to look at individual m-values,
#   we can use the ShellSpectra class instead.
#   See diagnostic_reading.py for a brief 
#   description of that data structure.
#
#   The power spectrum structure is simpler to
#   work with.  It contains the following attributes:
#    ----------------------------------
#    self.niter                                    : number of time steps
#    self.nr                                       : number of radii at which power spectra are available
#    self.lmax                                     : maximum spherical harmonic degree l
#    self.radius[0:nr-1]                           : radii of the shell slices output
#    self.inds[0:nr-1]                             : radial indices of the shell slices output
#    self.power[0:lmax,0:nr-1,0:niter-1,0:2]       : the velocity power spectrum.  The third
#                                                  : index indicates (0:total,1:m=0, 2:total-m=0 power)
#    self.mpower[0:lmax,0:nr-1,0:niter-1,0:2]      : the magnetic power spectrum
#    self.iters[0:niter-1]                         : The time step numbers stored in this output file
#    self.time[0:niter-1]                          : The simulation time corresponding to each time step
#    self.magnetic                                 : True if mpower exists
#
#    self.vals[0:lmax,0:mmax,0:nr-1,0:nq-1,0:niter-1] 
#                                                  : The complex spectra of the shells output 
#    -------------------------------------
##################################

from rayleigh_diagnostics import Power_Spectrum, Shell_Spectra
from diagnostic_tools import file_list, calculate_Ylms,renormalize_shell_spectra,gauss_coef,\
     dipole_latitude,dipole_longitude
import matplotlib.pyplot as plt
from matplotlib.pyplot import plot,ion,show
import numpy as np
from math import acos
import sys
import os 
cwd = os.getcwd()
r2d=180./np.pi  #radians/degree
ion()
pi=np.pi
fontsize=20
from matplotlib import rc, rcParams
rcParams['legend.fontsize'] = fontsize
rcParams['axes.labelsize'] = fontsize
rcParams['xtick.labelsize'] = fontsize
rcParams['ytick.labelsize'] = fontsize
rcParams['font.size'] = fontsize
#rc('text', usetex=True)


import argparse
parser = argparse.ArgumentParser( \
                       description="""Plot global averages (G_Avgs): KE vs time.""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False, default=None, 
                    help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False, default=None, 
                    help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-sn', '--savename', action='store', dest='savename', type=str, default='power_spectrum_gauss', 
                    required=False, help='savename of the figure')
args = parser.parse_args()

print("initial="+str(args.initial)+" final="+str(args.final))

def set_plot_fontsize(plt,fontsize=None,xlabel=None,ylabel=None):
    plt.ylabel(ylabels[i],{'fontsize':fontsize})
    plt.xlabel(xlabel,{'fontsize':fontsize})
    plt.yticks(size=fontsize)
    plt.xticks(size=fontsize)
    
#Set savefig to True to save to savefile.  Interactive plot otherwise.
savefig = False
#savefig = True
savefig = args.save
#savefile = 'power_spectrum_gauss'
savefile = args.savename
if savefig: print('savefig='+str(savefig))
window_title = 'spectral_trace ('+cwd+')'
initial=args.initial
final=args.final
print("Reading files...")
files = file_list(initial, final, 'Shell_Spectra')

#files=files[0]  #only use first file for now.............
nfiles=len(files)
#file = files[nfiles-2]  #take second to last file?
#print(file)

# spec.power will contain the power spectrum
# this object can also be initialized with magnetic=True
# to generate a magnetic power spectrum stored 
# in spec.mpower

spec0 = Power_Spectrum(files[0],magnetic=True,path='')  #get first spec.
sspec0 = Shell_Spectra(files[0],path=os.path.join(os.path.curdir,""))
# We use the lookup table to find where br, vtheta, and bphi are stored
br_index = sspec0.lut[801]
dims=[spec0.lmax,spec0.nr,spec0.niter]   #get dims.
spec = [Power_Spectrum('blank',dims) for i in range(nfiles)]  #define spec as a blank list to get a template object.
spec[0]=spec0  #def 1st element.
sspec = [Power_Spectrum('blank',dims) for i in range(nfiles)]  #define spec as a blank list to get a template object.
sspec[0]=sspec0  #def 1st element.
lmax=spec0.lmax
lmax_gauss=min([84,lmax])  #limit to <84 bc factorial(170)=inf, so lmax<84. 
nl=lmax+1
nr=spec0.nr
niter=spec0.niter
#print(spec0.power.shape)

#rad_index
rad_index=0#.  # only use top radius level?

#Initialize full arr with first spec file.
times=spec0.time
powers=np.zeros((lmax+1,nr,niter,3))
#print(powers.shape)
powers=spec0.power
mpowers=spec0.mpower
powers_allm=np.reshape(powers[:,rad_index,:,0],(nl,niter))    #sub matrix for powers(:,:,:,0) which is total
powers_m0=powers[:,rad_index,:,1]      #sub matrix for powers(:,:,:,1) which is m=0
powers_mne0=powers[:,rad_index,:,2]    #sub matrix for powers(:,:,:,2) which is m ne 0
###Note: assume mpower(l=0) should be zero?  Ignoring it. Maybe it's nonzero from transforming B(r,t,p) to (l,m,r)?
mpowers_allm=np.zeros((nl,niter))      #set all initial mpower(l=0) to zero?
mpowers_allm[1:,:]=np.reshape(mpowers[1:,rad_index,:,0],(nl-1,niter))    #sub matrix for powers(:,:,:,0) which is total
mpowers_dip=np.reshape(mpowers[1,rad_index,:,0],(niter))            #dipole mpower, l=1, m=0,+-1 (total dipole)
mpowers_dipaxi=np.reshape(mpowers[1,rad_index,:,1],(niter))         #axial dipole mpower, l=1, m=0.
#print("powers_allm.shape="+str(powers_allm.shape))
g10=np.real(sspec0.vals[1,0,0, br_index, :])
g11=np.real(sspec0.vals[1,1,0, br_index, :])
h11=np.imag(sspec0.vals[1,1,0, br_index, :])
# Convert Brlm to orthonormal
brlm_on=renormalize_shell_spectra(sspec0.vals[:lmax_gauss,:lmax_gauss,0,br_index,:])
# Compute Gauss
gauss=gauss_coef(brlm_on)

#dip_lat = np.arccos(g10/np.sqrt(g10**2+g11**2+h11**2))
#lon_shift = np.asarray([np.pi if g11[k] < 0 and h11[k] <0 else 0.0 for k in xrange(sspec.niter)])
#dip_lon = np.arccos(g11/np.sqrt(g11**2+h11**2)) + lon_shift

dip_lat = dipole_latitude(g10,g11,h11)
dip_lon = dipole_longitude(g10,g11,h11)


for i in range(1,nfiles):  #skip 1st.
    sys.stdout.write('file '+str(i)+'/'+str(nfiles)+' \r')
    sys.stdout.flush()
    spec_i = Power_Spectrum(files[i],magnetic=True,path='')
    sspec_i = Shell_Spectra(files[i],path=os.path.join(os.path.curdir,""))
    br_index_i = sspec_i.lut[801]
    spec[i]=spec_i                            #contains all spec[i]
    sspec[i]=sspec_i                            #contains all spec[i]
    time=spec_i.time
    ntimes=len(times)                         #length of previous i full time arr
    ntimes_i=len(time)                        #ntimes in this file.
    ntimes_new=ntimes+ntimes_i                #new full length     
    i_time_start=ntimes-1+1                       #first time in current file.
    i_time_end=i_time_start+ntimes_i          #end time of current file.
    #print("i="+str(i)+" file="+files[i])
    #print("ntimes="+str(ntimes)+" ntimes_i="+str(ntimes_i)+" ntimes_new="+str(ntimes_new)+
    #      " i_time_start="+str(i_time_start)+" i_time_end="+str(i_time_end))
    times=np.append(times,spec_i.time)        #extend times arr.
    power_i_2d=np.reshape(spec_i.power[:,rad_index,:,0],(nl,ntimes_i))  #get current spec_i 2d power arr (nl,nt)
    mpower_i_2d=np.reshape(spec_i.mpower[1:,rad_index,:,0],(nl-1,ntimes_i))  #get current spec_i 2d power arr (nl,nt)
    powers_allm_new=np.zeros((nl,ntimes_new))            #create new longer arr
    powers_allm_new[:,0:i_time_start]=powers_allm        #set all previous values.
    powers_allm_new[:,i_time_start:i_time_end]=power_i_2d  #set new values.
    powers_allm=powers_allm_new                          #update with new.
    mpowers_allm_new=np.zeros((nl,ntimes_new))           #create new longer arr
    mpowers_allm_new[:,0:i_time_start]=mpowers_allm      #set all previous values.
    mpowers_allm_new[1:,i_time_start:i_time_end]=mpower_i_2d  #set new values.  exclude l=0.
    mpowers_allm=mpowers_allm_new                        #update with new.
    ## mpowers
    mpowers_dip=np.append(mpowers_dip,spec_i.mpower[1,rad_index,:,0])
    mpowers_dipaxi=np.append(mpowers_dipaxi,spec_i.mpower[1,rad_index,:,1])
    # Convert Brlm to orthonormal
    brlm_on=renormalize_shell_spectra(sspec_i.vals[:lmax_gauss,:lmax_gauss,0,br_index_i,:])
    # Compute Gauss
    gauss_i=gauss_coef(brlm_on)
    gauss=np.append(gauss,gauss_i,axis=2)    #append along time axis
    # Define Components.
    g10_i=np.real(gauss_i[1,0,:])
    g11_i=np.real(gauss_i[1,1,:])
    h11_i=np.imag(gauss_i[1,1,:])
    g10=np.append(g10, g10_i)
    g11=np.append(g11, g11_i)
    h11=np.append(h11, h11_i)
    #dip_lat_i = np.arccos(g10_i/np.sqrt(g10_i**2+g11_i**2+h11_i**2))
    #lon_shift_i = np.asarray([np.pi if g11_i[k] < 0 and h11_i[k] <0 else 0.0 for k in xrange(sspec_i.niter)])
    #dip_lon_i = np.arccos(g11_i/np.sqrt(g11_i**2+h11_i**2)) + lon_shift_i
    dip_lat_i = dipole_latitude(g10_i,g11_i,h11_i)
    dip_lon_i = dipole_longitude(g10_i,g11_i,h11_i)
    dip_lat=np.append(dip_lat,dip_lat_i)
    dip_lon=np.append(dip_lon,dip_lon_i)

#Compute mpowers_tot as fn of time.
ntimes=len(times)
ntimes_stat=ntimes  #ntimes to use for statistics.
tra=max(times)-min(times)  #time range
i_trim=0  #default, compute stats from t=0.
#if tra>5. :
if tra>5. and times[0]==0:   #added times[0]==0 to avoid cutting too much. (12/5/17)
    i_trim=np.min(np.where(times-times[0] > 5.))  #cut out data before i_trim
    ntimes_stat=ntimes-i_trim  #ntimes to use for statistics
ind_stat=np.arange(i_trim,ntimes,1)  #dtype=np.uint8)  #create index array from i_trim to ntimes.
mpowers_tot=np.zeros(ntimes)
gauss_tot=np.zeros(ntimes)
gauss_tot_vec=np.zeros(ntimes)
gauss_nad=np.zeros(ntimes)  #sum of all glm terms except g10.
gauss_zonal=np.zeros(ntimes)   #sum of zonal terms (m=0)
lowes=np.zeros((lmax_gauss,ntimes))  #sum of all m for each l.
for t in range(ntimes):
    mpowers_tot[t]=np.sum(mpowers_allm[:,t])  #sum over all l at time t.
    gauss_tot[t]=np.sum([gauss[:,:,t].real,gauss[:,:,t].imag])  #sum real+imag?
    gauss_tot_vec[t]=np.linalg.norm([gauss[:,:,t].real,gauss[:,:,t].imag])  #vector sum = sqrt of sum of squares.
    gauss_nad[t]=gauss_tot[t]-g10[t]     #sum all terms except g10.
    gauss_zonal[t]=np.sum([gauss[:,0,t].real,gauss[:,0,t].imag])  
    for l in range(lmax_gauss):
        lowes[l,t]=np.linalg.norm([gauss[l,:l,t].real,gauss[l,:l,t].imag]) #sqrt(sum(glm^2+hlm^2))
# Time ave of each glm.
gauss_ave=np.zeros((lmax_gauss,lmax_gauss),dtype=complex)   #time ave of each glm
for l in range(lmax_gauss):
    for m in range(l):
        gauss_ave[l,m]=complex(np.mean(gauss[l,m,:].real),np.mean(gauss[l,m,:].imag))

# Get_lpower
sspec[0].get_lpower()  #adds .lpower to sspec[0]

# Lat Shift.
lat_shift_yes=np.zeros(ntimes)
dip_lat_shifted=np.zeros(ntimes)
for k in range(ntimes):
    dip_lat_shifted[k]=dip_lat[k]
    if g10[k]<0:
        lat_shift_yes[k]=1.
        dip_lat_shifted[k]=2*np.pi-dip_lat[k]
# Dipolarity
dipolarity=np.sqrt(mpowers_dip)/np.sqrt(mpowers_tot)

### PRINTING ###
print("ME mean: tot=%8.2e dip=%8.2e dip/tot=%8.2e from t=%8.2f-%8.2f" %(np.mean(mpowers_tot[ind_stat]),np.mean(mpowers_dip[ind_stat]),np.mean(mpowers_dip[ind_stat]/mpowers_tot[ind_stat]),times[ind_stat[0]],times[max(ind_stat)]))
print("ME final: tot=%8.2e dip=%8.2e dip/tot=%8.2e" %(mpowers_tot[ntimes-1],mpowers_dip[ntimes-1],mpowers_dip[ntimes-1]/mpowers_tot[ntimes-1]))

### PLOTTING ###
time_index = 0  #plot the first record?
# Scale time from kappa units to kyr
Pm=10.
Pr=1.
kyr_dip_decay=50.
print('Assume for time scaling: Pm=%f, Pr=%f'%(Pm,Pr))
times_mag=times/Pm
times_dip=times_mag/(1.538461/pi)**2.
times_kyr=times_dip*kyr_dip_decay

time=spec0.time
#kpower_l0=spec.power[0,rad_index,:,0]  #KE power in l=0, m=0 over time.
#kpower_l0_m0=spec.power[0,rad_index,:,1]  #KE power in l=m=0 over time.
#kpower_l0_conv=spec.power[0,rad_index,:,2]  #KE power in m ne 0.
ke_degree=0.  #1.
me_degree=1.

nfig=1
plt.figure(nfig,figsize=(15,10))
plt.figure(nfig).canvas.set_window_title(window_title)
lw = 1.5
label0='All m'
label1='m=0'
label2='Total - m=0'

# Plot ME Tot, Dipole, Axi Dip
plt.subplot(221)
plt.plot(times,mpowers_tot,label='Total',linewidth=lw)
plt.plot(times,mpowers_dip,label='Dipole',linewidth=lw)
plt.plot(times,mpowers_dipaxi,label='Axial Dipole',linewidth=lw)
plt.xlabel('Time')
plt.ylabel('Magnetic Power')
legend = plt.legend(loc='lower left', shadow=True, ncol = 1)
#plt.title(cwd,fontsize=10)

# Plot Dipolarity: dipole/total
plt.subplot(222)
plt.plot(times,mpowers_dip/mpowers_tot,label='dip/tot',linewidth=lw)
plt.plot(times,mpowers_dipaxi/mpowers_tot,label='dipaxi/tot',linewidth=lw)
plt.xlabel('Time')
plt.ylabel('Dipole/Total (CMB)')
legend = plt.legend(loc='lower left', shadow=True, ncol = 1)

# Plot Dipole Tilt
plt.subplot(223)
plt.plot(times,dip_lat*180./np.pi,linewidth=lw)
plt.xlabel('Time')
plt.ylabel('Dipole Co-Latitude [deg]')
plt.ylim(ymin=0,ymax=180)

# Plot Dipole longitude.
plt.subplot(224)
plt.plot(times,dip_lon*180./np.pi,linewidth=lw)
plt.xlabel('Time')
plt.ylabel('Dipole Longitude [deg]')
plt.ylim(ymin=0,ymax=360)

plt.tight_layout()
if (savefig):
    plt.savefig(savefile+str(nfig)+'.pdf')
    print('Saved '+savefile)
else:
    plt.show()

### Add second Figure for dipole lat-lon path.
nfig+=1
plt.figure(nfig,figsize=(10,10))
plt.subplot(221)  #create 2x2 subplots, but use top 2 as single mollweide.
plt.subplot2grid((2,2),(0,0),colspan=2,projection='mollweide')
plt.grid(True)
plt.plot(dip_lon-np.pi,dip_lat-np.pi/2.)  #lat here should be -90 to +90, lon between -180,+180
plt.title('Dipole Axis')

### Add 3rd plot for polar view.
# Compute seperate N/S arrays.
dip_lat_n=[]
dip_lat_s=[]
dip_lon_n=[]
dip_lon_s=[]
for p in range(ntimes):
    if dip_lat[p]<np.pi/2.:
        dip_lat_s=np.append(dip_lat_s,dip_lat[p])
        dip_lon_s=np.append(dip_lon_s,dip_lon[p])
    if dip_lat[p]>=np.pi/2.:
        dip_lat_n=np.append(dip_lat_n,dip_lat[p])
        dip_lon_n=np.append(dip_lon_n,dip_lon[p])
#Remove first element.
#dip_lat_s=dip_lat_s[:]
#dip_lat_n=dip_lat_n[:]
#dip_lon_s=dip_lon_s[:]
#dip_lon_n=dip_lon_n[:]
#Plot polar views.
plt.subplot2grid((2,2),(1,0),colspan=1,projection='polar')
plt.grid(True)
#Latitude labels
rvals=np.arange(0,1.1,1/6.)      #use six latitude labels
rlabels=90-np.arange(0,91,15)    #latitude labels, numbers.
rlabels=rlabels.astype(np.str)   #change to str array.
#North
if len(dip_lat_n) > 0:
    plt.plot(dip_lon_n,(np.pi/2.-(dip_lat_n-np.pi/2.))/(np.pi/2.))
    plt.rgrids(rvals[1:],rlabels[1:])
    plt.title('North Pole')
    plt.subplot2grid((2,2),(1,1),colspan=1,projection='polar')
    plt.grid(True)
#South
if len(dip_lat_s) > 0:
#plt.plot(dip_lon_s,(np.pi/2.-dip_lat_s)/(np.pi/2.))
    plt.plot(dip_lon_s,dip_lat_s/(np.pi/2.))
    plt.rgrids(rvals[1:],rlabels[1:])
    plt.title('South Pole')

plt.tight_layout()
if (savefig):
    plt.savefig(savefile+str(nfig)+'.pdf')
    print('Saved '+savefile)
else:
    plt.show()

################################################################################################

# Cut out initial adjustment time by using ind_stat only.
times_kyr=times_kyr[ind_stat]
g10=g10[ind_stat]
gauss=gauss[:,:,ind_stat]
gauss_tot=gauss_tot[ind_stat]
gauss_nad=gauss_nad[ind_stat]
gauss_zonal=gauss_zonal[ind_stat]
lowes=lowes[:,ind_stat]
mpowers_allm=mpowers_allm[:,ind_stat]
mpowers_dip=mpowers_dip[ind_stat]
dipolarity=dipolarity[ind_stat]
# Davies running ave, D14 eq 8.
g10_run=np.zeros(ntimes_stat)
g10_run[0]=g10[0]  #first ave is first val??  eq8 starts with t=1, where t is sample or timestep number.
glm_run=np.zeros(ntimes_stat)
gnad_run=np.zeros(ntimes_stat)      #nad=nonaxdip
gzonal_run=np.zeros(ntimes_stat)
for i in range(1,ntimes_stat):
    g10_run[i]=g10_run[i-1]*(i-1)/i + (1./i)*g10[i]
    glm_run[i]=glm_run[i-1]*(i-1)/i + (1./i)*gauss_tot[i]
    gnad_run[i]=gnad_run[i-1]*(i-1)/i + (1./i)*gauss_nad[i]
    gzonal_run[i]=gzonal_run[i-1]*(i-1)/i + (1./i)*gauss_zonal[i]
# Compute E-like stats
from elike_stats import elike_stats
adnad,oddeven,zonality,elike_means=elike_stats(gauss)
# Print stats
#print('ave dipolarity=%.2e, AD/NAD=%.2e, O/E=%.2e, Z/NZ=%.2e'%(np.mean(dipolarity),np.mean(adnad),np.mean(oddeven),np.mean(zonality)))
print('ave dipolarity=%.2e, AD/NAD=%.2e, O/E=%.2e, Z/NZ=%.2e, chi2=%.2e'%(np.mean(dipolarity),elike_means[0],elike_means[1],elike_means[2],elike_means[3]))

### Plot time averages
nfig+=1
plt.figure(nfig,figsize=(15,10))
# Plot g10(t), g10_run(t)
plt.subplot(221)
plot(times_kyr,g10)
plot(times_kyr,g10_run)
plt.xlabel('kyr')
plt.ylabel('g10, g10_run')
# Plot glm(t), glm_run(t)
plt.subplot(222)
plot(times_kyr,gauss_tot)
plot(times_kyr,glm_run)
plt.xlabel('kyr')
plt.ylabel('g_tot, g_tot_run')
# Plot nonaxdip=nad
plt.subplot(223)
#plot(times_kyr,gauss_nad)
plot(times_kyr,gauss_nad)
plot(times_kyr,gnad_run)
plt.xlabel('kyr')
plt.ylabel('(g_tot-g10), _run')
# Plot zonal terms only
plt.subplot(224)
plot(times_kyr,gauss_zonal)
plot(times_kyr,gzonal_run)
plt.xlabel('kyr')
plt.ylabel('g_zonal (m=0) , _run')

plt.tight_layout()
if (savefig):
    plt.savefig(savefile+str(nfig)+'.pdf')
    print('Saved '+savefile)
else:
    plt.show()
    
### Plot time averages of the first few harmonics
nfig+=1
plt.figure(nfig,figsize=(15,10))
plt.subplot(221)
plot(times_kyr,gauss[1,0,:].real,label='(1,0)')
plot(times_kyr,gauss[1,1,:].real,label='(1,+1)')
plot(times_kyr,gauss[1,1,:].imag,label='(1,-1)')
plot(times_kyr,gauss[2,0,:].real,label='(2,0)')
plot(times_kyr,gauss[2,1,:].real,label='(2,+1)')
plot(times_kyr,gauss[2,1,:].imag,label='(2,-1)')
plt.ylabel('glm')
plt.xlabel('kyr')
plt.legend()
plt.subplot(222)
lm_vals=[[1,0],[1,1],[2,0],[2,1],[2,2],[3,0],[3,1],[3,2],[3,3]]
nlm=len(lm_vals)
barwidth=0.35
xlabels=['']
for i in range(nlm):
    lm=(lm_vals[i][0],lm_vals[i][1])
    plt.bar(i,gauss_ave[lm].real,barwidth,label=str(lm)+'.real')
    plt.bar(i+barwidth,gauss_ave[lm].imag,barwidth,label=str(lm)+'.imag')
    xlabels.append(str(lm))
plt.xlim((0,nlm-barwidth))
plt.xlabel('time ave glm')
plt.xticks(np.arange(nlm)+barwidth*0.5,xlabels[1:])  #xtick at center of bar

plt.tight_layout()
if (savefig):
    plt.savefig(savefile+str(nfig)+'.pdf')
    print('Saved '+savefile)
else:
    plt.show()
    

### Plot spectral contour trace of B^2_l  (mpower)
# First plot dip_lat timeseries
nfig+=1
fig=plt.figure(nfig,figsize=(15,10))
xlabel='kyr'
ylabels=['Dipole Co-Latitude [deg]','Degree, l']
axlt=fig.add_subplot(211)
axlt.plot(times_kyr,dip_lat[ind_stat]*180./np.pi,color='black',linewidth=lw)
axlt.set_xlim(min(times_kyr),max(times_kyr))
axlt.set_ylabel(ylabels[0])
axlt.set_xlabel(xlabel)
axrt = axlt.twinx()
axrt.plot(times_kyr,lowes[1,:],color='green',linewidth=lw)
axrt.set_xlim(min(times_kyr),max(times_kyr))
axrt.set_ylabel('Dipole Energy')

# Use contourf
axl=fig.add_subplot(212)
nl=8
lmin=1
larr=range(lmin,nl+lmin)
y,x=np.meshgrid(larr,times_kyr)
z=mpowers_allm[lmin:nl+lmin,:]/mpowers_dip   #normalize to dipole
im=axl.contourf(x,y,np.transpose(z),cmap='PuRd')
axl.set_xlabel(xlabel)
axl.set_ylabel(ylabels[1])
axl.set_xlim(min(times_kyr),max(times_kyr))
label_cb='ME_l/ME_dip'
cb=fig.colorbar(im,label=label_cb,pad=0.12)
axr = axl.twinx()
axr.plot(times_kyr,dip_lat[ind_stat]*180./np.pi,color='black',linewidth=lw)
axr.set_xlim(min(times_kyr),max(times_kyr))
axr.set_ylabel(ylabels[0])


#plt.tight_layout()
if (savefig):
    plt.savefig(savefile+str(nfig)+'.pdf')
    print('Saved '+savefile)
else:
    plt.show()
    
######  Plot elike stats  #######
nfig+=1
plt.figure(nfig,figsize=(15,10))
plt.subplot(221,xlabel='kyr',ylabel='dipolarity')
plot(times_kyr,dipolarity)
plt.subplot(222,xlabel='kyr',ylabel='AD/NAD')
plot(times_kyr,adnad)
plt.subplot(223,xlabel='kyr',ylabel='O/E')
plot(times_kyr,oddeven)
plt.subplot(224,xlabel='kyr',ylabel='Z/NZ')
plot(times_kyr,zonality)

plt.tight_layout()
if (savefig):
    plt.savefig(savefile+str(nfig)+'.pdf')
    print('Saved '+savefile)
else:
    plt.show()

### Histogram of DM ###
nfig+=1
plt.figure(nfig,figsize=(15,10))
bins=50.
hist=plt.hist(lowes[1,:],bins=50,log=True)
plt.ylabel('N')
plt.xlabel('DM')
if (args.save):
    plt.savefig('hist_DM'+'.eps',format='eps')
