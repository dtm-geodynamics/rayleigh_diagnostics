#!/usr/bin/env python
### multi_update_run_stats.py 12/5/17 PED ###
# PURPOSE: run multiple instance of update_run_stats.py#

runnames=['ek1e-3/ra1e6/flux/bot-01','ek1e-3/ra1e6/flux/bot-02','ek1e-3/ra1e6/flux/bot-04','ek1e-3/ra1e6/flux/bot-05',\
        'ek1e-3/ra1e6/flux/bot-06','ek1e-3/ra1e6/flux/bot-08','ek1e-3/ra1e6/flux/bot-10']#,\
#        'ek3e-4/ra5e6/flux/bot-1','ek3e-4/ra5e6/flux/bot-5','ek3e-4/ra5e6/flux/bot-10',\
#        'ek3e-4/ra5e6/flux/bot-10/nr84','ek3e-4/ra5e6/flux/bot-12/nr84_nt168','ek3e-4/ra5e6/flux/bot-15/nr84',\
#        'ek3e-4/ra5e6/flux/bot-15/nr84_nt168','ek3e-4/ra5e6/flux/bot-17/nr84','ek3e-4/ra5e6/flux/bot-20/nr84']
paths=['/Users/pdriscoll/memex/rayleigh/rayleigh_git/rayleigh/runs/dynamos/maginit7/'+s for s in runnames]
print(paths)

from update_run_stats import update_run_stats
for ipath in range(len(paths)):
    update_run_stats(dir=paths[ipath])
print('Finished multi_update_run_stats.')
