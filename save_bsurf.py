#!/usr/bin/env python

####################################################################################################
#
#  Compute the PSV of a dynamo model
#  Sample Br(CMB,theta,phi), convert it to Bsurf
#  Plot Intensity vs Inclination (tan(Inc)=2tan(lat))
#  Plot histogram of Intensity over time.
#  Use shell_slices to extract Br.
#
#  Shell-Slice (Shell_Slices) plotting example
#  - Reads in a single Shell_Slice file.
#  - Plots vr, vphi, and entropy
#
#  This example routine makes use of the ShellSlice
#  data structure associated with the Shell_Slices output.
#  Upon initializing a ShellSlice object, the 
#  object will contain the following attributes:
#    ----------------------------------
#    self.niter                                    : number of time steps
#    self.nq                                       : number of diagnostic quantities output
#    self.nr                                       : number of shell slices output
#    self.ntheta                                   : number of theta points
#    self.nphi                                     : number of phi points
#    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
#    self.radius[0:nr-1]                           : radii of the shell slices output
#    self.inds[0:nr-1]                             : radial indices of the shell slices output
#    self.costheta[0:ntheta-1]                     : cos(theta grid)
#    self.sintheta[0:ntheta-1]                     : sin(theta grid)
#    self.vals[0:nphi-1,0:ntheta-1,0:nr-1,0:nq-1,0:niter-1] 
#                                                  : The shell slices 
#    self.iters[0:niter-1]                         : The time step numbers stored in this output file
#    self.time[0:niter-1]                          : The simulation time corresponding to each time step
#    self.version                                  : The version code for this particular output (internal use)
#    self.lut                                      : Lookup table for the different diagnostics output
#    -------------------------------------

#import matplotlib
#matplotlib.use("Agg")

import pylab as p 
import numpy as np
from diagnostic_reading import ShellSlice,ShellSpectra
from diagnostic_tools import file_list, calculate_Ylms, gauss_coef, norm_lm, norm_lm_schmidt
from diagnostic_plotting import lutlabels,getlabel
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import ticker
from matplotlib.pyplot import ion,plot,show
import argparse
from scipy.special import lpmv
pi=np.pi
plt.ion()

parser = argparse.ArgumentParser( \
                       description="""Plot PSV.""")
parser.add_argument('-i', '--initial', action='store', type=str, dest='initial', required=False, default=None, 
                    help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=str, dest='final', required=False, default=None, 
                    help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-d', '--default',action='store_true',dest='default',help='use default plotting options',
                    default=False)
args = parser.parse_args()

if args.default: print("default set")

# Set saveplot to True to save to a .png file. 
# Set to False to view plots interactively on-screen.
saveplot = args.save
savefile = 'shell_slice.png'

rnorm = 1./(1-0.35)
path='Shell_Slices'
files = file_list(args.initial, args.final, path=path)
nfiles=len(files)
path='Shell_Spectra'
filespec = file_list(args.initial, args.final, path=path)
nfilespec=len(filespec)

#Read in last shell slice
a=ShellSlice('/01000000',path='Shell_Slices')
#a = ShellSlice(filename=files[nfiles-1],path='')
s = ShellSpectra(filespec[nfilespec-1],path='')  #vals[0:lmax,0:mmax,0:nr-1,0:nq-1,0:niter-1]

#Default Plotting options.
user_codes = [401,402,403]  #Br,Btheta,Bphi
rad_inds = 1.   #use CMB only.
q=lutlabels(user_codes)
print(q.labels)
var_ind = a.lut[user_codes]
vnames=q.labels
br=a.vals[:,:,0,var_ind[0],:]
bt=a.vals[:,:,0,var_ind[1],:]
bp=a.vals[:,:,0,var_ind[2],:]
time = a.time

# Extract data
ntheta=a.ntheta
nphi=a.nphi
nl = s.nell
nm = s.nm
#br=np.zeros([nphi,ntheta,0])  #need to append to iters from other files.
#bt=np.zeros([nphi,ntheta,0])  #need to append to iters from other files.
#bp=np.zeros([nphi,ntheta,0])
bsr=np.zeros([nl,nm,0])
bst=bsr
bsp=bsr
time = np.zeros([0])
times = np.zeros([0])  #time from shellspectrum
print('Reading in data')
# Read in Shell_Slices
"""
for ifile,f in enumerate(files):
    a=ShellSlice(filename=f,path="")  #returns values at CMB and within domain.
    if ifile==0:
        br=a.vals[:,:,0,var_ind[0],:]
        bt=a.vals[:,:,0,var_ind[1],:]
        bp=a.vals[:,:,0,var_ind[2],:]
        time = a.time
    else:
        br=np.append(br,a.vals[:,:,0,var_ind[0],:],axis=2)
        bt=np.append(bt,a.vals[:,:,0,var_ind[1],:],axis=2)
        bp=np.append(bp,a.vals[:,:,0,var_ind[2],:],axis=2)
        time=np.append(time,a.time)
ntime=len(time)
"""

# Read in Shell_Spectra
"""
for ifile,f in enumerate(filespec):
    s=ShellSpectra(filename=f,path="")  #returns harmonic amplitudes.
    if ifile==0:
        bsr=s.vals[:,:,0,var_ind[0],:]  #complex
        bst=s.vals[:,:,0,var_ind[1],:]
        bsp=s.vals[:,:,0,var_ind[2],:]
        times = s.time
    else:
        bsr=np.append(bsr,s.vals[:,:,0,var_ind[0],:],axis=2)  #complex
        bst=np.append(bst,s.vals[:,:,0,var_ind[1],:],axis=2)
        bsp=np.append(bsp,s.vals[:,:,0,var_ind[2],:],axis=2)
        times=np.append(times,s.time)
"""
s=ShellSpectra(filename='/01000000',path='Shell_Spectra')  #vals[0:lmax,0:mmax,0:nr-1,0:nq-1,0:niter-1]
var_ind = s.lut[user_codes]
bsr=s.vals[:,:,0,var_ind[0],:]  #complex
bst=s.vals[:,:,0,var_ind[1],:]
bsp=s.vals[:,:,0,var_ind[2],:]
times = s.time
ntimes=len(times)
#inc = np.arctan(br/bt)  #inclination.

# Extrapolate from CMB to Surface.
r_cmb=3481e3
r_surf=6371e3
nl_surf=nl
nm_surf=nm
ntimes_surf=10
ntheta=a.ntheta
nphi=a.nphi
theta = np.linspace(pi,0,ntheta)
phi = np.linspace(0,2*pi,nphi)
br_cmb=np.zeros((ntheta,nphi))  #this is the transformed br(theta,phi) from blm
itime=9
print('Computing Gauss')
# Compute Gauss coef at cmb.
g_lm_cmb=np.zeros((nl_surf,nm_surf,ntimes_surf),dtype='complex')
for t in range(ntimes_surf):
    g_lm_cmb[:,:,t]=gauss_coef(bsr[:nl_surf,:nm_surf,t])
# Compute harmonic stuff to speed up calculations.
lpmvs=np.zeros((nm_surf+1,nl_surf+1,ntheta))
norms=np.zeros((nl_surf+1,nm_surf+1))
costheta=np.cos(theta)
for ell in range(1,nl_surf):
    for m in range(ell+1):
        lpmvs[m,ell,:]=lpmv(m,ell,costheta)
        norms[ell,m]=norm_lm(ell,m)
#        norms[ell,m]=norm_lm_schmidt(ell,m) 

# Compute Br(cmb,theta,phi) and compare to output of Br(cmb,theta,phi)
for iphi,phii in enumerate(phi):
    for ell in range(1,nl_surf):
        br_cmb[:,iphi]=br_cmb[:,iphi]+norms[ell,0]*lpmvs[0,ell,:]*(ell+1.)*(g_lm_cmb[ell,0,itime].real)
        for m in range(1,ell+1):
            br_cmb[:,iphi]=br_cmb[:,iphi]+1.0*norms[ell,m]*lpmvs[m,ell,:]*(ell+1.)*(
                g_lm_cmb[ell,m,itime].real*np.cos(m*phii)-g_lm_cmb[ell,m,itime].imag*np.sin(m*phii))

# Compute Br(l,m) from Br(theta,phi)
print('Computing Blm')
brlm=np.zeros((nm_surf,nl_surf))
dtheta=theta[1]-theta[0]
dphi=phi[1]-phi[0]
sintheta=np.sin(theta)
brlm=harmonic_transform(br,costheta,phi
for ell in range(1,nl_surf):
    for m in range(0,ell+1):
        print(str(ell)+','+str(m))
        num=complex(0,0)
        den=0.#[]
        for itheta,thetai in enumerate(theta):
            for iphi,phii in enumerate(phi):
                num += br[iphi,itheta]*norms[ell,m]*lpmvs[m,ell,costheta[itheta]]*complex(
#                    np.cos(m*phii),*np.sin(m*phii)) * sintheta[itheta]*dtheta*dphi
                    np.cos(m*phii),np.sin(m*phii)) * sintheta[itheta]*dtheta*dphi
                den += dtheta*dphi*sintheta[itheta]
        brlm[ell,m] += num/den
    
# Plot comparison of Br,Bt,Bp as (theta,phi) and (l,m)
ion()
nfig=1
nrow=1
ncol=3
f1 = p.figure(nfig,figsize=(15,5), dpi=80)
# Contour br_surf(theta,phi,t=0)
for panel in range(1,ncol+1):
    if panel==1:
        surf=np.transpose(br[:,:,0])  #.reshape(nphi,ntheta)
        title='Br output'
    if panel==2:
        surf=br_cmb[:,:]
        title='Br transformed'
    if panel==3:
        surf=np.transpose(br[:,:,0])-br_cmb
        title='Diff'
    ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
    twosigma = 2*np.std(surf)  #limits are set to +/- twosigma
    contour_levels = twosigma*np.linspace(-1,1,256)
    image1 = ax1.imshow(surf,vmin=-twosigma, vmax=twosigma,
                        extent=(-pi,pi,-pi/2,pi/2), clip_on=False,
                        aspect=0.5, interpolation='bicubic')
    plt.title(title)
    image1.axes.get_xaxis().set_visible(False)  # Remove the longitude and latitude axis labels
    image1.axes.get_yaxis().set_visible(False)
    image1.set_cmap('RdYlBu_r')  # Red/Blue map
    # Colorbar
    cbar = f1.colorbar(image1,orientation='horizontal', shrink=0.5)
    tick_locator = ticker.MaxNLocator(nbins=5)
    cbar.locator = tick_locator
    cbar.update_ticks()
print('max,min diff='+str(np.max(np.transpose(br[:,:,0])-br_cmb))+','+str(np.min(np.transpose(br[:,:,0])-br_cmb)))

exit()
###############################
    
# Compute B_surf
bsr_surf=np.zeros((nl_surf,nm_surf,ntimes_surf),dtype=complex)
bst_surf=np.zeros((nl_surf,nm_surf,ntimes_surf),dtype=complex)
bsp_surf=np.zeros((nl_surf,nm_surf,ntimes_surf),dtype=complex)
for i in range(nl_surf):
    ell=i+1.
    bsr_surf[i,:,:]=bsr[i,:nm_surf,:ntimes_surf]*(r_cmb/r_surf)**(ell+2)
    bsp_surf[i,:,:]=bsp[i,:nm_surf,:ntimes_surf]*(r_cmb/r_surf)**(ell+2)
    bst_surf[i,:,:]=bst[i,:nm_surf,:ntimes_surf]*(r_cmb/r_surf)**(ell+2)
# Convert Bsurf(l,m) to Bsurf(theta,phi)
br_surf=np.zeros((ntheta,nphi,ntimes_surf))
bt_surf=np.zeros((ntheta,nphi,ntimes_surf))
bp_surf=np.zeros((ntheta,nphi,ntimes_surf))
bmag_surf=np.zeros([ntheta,nphi,ntimes_surf])

print('calculating Ylms... this may take some time')
Ylmarray = calculate_Ylms(nl_surf,theta,phi)

print('entering summation')
for itime in range(ntimes_surf):
    print('itime:'+str(itime)+'/'+str(ntimes_surf))
    for itheta,thetai in enumerate(theta):
        #print('itime,itheta='+str(itime)+','+str(itheta))
        for iphi,phii in enumerate(phi):
            #print('itime,itheta,iphi='+str(itime)+','+str(itheta)+','+str(iphi))
            br_surf[itheta,iphi,itime] = np.sum(np.absolute(np.multiply(bsr_surf[:,:,itime], Ylmarray[:,:,itheta,iphi])))
            bt_surf[itheta,iphi,itime] = np.sum(np.absolute(np.multiply(bst_surf[:,:,itime], Ylmarray[:,:,itheta,iphi])))
            bp_surf[itheta,iphi,itime] = np.sum(np.absolute(np.multiply(bsp_surf[:,:,itime], Ylmarray[:,:,itheta,iphi])))
            bmag_surf[itheta,iphi,itime]=np.sqrt(br_surf[itheta,iphi,itime]**2+bt_surf[itheta,iphi,itime]**2+\
                                                 bp_surf[itheta,iphi,itime]**2)

# Save data.
#Ylmarray_save=np.ma.asarray(Ylmarray)
outfile=path+'/B_surf0'
np.savez(outfile,filespec=filespec,times=times,bsr_surf=bsr_surf,bsp_surf=bsp_surf,bst_surf=bst_surf,Ylmarray=Ylmarray,br_surf=br_surf,bt_surf=bt_surf,bp_surf=bp_surf,bmag_surf=bmag_surf)
print('saved '+outfile)
print('Done.')
