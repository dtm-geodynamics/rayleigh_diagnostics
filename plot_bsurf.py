#!/usr/bin/env python

###################################################################################
#

#import matplotlib
#matplotlib.use("Agg")

import pylab as p 
import numpy as np
from rayleigh_diagnostics import Shell_Slices,Shell_Spectra,Power_Spectrum
from diagnostic_tools import file_list, calculate_Ylms,renormalize_shell_spectra,inverse_transform,norm_lm
from diagnostic_plotting import lutlabels,getlabel
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import ticker
#matplotlib.use('GTK')
from matplotlib.pyplot import ion,plot,show
import argparse
pi=np.pi

parser = argparse.ArgumentParser( \
                       description="""Plot Br at Earth surface.""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False,
                    default=None, help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False,
                    default=None, help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-d', '--default',action='store_true',dest='default',
                    help='use default plotting options', default=False)
parser.add_argument('-tave', '--timeave',action='store',type=float,dest='timeave',help='time average',
                    default=None)
args = parser.parse_args()

if args.default: print("default set")

# Set saveplot to True to save to a .png file. 
# Set to False to view plots interactively on-screen.
savefig = args.save
savefile = 'bsurf'
savetype = '.pdf'
if args.timeave: savefile=savefile+'_tave%i'%args.timeave
Pm=10.
Pr=1.
kyr_dip_decay=50.
print('Assume for time scaling: Pm=%i, Pr=%i'%(Pm,Pr))
kyr_kappa=kyr_dip_decay/Pm/(1.538461/pi)**2.  #[kyr/t_kappa]
    
# Outline:
# 1. Read in a shell_spectra file.
# 2. Orthonormalize it.
# 3. extrapolate it to surface.
# 4. Inverse transform from harmonic space to theta/phi.
# 5. Plot.

# 1. Read in a shell_spectra file.
path='Shell_Spectra'
print("Reading files...")
files = file_list(args.initial, args.final, path)
nfiles=len(files)
print('Number files found: '+str(nfiles))
if args.default: file_ind=nfiles-1   #default to last file
if args.timeave: file_ind=0
if not args.default and not args.timeave:
    file_ind=int(raw_input('Enter index of file to plot (eg 0):').split(' ')[0])
sspec0 = Shell_Spectra(files[file_ind],path='')
tind=len(sspec0.time)-1  #default to last timestep
if not args.default and not args.timeave:  #ask user.
    #User choose time
    print('N Times:'+str(len(sspec0.time)))
    user_tind_in = raw_input('Choose index of time to plot (-1=last):')
    user_tind = int(user_tind_in)
    tind=user_tind

# We use the lookup table to find where br, vtheta, and bphi are stored
br_lut=801
bt_lut=802
bp_lut=803
br_index = sspec0.lut[br_lut]
if bt_lut in sspec0.qv: bt_index=sspec0.lut[bt_lut]
if bp_lut in sspec0.qv: bp_index=sspec0.lut[bp_lut]
dims=[sspec0.lmax,sspec0.nr,sspec0.niter]   #get dims.
lmax=sspec0.lmax
lmax_gauss=min([84,lmax])  #limit to <84 bc factorial(170)=inf, so lmax<84. 
nl=lmax+1
nr=sspec0.nr
niter=sspec0.niter
#rad_index
rad_index=0  # only use top radius level
#Initialize full arr with first spec file.
times=sspec0.time
time_title=times[tind]*kyr_kappa
brlm=sspec0.vals[:lmax_gauss+1,:lmax_gauss+1,rad_index,br_index,tind]
# 1b. if timeave set then restore and time ave brlm.
if args.timeave:
    timeave=args.timeave  #[kyr]  length to time ave over
    a=Shell_Spectra(files[0],path='')
    dtsum=0.
    brlmsum=np.zeros((nl,nl),dtype=complex)
    t_prev=a.time[0]*kyr_kappa  #used to compute the length of first timestep in each file.
    time_title=t_prev
    t_end=timeave+t_prev   #absolute end time when timeave is reached.
    for ifile in files:   #loop over all files until timeave reached
        a=Shell_Spectra(ifile,path='')
        times=a.time*kyr_kappa  #[kyr]
        ind=times<=t_end  #array of [T,F] where F occurs when t_end reached.
        dt=np.append(times[0]-t_prev,np.diff(times[ind]))  #time steps.  append(first,all others)
        brlmsum += np.sum(a.vals[:,:,0,a.lut[br_lut],ind]*dt,axis=2) #sum 2x2 arrays, each weighted by dt.
        dtsum += np.sum(dt)  #sum dt to check at end
        t_prev=times[-1]  #set prev to current last
        if t_prev >= t_end: break
    if t_prev<t_end: print('t_end never reached.')
    print('timeave=%f, dtsum=%f'%(timeave,dtsum))
    brlm_tave = brlmsum/dtsum  #tave is sum/timerange
    brlm=brlm_tave

# 2. Orthonormalize brlm.
# Convert Brlm to orthonormal
brlm_on=renormalize_shell_spectra(brlm)
if bt_lut in sspec0.qv: btlm_on=renormalize_shell_spectra(sspec0.vals[:lmax_gauss+1,:lmax_gauss+1,rad_index,bt_index,tind])
if bp_lut in sspec0.qv: bplm_on=renormalize_shell_spectra(sspec0.vals[:lmax_gauss+1,:lmax_gauss+1,rad_index,bp_index,tind])

# 3. extrapolate it to surface.
# Extrapolate orthonormal harmonics to surf and Compute Brtp(surface)
r_cmb=3481e3
r_surf=6371e3
r_ratio=r_cmb/r_surf
#r_ratio=1.#0.99
ntheta=(lmax+1)*3/2 
nphi=ntheta*2
costheta, weights = np.polynomial.legendre.leggauss(ntheta)
theta=np.arccos(costheta)
# 4. Inverse transform from harmonic space to theta/phi.
# Compute brtp from brlm_on
print('Inverse tranforming br ...')
brtp_trans=inverse_transform(brlm_on,theta,nphi,r_ratio,component='r')
print('Inverse tranforming bt ...')
bttp_trans=inverse_transform(brlm_on,theta,nphi,r_ratio,component='t')  #Note this uses brlm
print('Inverse tranforming bp ...')
bptp_trans=inverse_transform(brlm_on,theta,nphi,r_ratio,component='p')  #Note this uses brlm

# 4b. Compute inclination and declination at surface.
bx=-bttp_trans
by=bptp_trans
bz=-brtp_trans
dec=np.arctan(by/bx)
bh=np.sqrt(bx**2+by**2)
inc=np.arctan(bz/bh)
amp=np.sqrt(bx**2+by**2+bz**2)

##################################################################
# 5. Plot.
# Plot comparison of Br,Bt,Bp as (theta,phi) and (l,m)
ion()
# function "plot_bmap" used to render a surface.
def plot_bmap(surf,title,vrange=None):
    if not vrange:  vrange = 2*np.std(surf)  #limits are set to +/- twosigma
    vmin=-vrange
    if np.min(surf)>=0: vmin=0
    vmax=vrange
    contour_levels = vrange*np.linspace(-1,1,256)
    # Contour br_surf(theta,phi,t=0)
    image1 = ax1.imshow(surf,vmin=vmin, vmax=vmax,
        extent=(-np.pi,np.pi,-np.pi/2,np.pi/2), clip_on=False,
        aspect=0.5, interpolation='bicubic')
    image1.axes.get_xaxis().set_visible(False)  # Remove the longitude and latitude axis labels
    image1.axes.get_yaxis().set_visible(False)
    image1.set_cmap('RdYlBu_r')  # Red/Blue map
    plt.title(title)
    # Colorbar
    cbar = f1.colorbar(image1,orientation='horizontal')#, shrink=0.5)
    tick_locator = ticker.MaxNLocator(nbins=5)
    cbar.locator = tick_locator
    cbar.update_ticks()
# Begin setting up plots.
nfig=1
nrow=2
ncol=3
panel=1
f1 = p.figure(nfig,figsize=(15,10), dpi=80)
title_ft='t=%.2f [kyr] \nr_ratio=%.2f'%(time_title,r_ratio)
if args.timeave: title_ft=title_ft+' \ntime ave Delta t=%.2f'%(timeave)
plt.figtext(0.5,0.92,title_ft,ha='center')
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
# B_r
title=r'$B_r$'
plot_bmap(brtp_trans,title)
# B_theta
panel+=1
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
title=r'$B_\theta$'
plot_bmap(bttp_trans,title)
# B_phi
panel+=1
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
title=r'$B_\phi$'
plot_bmap(bptp_trans,title)
# dec
panel+=1
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
title='Declination [deg]'
plot_bmap(dec*180/pi,title,vrange=90)
# inc
panel+=1
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
title='Inclination [deg]'
plot_bmap(inc*180/pi,title,vrange=90)
# amp
panel+=1
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
title='Amplitude'
plot_bmap(amp,title)

plt.tight_layout()
if savefig:
    savename=savefile+'_t%.4f'%times[tind]+savetype
    plt.savefig(savename)
    print('Saved '+savename)
