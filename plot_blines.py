#!/usr/bin/env python

### plot_blines.py 10/21/18 PED ###
### Plot magnetic field lines from shell_spectra at cmb ###
### Code developed from plot_shell_spec.py and contour_bfield.py

from rayleigh_diagnostics import Power_Spectrum, Shell_Spectra
from diagnostic_tools import file_list, calculate_Ylms,renormalize_shell_spectra,gauss_coef,\
     dipole_latitude,dipole_longitude,polarity_chrons,elike_stats,rms_mean,rms_stdev,rms_sum,\
     plm_schmidt,convert_to_kyr,norm_lm_schmidt
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.pyplot import plot,ion,show
from matplotlib.patches import Circle
from matplotlib import cm, rc, rcParams
import numpy as np
from scipy.integrate import ode as ode
from scipy.special import lpmv
from math import acos
from itertools import product
import sys
import argparse
import os 
pi=np.pi
cwd = os.getcwd()
r2d=180./np.pi  #radians/degree

### Rayleigh version
br_lut = 801
#if args.version==1: br_lut=401

### Input parameters
eps_r=0.35  #r_i/r_o
r_o=1+1/(1/eps_r-1.)  #dimless outer boundary radius.
Pm=10.  #need to read this in from main_input?

class gauss_component:
    def __init__(self, l=0,m=0,glm=0,hlm=0,a=0):
        self.glm=glm
        self.hlm=hlm
        self.l=l
        self.m=m
        self.a=a
 
def components_list(gauss,a=1.):
    ### given gauss[l,m] return a list of gauss class components.
    # Loop over each gauss component.
    lmax=len(gauss[:,0])-1
    components = []
    for l in range(1,lmax+1):
        #for m in range(0,l):
        for m in range(0,l+1):
            components.append(gauss_component(l,m,gauss[l,m].real,gauss[l,m].imag,a))
            #print('components: l=%d m=%d'%(l,m))
    return(components)

def B_gauss(l,m,glm,hlm, a, r,theta,phi):
    ### Given glm,hlm,a,r,theta,phi return Bx,By,Bz(r,theta,phi)
    mu = np.cos(theta)
    # Now Plm=Nlm*Llm where Nlm is Schmidt norm and Llm is lpmv (unnormalized Assoc. Leg. Poly.)
    Nlm = norm_lm_schmidt(l,m)
    Llm = lpmv(m,l,mu)
    L_l_mp1 = lpmv(m+1,l,mu); L_l_mm1 = lpmv(m-1,l,mu)
    L_lm1_mp1 = lpmv(m+1,l-1,mu); L_lm1_mm1 = lpmv(m-1,l-1,mu)
    L_lp1_mp1 = lpmv(m+1,l+1,mu); L_lp1_mm1 = lpmv(m-1,l+1,mu)
    B_r = +1*(a/r)**(l+2.)*(l+1.)*Nlm*Llm*(glm*np.cos(m*phi)+hlm*np.sin(m*phi))
    B_theta = 0.5*(a/r)**(l+2.)*Nlm*((l+m)*(l-m+1.)*L_l_mm1-L_l_mp1)*(glm*np.cos(m*phi)+hlm*np.sin(m*phi))
    B_phi = -0.5*(a/r)**(l+2.)*Nlm*(L_lp1_mp1+(l-m+1)*(l-m+2)*L_lp1_mm1)*(glm*np.sin(m*phi)-hlm*np.cos(m*phi))
    #B_x = np.sin(theta)*np.cos(phi)*B_r + np.cos(theta)*np.cos(phi)*B_theta + np.sin(phi)*B_phi
    B_x = np.sin(theta)*np.cos(phi)*B_r + np.cos(theta)*np.cos(phi)*B_theta - np.sin(phi)*B_phi
    B_y = np.sin(theta)*np.sin(phi)*B_r + np.cos(theta)*np.sin(phi)*B_theta + np.cos(phi)*B_phi
    #B_y = 0     #2D only!!!
    B_z = np.cos(theta)*B_r - np.sin(theta)*B_theta
    #print('B_total: r=%.3f theta=%.3f phi=%f Bx=%.2e By=%.2e Bz=%.2e Br=%.2e Bt=%.2e Bp=%.2e l=%d m=%d glm=%.2e hlm=%.2e'%(r,theta,phi,B_x,B_y,B_z,B_r,B_theta,B_phi,l,m,glm,hlm))
    return(B_x,B_y,B_z)

def B_total(x,y,z,components):
    # Compute a,r,theta,phi
    r = np.sqrt(x**2+y**2+z**2)
    phi = np.arctan2(y,x)
    theta = np.arctan2(np.sqrt(x**2+y**2),z)
    #mu = np.cos(theta)
    Bx,By,Bz = 0,0,0
    for C in components:
        # Compute source contribution to field.
        B=B_gauss(C.l,C.m,C.glm,C.hlm, C.a, r,theta,phi)
        Bx=Bx+B[0]; By=By+B[1]; Bz=Bz+B[2]
    #if Bx*By*Bz==0.:
    #print('B_total: r=%.2f t=%.2f p=%.2f x=%.2f y=%.2f z=%.2f Bx=%.2e By=%.2e Bz=%.2e'%(r,theta,phi,x,y,z,Bx,By,Bz))
    return [Bx,By,Bz]

def B_dir(t, y, components):
    Bx,By,Bz = B_total(y[0],y[1],y[2], components)
    n=np.sqrt(Bx**2+By**2+Bz**2)
    if n<= 0.: exit()
    #print('B_dir: Bx=%.2e By=%.2e Bx=%.2e n=%.2e'%(Bx,By,Bz,n))
    return [Bx/n,By/n,Bz/n]  #normalized unit vector directions?  New coordinates?

#def Phi_gauss(l,m,glm,hlm,a,r,theta,phi,Plm):
#    ### Compute mag potential(r,theta,phi)
#    phi = a*(a/r)**(l+1.)*Plm*(glm*np.cos(m*phi)+hlm*np.sin(m*phi))
#    return(phi)

def Phi_total(x, y, z, components, plms):
    # Compute a,r,theta,phi
    r = np.sqrt(x**2+y**2+z**2)
    phi = np.arctan2(y,x)
    theta = np.arctan2(np.sqrt(x**2+y**2),z)
    mu = np.cos(theta)
    Phi=0
    for C in components:
        Plm = plms[C.l,C.m]
        #Plm = plm_schmidt(C.l,C.m,mu)
        phi_lm = C.a*(C.a/r)**(C.l+1.)*Plm*(C.glm*np.cos(C.m*phi)+C.hlm*np.sin(C.m*phi))
        Phi = Phi+phi_lm
    return Phi

def read_gauss(file,lmax=8):
    gauss=np.zeros((lmax+1,lmax+1,1),dtype=complex)
    time=np.zeros(1)
    a=Shell_Spectra(file,path='')   #note path is already in filename 'file'.
    #spec_i = Power_Spectrum(file,magnetic=True,path='')
    br_index=a.lut[br_lut]
    # Convert Brlm to orthonormal
    brlm_on = renormalize_shell_spectra(a.vals[:lmax+1,:lmax+1,0,br_index,:])
    # Compute Gauss
    gauss = gauss_coef(brlm_on)
    time = a.time
    return(time,gauss,a.iters)

def blines_meridional(components,nlines=40,ar=1.,xmin=-3,xmax=+3):
    ### Trace B-lines of components in meridional plane.
    ymin = xmin; ymax = xmax; zmin = xmin; zmax = xmax;
    # trace angles: alpha like phi in x-y plane, beta like theta in y-z plane.
    alpha = np.arctan2(0,1)  #phi value of plane normal to view.
    nbeta = nlines; beta_max = 2.*pi; beta_step = beta_max/nbeta
    beta_arr = np.linspace(beta_step/2, (nbeta-1)*beta_max/nbeta+beta_step/2, nbeta)
    #beta_arr = beta_arr[1:2]    #isolate a bad case.
    dt=0.01*ar  #line trace step size
    rtol=1e-6
    #rtol=1e-4
    integrator='vode'
    method='' #'bdf'
    nsteps=500.
    x_init = np.sqrt(ar**2+dt**2)*np.sin(beta_arr)*np.cos(alpha)
    y_init = np.sqrt(ar**2+dt**2)*np.sin(beta_arr)*np.sin(alpha)
    z_init = np.sqrt(ar**2+dt**2)*np.cos(beta_arr)
    xs,ys,zs = [],[],[]
    ### Loop over beta angles.
    for i,beta in enumerate(beta_arr):
        #print('i=%d beta=%f'%(i,beta))   #to isolate a bad case.
        # Positive dt
        r=ode(B_dir)
        r.set_integrator(integrator,method=method,rtol=rtol,nsteps=nsteps).set_f_params(components)
        x = [x_init[i]]; y = [y_init[i]]; z = [z_init[i]]
        r.set_initial_value([x[0], y[0], z[0]], 0)
        while r.successful():
            r.integrate(r.t+dt)
            x.append(r.y[0]); y.append(r.y[1]); z.append(r.y[2])
            hit_charge=False
            ## check if field line left drwaing area or ends in some charge
            #for C2 in components:
            if np.sqrt(r.y[0]**2+r.y[1]**2+r.y[2]**2)<ar:
                    hit_charge=True
            if hit_charge or (not (xmin<r.y[0] and r.y[0]<xmax)) or \
                    (not (ymin<r.y[1] and r.y[1]<ymax)) or (not (zmin<r.y[2] and r.y[2]<zmax)):
                break
        xs.append(x); ys.append(y); zs.append(z)
        # Negative dt
        r=ode(B_dir)
        r.set_integrator(integrator,method=method,rtol=rtol,nsteps=nsteps).set_f_params(components)
        x = [x_init[i]]; y = [y_init[i]]; z = [z_init[i]]
        r.set_initial_value([x[0], y[0], z[0]], 0)
        while r.successful():
            r.integrate(r.t-dt)
            x.append(r.y[0]); y.append(r.y[1]); z.append(r.y[2])
            hit_charge=False
            ## check if field line left drwaing area or ends in some charge
            for C2 in components:
                if np.sqrt(r.y[0]**2+r.y[1]**2+r.y[2]**2)<ar:
                    hit_charge=True
            if hit_charge or (not (xmin<r.y[0] and r.y[0]<xmax)) or \
                    (not (ymin<r.y[1] and r.y[1]<ymax)) or (not (zmin<r.y[2] and r.y[2]<zmax)):
                break
        xs.append(x); ys.append(y); zs.append(z)
    return(xs,ys,zs)

def compute_plms(components,x,y,z):
    plms = np.zeros((components[-1].l+1,components[-1].l+1,len(x),len(z)))  #(l,m,theta)
    for ix,xx in enumerate(x):
        theta = np.arctan2(np.sqrt(xx**2+y**2),z)  #f(z)
        for C in components:
            plms[C.l,C.m,ix,:]  = plm_schmidt(C.l,C.m,np.cos(theta))
    return(plms)
    
def meridional_potential(components,num=50,ar=1,xmin=-3,xmax=+3):
    # calculate magnetic potential field in meridional plane.  grid=num*num
    xxs = []; zzs = []; vvs = []
    y = 0.; yy = 0.
    x = np.linspace(xmin,xmax,num)
    z = np.linspace(xmin,xmax,num)
    plms = compute_plms(components,x,y,z)
    #for xx,zz in product(x,z):
    for ix,xx in enumerate(x):
        for iz,zz in enumerate(z):
            if np.sqrt(xx**2+zz**2)>=ar:   #only save V outside of r=a
                #plm_mu = plm_schmidt(C.l,C.m,mu)
                # Problem with following is that mu[iz] is not the right value.
                plm_mu = plms[:,:,ix,iz]
                xxs.append(xx); zzs.append(zz)
                vvs.append(Phi_total(xx,yy,zz,components,plm_mu))
    xxs = np.array(xxs); zzs = np.array(zzs); vvs = np.array(vvs)
    return(xxs,zzs,vvs)

def meridional_bmap(components,num=50,ar=1,xmin=-3,xmax=+3):
    # calculate magnetic potential field in meridional plane.  grid=num*num
    xxb = []; zzb = []; Bx = []; By = []; Bz = []
    y = 0.; yy = 0.
    x = np.linspace(xmin,xmax,num)
    z = np.linspace(xmin,xmax,num)
    for ix,xx in enumerate(x):
        for iz,zz in enumerate(z):
            if np.sqrt(xx**2+zz**2)>=ar:   #only save V outside of r=a
                xxb.append(xx); zzb.append(zz)
                Bxyz = B_total(xx,yy,zz,components)
                Bx.append(Bxyz[0])
                By.append(Bxyz[1])
                Bz.append(Bxyz[2])
    xxb = np.array(xxb); zzb = np.array(zzb)
    Bx = np.array(Bx); By = np.array(By); Bz = np.array(Bz)
    return(xxb,zzb,Bx,By,Bz)

def render_blines(components,xs,zs,ar=1.,asg=1.,xmin=-3,xmax=+3,title=''):
    zmin = xmin; zmax = xmax
    plt.figure(figsize=(15, 10),facecolor="w")
    # plot field line
    for x, z in zip(xs,zs): plt.plot(x, z, color="k")
    c1=plt.Circle((0,0),ar,zorder=10,facecolor='grey',edgecolor='none')   #filled circle at r=ar (render)
    c2=plt.Circle((0,0),1.,zorder=10,color='black',fill=False,linewidth=2)  #open circle at r=1 (E surface)
    ax=plt.gca().add_patch(c1)
    ax=plt.gca().add_patch(c2)
    plt.xlabel('$x$')
    plt.ylabel('$z$')
    plt.title(title,fontsize=18)
    plt.xlim(xmin, xmax)
    plt.ylim(zmin, zmax)
    plt.axes().set_aspect('equal')

def render_potential(xxs,zzs,vvs,clim0=None,clim1=None):
    # plot potential
    if clim0 is not None and clim1 is not None:
        plt.tricontourf(xxs,zzs,vvs,np.linspace(clim0, clim1, 100),cmap=cm.jet)
        cbar = plt.colorbar(ticks=np.linspace(clim0,clim1,5))
    else:
        plt.tricontourf(xxs,zzs,vvs,ticks,cmap=cm.jet)#,vmin=clim0,vmax=clim1)
        cbar = plt.colorbar()
    #plt.tricontour(xxs,zzs,vvs,1)    #show potential contour lines
    cbar.set_label("Magnetic Potential")
    #plt.show()

def render_bmap(xxb,zzb,Bcomp,clim0=None,clim1=None,title='B component'):
    # contour a component of B.
    if clim0 is not None and clim1 is not None:
        plt.tricontourf(xxb,zzb,Bcomp,np.linspace(clim0, clim1, 100),cmap=cm.jet)
        cbar = plt.colorbar(ticks=np.linspace(clim0,clim1,5))
    else:
        plt.tricontourf(xxb,zzb,Bcomp,ticks,cmap=cm.jet)#,vmin=clim0,vmax=clim1)
        cbar = plt.colorbar()
    #plt.tricontour(xxs,zzs,vvs,1)    #show potential contour lines
    cbar.set_label(title)
    #plt.show()

def save_blines_fig(savename='bfield_lines_1.png',dpi=250):
    plt.savefig(savename,dpi=dpi,bbox_inches="tight",pad_inches=0.02)


    
### This block executes if this script is called from command line.
### This block is ignored for "import plot_blines" stuff.
if __name__ == "__main__":
    plt.ion()
    parser = argparse.ArgumentParser(description="""Create magnetic field streamlines""")
    parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                        required=False, help='save the figure to file instead of plotting interactively')
    parser.add_argument('-sn', '--savename', action='store', type=str, dest='savename', required=False,
                        default=False, help='name of file to save')
    parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False,
                        default=None,help='initial file index to plot from')
    parser.add_argument('-ii', '--initiali', action='store', type=float, dest='initiali', required=False,
                        default=None,help='initial timestep index to plot from')
    parser.add_argument('-d', '--default', action='store_const', dest='default', const=True, default=False, 
                        required=False, help='default options')
    parser.add_argument('-np', '--nopotential', action='store_const', dest='nopotential',
                        const=True, default=False, required=False, help='do not plot potential')
    parser.add_argument('-lmax', '--lmax', action='store', type=int, dest='lmax', required=False,
                        default=8,help='lmax to plot')
    parser.add_argument('-np', '--nopotential', action='store_const', dest='nopotential', const=True,
                        default=False, required=False, help='no potential field calculation')
    parser.add_argument('-bmap', '--bmap', action='store_const', dest='bmap', const=True,
                        default=False, required=False, help='contour B')
    parser.add_argument('-ii', '--initiali', action='store', type=int, dest='initiali', required=False,
                        default=None,help='initial timestep index to plot from')
    parser.add_argument('-t', '--test', action='store_const', dest='test', const=True,
                        default=False, required=False, help='test with single harmonic term')
    parser.add_argument('-clim0', '--clim0', action='store', type=float, dest='clim0', required=False,
                        default=-2,help='minimum contour limit')
    parser.add_argument('-clim1', '--clim1', action='store', type=float, dest='clim1', required=False,
                        default=+2,help='maximum contour limit')
    parser.add_argument('-xmin', '--xmin', action='store', type=float, dest='xmin', required=False,
                        default=-3,help='coordinate range')
    parser.add_argument('-xmax', '--xmax', action='store', type=float, dest='xmax', required=False,
                        default=+3,help='coordinate range')
    args = parser.parse_args()
    #Set savefig to True to save to savefile.  Interactive plot otherwise.
    if args.savename: args.save=True
    savefig = args.save
    savefile = args.savename
    if savefig: print('savefig='+str(savefig))
    asg = 1.   #radius of Gauss sources (ie E surface or CMB)
    ar = 3481./6371   #radius to render above
    if not args.test:   #if not a test then read shell_spec
        initial=args.initial
        ### Shell_Spectra
        files_ss = file_list(initial, initial, path='Shell_Spectra')
        file=files_ss[0]
        print('Reading '+file+'...')
        #lmax=8  #number of gauss coef to compute stats for.
        lmax = args.lmax
        time,gauss,iters = read_gauss(file,lmax)
        # Choose time index to plot.
        ntimes=len(time)
        if not args.default and not args.initiali and ntimes>1:  #if index not input by user then ask them.
            user_time = raw_input('Choose iteration to plot out of %i: '%(ntimes-1))
            if int(user_time)==-1: user_time = ntimes-1
            tindex = int(user_time)
        elif args.default: tindex = ntimes-1
        elif args.initiali: tindex =  args.initiali
        title = '%.2f kyr'%(convert_to_kyr(time[tindex])[0])
        ### Gauss components
        components = components_list(gauss[:,:,tindex],asg)
        #exit()
    if args.test:   #if test then ask user for harmonic term and amplitude
        user_in = raw_input('Enter l,m,glm,hlm:')
        user_arr = []
        for x in user_in.split(','): user_arr.append(float(x))
        components = [gauss_component(user_arr[0],user_arr[1],user_arr[2],user_arr[3],asg)]
        title = 'test: l=%d m=%d g=%.2f h=%.2f'%(user_arr[0],user_arr[1],user_arr[2],user_arr[3])
    ### Calculate field lines
    print('Calculating field lines...')
    xmin = args.xmin; xmax = args.xmax
    xs,ys,zs = blines_meridional(components,nlines=40,ar=ar,xmin=xmin,xmax=xmax)
    clim0 = args.clim0; clim1 = args.clim1
    render_blines(components,xs,zs,ar=ar,asg=asg,xmin=xmin,xmax=xmax,title=title)
    ### Calculate potential
    if not args.nopotential and not args.bmap:
        xxs,zzs,vvs = meridional_potential(components,num=25,ar=ar,xmin=xmin,xmax=xmax)
        render_potential(xxs,zzs,vvs,clim0=clim0,clim1=clim1)
    ### Calculate bmap
    if args.bmap:
        xxb,zzb,Bx,By,Bz = meridional_bmap(components,num=25,ar=ar)
        Bcomp = Bx
        title = 'Bx'
        render_bmap(xxb,zzb,Bcomp,clim0=clim0,clim1=clim1,title=title)        
    if args.save:
        if args.savename: save_blines_fig(args.savename)
        else: save_blines_fig()
        print('Saved '+args.savename)
