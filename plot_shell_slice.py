#!/usr/bin/env python

####################################################################################################
#
#  Shell-Slice (Shell_Slices) plotting example
#  - Reads in a single Shell_Slice file.
#  - Plots vr, vphi, and entropy
#
#  This example routine makes use of the ShellSlice
#  data structure associated with the Shell_Slices output.
#  Upon initializing a ShellSlice object, the 
#  object will contain the following attributes:
#    ----------------------------------
#    self.niter                                    : number of time steps
#    self.nq                                       : number of diagnostic quantities output
#    self.nr                                       : number of shell slices output
#    self.ntheta                                   : number of theta points
#    self.nphi                                     : number of phi points
#    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
#    self.radius[0:nr-1]                           : radii of the shell slices output
#    self.inds[0:nr-1]                             : radial indices of the shell slices output
#    self.costheta[0:ntheta-1]                     : cos(theta grid)
#    self.sintheta[0:ntheta-1]                     : sin(theta grid)
#    self.vals[0:nphi-1,0:ntheta-1,0:nr-1,0:nq-1,0:niter-1] 
#                                                  : The shell slices 
#    self.iters[0:niter-1]                         : The time step numbers stored in this output file
#    self.time[0:niter-1]                          : The simulation time corresponding to each time step
#    self.version                                  : The version code for this particular output (internal use)
#    self.lut                                      : Lookup table for the different diagnostics output
#    -------------------------------------

#import matplotlib
#matplotlib.use("Agg")

import pylab as p 
import numpy as np
from rayleigh_diagnostics import Shell_Slices
from diagnostic_tools import file_list
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import ticker
#matplotlib.use('MacOSX')
matplotlib.use('GTK3Agg')

from diagnostic_plotting import lutlabels
from diagnostic_plotting import getlabel
import argparse

### Check python version
import sys
if sys.version_info[0]==3:
    raw_input=input

parser = argparse.ArgumentParser( \
                       description="""Plot Shell Slice (spherical surface).""")
parser.add_argument('-i', '--initial', action='store', type=str, dest='initial', required=False, default=None, 
                    help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=str, dest='final', required=False, default=None, 
                    help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-d', '--default',action='store_true',dest='default',help='use default plotting options',
                    default=False)
args = parser.parse_args()

if args.default: print("default set")

# Set saveplot to True to save to a .png file. 
# Set to False to view plots interactively on-screen.
saveplot = args.save
#savefile = 'shell_slice.png'
savefile = 'shell_slice'
savesuff = '.pdf'

rnorm = 1./(1-0.35)
path='Shell_Slices'
files = file_list(args.initial, args.final, path=path)
nfiles=len(files)
#print("args.initial,final="+args.initial+" "+args.final)
#print("files="+str([f for f in files]))

# Read in our shell slice
print('Number files found: '+str(nfiles))
if args.default: file_ind=nfiles-1
if not args.default:
    file_ind=int(raw_input('Enter index of file to plot (eg 0):').split(' ')[0])
a = Shell_Slices(filename=files[file_ind],path='')

#Default Plotting options.
#user_codes=[1,501,801]  #vr,entropy,Br
user_codes=[1,2,3]  #vr,entropy,Br
#rad_inds=[0,1,2]       #use top 3 radial levels.
rad_inds=np.arange(a.nr)  #use all nr available.
if a.nr > 3 : rad_inds=[0,1,2]  # use only first three.
tind=len(a.time)-1     # Default: use last time step

if not args.default:    #ask user.
    #Print Info
    print(' ')  # blank line
    print('Iterations:'+str(a.niter)+' Quantities:'+str(a.nq)+' Codes:'+str(a.qv))
    # Ask user to choose
    user_codes_in = raw_input('Choose 3 codes to plot (eg 1 2 3):').split(' ')   #will take in a string of numbers separated by a space
    user_codes = [int(num) for num in user_codes_in]   #make int array.
    print('user_codes:',user_codes)

#NEXT: print the label for each code.  for this we need to create a look up table for labels, like p.labels.
q=lutlabels(user_codes)
print(q.labels)

#Identify the variables indices for vr,vphi, and entropy
var_inds = [a.lut[user_codes[0]],a.lut[user_codes[1]],a.lut[user_codes[2]]]  #use user codes.

if not args.default:  #ask user.
    #User choose radial levels.
    print('available: r/R='+str(a.radius/rnorm)+' are Levels:'+str(a.inds))
    user_rad_ind_in = raw_input('Choose index of these radial levels to plot (eg 0 1 2):').split(' ')
    user_rad_ind = [int(num) for num in user_rad_ind_in]
    print('user_rad_ind:',user_rad_ind)
    rad_inds=user_rad_ind
    #User choose time
    tind = 0 # grab time index 0 (the first record of the file)
    print('Times:'+str(a.time))
    user_tind_in = raw_input('Choose index of time to plot (-1=last):')
    user_tind = int(user_tind_in)
    if user_tind==-1: user_tind=len(a.time)-1  # use last time index.
    print('user_tind:',user_tind)
    tind=user_tind

print('Plotting at time=%.4f'%a.time[tind])

#Tex can be enclosed in dollar signs within a string.  The r in front of the string is necessary for strings enclosing Tex
#units = [r'm s$^{-1}$', r'm s$^{-1}$', r'erg g$^{-1}$ K$^{-1}$']  
#vnames = [r'v$_r$', r'v$_\phi$', "S'"]
vnames=q.labels
ncol = len(var_inds)
nrow = len(rad_inds)
nplots = ncol*nrow

#print('ncol='+str(ncol)+',nrow='+str(nrow))
#Create the plots.  This first portion only handles the projection and colorbars.
#Labeling comes further down.
ind = 1
f1 = p.figure(figsize=(5.5*3, 5*3), dpi=80)
for  j in range(nrow):
    for i in range(ncol):
        sslice = a.vals[:,:,rad_inds[j],var_inds[i],tind].reshape(a.nphi,a.ntheta)
        if (user_codes[i] == 501):  #if entropy, subtract mean.
            sslice = sslice-np.mean(sslice) #subtract ell=0 from entropy
        #else:
        #    sslice = sslice/100.0 # convert the velocity field to m/s
        sslice = np.transpose(sslice)
        
        # Set the projection
        ax1 = f1.add_subplot(nrow,ncol,ind, projection="mollweide")
        ind = ind+1
        twosigma = 2*np.std(sslice)  #limits are set to +/- twosigma
        contour_levels = twosigma*np.linspace(-1,1,256)
        thetas = np.linspace(-np.pi/2, np.pi/2, a.ntheta)
        phis = np.linspace(-np.pi, np.pi, a.nphi)
        image1 = ax1.pcolormesh(phis, thetas, sslice,vmin=-twosigma,vmax=twosigma,
                                clip_on=False,shading='gouraud')
        image1.axes.get_xaxis().set_visible(False)  # Remove the longitude and latitude axis labels
        image1.axes.get_yaxis().set_visible(False)
        image1.set_cmap('RdYlBu_r')  # Red/Blue map
        # Colorbar
        cbar = f1.colorbar(image1,orientation='horizontal', shrink=0.5)
        tick_locator = ticker.MaxNLocator(nbins=5)
        cbar.locator = tick_locator
        cbar.update_ticks()

        if i==0 and j==0:
            # Add phi=0 into first frame.
            ax1.text(-np.pi,0,r'$\phi=0$',horizontalalignment='right')



# Next, set various parameters that control the plot layout
# These parameters can also be set interactively when using plt.show()
pbottom = 0.05
pright = 0.95
pleft = 0.15
ptop = 0.95
phspace = 0.1
pwspace = 0.03

p.subplots_adjust(left = pleft, bottom = pbottom, right = pright, top = ptop, wspace=pwspace,hspace=phspace) 

# Add information about radial depth to the left margin
rspace = (ptop-pbottom)/nrow
lilbit = 0.05
xpos = 0.1*pleft
for i in range(nrow):
    if nrow>1: ypos = ptop-rspace*0.4-i*(rspace+phspace/(nrow-1))
    else: ypos = ptop-rspace*0.4
    ratio = float(a.radius[rad_inds[i]]/rnorm)
    r_str = r'r/R = %1.3f' % ratio
    f1.text(xpos,ypos,r_str,fontsize=16)
    ypos = ypos-lilbit
    ind_str = 'radial index: '+str(a.inds[rad_inds[i]])
    f1.text(xpos,ypos,ind_str,fontsize=16)

#Label the plots (entropy, v_r etc.)
cspace = (pright-pleft)/ncol
for i in range(ncol):
    ypos = ptop
    xpos = pleft+cspace*0.47+i*cspace 
    f1.text(xpos,ypos,vnames[i],fontsize=20)

#Save or display the figure
if (saveplot):
    filename=savefile+'_t%.4f'%a.time[tind]+savesuff
    p.savefig(filename)
    print('Saved '+filename)
else:
    plt.show()

