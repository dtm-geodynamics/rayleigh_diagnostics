#!/usr/bin/env python

###################################################################################
"""  plot_sph_modes.py 8/1/18 PED
     Started from plot_bsurf.py
     PURPOSE: read in sph_modes data and plot it.
"""

#import matplotlib
#matplotlib.use("Agg")

import pylab as p 
import numpy as np
from rayleigh_diagnostics import Shell_Slices,Shell_Spectra,Power_Spectrum,SPH_Modes
from diagnostic_tools import file_list, calculate_Ylms,renormalize_shell_spectra,inverse_transform,norm_lm,dipole_latitude,dipole_longitude,gauss_coef,convert_to_kyr
from diagnostic_plotting import lutlabels,getlabel
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import ticker
#matplotlib.use('GTK')
from matplotlib.pyplot import ion,plot,show
import argparse
pi=np.pi

parser = argparse.ArgumentParser( \
                       description="""Plot SPH_MODES.""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False,
                    default=None, help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False,
                    default=None, help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-d', '--default',action='store_true',dest='default',
                    help='use default plotting options', default=False)
parser.add_argument('-tave', '--timeave',action='store',type=float,dest='timeave',help='time average',
                    default=None)
args = parser.parse_args()

if args.default: print("default set")

# Set saveplot to True to save to a .png file. 
# Set to False to view plots interactively on-screen.
savefig = args.save
savefile = 'sph_modes'
savetype = '.pdf'
if args.timeave: savefile=savefile+'_tave%i'%args.timeave
Pm=10.
Pr=1.
kyr_dip_decay=50.
print('Assume for time scaling: Pm=%i, Pr=%i'%(Pm,Pr))
kyr_kappa=kyr_dip_decay/Pm/(1.538461/pi)**2.  #[kyr/t_kappa]
    
# Outline:
# 1. Read in a sph_modes file.
# 2. Plot.

# 1. Read in a file.
path='SPH_Modes'
print("Reading files...")
files = file_list(args.initial, args.final, path)
nfiles=len(files)
print('Number files found: '+str(nfiles))
sph_modes = SPH_Modes(files[0],path='')
""" SPH_Modes returns:
self.nell
self.lvals
self.vals[m,lvals,rad,qv,iter]
"""
nell=sph_modes.nell; lvals=sph_modes.lvals; niter=sph_modes.niter

# We use the lookup table to find where br, vtheta, and bphi are stored
br_lut=801
bt_lut=802
bp_lut=803
br_index = sph_modes.lut[br_lut]
if bt_lut in sph_modes.qv: bt_index=sph_modes.lut[bt_lut]
if bp_lut in sph_modes.qv: bp_index=sph_modes.lut[bp_lut]
dims=[sph_modes.nell,sph_modes.nr,sph_modes.niter]   #get dims.
nr=sph_modes.nr
niter=sph_modes.niter
rad_index=0  # only use top radius level
#Initialize full arr with first spec file.
times=sph_modes.time
#brlm=sph_modes.vals[:,:,rad_index,br_index,:].reshape(nell,max(lvals)+1,niter) #reshape to swap m and l.
brml=sph_modes.vals[:,:,rad_index,br_index,:].reshape(max(lvals)+1,nell,niter) 

### 1b. Restore all remaining files and append data.
for filei in files:
    sph_modesi = SPH_Modes(filei,path='')
    times = np.append(times,sph_modesi.time)
    #brlm = np.append(brlm,sph_modesi.vals[:,:,rad_index,br_index,:].reshape(sph_modesi.nell,max(sph_modesi.lvals)+1,sph_modesi.niter),axis=2)
    brml = np.append(brml,sph_modesi.vals[:,:,rad_index,br_index,:].reshape(max(sph_modesi.lvals)+1,sph_modesi.nell,sph_modesi.niter),axis=2)
    #print('niter=%d vals.shape=%d time[0,-1]=[%.2f,%.2f] dtime/niter=%.2f'%(sph_modesi.niter,len(sph_modesi.vals[0,0,0,0,:]),sph_modesi.time[0],sph_modesi.time[-1],(sph_modesi.time[-1]-sph_modesi.time[0])/sph_modesi.niter))
# Note the reshape does not swap m and l.
brlm = np.zeros((nell,max(lvals)+1,len(brml[0,0,:])),dtype='complex')
for ell in range(nell):
    brlm[ell,:,:]=brml[:,ell,:]  #swap ell and m so brlm[l,m,t]
times_kyr,times_dip = convert_to_kyr(times)
#if args.default: times = times_kyr  #plot times in [kyr]
times = times_kyr  #plot times in [kyr]
    
# 2. Compute dipole angles
sqrt2=np.sqrt(2)  #renormalization factor, apply to m>0.
if 1 in sph_modes.lvals:
    # Define Components.
    idip=np.where(sph_modes.lvals==1.)[0]
    g10=brlm[idip,0,:].real.flatten()  #flatten to reduce to 1D
    g11=sqrt2*brlm[idip,1,:].real.flatten()  #flatten to reduce to 1D
    h11=sqrt2*brlm[idip,1,:].imag.flatten()  #flatten to reduce to 1D
    dip_lat=dipole_latitude(g10,g11,h11)*180./pi    #[deg]
    dip_lon=dipole_longitude(g10,g11,h11)*180./pi   #[deg]

##################################################################
# 5. Plot.
ion()
# Begin setting up plots.
nfig=1
nrow=1
ncol=2
panel=1
f1 = p.figure(nfig,figsize=(15,10), dpi=80)
xtitle = 't'
if args.default: xtitle='kyr'
#title_ft='t=%.2f [kyr] \nr_ratio=%.2f'%(time_title,r_ratio)
#if args.timeave: title_ft=title_ft+' \ntime ave Delta t=%.2f'%(timeave)
#plt.figtext(0.5,0.92,title_ft,ha='center')
plt.subplot(211)
plot(times,g10.real,'-',label='g10')
plot(times,g11.real,'-',label='g11')
plot(times,h11.imag,'-',label='h11')
plt.ylim(min([g10.min(),g11.min(),h11.min()]),max([g10.max(),g11.max(),h11.max()]))
plt.legend()
plt.xlabel(xtitle)
plt.ylabel(r'$g_{lm}$ or $h_{lm}$')
plt.subplot(212)
plot(times,dip_lon,'-',label='Dip. Lon.',color='g')
plot(times,dip_lat,'-',label='Dip. Lat.',color='b')
plt.legend()
plt.xlabel(xtitle)
plt.ylabel('Dip. Lat., Lon. [deg]')
plt.ylim(0,360)

#plt.tight_layout(True)
if savefig:
    savename=savefile+savetype
    plt.savefig(savename)
    print('Saved '+savename)
