#!/usr/bin/env python

#Equatorial Slice movie Example.
#Displays a single variable from the equatorial_slice data structure and stitches movie from all times within one file
#
# Equatorial Slice Data stucture format:
#    """Rayleigh Equatorial Slice Structure
#    ----------------------------------
#    self.niter                                    : number of time steps
#    self.nq                                       : number of diagnostic quantities output
#    self.nr                                       : number of radial points
#    self.nphi                                     : number of phi points
#    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
#    self.radius[0:nr-1]                           : radial grid
#    self.vals[0:phi-1,0:nr-1,0:nq-1,0:niter-1]    : The equatorial_slices
#    self.phi[0:nphi-1]                            : phi values (in radians)
#    self.iters[0:niter-1]                         : The time step numbers stored in this output file
#    self.time[0:niter-1]                          : The simulation time corresponding to each time step
#    self.version                                  : The version code for this particular output (internal use)
#    self.lut                                      : Lookup table for the different diagnostics output
#    """

from rayleigh_diagnostics import Equatorial_Slices
import numpy as np
import matplotlib 
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.pyplot import ion,plot
from diagnostic_plotting import lutlabels,getlabel
from diagnostic_tools import file_list
import argparse
import matplotlib.animation as animation
import matplotlib.pyplot as plt
from matplotlib import ticker

### Check python version
import sys
if sys.version_info[0]==3:
    raw_input=input

parser = argparse.ArgumentParser(description="""Create movie of equatorial slices""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False, default=None, 
                    help='initial index to plot from')
#parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False, default=None, 
#                    help='final index to plot to')
#parser.add_argument('-i', '--index', action='store', type=float, dest='index', required=False, default=None, 
#                    help='index to plot')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-q', '--quantity', action='store', dest='quantity', type=int, nargs='*', default=[501], 
                    required=False, help='quantity')
parser.add_argument('-d', '--default',action='store_true',dest='default',help='use default plotting options',
                    default=False)  #otherwise queue user.

args = parser.parse_args()

#Set savefig to True to save to savefile.  Interactive plot otherwise.
'''savefig = False
#savefig = True
savefig = args.save
savefile = 'eq_slice'
if savefig: print('savefig='+str(savefig))'''
    
path='Equatorial_Slices'
initial=args.initial
final=None #args.final
files = file_list(initial,final,path=path)
nfiles=len(files)

q_code = 501 #default q_code

a = Equatorial_Slices(filename=files[0],path='')
if not args.default:
    print('Codes: '+str(a.qv))
    user_codes = raw_input('Enter a quantity code: ').split(' ')
    q_codes = [int(num) for num in user_codes]
    q_code = q_codes[0] #disregards excess codes

#Set up the grid
nr = a.nr
nphi = a.nphi
r = a.radius/np.max(a.radius)
phi = np.zeros(nphi+1,dtype='float64')
phi[0:nphi] = a.phi
phi[nphi] = np.pi*2  # For display purposes, it is best to have a redunant data point at 0,2pi
radius_matrix, phi_matrix = np.meshgrid(r,phi)
X = radius_matrix * np.cos(phi_matrix) 
Y = radius_matrix * np.sin(phi_matrix)

#Extract fields to plot
qindex = a.lut[q_code]
qtitle = getlabel(q_code)
print('variable selected: ' + qtitle)
eqslice = np.zeros((nphi+1,nr)) #only one quantity code - I removed the third dimension (nquants)
niter=np.zeros(nfiles)

for i,f in enumerate(files):
    #a = Equatorial_Slice(filename=f,path='')
    a = Equatorial_Slices(filename=f,path='')
    niter[i]=a.niter
niters=np.sum(niter)

#Choose time to plot
#if args.index==None:  #if index not input by user then ask them.
#    tindex = 0          # Display the first timestep from the file
#    user_time = raw_input('Choose iteration to plot out of %i: '%(niters-1))
#    tindex = int(user_time)
#else: tindex=args.index
tindex=0

# find ifile where tindex lives.
niter_cumsum=np.cumsum(niter).astype(int)-niter.astype(int)
ifile=np.max(np.where(niter_cumsum<=tindex))
itime=tindex-niter_cumsum[ifile]   #index within ifile of tindex


############### begin plotting ################

a = Equatorial_Slices(filename=files[ifile],path='')
print('iteration %f' %tindex)
eqslice[0:nphi,:] =a.vals[:,:,qindex, itime] #.reshape(nphi,nr)
eqslice[nphi,:] = eqslice[0,:]  #replicate phi=0 values at phi=2pi

f1 = plt.figure(dpi=900)
pleft = 0.15
pright = 0.95
ptop = 0.95
cspace = (pright-pleft)
ypos = ptop
xpos = 0.45
fs=20


eqslice_t=a.vals[:,:,qindex,:].reshape(a.nphi,a.nr, a.niter)  # all times
#subtract out mean for entropy
if (q_code==501):
    #eqslice[:,i] = eqslice[:,i]-np.mean(eqslice[:,i])
    eqslice_t = eqslice_t-np.mean(eqslice_t)

# Set the projection
ax1 = f1.add_subplot(1,1,1, projection="rectilinear") 
twosigma = 2*np.std(eqslice_t)  #limits are set to +/- twosigma
contour_levels = twosigma*np.linspace(-1,1,256)

#Frame sampling
dt=np.mean(np.diff(a.iters)) #ave dt
indexfactor=1000/dt  #this is multiplied by the animate index i to lower sampling rate of data.
indexfactor=int(np.max([1,indexfactor]))   #if indexfactor<1, make it 1.
nframes=int(a.niter/indexfactor)
print('indexfactor=%i nframes=%i'%(indexfactor,nframes))

def animate(i):
    
    f1.clear()
    ax1 = f1.add_subplot(1,1,1, projection="rectilinear")
    
    global eqslice #tells python that eqslice is the variable from outside the method
    
    #saves last slot
    #eqslice[0:nphi, :] = a.vals[:,:,qindex,i].reshape(a.nphi,a.nr)       # snapshot
    index = i*indexfactor
    eqslice[0:nphi, :] = eqslice_t[:,:,index]
    eqslice[nphi,:] = eqslice[0,:]
    
    img = ax1.pcolormesh(X,Y,eqslice[:,:],cmap='jet',shading='gouraud',edgecolors=None, vmin=-twosigma, vmax=twosigma)
    f1.colorbar(img, label=qtitle, orientation='vertical')
    plt.title('t=%.3f'%(a.time[i]))
    plt.axis('off') 

    return ax1 

# Make Movie.
interval=10  #ms btwn frames, defaults to 200.
print('Making movie...')
movie_name=qtitle+'_eqslice_'+str('t%.3f'%max(a.time))+'_'+str('n%i'%(a.niter))+'.mp4'
anim = animation.FuncAnimation(f1, animate, frames=nframes, repeat=False,interval=interval)
anim.save(movie_name, writer=animation.FFMpegWriter())
print('Saved '+movie_name)
