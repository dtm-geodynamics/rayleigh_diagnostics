#!/usr/bin/env python

### compare_runs.py 12/5/17  PED       ###
#PURPOSE: compare multiple rayleigh runs #
# using data the run_stats files.        #

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import plot
import argparse

parser = argparse.ArgumentParser(description="""Compare Davies dynamo stats.""")
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
args = parser.parse_args()
savename='compare_rayleigh'

def set_plot_fontsize(plt,fontsize=None,xlabel=None,ylabel=None):
    plt.ylabel(ylabels[i],{'fontsize':fontsize})
    plt.xlabel(xlabel,{'fontsize':fontsize})
    plt.yticks(size=fontsize)
    plt.xticks(size=fontsize)

### Read run_stats.txt into data
dir=['ek1e-3/ra1e6/flux/bot-06','ek1e-3/ra1e6/flux/bot-08','ek1e-3/ra1e6/flux/bot-10',\
    'ek3e-4/ra5e6/flux/bot-1','ek3e-4/ra5e6/flux/bot-5','ek3e-4/ra5e6/flux/bot-10',\
    'ek3e-4/ra5e6/flux/bot-10/nr84',\
    #'ek3e-4/ra5e6/flux/bot-12/nr84_nt168',
    'ek3e-4/ra5e6/flux/bot-15/nr84',\
    'ek3e-4/ra5e6/flux/bot-15/nr84_nt168','ek3e-4/ra5e6/flux/bot-17/nr84','ek3e-4/ra5e6/flux/bot-20/nr84']
files=['/Users/pdriscoll/memex/rayleigh/rayleigh_git/rayleigh/runs/dynamos/maginit7/'+s+'/run_stats.txt' for s in dir]
nfiles=len(files)
#print(files)

data={}  #a dict
ekman=np.zeros(len(files))
for ifile,file in enumerate(files):
    ekman[ifile]=float(dir[ifile].split('/')[0][2:])
    f=open(file,'r')
    next(f)  #skip first line (header of dir)
    if ifile==0:  #define elements of dict by first element in each line.
        for line in f:
            data[line.split()[0]]=[line.split()[1:]]

    else:         #enter values where they should go in dict.
        for line in f:
            data[line.split()[0]].append(line.split()[1:])
# Parse data into arrays
keys=data.keys()
nrev=np.array(data['nrev,nexecursions,n_eq_cross'])[:,0].astype(int)
nexcursions=np.array(data['nrev,nexecursions,n_eq_cross'])[:,1].astype(int)
n_eq_cross=np.array(data['nrev,nexecursions,n_eq_cross'])[:,2].astype(int)
adnad=np.array(data['AD/NAD,O/E,Z/NZ,chi2'])[:,0].astype(float)
oe=np.array(data['AD/NAD,O/E,Z/NZ,chi2'])[:,1].astype(float)
znz=np.array(data['AD/NAD,O/E,Z/NZ,chi2'])[:,2].astype(float)
brms=np.array(data['gauss[brms,diprms,dipolarity]'])[:,0].astype(float)
diprms=np.array(data['gauss[brms,diprms,dipolarity]'])[:,1].astype(float)
dipolarity=np.array(data['gauss[brms,diprms,dipolarity]'])[:,2].astype(float)
len_kappa=np.array(data['length[kappa,dip,kyr]'])[:,0].astype(float)
len_dip=np.array(data['length[kappa,dip,kyr]'])[:,1].astype(float)
len_kyr=np.array(data['length[kappa,dip,kyr]'])[:,2].astype(float)
lmax=8
gauss=np.zeros((len(files),lmax+1,lmax+1),dtype=complex)
for i in range(len(files)):
    for l in range(1,lmax+1):
        keylm='g,h(l='+str(l)+')'
        for m in range(l+1):
            gauss[i,l,m]=complex(np.array(data[keylm])[i,m*2].astype(float),np.array(data[keylm])[i,m*2+1].astype(float))
# Parse Ekman number from dir name.
ind_ek1e3=np.where(ekman==1e-3)[0]
ind_ek3e4=np.where(ekman==3e-4)[0]
sym=np.chararray(nfiles) ### Define symbol by Ekman number
sym[ind_ek1e3]='o'
sym[ind_ek3e4]='s'
colors=np.chararray(nfiles)
colors[ind_ek1e3]='r'
colors[ind_ek3e4]='b'
### Plot data
plt.ion()
ms=10.  #markersize
nfig=1
fs=(15,10)
plt.figure(nfig,figsize=fs)
for i in range(nfiles): plot(n_eq_cross[i],nrev[i],sym[i])

if (args.save):
    plt.savefig(savename+'_1.pdf')

nfig+=1
#sym='s'  #square
plt.figure(nfig,figsize=fs)
plt.subplot(221)
xlabel='N eq crossing'
ylabels=['DM','AD/NAD','O/E','dipolarity']
plot(n_eq_cross[ind_ek1e3],diprms[ind_ek1e3],sym[ind_ek1e3[0]],markersize=ms,color=colors[ind_ek1e3[0]],\
         label='Ek=1e-3')
plot(n_eq_cross[ind_ek3e4],diprms[ind_ek3e4],sym[ind_ek3e4[0]],markersize=ms,color=colors[ind_ek3e4[0]],\
         label='Ek=3e-4')
plt.xlabel(xlabel)
plt.ylabel(ylabels[0])
plt.legend(numpoints=1)
plt.subplot(222)
for i in range(nfiles): plot(n_eq_cross[i],adnad[i],sym[i],markersize=ms,color=colors[i])
plt.xlabel(xlabel)
plt.ylabel(ylabels[1])
plt.subplot(223)
for i in range(nfiles): plot(n_eq_cross[i],oe[i],sym[i],markersize=ms,color=colors[i])
plt.xlabel(xlabel)
plt.ylabel(ylabels[2])
plt.subplot(224)
for i in range(nfiles): plot(n_eq_cross[i],dipolarity[i],sym[i],markersize=ms,color=colors[i])
plt.xlabel(xlabel)
plt.ylabel(ylabels[3])


if (args.save):
    fs=20.  #fontsize
    ts=20   #tick label size
    for i in range(4):
        plt.subplot(221+i)
        if i==0: plt.legend(numpoints=1,fontsize=fs)
        set_plot_fontsize(plt,fs,xlabel,ylabels[i])
    plt.savefig(savename+'_2.pdf')    
        
