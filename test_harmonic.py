from scipy.special import sph_harm, lpmv
from scipy.integrate import dblquad
import numpy as np
from diagnostic_tools import harmonic_transform
import math 


ntheta=60
lmax = 2*ntheta/3 - 1
nphi=ntheta*2
costheta, weights = np.polynomial.legendre.leggauss(ntheta)
theta=np.arccos(costheta)
phi=np.linspace(0,2*np.pi,nphi+1)[:-1]

def harmonic_continuous_mu(func, lmax):
  transformed = np.zeros((lmax+1,lmax+1), dtype='complex')
  for l in xrange(lmax+1):
    for m in xrange(l+1):
      transformed[l,m] = complex(dblquad(lambda phi, mu: (func(phi, mu)*np.conj(sph_harm(m,l,phi,np.arccos(mu)))).real, \
                                         -1., 1., lambda mu: 0, lambda mu: 2*np.pi)[0], \
                                 dblquad(lambda phi, mu: (func(phi, mu)*np.conj(sph_harm(m,l,phi,np.arccos(mu)))).imag, \
                                         -1., 1., lambda mu: 0, lambda mu: 2*np.pi)[0])
  return transformed
     
def func_mu(phi, mu, m=1, l=1):
  return sph_harm(m,l,phi,np.arccos(mu))

#test_cont_mu = harmonic_continuous_mu(func_mu, lmax)
#

def harmonic_continuous_theta(func, lmax):
  transformed = np.zeros((lmax+1,lmax+1), dtype='complex')
  for l in xrange(lmax+1):
    for m in xrange(l+1):
      transformed[l,m] = complex(dblquad(lambda phi, theta: (func(phi, theta)*np.conj(sph_harm(m,l,phi,theta))).real*np.sin(theta), \
                                         0., np.pi, lambda theta: 0, lambda theta: 2*np.pi)[0], \
                                 dblquad(lambda phi, theta: (func(phi, theta)*np.conj(sph_harm(m,l,phi,theta))).imag*np.sin(theta), \
                                         0., np.pi, lambda theta: 0, lambda theta: 2*np.pi)[0])
  return transformed
     
def func_theta(phi, theta, m=1, l=1):
  return sph_harm(m,l,phi,theta)

#test_cont_theta = harmonic_continuous_theta(func_theta, lmax)

## DISCRETE

costhetag, phig = np.meshgrid(costheta,phi)
thetag, phig = np.meshgrid(theta,phi)


def compute_lpmvs_python(lmax, costhetas, use_mg0corrn=False):
  mg0corrn = 1.0
  if use_mg0corrn: mg0corrn = (2**0.5)  # this factor is not in the Rayleigh complute_plm code but was found to be necessary to correct
  lpmvs = np.zeros((lmax+1, lmax+1, len(costhetas)))
  for l in xrange(lmax+1):
    norm = np.sqrt((2.*l+1.)/(4.*np.pi))
    lpmvs[0,l,:] = norm*lpmv(0,l,costhetas)
    for m in xrange(1,l+1):
      norm = np.sqrt(((2.*l+1.)/(4.*np.pi))*(float(math.factorial(l-m))/float(math.factorial(l+m))))
      lpmvs[m,l,:] = mg0corrn*norm*lpmv(m,l,costhetas)
  return lpmvs


def harmonic_discrete_mu_real(data):  # requires real input
  fft_out = np.fft.rfft(data)/nphi
  fft_out[:,1:] = 2.*fft_out[:,1:]  # done in rayleigh
  lpmvs = compute_lpmvs_python(lmax, costheta)
  transformed = np.zeros((lmax+1,lmax+1), dtype='complex')
  for l in xrange(lmax+1):
    for m in xrange(l+1):
      transformed[l, m] = sum(2.*np.pi*fft_out[:,m]*lpmvs[m,l,:]*weights)
  return transformed

def harmonic_discrete_mu_complex(data):  # accepts complex input
  fft_out = np.fft.fft(data)/nphi
  lpmvs = compute_lpmvs_python(lmax, costheta)
  transformed = np.zeros((lmax+1,2*lmax+1), dtype='complex')
  ms = range(0, lmax+1)+range(-lmax, 0)
  for l in xrange(lmax+1):
    for im in xrange(2*lmax+1):
      m = ms[im]
      transformed[l, im] = sum(2.*np.pi*fft_out[:,im]*lpmvs[abs(m),l,:]*weights)
  return transformed

test_disc_mu_complex_in = func_mu(phig, costhetag, m=1, l=1).transpose()
test_disc_mu_complex = harmonic_discrete_mu_complex(test_disc_mu_complex_in)

test_disc_mu_real_in = func_mu(phig, costhetag, m=0, l=0).real.transpose()  # must be m=0 for real
test_disc_mu_real = harmonic_discrete_mu_real(test_disc_mu_real_in)


def inverse_discrete_mu_real(data):
  transformed = np.zeros((ntheta, nphi))
  lpmvs = compute_lpmvs_python(lmax, costheta)
  for iphi in xrange(nphi):
    for l in xrange(lmax):
      for m in xrange(l+1):
        transformed[:,iphi] += (data[l,m].real*np.cos(m*phi[iphi]) - data[l,m].imag*np.sin(m*phi[iphi]))*lpmvs[m,l,:]
  return transformed

test_disc_mu_real_inv = inverse_discrete_mu_real(test_disc_mu_real)






