# Meridional Slice Plotting Example.
# Displays a single variable from the meridional_slice data structure
# (in the r-theta plane)
#
#    """Rayleigh Meridional Slice Structure
#    ----------------------------------
#    self.niter                                    : number of time steps
#    self.nq                                       : number of diagnostic quantities output
#    self.nr                                       : number of radial points
#    self.ntheta                                   : number of theta points
#    self.nphi                                     : number of phi points sampled
#    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
#    self.radius[0:nr-1]                           : radial grid
#    self.costheta[0:ntheta-1]                     : cos(theta grid)
#    self.sintheta[0:ntheta-1]                     : sin(theta grid)
#    self.phi[0:nphi-1]                            : phi values (radians)
#    self.phi_indices[0:nphi-1]                    : phi indices (from 1 to nphi)
#    self.vals[0:nphi-1,0:ntheta-1,0:nr-1,0:nq-1,0:niter-1] : The meridional slices 
#    self.iters[0:niter-1]                         : The time step numbers stored in this output file
#    self.time[0:niter-1]                          : The simulation time corresponding to each time step
#    self.version                                  : The version code for this particular output (internal use)
#    self.lut                                      : Lookup table for the different diagnostics output
#    """

#from diagnostic_reading import Meridional_Slice
from rayleigh_diagnostics import Meridional_Slices
from diagnostic_plotting import getlabel,momentlabel
from diagnostic_tools import file_list
import matplotlib.pyplot as plt
from matplotlib.pyplot import show,ion,plot
import numpy as np
import argparse
import os
import rayleigh_codes
import matplotlib as mpl
cwd = os.getcwd()
mpl.rc('text',usetex=True)
mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

### Check python version
import sys
if sys.version_info[0]==3:
    raw_input=input
q1_def=3; q2_def=501

parser = argparse.ArgumentParser(description="""Plot radial profiles (Shell_Avgs).""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False,
                    default=None, help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False,
                    default=None, help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-savename',dest='savename',type=str,default='meridional_slice')
parser.add_argument('-q1', '--quantities1', action='store', dest='quantities1', type=int, nargs='*',
                    default=q1_def, required=False, help='quantity in panel 1')
parser.add_argument('-q2', '--quantities2', action='store', dest='quantities2', type=int, nargs='*',
                    default=q2_def, required=False, help='quantity in panel 2')
parser.add_argument('-d', '--default',action='store_true',dest='default',
                    help='use default plotting options',default=False)  #otherwise queue user.
args = parser.parse_args()

# Set saveplot to True to save to a file. 
# Set to False to view plots interactively on-screen.
saveplot = args.save  #False
if not saveplot: plt.ion()
savefile = args.savename #'radial_profile.pdf'  #If pdf gives issues, try .png instead.
window_title = 'meridional_slice ('+cwd+')'
# Get file list
files = file_list(path='Meridional_Slices',initial=args.initial)
nfiles = len(files)
print('%i files found'%nfiles)
#a = ShellAverage(files[0],path='')  #a.vals[nr,moment,nq,niter]
#a = Shell_Avgs(files[0],path='')  #a.vals[nr,moment,nq,niter]
a = Meridional_Slices(files[0],path='')  #a.vals[nr,moment,nq,niter]

### OLD ###
#timestep = '00110000'
#quantity_code = 401  # read in temperature
tindex = 0          # Display the first timestep from the file
pindex = 0          # Display the 5th phi-value output 
#a = Meridional_Slices(timestep)
remove_mean1 = False; remove_mean2 = False #defaults
# Get quantities to plot
if not args.default:    #then ask user.
    print('Iterations:'+str(a.niter)+' Quantities:'+str(a.nq)+' Codes:'+str(a.qv))
    user_codes1_in = raw_input('Choose code for first panel (eg 401):').split(' ')  
    user_codes2_in = raw_input('Choose code for second panel (eg 501):').split(' ') 
    #q1 = [int(num) for num in user_codes1_in]   #make int array.
    #q2 = [int(num) for num in user_codes2_in]   #make int array.
    q1 = int(user_codes1_in[0]); q2 = int(user_codes2_in[0])
else:  #use defaults
    q1 = args.quantities1  
    q2 = args.quantities2
    remove_mean1 = False  # remove the ell=0 mean
    remove_mean2 = False
    

#Set up the grid
nr = a.nr
ntheta = a.ntheta
r = a.radius/np.max(a.radius)
# We apply a shift in theta so that theta=0 corresponds to equator and the poles appear where we expect them to...
theta = np.arccos(a.costheta)-np.pi/2  
radius_matrix, theta_matrix = np.meshgrid(r,theta)
X = radius_matrix * np.cos(theta_matrix)
Y = radius_matrix * np.sin(theta_matrix)

#qindex = a.lut[quantity_code]
qindex1 = a.lut[q1]
qindex2 = a.lut[q2]
field1 = np.zeros((ntheta,nr),dtype='float64')
field1[:,:] = a.vals[pindex,:,:,qindex1,tindex]
field2 = np.zeros((ntheta,nr),dtype='float64')
field2[:,:] = a.vals[pindex,:,:,qindex2,tindex]

#remove the mean if desired
if (remove_mean1):
    for i in range(nr):
        field1[:,i] = field1[:,i]-np.mean(field1[:,i])
    print('Mean removed from qv %i'%q1)
if (remove_mean2):
    for i in range(nr):
        field2[:,i] = field2[:,i]-np.mean(field2[:,i])
    print('Mean removed from qv %i'%q2)
radtodeg = 180.0/np.pi
#print 'Displaying meridional slice at phi (radians, degrees) = ', a.phi[pindex], a.phi[pindex]*radtodeg
print('Displaying meridional slice at phi (radians, degrees) = ', a.phi[pindex], a.phi[pindex]*radtodeg)

#Plot
#plt.figure(plt.gcf().number)
#img = plt.pcolormesh(X,Y,field1,cmap='jet')
#plt.figure(plt.gcf().number+1)
#img = plt.pcolormesh(X,Y,field2,cmap='jet')

### contourf
nlevels=100
levels1=np.linspace(field1.min(),field1.max(),nlevels)
levels2=np.linspace(field2.min(),field2.max(),nlevels)
figsize = (7,10)
plt.figure(plt.gcf().number+1,figsize=figsize)
plt.contourf(X,Y,field1,cmap='jet',levels=levels1)
plt.gca().axes.xaxis.set_ticklabels([]); plt.gca().axes.yaxis.set_ticklabels([]) #supress axis labels
plt.axis('off')
plt.gca().set_aspect('equal', adjustable='box')
plt.colorbar(label=rayleigh_codes.index_to_tex[q1]+' '+getlabel(q1))
if args.save:
    plt.savefig(args.savename+'_%i.pdf'%q1)
    print('Saved '+args.savename+'_%i.pdf'%q1)
### Figure 2    
plt.figure(plt.gcf().number+1,figsize=figsize)
plt.contourf(X,Y,field2,cmap='jet',levels=levels2)
plt.gca().axes.xaxis.set_ticklabels([]); plt.gca().axes.yaxis.set_ticklabels([]) #supress axis labels
plt.axis('off')
plt.gca().set_aspect('equal', adjustable='box')
plt.colorbar(label=rayleigh_codes.index_to_tex[q2]+' '+getlabel(q2))
if args.save:
    plt.savefig(args.savename+'_%i.pdf'%q2)
    print('Saved '+args.savename+'_%i.pdf'%q2)
