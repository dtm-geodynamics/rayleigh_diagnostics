#!/usr/bin/env python

###############################################
#
#  Global-Averages (G_Avgs) plotting
#  Plots average KE vs time
#
#  This example routine makes use of the GlobalAverage
#  data structure associated with the G_Avg output.
#  Upon initializing a GlobalAverage object, the 
#  object will contain the following attributes:
#
#    ----------------------------------
#    self.niter                  : number of time steps
#    self.nq                     : number of diagnostic quantities output
#    self.qv[0:nq-1]             : quantity codes for the diagnostics output
#    self.vals[0:niter-1,0:nq-1] : Globally averaged diagnostics as function of time and quantity index
#    self.iters[0:niter-1]       : The time step numbers stored in this output file
#    self.time[0:niter-1]        : The simulation time corresponding to each time step
#    self.lut                    : Lookup table for the different diagnostics output


from rayleigh_diagnostics import G_Avgs
from diagnostic_tools import file_list
from diagnostic_plotting import getlabel
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import plot,ion,show
import numpy as np
import argparse
import os 
cwd = os.getcwd()
#ion()

parser = argparse.ArgumentParser(description="""Plot global averages (G_Avgs): KE vs time.""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False,
                    default=None, help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False,
                    default=None, help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-q1', '--quantities1', action='store', dest='quantities1', type=int, nargs='*',
                    #default=[125,126,127,128],
                    default=[401],required=False, help='quantities in panel 1')
parser.add_argument('-q2', '--quantities2', action='store', dest='quantities2', type=int, nargs='*',
                    #default=[475,476,477,478],
                    default=[1101],required=False, help='quantities in panel 2')
parser.add_argument('-d', '--default',action='store_true',dest='default',
                    help='use default plotting options (q1:401,402,403,404;q2:1101,1102,1103,1104)',
                    default=False)  #otherwise queue user.
args = parser.parse_args()

# Set saveplot to True to save to a .png file. 
# Set to False to view plots interactively on-screen.
saveplot = args.save
savefile = 'energy_trace.pdf'  #Change .pdf to .png if pdf conversion gives issues

print("initial="+str(args.initial)+" final="+str(args.final))
files = file_list(args.initial, args.final, path='G_Avgs')
#The indices associated with our various outputs are stored in a lookup table
#as part of the GlobalAverage data structure.  We define several variables to
#hold those indices here:
a = G_Avgs(filename=files[0],path='')  # read a file to get the lookup table
# Note a only has file[0], should probably look at all file outputs here!

# Get quantities to plot
if not args.default:    #then ask user.
    #Print Info
    print(' ')  # blank line
    print('Iterations:'+str(a.niter)+' Quantities:'+str(a.nq)+' Codes:'+str(a.qv))
    # Ask user to choose
    user_codes1_in = raw_input('Choose up to 4 codes for first panel (eg 1 2 3):').split(' ')   #will take in a string of numbers separated by a space
    user_codes2_in = raw_input('Choose up to 4 codes for second panel (eg 1 2 3):').split(' ')   #will take in a string of numbers separated by a space
    q1 = [int(num) for num in user_codes1_in]   #make int array.
    q2 = [int(num) for num in user_codes2_in]   #make int array.
else:  #use defaults
    q1=args.quantities1  
    q2=args.quantities2

print('Panel 1: labels quantities:'+str(getlabel(q1))+str(q1))
print('Panel 2: labels quantities:'+str(getlabel(q2))+str(q2))

# We also define an empty list to hold the time.  This time
# will be absolute time since the beginning of the run, 
# not since the beginning of the first output file.  
# In this case, the run was dimensional, so the time is recorded 
# in seconds.  We will change that to days as we go.
data = {}  #dictionary
time = {}

# Next, we loop over all files and grab the desired data
# from the file, storing it in appropriate lists as we go.
for f in files:
    #a = GlobalAverage(filename=f,path='')
    a = G_Avgs(filename=f,path='')

    for iq in a.qv:
        if iq not in data:  #initialize them.
            data[iq]=[]
            time[iq]=[]
    for i,d in enumerate(a.time):
        for iq in a.qv:
            data[iq].append(a.vals[i,a.lut[iq]])
            time[iq].append(d)

#Define times.
time1=time[q1[0]]  #array of times for q1[0] quantity.
ntimes=len(time1)
tra=max(time1)-min(time1)  #time range
i_trim=0.  #default, compute stats from t=0.
if tra>5. :
    i_trim=np.min(np.where(time1-time1[0] > 5.))  #cut out data before i_trim
    ntime_stat=ntimes-i_trim  #ntimes to use for statistics
ind_stat=np.arange(i_trim,ntimes,dtype=np.uint64)

# Labels
labels=getlabel(data.keys())

#Print stats.
print(" ")
if 125 in data:  # if KE available
    ke=data[125]
    print("KE=%8.2e +/- %8.2e  for t=[%4.2f,%4.2f]" %(np.mean(ke),np.std(ke),time1[ind_stat[0]],time1[max(ind_stat)]))
if 475 in data:  # if ME available
    me=data[475]
    print("ME=%8.2e +/- %8.2e  for t=[%4.2f,%4.2f]" %(np.mean(me),np.std(me),time1[ind_stat[0]],time1[max(ind_stat)]))

# Plot two panels
# Panel 1:  Total KE and its breakdown.
# Panel 2:  Total ME and its breakdown.
# Figure 1
if (saveplot):
    plt.figure(1,figsize=(7.5, 4.0), dpi=300)
    plt.rcParams.update({'font.size': 12})
else:
    plt.figure(1,figsize=(15,5),dpi=100)
    plt.rcParams.update({'font.size': 14})
# Plot Kinetic Energy.
plt.subplot(121)
#plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
q1_found=0
for iq in q1:
    if iq in data:
        plot(time[iq],data[iq],label=getlabel(iq))
        q1_found=1
if q1_found:
    plt.yscale('log')
    plt.xlabel('Time')  # (days)')
    plt.ylabel(getlabel(q1[0]))  #default ylabel as first element in q1
    #plt.title(cwd,fontsize=10)
    if (saveplot):
        legend = plt.legend(loc='lower left', shadow=True, ncol = 2, fontsize = 'x-small') 
    else:
        legend = plt.legend(loc='lower left', shadow=True, ncol = 2)
else:
    print('No '+str(getlabel(q1))+' found.')

# Plot Magnetic Energy.    
plt.subplot(122)
t1='ME'
q2_found=0
for iq in q2:
    if iq in data:
        plot(time[iq],data[iq],label=getlabel(iq))
        q2_found=1
if q2_found:
    plt.yscale('log')
    plt.xlabel('Time')
    plt.ylabel(getlabel(q2[0]))  #default ylabel as first element in q2.
    if (saveplot):
        legend = plt.legend(loc='lower left', shadow=True, ncol = 2, fontsize = 'x-small') 
    else:
        legend = plt.legend(loc='lower left', shadow=True, ncol = 2) 
    plt.tight_layout()
else:
    print('No '+str(getlabel(q2))+' found.')

# If any data found show it.
if q1_found or q2_found:
    if (saveplot):
        plt.savefig(savefile)
        print('Saved '+savefile)
    else:
        plt.show()
