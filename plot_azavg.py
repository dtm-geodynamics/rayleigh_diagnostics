#!/usr/bin/env python

####################################################################################################
#
#  Azimuthal-Averages (AZ_Avgs) plotting example
#  Time-averages data from time steps 3 million 
#  through 3.3 million.
#  Plots Entropy, Differential Rotation, and Mass Flux
#
#  This example routine makes use of the AZAverage
#  data structure associated with the AZ_Avg output.
#  Upon initializing an AZAverage object, the 
#  object will contain the following attributes:
#    ----------------------------------
#    self.niter                                    : number of time steps
#    self.nq                                       : number of diagnostic quantities output
#    self.nr                                       : number of radial points
#    self.ntheta                                   : number of theta points
#    self.qv[0:nq-1]                               : quantity codes for the diagnostics output
#    self.radius[0:nr-1]                           : radial grid
#    self.costheta[0:ntheta-1]                     : cos(theta grid)
#    self.sintheta[0:ntheta-1]                     : sin(theta grid)
#    self.vals[0:ntheta-1,0:nr-1,0:nq-1,0:niter-1] : The phi-averaged diagnostics 
#    self.iters[0:niter-1]                         : The time step numbers stored in this output file
#    self.time[0:niter-1]                          : The simulation time corresponding to each time step
#    self.version                                  : The version code for this particular output (internal use)
#    self.lut                                      : Lookup table for the different diagnostics output
#    ----------------------------------

from rayleigh_diagnostics import AZ_Avgs, TimeAvg_AZAverages
from diagnostic_tools import file_list
from diagnostic_plotting import getlabel,momentlabel
import matplotlib.pyplot as plt
import pylab as p 
import numpy as np
#from azavg_util import *
import rayleigh_codes
import matplotlib as mpl
import argparse
import os
#mpl.rc('text', usetex=True)
#mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

### Check python version
import sys
if sys.version_info[0]==3:
    raw_input=input
#dir_path = os.path.dirname(os.path.realpath(__file__))
cwd = os.getcwd()
q1_def=3; q2_def=501
parser = argparse.ArgumentParser( \
                       description="""Plot global averages (G_Avgs): KE vs time.""")
parser.add_argument('-i', '--initial', action='store', type=float, dest='initial', required=False, default=None, 
                    help='initial index to plot from')
parser.add_argument('-f', '--final', action='store', type=float, dest='final', required=False, default=None, 
                    help='final index to plot to')
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-savename',dest='savename',type=str,default='az_avgs')
parser.add_argument('-q1', '--quantities1', action='store', dest='quantities1', type=int, nargs='*',
                    default=q1_def, required=False, help='quantity in panel 1')
parser.add_argument('-q2', '--quantities2', action='store', dest='quantities2', type=int, nargs='*',
                    default=q2_def, required=False, help='quantity in panel 2')
parser.add_argument('-d', '--default',action='store_true',dest='default',
                    help='use default plotting options',default=False)  #otherwise queue user.
args = parser.parse_args()

# Set saveplot to True to save to a file. 
# Set to False to view plots interactively on-screen.
saveplot = args.save  #False
if not saveplot: plt.ion()
savefile = args.savename #'radial_profile.pdf'  #If pdf gives issues, try .png instead.
window_title = 'AZ_Avgs ('+cwd+')'
# Get file list
files = file_list(path='AZ_Avgs',initial=args.initial)
nfiles = len(files)
print('%i files found'%nfiles)
#a = ShellAverage(files[0],path='')  #a.vals[nr,moment,nq,niter]
#a = Shell_Avgs(files[0],path='')  #a.vals[nr,moment,nq,niter]
a = AZ_Avgs(files[0],path='')  #a.vals[nr,moment,nq,niter]

# time index to grab (this file only has one time since it is an average)
ind = 0
### OLD ###
#timestep = '00110000'
#quantity_code = 401  # read in temperature
tindex = 0          # Display the first timestep from the file
pindex = 0          # Display the 5th phi-value output 
#a = Meridional_Slices(timestep)
remove_mean1 = False; remove_mean2 = False #defaults
# Get quantities to plot
if not args.default:    #then ask user.
    print('Iterations:'+str(a.niter)+' Quantities:'+str(a.nq)+' Codes:'+str(a.qv))
    user_codes1_in = raw_input('Choose code for first panel (eg 401):').split(' ')  
    user_codes2_in = raw_input('Choose code for second panel (eg 501):').split(' ') 
    #q1 = [int(num) for num in user_codes1_in]   #make int array.
    #q2 = [int(num) for num in user_codes2_in]   #make int array.
    q1 = int(user_codes1_in[0]); q2 = int(user_codes2_in[0])
else:  #use defaults
    q1 = args.quantities1  
    q2 = args.quantities2
    remove_mean1 = False  # remove the ell=0 mean
    remove_mean2 = False

n_r = a.nr
n_t = a.ntheta

vphi    = a.vals[:,:,a.lut[3],ind].reshape(n_t,n_r)
entropy = a.vals[:,:,a.lut[501],ind].reshape(n_t,n_r)

sintheta = a.sintheta
costheta = np.cos(np.arcsin(sintheta))
radius = a.radius

costheta[0:int(n_t/2)] = -costheta[0:int(n_t/2)] #sintheta is symmetric, so arcsin gives redundant values across equator
#costheta grid in Rayleigh runs from -1 to 1 (south pole to north pole..)

#Compute omega and subtract mean from entropy
Omega=np.zeros((n_t,n_r))

#Subtrace the ell=0 component from entropy at each radius
for i in range(n_r):
    entropy[:,i]=entropy[:,i] - np.mean(entropy[:,i])

#Convert v_phi to an Angular velocity
for i in range(n_r):
    Omega[:,i]=vphi[:,i]/(radius[i]*sintheta[:])
Omega = Omega*1.e9/(2.*np.pi) # s^-1 -> nHz


#Set up the grid
nr = a.nr
ntheta = a.ntheta
r = a.radius/np.max(a.radius)
# We apply a shift in theta so that theta=0 corresponds to equator and the poles appear where we expect them to...
theta = np.arccos(a.costheta)-np.pi/2  
radius_matrix, theta_matrix = np.meshgrid(r,theta)
X = radius_matrix * np.cos(theta_matrix)
Y = radius_matrix * np.sin(theta_matrix)

#qindex = a.lut[quantity_code]
qindex1 = a.lut[q1]
qindex2 = a.lut[q2]
field1 = np.zeros((ntheta,nr),dtype='float64')
field1[:,:] = a.vals[:,:,qindex1,tindex]
field2 = np.zeros((ntheta,nr),dtype='float64')
field2[:,:] = a.vals[:,:,qindex2,tindex]
nlevels=100
extreme1 = max([abs(field1.min()),field1.max()])
extreme2 = max([abs(field2.min()),field2.max()])
levels1=np.linspace(field1.min(),field1.max(),nlevels)
levels2=np.linspace(field2.min(),field2.max(),nlevels)
cmap1 = 'jet'; cmap2= 'jet'
if q1 == 401: cmap1 = 'YlOrRd'
if q2 == 401: cmap2 = 'YlOrRd'
if field1.min()<0: levels1=np.linspace(-extreme1,+extreme1,nlevels)  #center at zero
if field2.min()<0: levels2=np.linspace(-extreme2,+extreme2,nlevels)  #center at zero

#remove the mean if desired
if (remove_mean1):
    for i in range(nr):
        field1[:,i] = field1[:,i]-np.mean(field1[:,i])
    print('Mean removed from qv %i'%q1)
if (remove_mean2):
    for i in range(nr):
        field2[:,i] = field2[:,i]-np.mean(field2[:,i])
    print('Mean removed from qv %i'%q2)
radtodeg = 180.0/np.pi

### contourf
### Figure 1
figsize = (7,10)
plt.figure(plt.gcf().number+1,figsize=figsize)
plt.contourf(X,Y,field1,cmap=cmap1,levels=levels1)
plt.gca().axes.xaxis.set_ticklabels([]); plt.gca().axes.yaxis.set_ticklabels([]) #supress axis labels
plt.axis('off')
plt.gca().set_aspect('equal', adjustable='box')
#plt.colorbar(label=rayleigh_codes.index_to_tex[q1]+' '+getlabel(q1))
plt.colorbar(label=getlabel(q1))
if args.save:
    plt.savefig(args.savename+'_%i.pdf'%q1)
    print('Saved '+args.savename+'_%i.pdf'%q1)
### Figure 2    
plt.figure(plt.gcf().number+1,figsize=figsize)
plt.contourf(X,Y,field2,cmap=cmap2,levels=levels2)
plt.gca().axes.xaxis.set_ticklabels([]); plt.gca().axes.yaxis.set_ticklabels([]) #supress axis labels
plt.axis('off')
plt.gca().set_aspect('equal', adjustable='box')
#plt.colorbar(label=rayleigh_codes.index_to_tex[q2]+' '+getlabel(q2))
plt.colorbar(label=getlabel(q2))
if args.save:
    plt.savefig(args.savename+'_%i.pdf'%q2)
    print('Saved '+args.savename+'_%i.pdf'%q2)
