#!/usr/bin/env python

####################################################################################################
#

#import matplotlib
#matplotlib.use("Agg")

import pylab as p 
import numpy as np
from diagnostic_reading import ShellSlice,ShellSpectra,PowerSpectrum
from diagnostic_tools import file_list, calculate_Ylms,renormalize_shell_spectra,inverse_transform,norm_lm
from diagnostic_plotting import lutlabels,getlabel
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import ticker
#matplotlib.use('GTK')
from matplotlib.pyplot import ion,plot,show
import argparse
pi=np.pi

path='Shell_Spectra'
print("Reading files...")
initial=None
final=None
files = file_list(initial, final, path)
nfiles=len(files)

sspec0 = ShellSpectra(files[1],path='')

# We use the lookup table to find where br, vtheta, and bphi are stored
br_index = sspec0.lut[401]
dims=[sspec0.lmax,sspec0.nr,sspec0.niter]   #get dims.
lmax=sspec0.lmax
lmax_gauss=min([84,lmax])  #limit to <84 bc factorial(170)=inf, so lmax<84. 
nl=lmax+1
nr=sspec0.nr
niter=sspec0.niter
sspec = [PowerSpectrum('blank',dims) for i in range(nfiles)]  #define spec as a blank list to get a template object.
sspec[0]=sspec0  #def 1st element.
# Read all files next???

#rad_index
rad_index=0  # only use top radius level

#Initialize full arr with first spec file.
times=sspec0.time
# Convert Brlm to orthonormal
brlm=np.zeros((nl,nl),dtype=complex)
brlm[1,0]=complex(1,0)  #axial dipole.

# Extrapolate orthonormal harmonics to surf and Compute Brtp(surface)
r_cmb=3481e3
r_surf=6371e3
r_ratio=r_cmb/r_surf
#r_ratio=1.  #test to see if Brtp_cmb is same as in shell_slices.
ntheta=96
nphi=ntheta*2
costheta, weights = np.polynomial.legendre.leggauss(ntheta)
theta=np.arccos(costheta)
# Define analytical solution
brtp = np.zeros((ntheta,nphi))
bttp = np.zeros((ntheta,nphi))
bptp = np.zeros((ntheta,nphi))
for iphi in xrange(nphi):
    brtp[:,iphi] = norm_lm(1,0)*brlm[1,0].real*(r_ratio)**3*costheta
    bttp[:,iphi] = norm_lm(1,0)*brlm[1,0].real/2*(r_ratio)**3*np.sin(theta)
    bptp[:,iphi] = np.zeros(ntheta)

# Compute brtp from brlm_on
brtp_trans=inverse_transform(brlm[:,:],theta,nphi,r_ratio,component='r')
bttp_trans=inverse_transform(brlm[:,:],theta,nphi,r_ratio,component='t')
bptp_trans=inverse_transform(brlm[:,:],theta,nphi,r_ratio,component='p')

##################################################################
# Plot comparison of Br,Bt,Bp as (theta,phi) and (l,m)
ion()
# function "plot_bmap" used to render a surface.
def plot_bmap(surf,title):
    twosigma = 2*np.std(surf)  #limits are set to +/- twosigma
    contour_levels = twosigma*np.linspace(-1,1,256)
    # Contour br_surf(theta,phi,t=0)
    image1 = ax1.imshow(surf, #vmin=-twosigma, vmax=twosigma,
        extent=(-np.pi,np.pi,-np.pi/2,np.pi/2), clip_on=False,
        aspect=0.5, interpolation='bicubic')
    image1.axes.get_xaxis().set_visible(False)  # Remove the longitude and latitude axis labels
    image1.axes.get_yaxis().set_visible(False)
    image1.set_cmap('RdYlBu_r')  # Red/Blue map
    plt.title(title)
    # Colorbar
    if np.ptp(surf)>0:  #if range of data is greater than zero
        cbar = f1.colorbar(image1,orientation='horizontal', shrink=0.5)
        tick_locator = ticker.MaxNLocator(nbins=5)
        cbar.locator = tick_locator
        cbar.update_ticks()
# Setup figure
nfig=1
nrow=3
ncol=3
panel=1
f1 = p.figure(nfig,figsize=(10,10), dpi=80)
### Br plots ###
# Known
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
plt.figtext(0.5,0.95,'test AD \nr_ratio=%f'%(r_ratio),ha='center')
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
title=r'$B_r$ known'
plot_bmap(brtp,title)
# Transformed
panel+=1
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
title=r'$B_r$ transformed'
plot_bmap(brtp_trans,title)
# Difference
panel+=1
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
title=r'$B_r$ known-transformed'
plot_bmap(brtp-brtp_trans,title)
### B_theta Plots.###
# Known
panel+=1
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
title=r'$B_\theta$ known'
plot_bmap(bttp,title)
# Transformed
panel+=1
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
title=r'$B_\theta$ transformed'
plot_bmap(bttp_trans,title)
# Difference
panel+=1
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
title=r'$B_\theta$ known-transformed'
plot_bmap(bttp-bttp_trans,title)
### B_phi Plots.###
# Known
panel+=1
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
title=r'$B_\phi$ known'
plot_bmap(bptp,title)
# Transformed
panel+=1
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
title=r'$B_\phi$ transformed'
plot_bmap(bptp_trans,title)
# Difference
panel+=1
ax1 = f1.add_subplot(nrow,ncol,panel, projection="mollweide")
title=r'$B_\phi$ known-transformed'
plot_bmap(bptp-bptp_trans,title)
