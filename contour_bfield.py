### contour_efield.py 10/17/18 PED ###
### Code copied from:
# https://pythonmatplotlibtips.blogspot.com/2017/12/draw-beautiful-electric-field-lines.html

import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import numpy as np
from scipy.integrate import ode as ode
from matplotlib import cm
from itertools import product
from diagnostic_tools import *
import sys
import argparse
pi=np.pi
plt.ion()
parser = argparse.ArgumentParser(description="""Create magnetic field streamlines""")
parser.add_argument('-s', '--save', action='store_const', dest='save', const=True, default=False, 
                    required=False, help='save the figure to file instead of plotting interactively')
parser.add_argument('-sn', '--savename', action='store', type=str, dest='savename', required=False,
                    default=False, help='name of file to save')
args = parser.parse_args()


class gauss:
    def __init__(self, l=0,m=0,glm=0,hlm=0,a=0):
        self.glm=glm
        self.hlm=hlm
        self.l=l
        self.m=m
        self.a=a
 
def B_gauss(l,m,glm,hlm, a, r,theta,phi,Plm,Plmp1):
    ### Given glm,hlm,a,r,theta,phi return Bx,By,Bz(r,theta,phi)
    mu = np.cos(theta)
    B_r = +1*(a/r)**(l+2.)*(l+1.)*Plm*(glm*np.cos(m*phi)+hlm*np.sin(m*phi))
    B_theta = -(a/r)**(l+2.)*(m*mu/np.sin(theta)*Plm+Plmp1)*(glm*np.cos(m*phi)+hlm*np.sin(m*phi))
    B_phi = 1./(np.sin(theta))*(a/r)**(l+2.)*m*Plm*(glm*np.sin(m*phi)-hlm*np.cos(m*phi))
    B_x = np.sin(theta)*np.cos(phi)*B_r + np.cos(theta)*np.cos(phi)*B_theta + np.sin(phi)*B_phi
    B_y = np.sin(theta)*np.sin(phi)*B_r + np.cos(theta)*np.sin(phi)*B_theta + np.cos(phi)*B_phi
    B_z = np.cos(theta)*B_r - np.sin(theta)*B_theta
    #print('l=%d m=%d glm=%.2f hlm=%.2f a=%.2f r=%.4f theta=%.4f phi=%.4f Plm=%e Plmp1=%e Bx=%e By=%e Bz=%e'%(l,m,glm,hlm,a,r,theta,phi,Plm,Plmp1,B_x2,B_y2,B_z2))
    #sys.exit(0)
    return(B_x,B_y,B_z)

def B_total(x,y,z,components):
    # Compute a,r,theta,phi
    r = np.sqrt(x**2+y**2+z**2)
    phi = np.arctan2(y,x)
    theta = np.arctan2(np.sqrt(x**2+y**2),z)
    mu = np.cos(theta)
    Bx,By,Bz = 0,0,0
    for C in components:
        # Plm
        Plm = plm_schmidt(C.l,C.m,mu); Plmp1 = plm_schmidt(C.l,C.m+1,mu)
        # Compute source contribution to field.
        B=B_gauss(C.l,C.m,C.glm,C.hlm, C.a, r,theta,phi,Plm,Plmp1)
        Bx=Bx+B[0]; By=By+B[1]; Bz=Bz+B[2]
    #print('B_total: r=%f theta=%f phi=%f Bx=%f By=%f Bz=%f'%(r,theta,phi,Bx,By,Bz))
    return [Bx,By,Bz]

def B_dir(t, y, components):
    Bx,By,Bz = B_total(y[0],y[1],y[2], components)
    n=np.sqrt(Bx**2+By**2+Bz**2)
    return [Bx/n,By/n,Bz/n]  #normalized unit vector directions?  New coordinates?

def V_gauss(l,m,glm,hlm,a,r,theta,phi,Plm):
    ### Compute mag potential(r,theta,phi)
    phi = a*(a/r)**(l+1.)*Plm*(glm*np.cos(m*phi)+hlm*np.sin(m*phi))
    return(phi)

def Phi_total(x, y, z, components):
    # Compute a,r,theta,phi
    #z = 0.
    r = np.sqrt(x**2+y**2+z**2)
    phi = np.arctan2(y,x)
    theta = np.arctan2(np.sqrt(x**2+y**2),z)
    mu = np.cos(theta)
    V=0
    for C in components:
        Plm = plm_schmidt(C.l,C.m,mu)
        Vp = V_gauss(C.l, C.m,C.glm,C.hlm,C.a,r,theta,phi,Plm)
        V = V+Vp
    return V

### Gauss components
a = 1.   #radius of Gauss sources (ie E surface or CMB)
components=[gauss(1.,0,1.,0,a),gauss(1.,1,0.5,0,a),gauss(2.,1,0.2,0,a),gauss(2.,2,-0.3,0,a),gauss(3.,3,0.5,0,a)]
        
# calculate field lines
x0, x1=-3, 3
y0, y1=-3, 3
z0, z1=-3, 3
#R=0.01
dt=0.05*a
xs,ys,zs = [],[],[]
# trace angles: alpha like phi in x-y plane, beta like theta in y-z plane.
alpha = np.arctan2(0,1)  #phi value of plane normal to view.
nbeta = 40; beta_max = 2.*pi; beta_step = beta_max/nbeta
beta_arr = np.linspace(beta_step/2, (nbeta-1)*beta_max/nbeta+beta_step/2, nbeta)
x_init = np.sqrt(a**2+dt**2)*np.sin(beta_arr)*np.cos(alpha)
y_init = np.sqrt(a**2+dt**2)*np.sin(beta_arr)*np.sin(alpha)
z_init = np.sqrt(a**2+dt**2)*np.cos(beta_arr)
### Loop over beta angles.
for i,beta in enumerate(beta_arr):
    # Positive dt
    r=ode(B_dir); r.set_integrator('vode').set_f_params(components)
    x = [x_init[i]]; y = [y_init[i]]; z = [z_init[i]]
    r.set_initial_value([x[0], y[0], z[0]], 0)
    #print('beta=%f x=%f y=%f z=%f'%(beta,x[0],y[0],z[0]))
    while r.successful():
        r.integrate(r.t+dt)
        x.append(r.y[0]); y.append(r.y[1]); z.append(r.y[2])
        hit_charge=False
        ## check if field line left drwaing area or ends in some charge
        for C2 in components:
            if np.sqrt(r.y[0]**2+r.y[1]**2+r.y[2]**2)<C2.a:
                hit_charge=True
        if hit_charge or (not (x0<r.y[0] and r.y[0]<x1)) or \
                (not (y0<r.y[1] and r.y[1]<y1)) or (not (z0<r.y[2] and r.y[2]<z1)):
            #print('Break: hit_charge=%d x=%.3f y=%.3f z=%.3f t=%.3f'%(hit_charge,r.y[0],r.y[1],r.y[2],r.t))
            break
    xs.append(x)
    ys.append(y)
    zs.append(z)
    # Negative dt
    r=ode(B_dir); r.set_integrator('vode').set_f_params(components)
    x = [x_init[i]]; y = [y_init[i]]; z = [z_init[i]]
    r.set_initial_value([x[0], y[0], z[0]], 0)
    while r.successful():
        r.integrate(r.t-dt)
        x.append(r.y[0]); y.append(r.y[1]); z.append(r.y[2])
        hit_charge=False
        ## check if field line left drwaing area or ends in some charge
        for C2 in components:
            if np.sqrt(r.y[0]**2+r.y[1]**2+r.y[2]**2)<C2.a:
                hit_charge=True
        if hit_charge or (not (x0<r.y[0] and r.y[0]<x1)) or \
                (not (y0<r.y[1] and r.y[1]<y1)) or (not (z0<r.y[2] and r.y[2]<z1)):
            #print('Break: hit_charge=%d x=%.3f y=%.3f z=%.3f t=%.3f'%(hit_charge,r.y[0],r.y[1],r.y[2],r.t))
            break
    xs.append(x)
    ys.append(y)
    zs.append(z)

# calculate electric potential
vvs = []
xxs = []
yys = []
zzs = []
numcalcv = 50
yy=0.  #only compute V in x-z plane.
for xx,zz in product(np.linspace(x0,x1,numcalcv),np.linspace(z0,z1,numcalcv)):
    if np.sqrt(xx**2+zz**2)>=a:   #only save V outside of r=a
        xxs.append(xx)
        zzs.append(zz)
        vvs.append(Phi_total(xx,yy,zz,components))
xxs = np.array(xxs)
zzs = np.array(zzs)
vvs = np.array(vvs)

plt.figure(figsize=(15, 10),facecolor="w")

# plot field line
for x, z in zip(xs,zs):
    plt.plot(x, z, color="k")
c1=plt.Circle((0,0),components[0].a,zorder=10,facecolor='grey')
ax=plt.gca().add_patch(c1)
# Plot source tracer initial pos
#plt.plot(x_init,z_init,'o',)

# plot potential
clim0,clim1 = -2,2
#plt.tricontour(xxs,zzs,vvs,10,colors="0.3")
plt.tricontourf(xxs,zzs,vvs,100,cmap=cm.jet)
cbar = plt.colorbar()
cbar.set_clim(clim0,clim1)
cbar.set_ticks([-2,-1.5,-1,-0.5,0,0.5,1,1.5,2])
cbar.set_label("Magnetic Potential")
plt.xlabel('$x$')
plt.ylabel('$z$')
plt.xlim(x0, x1)
plt.ylim(y0, y1)
plt.axes().set_aspect('equal')
if args.save:
    savename='bfield_lines_1.png'
    plt.savefig(savename,dpi=250,bbox_inches="tight",pad_inches=0.02)
    print('Saved '+savename)
plt.show()
